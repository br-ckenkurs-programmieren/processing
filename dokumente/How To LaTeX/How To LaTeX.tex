\newcommand{\stylefolder}{../../style}
\input{\stylefolder/style.tex}

\lstset{language={TeX}}
\titleformat{\section}
{\normalfont\large\bfseries}
{\thesection \hspace{1mm} -- }
{1mm}
{}

\setcounter{tocdepth}{3}
\settitle{How To \LaTeX}
\setfolder{htl}
\fancyhead[R]{}

\begin{document}

\begin{titlepage}
	\begin{center}

		{\Large\bfseries \pagetitle}

		\vspace{0.5em}

		Eine Einführung in die Welt von {\LaTeX} \& den \LaTeX-Dokumenten des Brückenkurses

		\vspace{3em}

		\textbf{Hendrik Wagner}

		\vspace{45em}

		Stand \today

	\end{center}
\end{titlepage}



\title

\section*{Zu diesem Dokument}

Dieses Dokument soll den Tutor:innen eine Grundlage für die Arbeit mit LaTeX bieten.
Dabei genügt es oft, nur Kapitel 1 zu lesen, um das Wichtigste zu erfahren, und um dann per Trial-And-Error fortzufahren.
Wenn größere Änderungen gemacht oder neue Aufgabenzettel / größere Aufgaben erstellt werden, empfiehlt es sich, dieses Dokument vollständig zu lesen.

Bei größeren Änderungen, besonders im \code{style}-Dokument, empfiehlt es sich, diese Änderungen in dem Dokument nachträglich zu erläutern und Abschnitte entsprechend zu aktualisieren.

\tableofcontents

\pagebreak


\section{Generelles Know-How}

\subsection{Was ist \LaTeX{}?}

\href{https://de.wikipedia.org/wiki/LaTeX}{\LaTeX} ist ein Paket von Software, welches auf das Textsatzsystem \href{https://de.wikipedia.org/wiki/TeX}{\TeX} aufbaut und das Erstellen von Textdokumenten mithilfe von Makros ermöglicht und vereinfacht.

Heutzutage ist \TeX{} bzw. \LaTeX{} ein Standard für das Erstellen von Briefen, professionellen Dokumenten, Büchern, wissenschaftlichen Arbeiten, uvm.

\subsection{Installation}

Für das Arbeiten mit \LaTeX{} wird eine Sammlung von Programmen benötigt, welche von \LaTeX{} für verschiedene Funktionen verwendet werden.

Wir empfehlen dazu die Installation von \href{https://www.tug.org/texlive/acquire-netinstall.html}{\underline{\TeX{} Live}}. \\
Außerdem wird die Arbeit mit einem IWE (\textit{Integrated Writing Environment}) empfohlen, hierzu kann entweder \href{https://www.texstudio.org/}{\underline{\TeX{}studio}}
oder aber \href{https://code.visualstudio.com/}{\underline{Visual Studio Code}} mit der \href{https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop}{\underline{\LaTeX-Workshop Extension}} verwendet werden.

In den meisten Fällen sollte es dabei nach erfolgreicher Installation (wobei {\TeX} Live erfahrungsmäßig einige Zeit benötigt, um alle Dateien herunterzuladen) sofort möglich sein, an Dokumenten zu arbeiten und diese zu kompilieren.

\subsection{Wie das Arbeiten mit \LaTeX{} funktioniert}

\LaTeX{} basiert auf Makros, mit welchen alles im Quellcode des Dokuments definiert werden kann. \\
Erst wenn das Dokument kompiliert wird, werden diese Makros in Befehle umgewandelt und auf das Dokument angewandt.

\subsubsection{Makros}

Makros werden üblicherweise für einmalige Aktionen im Dokument verwendet.
Hier ein paar Beispiele:

\begin{itemize}
	\item \code{\\vspace\{2em\}} erzeugt einen vertikaler Leerraum von zwei Textzeilen
	\item \code{\\section\{Über \\LaTeX\}} erzeugt eine Überschrift mit stilisierten {\LaTeX}
	\item \code{\\textbf\{Wichtig!\}} erzeugt einen \textbf{fett} geschriebenen Text
	\item \code{\\emph\{Hallo!\}} erzeugt einen \emph{kursiv} geschriebener Text
	\item \code{\\href\{https://google.com\}\{Google\}} erzeugt einen \href{https://google.com}{Link}
	\item \code{\\includegraphics\[width=5em\]\{img/myImg.png\}} lädt ein Bild
	\item \code{\\pagebreak} startet eine neue Seite
\end{itemize}

Zudem kann man mit \code{\\\\} einen Zeilenumbruch ohne Leerzeile erzeugen.

\subsubsection{Umgebungen}

Manchmal möchte man mehrere Zeilen Text auf eine bestimmte Art stilisieren.
In diesem Fall werden die Makros \code{\\begin\{envname\}} und \code{\\end\{envname\}} verwendet.

Text zwischen diesen Makros wird dann mit den entsprechenden Stilisierungen versehen.
Dabei gibt es Umgebungen für Codeblöcke, Auflistungen, Tabellen, Rechnungen, uvm.

\pagebreak

\subsubsection{Beispiel}

\begin{lstlisting}[caption=Quellcode eines \LaTeX{}-Dokuments]
% Art des Dokuments
\documentclass{article}

% Packages
\usepackage[utf8]{inputenc}
\usepackage{fancyhdr}

% Definitionen
\newcommand{myNewCommand}{SomethingHere}
\newenvironment{myNewEnvironment}{doSomething}{somethingElse}

% Das eigentliche Dokument
\begin{document}

	\section*{Zu Beginn: Einleitung}

	Architecto culpa esse voluptatum sit sed quisquam cumque.
	Officiis veniam est enim quasi dolorum quos fugit.
	Recusandae velit error voluptatum sequi nihil.

	Rem ullam aliquid culpa autem qui dolores.
	Omnis voluptas similique ea eius veritatis iusto.

	\subsection*{Unterabschnitt}

	Suscipit maxime nulla adipisci.
	Dolorem culpa et mollitia optio minima et ratione.
	Repellendus molestiae nesciunt fugit dolores.

\end{document}
\end{lstlisting}

Generell wird so ziemlich alles mit Makros im Stil \code{\\befehlname\{Argument\}} erreicht.
Der oben gezeigte Textblock würde übrigens das folgende Ergebnis nach dem Kompilieren produzieren:

\begin{center}
	\begin{minipage}{0.6\textwidth}
		\begin{mdframed}[linewidth=2]
			\section*{Zu Beginn: Einleitung}

			Architecto culpa esse voluptatum sit sed quisquam cumque.
			Officiis veniam est enim quasi dolorum quos fugit.
			Recusandae velit error voluptatum sequi nihil. \\

			Rem ullam aliquid culpa autem qui dolores.
			Omnis voluptas similique ea eius veritatis iusto.

			\subsection*{Unterabschnitt}

			Suscipit maxime nulla adipisci.
			Dolorem culpa et mollitia optio minima et ratione.
			Repellendus molestiae nesciunt fugit dolores.

		\end{mdframed}
	\end{minipage}
\end{center}

Zu beobachten ist, dass wie z. B. bei \textit{Markdown} die Zeilenumbrüche nur beachtet werden, wenn Sie im Quellcode doppelt erfolgen.
Auch wird in der Ausgabe lediglich der Inhalt der \code{document}-Umgebung beachtet, alles außerhalb wird nicht ausgegeben.

\pagebreak

\subsection{Übersicht: Aufgabenzettel im Brückenkurs}

{\LaTeX} kann sehr überfordernd wirken, wenn man bis jetzt noch nicht mit einem solchen Programm gearbeitet hat.
Allerdings sind die Dokumente so konzipiert, dass viel der Denkarbeit von dem/der Verfasser:in abgenommen wird, sodass diese sich vollständig auf die Inhalte konzentrieren können.
Schauen wir uns dazu zunächst den Aufbau des \code{tag1.tex}-Dokuments an:

\begin{lstlisting}
\input{style/style.tex}

\settitle{Aufgabenblatt 1 - Variablen, Operatoren, Verzweigungen}
\setfolder{tag1}

% DOKUMENT

\begin{document}

	\include{extras/einleitung.tex}

	{\Large\bfseries\pagetitle}

	% Intro

	\xput{101}

	% Recommended

	\xput{129}

	\pagebreak

	% ...

\end{document}
\end{lstlisting}

\begin{enumerate}
	\item Hier wird das \code{style}-Dokument geladen. Dieses beinhaltet alles, was wir für das Erstellen von Aufgabenblättern benötigen.
	\item Hier wird der Titel des Dokuments festgelegt.
	\item Dieser Befehl wird benötigt, um die Dateien im \code{tag1}-Ordner zu finden. Generell wird also immer der Name des Ordners angegeben, in welchem alle Aufgaben dieses Blattes gespeichert sind.
	\item Ein Kommentar, der den Beginn des Dokuments ankündigt
	\item Das Dokument beginnt!
	\item Hier wird ein Einleitungsdokument geladen – ein weiteres .tex-Dokument.
	\item Der Titel wird groß und fettgedruckt angezeigt.
	\item Mit Kommentaren können Abschnitte definiert werden (aber nur im Quelltext – Kommentare werden in Kompilierzeit komplett ignoriert!).
	\item Hier wird es spannend – der \code{\\xput\{id\}}-Befehl nimmt als einziges Argument eine Aufgaben-ID entgegen und wandelt dies in einen Befehl ähnlich dem der Einleitung um.
	      Dafür muss die entsprechende Aufgabe eine festgelegte Formatierung und Speicherung haben, dazu später mehr.
\end{enumerate}

\pagebreak

\subsubsection{Eine Aufgabe}

Nachdem wir nun den Aufbau der Aufgabenzettel verstehen, müssen wir auch nachvollziehen, wie denn eine einzelne Aufgabe aufgebaut ist.

Schauen wir uns einmal Aufgabe \code{129} an:

\begin{lstlisting}
% Lernziele:
% Räumliches Denken, Tricks mit Zeichnungen
% Verwendung von Variablen statt Magic Numbers

\section{Dreidimensionaler Würfel}
\recommended{15 Z.}{\mathe \variablen \geometrie}

Zeichnen Sie einen scheinbar dreidimensionalen Würfel, mithilfe von zwei \code{square(x, y, extent)}- und vier \code{line(x1, y1, x2, y2)}-Befehlen.
Legen Sie die Größe der Rechtecke (bzw. Seitenlänge des Würfels) sowie die Position in drei Variablen fest.

Können Sie mithilfe von zwei weiteren Variablen die Verschiebung der Vorderseite in Relation zur Hinterseite (die \enquote{Tiefe}) festlegen?

\imagebox{\tg/\id/img/3d_cube.png}{.4\textwidth}
\end{lstlisting}

Hier fällt auf, dass das Aufgabendokument sehr minimalistisch gehalten ist - es enthält keine Style-Information und kein \code{\\begin\{document\}}, etc.
Das hat unter anderem leider zur Folge, dass das Aufgabendokument als solches \textbf{nicht kompiliert werden kann}, sondern erst via des Aufgabenzettels (oder Alternativen dazu, welche das \code{style}-Dokument importieren) zum PDF umgewandelt werden kann.

Auch machen wir uns im Dokument der Aufgabe keine Sorgen um Aufgaben-ID, Nummerierung der Aufgabe oder Sortierung der Aufgaben - diese Dinge werden uns teilweise abgenommen und teilweise entscheiden wir dies im Aufgabenzettel des entsprechenden Tages.

Dabei sind einige wichtige Regeln zu befolgen:

\begin{enumerate}
	\item Jeder Aufgabenzettel liegt in einem Verzeichnis, welches einen \code{style}-Ordner enthält, in welchem die Style-Festlegungen gespeichert sind
	\item Jede Aufgabe liegt in dem Ordner, wessen Namen im Aufgabenzettel via \code{\\tg} festgelegt wird
	\item Der Speicherort der Aufgabe selbst ist \code{tg/ID/ID.tex}, also z. B. \code{tag1/129/129.tex}.
	\item Bild- und andere Dateien für eine Aufgabenstellung sind in \code{tg/ID/img/} bzw. \code{tag1/129/img/} zu finden.
\end{enumerate}

Zudem werden Lösungen im gleichen Ort unter \code{/code/} gespeichert.

\textbf{Musterlösungen} funktionieren vom Konzept gleich, und werden in der \code{.tex}-Datei durch ein \code{l} gekennzeichnet, also z. B. \code{tag1l.tex} und \code{tag1/129/129l.tex}.

Machen Sie sich mit den Dateien vertraut, offene Fragen klären sich oft auch durch das eigenständige Erkunden der Verzeichnisse.

\textbf{Sie haben das Ende der Übersicht erreicht.}
Lesen Sie weiter, falls sie noch offene Fragen haben.

\pagebreak

\section{Aufgabenzettel im Detail}

Nachdem Sie nun eine Einleitung in die Grundlagen von LaTeX und diesem Projekt haben, beschäftigen wir uns nun mit den einzelnen Elementen in größerem Detail.
Dabei werde ich Syntax und die Ideen dahinter bestmöglich zusammenbringen, um dieses Dokument als Nachschlagewerk aufzubauen.

\subsection{Ordnerstruktur der Aufgabenzettel} \label{hauptordner}

Aufgabenzettel sollten einen \enquote{persönlichen} Ordner haben, in welchem alle zugehörigen Aufgaben liegen.
So ist zum Beispiel für das Aufgabendokument von Tag 1 das folgende Prinzip vorgesehen:

\begin{lstlisting}
/../style/...
/tag1.tex
/tag1l.tex
/tag1/...
\end{lstlisting}

\begin{enumerate}
	\item Das \code{style}-Verzeichnis hat einen eigenen Ordner, in welchem zu dem \code{style.tex}-Dokuments auch Bilder und Sprachdefinitionen liegen, welche für die Darstellung benötigt werden.
	      Dieser Ordner muss nicht kopiert werden, um an anderen Orten verwendet werden zu können.
	      Siehe \ref{aufgabenzettel}.
	\item Das Aufgabendokument selbst.
	      Siehe \ref{aufgabenzettel}.
	\item Das Dokument mit Musterlösungen.
	      Siehe \ref{musterzettel}.
	\item Der Ordner, indem sich alle weiteren Dokumente von diesem Aufgabenzettel befinden.
	      Siehe \ref{unterordner}.
\end{enumerate}

\subsection{Grundelemente der Aufgabenzettel} \label{aufgabenzettel}

Ein Aufgabenzettel hat einige Makros, welche zwingend definiert / ausgeführt werden müssen.
In den meisten Fällen genügt es, diese Elemente aus einem anderen Dokument zu kopieren und anzupassen.
Hier eine Übersicht:

\begin{lstlisting}
\newcommand{\stylefolder}{../style}
\input{\stylefolder/style.tex}

\settitle{Aufgabenblatt 1 - Variablen, Operatoren, Verzweigungen}
\setfolder{tag1}

% DOKUMENT

\begin{document}

\end{document}
\end{lstlisting}

\begin{enumerate}
	\item Hier wird der Ort des Ordners, in welchem das \code{style}-Dokument liegt, angegeben.
	\item Dieser Befehl lädt das \code{style}-Dokument, welches Grundlage für alle Makros ist.
	      Siehe auch \ref{hauptordner}.
	\item Dieser Befehl aktualisiert den Titel der Seite.
	      Dieser wird bei Aufruf von \code{\{\\Large\\bfseries\\pagetitle\}} sowie unten links angezeigt.
	\item Dieser Befehl bestimmt den Ordner, in welchem \LaTeX nach Aufgabendokumenten suchen wird.
	\item Ein Kommentar.
	\item Anfang des Dokuments.
	      Darunter werden alle weiteren Elemente eingefügt.
	      Siehe \ref{aufgabenzettelinhalt}.
	\item Ende des Dokuments.
	      Sollte das letzte Makro im Dokument sein.
\end{enumerate}

\subsection{Grundelemente der Musterlösungszettel} \label{musterzettel}

Musterlösungszettel sind vom Aufbau bis auf ein neues Makro praktisch identisch zu dem von Aufgabenzettel.
Der häufigste Fehler ist das fälschliche Deklarieren des Ordners zu \code{ordnerNamel} statt \code{ordnerName} - die Musterlösungen befinden sich auch im Aufgabenordner!

Hier einmal der Kopf des Musterlösungszettels:

\begin{lstlisting}
\newcommand{\stylefolder}{../style}
\input{\stylefolder/style.tex}

\settitle{Musterlösungen für Blatt 1} \loesungen
\setfolder{tag1}
\end{lstlisting}

\begin{enumerate}
	\item Siehe \ref{aufgabenzettel}.
	\item Siehe \ref{aufgabenzettel}.
	\item Hier wird der Titel festgelegt.
	      Zusätzlich wird das Makro \code{\\loesungen} aufgerufen, welches jeweils den Text vor Abschnitten anpasst, und statt Aufgabendokumenten die Musterlösungsdokumente lädt.
	\item Siehe \ref{aufgabenzettel}.
\end{enumerate}

\pagebreak

\subsection{Inhaltlicher Aufbau von Aufgabe- und Musterlösungszettel} \label{aufgabenzettelinhalt}

Der Inhalt, also das was zwischen den \code{\\begin\{document\}} und \code{\\end\{document\}}-Makros zu finden ist, sollte zwischen Aufgaben- und Musterlösungszettel immer parallel gehalten werden. Hier ein direkter Vergleich:

\begin{multicols}{2}
	\begin{lstlisting}[caption=Aufgaben Tag 1]
\begin{document}

	\include{extras/einleitung.tex}

	{\Large\bfseries\pagetitle}

	% Intro
	\xput{101}

	% Recommended
	\xput{129}
	\pagebreak
	\xput{102}
	\xput{128}
	\pagebreak
	\xput{103}

	% Easy
	\pagebreak
	\xput{105}
	\xput{130}

	% Medium
	\pagebreak
	\xput{131}
	\xput{132}

	% Hard
	\pagebreak
	\xput{110}
	\pagebreak
	\xput{134}

	% Expert
	\xput{133}


\end{document}
	\end{lstlisting}
	\columnbreak
	\begin{lstlisting}[caption=Musterlösungen Tag 1]
\begin{document}



	{\Large\bfseries\pagetitle}

	% Intro
	\xput{101}

	% Recommended
	\pagebreak
	\xput{129}
	\xput{102}
	\pagebreak
	\xput{128}
	\pagebreak
	\xput{103}

	% Easy
	\pagebreak
	\xput{105}
	\xput{130}

	% Medium
	\pagebreak
	\xput{131}
	\xput{132}

	% Hard
	\pagebreak
	\xput{110}
	\xput{134}

	% Expert
	\pagebreak
	\xput{133}

\end{document}
	\end{lstlisting}
\end{multicols}

Inhalt und Reihenfolge der Dokumente sind praktisch identisch – beide haben eine Reihe von \code{\\xput\{ID\}}-Makros, getrennt von Kommentaren, welche den Schwierigkeitsgrad beschreiben.

\subsubsection{Das Makro \textit{xput}}

Das Makro \code{\\xput\{ID\}} wird verwendet, um eine bestimmte Aufgabe zu laden.
Dabei hat das Verwenden von \code{\\xput\{ID\}} im Vergleich zu \code{\\include\{file\}} eine Reihe von Vorteilen:

\begin{itemize}
	\item Es aktualisiert die aktuell bearbeitete ID, wodurch die Dokumente gesucht werden (statt \code{ID/ID.tex} angeben zu müssen).
	\item Durch die aktualisierte ID funktioniert \code{\\img}, welches den Pfad des \code{/img/} Ordners der entsprechenden Aufgabe/Musterlösung speichert.
	\item Die ID wird in der Seitenbox der Aufgabe/Musterlösung angezeigt.
\end{itemize}

\subsubsection{Das Makro \textit{pagebreak}}

Funktioniert sehr simpel:
Es beginnt eine neue Seite im Dokument.
Sollte platziert werden, wenn sonst Aufgaben oder Musterlösungen ungünstig unterbrochen werden, weil die Seite voll ist.

\subsection{Ordner eines Aufgabenzettels} \label{unterordner}

\begin{lstlisting}
/101/
/102/
/103/
/104/
...
\end{lstlisting}

\subsection{Ordner einer Aufgabe}

\begin{lstlisting}
/code/
/img/
/js/
123.tex
123l.tex
\end{lstlisting}

\pagebreak

\section{Wichtige Elemente für Aufgabenerstellende}

Dieses Kapitel beschäftigt sich mit den einzelnen Elementen, welche oft beim Erstellen von Aufgaben benötigt werden.

\subsection{Abschnitte / Überschriften}

\LaTeX ~hat verschiedene Makros, um Überschriften anzuzeigen.
Hier eine Übersicht:

\begin{minipage}{.4\linewidth}
	\begin{lstlisting}
\section{Foo}

\subsection{Bar}

\subsubsection{Bar2}

\section*{Boo}

\subsection*{Far}

\subsubsection*{Far2}
	\end{lstlisting}
\end{minipage} \hfil
\begin{minipage}{.55\linewidth}
	\begin{mdframed}[linewidth=2]
		\section*{Aufgabe 4 -- Foo}

		\subsection*{4.1 ~ Bar}

		\subsubsection*{4.1.1 ~ Bar2}

		\section*{Boo}

		\subsection*{Far}

		\subsubsection*{Far2}
	\end{mdframed}
\end{minipage}

Eine Aufgabe sollte genau eine solche \code{\\section} haben, und beliebig viele (oder keine) \code{\\subsection}s.
Die Verwendung von \code{\\subsubsection} in Aufgaben ist relativ unüblich.

Abschnitte mit \code{*} werden von Inhaltsangaben nicht berücksichtigt, enthalten keine Nummerierung und erhöhen diese auch nicht.

\subsection{Seitenleiste}
\setid{123}
\easy{14 Z.\\22 Z.}{\schleifen \verzweigungen \methoden}

Direkt unter der Überschrift soll in der Aufgabe eine Seitenleiste eingebaut werden.
Dafür gibt es einige Befehle:

\begin{lstlisting}
\recommended{14 Z.}{\mathe \variablen \verzweigungen}
\easy{14 Z.\\22 Z.}{\verzweigungen \schleifen \methoden}
\medium{10 Z.}{\geometrie \dreid}
\hard{42 Z.}{\methoden \klassen \vektoren}
\expert{80 Z.}{\mathe}
\end{lstlisting}

Links ist der zweite Befehl angezeigt.

\subsubsection{Anforderungen}

Die Reihenfolge der Anforderungen ist nicht willkürlich und ist in der aktuellen Form im Style-Dokument angezeigt.
Hier aber eine Übersicht:

\begin{enumerate}
	\item Mathe \code{\\mathe}
	\item Variablen \code{\\variablen}
	\item Verzweigungen \code{\\verzweigungen}
	\item Schleifen \code{\\schleifen}
	\item Arrays \code{\\arrays}
	\item Geometrie \code{\\geometrie}
	\item 3D \code{\\dreid}
	\item Zufall \code{\\zufall}
	\item Maus \code{\\mouse}
	\item Keyboard \code{\\keyboard}
	\item Zeit \code{\\zeit}
	\item Funktionen / Methoden \code{\\methoden}
	\item Klassen \code{\\klassen}
	\item Vektoren \code{\\vektoren}
\end{enumerate}

Auch fallen nach einigen Tagen Anforderungen weg:

\begin{enumerate}[label=Tag \arabic*:]
	\item Alle
	\item Alle
	\item Ausgeschlossen: Variablen, Verzweigungen
	\item Ausgeschlossen: Schleifen, Zufall
	\item Ausgeschlossen: Arrays
\end{enumerate}

\subsubsection{Zeilenangabe}

Die Zeilenangabe erfolgt üblicherweise als letztes im Aufgabenerstellungsprozess.
Am leichtesten zu ermitteln sind die Zeilen anhand der Musterlösung, da die automatische Nummerierung dort bereits leere Zeilen überspringt.
Gibt es Teilaufgaben, werden die Zeilenangaben untereinander aufgelistet.

\clue{Falls bei Teilaufgaben Code ausgeblendet wird, dürfen die dadurch verschwundenen Zeilen natürlich in der Zeilenangabe nicht vergessen werden!}

\subsection{Bilder}

Bilder werden immer im Ordner der Aufgabe unter \code{/img/} gespeichert.
Dann kann ein Aufbau wie dieser verwendet werden, um ein einfaches Bild einzufügen:

\begin{lstlisting}
\imagebox{\img/MNI-Logo.pdf}{.7\linewidth}
\end{lstlisting}

\imagebox{\stylefolder/base/MNI-Logo.pdf}{.7\linewidth}

Das Makro \code{\\img} wird automatisch ersetzt mit dem Pfad zum derzeitigen Ordner, sodass hier nur der Name des entsprechenden Bildes dahinter angegeben werden muss.
Das zweite Argument ist die Weite des Bildes, in diesem Fall der gesamte verfügbare Platz \textit{mal} 0.7.

Um das Bild zu positionieren, gibt es einige Möglichkeiten:

\subsubsection{Zentrieren}

\begin{center}
	Genau wie Text können mit dem Environment \code{center} auch Bilder oder andere Elemente zentriert werden.
	Die Verwendung ist dabei einfach:
\end{center}

\begin{lstlisting}
\begin{center}
	\imagebox{\stylefolder/base/hard.pdf}{.4\linewidth} \\
	Etwas Text.
\end{center}
\end{lstlisting}

\begin{center}
	\imagebox{\stylefolder/base/hard.pdf}{.4\linewidth} \\
	Etwas Text.
	Hier ist der Pfad des Bildes anders - natürlich kann auch hier mit dem Makro \code{\\img} und lokalen Dateien gearbeitet werden.
\end{center}

\subsubsection{Minipages}

\begin{minipage}{.5\linewidth}
	Eine gute Möglichkeit, Bilder neben Text zu platzieren, bieten \textit{Minipages}.
	Dabei wird sowohl der Textblock in eine solche Minipage platziert, als auch das Bild (oder andere Elemente).
	Die Weite der beiden Minipages, gegeben als erstes Argument des Environments, zusammen etwas weniger als die gesamte \code{\\linewidth} sein.
	Minipages können noch viel mehr, aber für uns genügt diese Anwendungsweise.
\end{minipage} \hfil
\begin{minipage}{.35\linewidth}
	\imagebox{\stylefolder/base/easy.pdf}{\linewidth}
\end{minipage}

\begin{lstlisting}
\begin{minipage}{.55\linewidth}
	Hier etwas Text
\end{minipage} \hfil
\begin{minipage}{.35\linewidth}
	\imagebox{\stylefolder/base/easy.pdf}{\linewidth}
\end{minipage}
\end{lstlisting}

Das \code{\\hfil}-Makro wird dabei verwendet, um den Platz zwischen den beiden Minipages optimal zu füllen.
Minipages sind von selbst nicht zentriert, weshalb es sinnvoll ist, sie von einem \code{center}-Environment zu umgeben.

\subsubsection{Wrapfigures}

\begin{wrapfigure}{l}{.12\linewidth}
	\vspace{-1.7em}
	\imagebox{\stylefolder/base/expert.pdf}{\linewidth}
	\vspace{-3em}
\end{wrapfigure}

Eine weitere, etwas unbeliebte Möglichkeit, Bilder und Text miteinander zu integrieren, sind \textit{Wrapfigures}.
Wie der Name sagt, sind es Elemente, welche von Text umfasst werden.

Leider ist oft die Platzierung verschoben, weshalb mit \code{\\vspace}-Makros nachgeholfen werden muss.

Ist dies gut aber erreicht, sehen solche Wrapfigures sehr elegant aus.

\begin{lstlisting}
\begin{wrapfigure}{l}{.12\linewidth}
	\vspace{-1.7em} % Bild höher schieben
	\imagebox{\stylefolder/base/expert.pdf}{\linewidth}
	\vspace{-3em} % Text früher wieder links einschlagen lassen
\end{wrapfigure}

Hier kommt Text...
\end{lstlisting}

Das erste Argument gibt die Seite der Wrapfigure vor (\code{l} für links oder \code{r} für rechts).
Das zweite Argument legt die Weite fest.

\subsection{Bilder mit Play-Button}

Mit dem Makro \code{\\imageplay} können Bilder mit einem Play-Button versehen werden.
Die URL für den Knopf wird dabei automatisch generiert, es muss aber als drittes Argument der Name der \code{.js}-Datei angegeben werden, wie sie im Ordner \code{/js/} der Aufgabe zu finden sein sollte.

\begin{center}
	\begin{minipage}{.5\linewidth}
		\begin{lstlisting}
\resizebutton{.8}
\imageplay{\stylefolder/base/medium.pdf}{\linewidth}{1}
\resizebutton{1}
		\end{lstlisting}

	Das Makro \code{\\resizebutton} ändert einen Faktor, um den der Play-Button vergrößert oder verkleinert werden soll.
	Es muss nicht verwendet werden - aber falls es eingesetzt wird, muss nach dem Aufruf der Faktor wieder auf 1 zurückgesetzt werden.
	\end{minipage} \hfil
	\begin{minipage}{.35\linewidth}
		\resizebutton{.8}
		\imageplay{\stylefolder/base/medium.pdf}{\linewidth}{1}
		\resizebutton{1}
	\end{minipage}
\end{center}

\pagebreak

\subsection{Formatierungen}

Ähnlich wie \textit{Markdown} kann auch \LaTeX~ alle üblichen Formatierungen realisieren.
Hier die wichtigsten:

\begin{itemize}
	\item \textbf{Fettgedruckt}
	\item \textit{Kursiv} (Für Hinweise: \clue{Ein Hinweis})
	\item \sout{Durchgestrichen}
	\item \underline{Unterstrichen} (eher unüblich)
\end{itemize}

\begin{lstlisting}
\textbf{Fettgedruckt}
\textit{Kursiv} (Für Hinweise: \clue{Ein Hinweis})
\sout{Durchgestrichen}
\underline{Unterstrichen} (eher unüblich!)
\end{lstlisting}

\subsubsection{Auflistungen}

In \LaTeX~ sind Auflistungen wie vieles andere Environments:

\begin{center}
	\begin{minipage}{.45\linewidth}
		Hier ist eine Aufzählung:

		\begin{enumerate}
			\item Hallo
			\item Welt
		\end{enumerate}

		Und hier eine Auflistung:

		\begin{itemize}
			\item Foo
			\item Bar
		\end{itemize}
	\end{minipage} \hfil
	\begin{minipage}{.45\linewidth}
		\begin{lstlisting}
Hier ist eine Aufzählung:

\begin{enumerate}
	\item Hallo
	\item Welt
\end{enumerate}

Und hier eine Auflistung:

\begin{itemize}
	\item Foo
	\item Bar
\end{itemize}
		\end{lstlisting}
	\end{minipage}
\end{center}

Optional könnten die Symbole oder Nummerierungen angepasst werden (dies ist aber unüblich).
Dazu kann mehr in der Package \code{enumitem} gefunden werden (Google!).

\subsection{Code / Listings}

Code kann sowohl inline als auch als Block dargestellt werden.
In den Aufgabendokumenten haben beide Versionen das Syntax-Highlighting von Processing automatisch implementiert.

\subsubsection{Inline}

Für Inline-Code wird das Makro \code{\\code} verwendet.
Dabei sollen bei Processing-Funktionen immer die relevanten Felder mit kurzen Variablen erkennbar gemacht werden:

\lstset{language={Processing}}

\begin{enumerate}
	\item Ein Rechteck: \code{rect(x, y, w, h)}.
	\item Farbe: \code{fill(r, g, b)}.
	\item Eine Variable: \code{int i}.
\end{enumerate}

\lstset{language={TeX}}

\begin{lstlisting}
Ein Rechteck: \code{rect(x, y, w, h)}.
Farbe: \code{fill(r, g, b)}.
Eine Variable: \code{int i}.
\end{lstlisting}

\subsubsection{Codeblocks}

Codeblocks bestehen aus der Umgebung \code{lstlisting}.
Einrückung wird dabei wie vorhanden dargestellt.

\lstset{language={Processing}}

\begin{lstlisting}
class Tree {
	PVector pos;
	int height;

	Tree(PVector pos) { // Konstruktor
		this.pos = pos;
	}
}
\end{lstlisting}

\lstset{language={TeX}}

\begin{lstlisting}
	\begin{lstlisting}
class Tree {
	PVector pos;
	int height;

	Tree(PVector pos) { // Konstruktor
		this.pos = pos;
	}
}
	\änd{lstlisting} % Hier umgehe ich nur, dass das Listing abbricht
\end{lstlisting}

\end{document}
