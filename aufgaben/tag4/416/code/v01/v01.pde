Box[] b;
ProductionLine [] pl;

void setup() {
  size(500, 500);
  frameRate(60);
  b = new Box[2];
  pl = new ProductionLine[4];

  b[0] = new Box(100, 400, 40);
  b[1] = new Box(300, 400, 75);

  pl[0] = new ProductionLine(100, 100, 200, 30, 2);
  pl[1] = new ProductionLine(300, 100, 30, 200, 3);
  pl[2] = new ProductionLine(130, 300, 200, 30, 0);
  pl[3] = new ProductionLine(100, 130, 30, 200, 1);
}

void draw() {
  background(255);

  for (int i = 0; i < b.length; i++) {    
    for (int j = 0; j < pl.length; j++) {
      pl[j].show();
      b[i].moveOnLine(pl[j]);
    }
  }

  for (int i = 0; i < b.length; i++) {
    b[i].updateIfDragged();
    b[i].show();
  }
}

class ProductionLine {
  //xpos, ypos, width, height, direction
  //d=0 right, d=1 top, d=2 right, d=3 bottom
  int x, y, w, h, dir;

  ProductionLine(int x, int y, int w, int h, int dir) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.dir = dir;
  }

  // Zeichnet das Fließband mit seinem Rand und den Linien die je nach Ausrichtung vertikal oder horizontal verlaufen
  void show() {
    fill(150);
    rect(x, y, w, h);

    if (dir == 1 || dir == 3) {
      for (int i = 1; i < h / 10; i++) 
        line(x + 5, y + i * 10, x + w - 5, y + i * 10);
    } else if (dir == 0 || dir == 2) {
      for (int i = 1; i < w / 10; i++) 
        line(x + i * 10, y + 5, x + i * 10, y + h - 5);
    } else
      print("Invalid Direction!");
  }
}

class Box {
  int x;
  int y;
  int w;
  boolean selected; // box was selected with drag+drop

  Box(int x, int y, int w) {
    this.x = x;
    this.y = y;
    this.w = w;
  }

  // Displays the Box as a rect with two diagonal lines
  void show() {
    fill(240, 220, 160);
    rect(x, y, w, w);
    line(x, y, x + w, y + w);
    line(x + w, y, x, y + w);
  }

  void updateIfDragged() {
    selected = false;

    if (!(mouseX >= this.x && mouseX <= this.x + this.w && 
      mouseY >= this.y && mouseY <= this.y + this.w && mousePressed))
      return; // Box is not selected

    selected = true;
    int xDist = mouseX - pmouseX;
    int yDist = mouseY - pmouseY;
    this.x += xDist;
    this.y += yDist;
  }

  /**
   * Moves the Box across the production line
   **/
  void moveOnLine(ProductionLine p) {
    if (selected)
      return; // do not move if selected

    if (this.x > p.x + p.w 
      || this.x + this.w < p.x 
      || this.y > p.y + p.h 
      || this.y + this.w < p.y)
      return; // line is not touching box 

    switch (p.dir) {
    case 0:
      this.x-=1;
      break;
    case 1: 
      this.y-=1;
      break;
    case 2:
      this.x+=1;
      break;
    case 3:
      this.y+=1;
      break;
    }
  }
}
