
var b;

var pl;

function setup() {
    initializeFields();
    createCanvas(500, 500);
    frameRate(60);
    b = new Array(2);
    pl = new Array(4);
    b[0] = new Box(100, 400, 40);
    b[1] = new Box(300, 400, 75);
    pl[0] = new ProductionLine(100, 100, 200, 30, 2);
    pl[1] = new ProductionLine(300, 100, 30, 200, 3);
    pl[2] = new ProductionLine(130, 300, 200, 30, 0);
    pl[3] = new ProductionLine(100, 130, 30, 200, 1);
}

function draw() {
    background(255);
    for (let i = 0; i < b.length; i++) {
        for (var j = 0; j < pl.length; j++) {
            pl[j].show();
            b[i].moveOnLine(pl[j]);
        }
    }
    for (let i = 0; i < b.length; i++) {
        b[i].updateIfDragged();
        b[i].show();
    }
}


class ProductionLine {

    constructor(x, y, w, h, dir) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.dir = dir;
    }

    // Zeichnet das Fließband mit seinem Rand und den Linien die je nach Ausrichtung vertikal oder horizontal verlaufen
    show() {
        fill(150);
        rect(this.x, this.y, this.w, this.h);
        if (this.dir == 1 || this.dir == 3) {
            for (let i = 1; i < this.h / 10; i++) line(this.x + 5, this.y + i * 10, this.x + this.w - 5, this.y + i * 10);
        } else if (this.dir == 0 || this.dir == 2) {
            for (let i = 1; i < this.w / 10; i++) line(this.x + i * 10, this.y + 5, this.x + i * 10, this.y + this.h - 5);
        } else
            print("Invalid Direction!");
    }
}
class Box {

    constructor(x, y, w) {
        this.x = x;
        this.y = y;
        this.w = w;
    }

    // Displays the Box as a rect with two diagonal lines
    show() {
        fill(240, 220, 160);
        rect(this.x, this.y, this.w, this.w);
        line(this.x, this.y, this.x + this.w, this.y + this.w);
        line(this.x + this.w, this.y, this.x, this.y + this.w);
    }

    updateIfDragged() {
        this.selected = false;
        if (!(mouseX >= this.x && mouseX <= this.x + this.w && mouseY >= this.y && mouseY <= this.y + this.w && mouseIsPressed))
            // Box is not selected
            return;
        this.selected = true;
        let xDist = mouseX - pmouseX;
        let yDist = mouseY - pmouseY;
        this.x += xDist;
        this.y += yDist;
    }

    /**
     * Moves the Box across the production line
     */
    moveOnLine(p) {
        if (this.selected)
            // do not move if selected
            return;
        if (this.x > p.x + p.w || this.x + this.w < p.x || this.y > p.y + p.h || this.y + this.w < p.y)
            // line is not touching box
            return;
        switch (p.dir) {
            case 0:
                this.x -= 1;
                break;
            case 1:
                this.y -= 1;
                break;
            case 2:
                this.x += 1;
                break;
            case 3:
                this.y += 1;
                break;
        }
    }
}

function initializeFields() {
    b = null;
    pl = null;
}

