int konfetti_count = 100;  //Anzahl der Konfetti ausgehend vom Klick
float gravity = - 200;  //Stärke der Gravitation (ist negativ da Y-Koordinaten nach unten steigen).
float ystart;   //Y Startpunkt der Konfetti wird mit Klick festgelegt
float xstart;   //X Startpunkt der Konfetti wird mit Klick festgelegt
float minVelocity = 40;   // Minimale Geschwindigkeit eines Konfetti
float maxVelocity = 150;  // Maximale Geschwindigkeit eines Konfetti

Konfetti [] konfettis = new  Konfetti[konfetti_count];  //Array welches die Konfetti enthält

void setup() {
  size( 500, 500);
}

void draw() {
  background(255);
  if (konfettis[0] != null) {  //Verhindert dass auf ein noch nicht befülltes Array zugegriffen wird
    for (int i = 0; i < konfetti_count; i++) {
      konfettis[i].update();
    }
  }
}

void mousePressed() {
  if (mouseButton == LEFT) {
    ystart = mouseY;   //
    xstart = mouseX;
    frameCount = 0;  // Setzt den Zähler der Frames zurück für die Rechnung mit: (Sekunden seit Klick)
    for (int i = 0; i < konfetti_count; i++) {
      Konfetti particle = new Konfetti();   //Erstellt ein neues Konfetti Objekt an der Stelle der Maus
      konfettis[i] = particle;
    }
  }
}

class Konfetti {
  float velocity; //Geschwindigkeit des Konfetti
  float xPos;    // X Position des Konfetti
  float yPos;    // Y Position des Konfetti
  float direction;//Richtung der Flugbahn
  color paint;    //Farbe des Konfetti

  Konfetti() {
    this.velocity = random(minVelocity, maxVelocity);    //Geschwindigkeit wird zufällig gesetzt
    this.xPos = xstart;                 //Alle Konfetti starten am Punkt des Klicks
    this.yPos = ystart;
    this.paint = color(random(255), random(255), random(255));  //Farbe wird zufällig gewählt
    this.direction = random(0, 2*PI);     // Richtung der Flugbahn wird zufällig gewählt
  }

  void update() {  // Errechnet die neue Position des Konfetti
    float seconds = (frameCount/frameRate); // Ergibt die Skunden seit dem Klick
    this.xPos = xstart + velocity * cos(direction) * seconds;   //das Zeit-Orts-Gesetz der gleichförmigen Bewegung in x-Richtung
    this.yPos = ystart + (velocity * sin(direction)) * seconds - 0.5 * gravity * sq(seconds); //das Zeit-Orts-Gesetz der gleichförmigen Bewegung in y-Richtung
    fill(paint);
    circle(this.xPos, this.yPos, 20);
  }
}
