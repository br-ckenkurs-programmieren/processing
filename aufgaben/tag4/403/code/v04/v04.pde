// Diese Lösung verwendet mehrere Arrays. 
// Der Konfettiregen kann aber auch über ein Array für Objekte der Klasse "Partikel" (selbst erstellt) umgesetzt werden.

int anzahl = 1000;
float gravity = 0.2;
int durchmesser = 20;

float[] x = new float[anzahl];
float[] vx = new float[anzahl];

float[] y = new float[anzahl];
float[] vy = new float[anzahl];

color[] c = new color[anzahl];

void setup() {
  size(1000, 1000);
  background(0);
  noStroke();

  for (int i = 0; i < anzahl; i++) {
    // Dieser Ansatz ist einfach, führt allerdings zu einer rechteckigen Verteilung der Partikel
    x[i] = width/2;
    vx[i] = random(-5, 5);

    y[i] = height/2;
    vy[i] = random(-20, -5);

    c[i] = color(random(256), random(256), random(256));
  }
}

void draw() {
  background(0);
  // Berechne eine neue Position für jeden Partikel
  for (int i = 0; i < anzahl; i++) {
     
    x[i] += vx[i];
    y[i] += vy[i];

    vy[i] += gravity;    

  // Ersetze aus dem Fenster gefallene Partikel
    if (x[i] <= 0-durchmesser/2 || x[i] >= width+durchmesser/2 || y[i] <= 0-durchmesser/2 || y[i] >= height+durchmesser/2) {

      x[i] = mouseX;
      y[i] = mouseY;

      vx[i] = random(-5, 5);
      vy[i] = random(-20, -5);

      c[i] = color(random(256), random(256), random(256));
    }
    // Zeichne jeden Partikel in seiner zugehörigen Farbe
    fill(c[i]);
    ellipse(x[i], y[i], durchmesser, durchmesser);
  }
}
