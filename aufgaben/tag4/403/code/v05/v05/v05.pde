int anzahl = 1000; 
Partikel[] partikel = new Partikel[anzahl];

void setup(){
  size(1000, 1000);
  background(0);
  noStroke();
  // einzelne Partikel erstellen
  for (int i = 0; i < anzahl; i++){
    // Zufällige Verteilung der Partikel zu Anfang
    partikel[i] = new Partikel(random(0, width), random(0, height)); 
  }
}

void draw(){
  background(0);
  for (int i = 0; i < anzahl; i++){
    // Berechne eine neue Position für jeden Partikel
    partikel[i].move();
     // Ersetze aus dem Fenster gefallene Partikel
    if( ! (partikel[i].imFenster( -(partikel[i].durchmesser/2), width + (partikel[i].durchmesser/2), -(partikel[i].durchmesser/2), height + (partikel[i].durchmesser/2)))){
    // Zeichne jeden Partikel in seiner zugehörigen Farbe
      partikel[i] = new Partikel(mouseX, mouseY);
    }
    // Zeichne jeden Partikel in seiner zugehörigen Farbe 
    partikel[i].show();
  }
}
