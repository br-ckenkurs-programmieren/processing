class Partikel{
  float durchmesser = 20;
  float gravity = 0.2;
  float x;
  float y;
  float vx;
  float vy;
  color c;
  
  Partikel(float x, float y){
   float mF = 256.0;
    this.x = x;
    this.y = y;
    this.vx = random(-5, 5);
    this.vy = random(-20, -5);
    this.c = color(floor(random(mF)), floor(random(mF)), floor(random(mF))); 
  }
  
  // Neue Position errechnen
  void move(){
    this.x += this.vx;
    this.y += this.vy;
    this.vy += this.gravity;  
  }
  
  // Darstellen an aktueller Position
  void show(){
    fill(this.c);
    circle(x, y, this.durchmesser);
  }
  
  // Übergrüfen, ob der Partikel sich in einem bestimmten, viereckigen Bereich befindet
  boolean imFenster(float linkeGrenze, float rechteGrenze, float obereGrenze, float untereGrenze){
    if (this.x <= linkeGrenze || this.x >= rechteGrenze || this.y <= obereGrenze || this.y >= untereGrenze) {
      return false; 
    } else {
      return true;
    }
  }
}
