// Diese Version verfehlt die Aufgabenstellung zum Teil und geht das ganze
// leider auch relativ unschön an.

boolean pressed = false;
float[] x = new float[12]; // Container für die x-Positionen von 12 Partikel
float[] y = new float[12]; // Container für die y-Positionen von 12 Partikel
float[] xv = {-5, -4, -2, 0, 2, 4, 5, 4, 2, 0, -2, -4}; // alle x-Richtungen für die verschiedenen x[]
float[] yv = {0, 2, 4, 5, 4, 2, 0, -2, -4, -5, -4, -2}; // alle y-Richtungen für die verschiedenen y[]
int counter = 0;
float Mx;
float My;

void setup() {
  size(1000, 500);
  frameRate(40); // Geschwindigkeit der Partikel
  background(255);
}

void draw() {
  if (pressed == true) {
   background(255);        // Hintergrund in void draw(), damit die Partikel keine Spur ziehen
    if (counter <= 11) { // Zähler durch den die Weite der Partikel-Flugbahn auf 10 limitiert wird

      for (int i = 0; i <= 10; i++) {
        noStroke();                                 // keine Ränder
        fill(random(200), random(256), random(200));  // erzeuge zufällige Farbe
        rect(Mx + x[i], My + y[i], 5, 5);            // erzeuge Partikel ausgehend von Mausposition
        x[i] = x[i] + xv[i];                      // erzeuge neue x-Position (Flugbahn)
        y[i] = y[i] + yv[i];                      // erzeuge neue y-Position (Flugbahn)
      }
    }
  }
  counter++;      // gehandelte Frames von void draw() werden gezählt
}

void mousePressed() {
  pressed = true;          // verhindert, dass vor dem 1. Klick Partikel erzeugt werden
  counter = 0;            // Rücksetzen des Framezählers
  Mx = mouseX;            
  My = mouseY;
  for (int i = 0; i <= 10; i++) {       // Rücksetzen der Anfangs-Postition der Flugbahn
    x[i] = 0;
    y[i] = 0;
  }
}
