class Confetti {
  //Ein PVector kann einen Vektor mit x, y un z Wert speichern.
  //zusätzlich hat man dadurch viele praktische Methoden mitgeliefert, die man für Vektoren gebrauchen kann
  PVector position;
  PVector velocity;

  color fillColor = color(random(255), random(255), random(255));

  static final float maxSpeed = 3; //Wird nur einmal für die Klasse abgespeichert und kann nicht mehr verändert werden
  static final float maxAngle = 40;
  static final float circleSize = 10;
  final PVector gravity = new PVector(0, 0.05); //Schwerkraft wirkt sich in y-Richtung (nach unten) aus, nicht aber in x-Richtung

  // Konstruktor erzeugt neues Confetti und berechnet zufällig die Geschwindigkeit und somit die Bewegungsrichtung
  Confetti(PVector position) {
    this.position = position;
    velocity = PVector.fromAngle(radians(random(- maxAngle -90, maxAngle -90))).mult(random(maxSpeed));
  }
  // Updated Position des Confettis und ändert die Geschwindigkeit unter Berücksichtigung der Schwerkraft
  void update() {
    if (position.y < height - circleSize) {
      position.add(velocity);
      velocity.add(gravity);
    }
  }
  // Zeichnet Confetti an akutelle Position
  void display() {
    fill(fillColor);
    circle(position.x, position.y, circleSize);
  }
}
