int confettiIndex = 0;
// Array, das die erzeugten Confettis speichert
Confetti[] confetties= new Confetti[1000];

boolean arrayFull = false;

void setup() {
  size(500, 500);
}

void draw() {
  background(255);
  // Alle existierenden Confetti werden in ihrer Position aktualisiert und neu gezeichnet
  for (int i = 0; i < (arrayFull?confetties.length:confettiIndex); i++) {
    confetties[i].update();
    confetties[i].display();
  }
  //saveFrame("frames/confetti-####.jpg");
}

// Bei gedrückter Maustaste werden 20 neue Confetti erzeugt und dem Array hinzugefügt
// Das alte Array wird weiter verwendet, damit die Confetti vom letzten Mausklick nicht gelöscht werden sondern "liegen bleiben"
void mousePressed() {
  for (int i = 0; i < 20; i++) {
    confetties[confettiIndex++]= new Confetti(new PVector(mouseX, mouseY));
    if (confettiIndex >= confetties.length) { //ab diesem Zeitpunkt werden alte Konfetties mit neuen überschrieben
      confettiIndex = 0;
      arrayFull = true;
    }
  }
}
