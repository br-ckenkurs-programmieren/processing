// Diese Lösung verwendet mehrere Arrays. 
// Der Konfettiregen kann aber auch über ein Array für Objekte der Klasse "Partikel" (selbst erstellt) umgesetzt werden.

let anzahl = 1000;
let gravity = 0.2;
let durchmesser = 20;

let x = new Array(anzahl);
let vx = new Array(anzahl);

let y = new Array(anzahl);
let vy = new Array(anzahl);

let c = new Array(anzahl);

function setup() {
  createCanvas(1000, 1000);
  background(0);
  noStroke();
  
  //einzelne Partikel erstellen
  for (let i = 0; i < anzahl; i++) {
    // Zufällige Verteilung der Partikel zu Anfang
    x[i] = random(0, width);
    vx[i] = random(-5, 5);

    y[i] = random(0, height);
    vy[i] = random(-20, -5);

    c[i] = color(random(256), random(256), random(256));
  }
}

function draw() {
  background(0);
  // Berechne eine neue Position für jeden Partikel
  for (let i = 0; i < anzahl; i++) {
     
    x[i] += vx[i];
    y[i] += vy[i];

    vy[i] += gravity;    

  // Ersetze aus dem Fenster gefallene Partikel
    if (x[i] <= 0-durchmesser/2 || x[i] >= width+durchmesser/2 || y[i] <= 0-durchmesser/2 || y[i] >= height+durchmesser/2) {

      x[i] = mouseX;
      y[i] = mouseY;

      vx[i] = random(-5, 5);
      vy[i] = random(-20, -5);

      c[i] = color(random(256), random(256), random(256));
    }
    // Zeichne jeden Partikel in seiner zugehörigen Farbe
    fill(c[i]);
    ellipse(x[i], y[i], durchmesser, durchmesser);
  }
}
