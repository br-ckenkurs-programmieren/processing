function setup() {
  createCanvas(400, 400);
  background(255);
  var c1 = new Circle(200, 200, 220);
  c1.show();
  c1.d = 60;
  c1.show();
  c1.x += 50;
  c1.y -= 50;
  c1.show();
  new Circle(150, 250, 60).show();
}

class Circle {
  constructor(x, y, d) {
    this.x = x;
    this.y = y;
    this.d = d;
  }

  show() {
    circle(this.x, this.y, this.d);
  }
}
