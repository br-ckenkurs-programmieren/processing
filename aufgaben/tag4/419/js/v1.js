var v = 5;
var m1, m2, m3, m4;

function setup() {
  createCanvas(800, 400);
  strokeWeight(2);
  m1 = new Motor(0, new Point(-(width/2) + 0.5*(width/4), -30));
  m2 = new Motor(1, new Point(-(width/2) + 1.5*(width/4), -30));
  m3 = new Motor(2, new Point(-(width/2) + 2.5*(width/4), -30));
  m4 = new Motor(3, new Point(-(width/2) + 3.5*(width/4), -30));
}

function draw() {
  rectMode(CENTER);
  textAlign(CENTER);
  textSize(20);
  background(255);
  translate(width / 2, height / 2);

  m1.simulate();
  m2.simulate();
  m3.simulate();
  m4.simulate();

  m1.show();
  m2.show();
  m3.show();
  m4.show();

  //saveFrame("OttoMotor####.png");
}

class Motor {

  constructor(step, pos) {
    this.timeRange = v * 100;
    this.time = (this.timeRange / 4) * step;
    this.pos = pos;
  }

  simulate() {
    this.time = (this.time + 1) % this.timeRange;
  }

  show() {
    let motorHeight = 300; // length of motor (outer box)
    let motorWidth = 120; // width of motor box
    let motorOffset = -10; // vertical offset of motor box (visual only)
    let pistonHeight = 50;
    let pistonWidth = 100;
    let diskCenter = 100; // y offset to the center of the motor
    let pistonDistance = 130; // length of the axel that pushes away the piston
    let rot = radians((this.time / this.timeRange) * 720 - 90); // calculates the current position of the rotation
    let currentState = floor(map(this.time, 0, this.timeRange, 0, 4)); // determines the state the motor is currently in

    let motor = new Point(0, 10);
    let disk = new Point(0, diskCenter); // rotating disk at the bottom
    let edge = rotateAroundZero(new Point(0, diskCenter / 2), rot); // the rotating point
    edge.y += diskCenter; // moves the point down by 50 to fit the circle, this is a little hacky
    let piston = new Point(0, edge.y - pistonDistance); // the center of the piston that pushes up and down
    let textp = new Point(0, motorHeight / 2 + 30);
    let diskToEdge = new Line(disk.x, disk.y, edge.x, edge.y); // line from center of disk to the edge
    let edgeToPiston = new Line(edge.x, edge.y, piston.x, piston.y); // line from edge of the disk to the piston

    let centerAirTop = ((motor.y - motorHeight / 2) + (piston.y - pistonHeight / 2))/2; // top center between piston and motor (for gas etc)
    let centerAirHeight = dist(0, (motor.y - motorHeight / 2), 0, (piston.y - pistonHeight / 2));

    push();
    let xVal = this.pos.getX();
    let yVal = this.pos.getY();
    translate(xVal, yVal);

    fill(255);
    stroke(0);
    rect(motor.x, motor.y, motorWidth, motorHeight); //outer box

    switch(currentState) { //draws state specific objects
    case 0: 
      fill(186, 145, 184, (this.time / this.timeRange) * 300);
      stroke(45, 28, 45, (this.time / this.timeRange) * 255);
      rect(piston.x, centerAirTop, motorWidth - 10, centerAirHeight - 10);

      fill(0);
      text("1: Ansaugen", textp.x, textp.y);
      break;

    case 1:
      fill(186, 145, 184, (this.time / this.timeRange) * 300);
      stroke(45, 28, 45, 50);
      rect(piston.x, centerAirTop, motorWidth - 10, centerAirHeight - 10);

      fill(0);
      text("2: Verdichten", textp.x, textp.y);
      break;

    case 2:
      fill(255, (this.time / this.timeRange) * 500 - 200, 0, 500 - (this.time / this.timeRange) * 500);
      stroke(109, 8, 8, 255 - (this.time / this.timeRange) * 255);
      rect(piston.x, centerAirTop, motorWidth - 10, centerAirHeight - 10);

      fill(0);
      text("3: Arbeiten", textp.x, textp.y);
      break;

    case 3:
      fill(255, (this.time / this.timeRange) * 500 - 200, 0, 500 - (this.time / this.timeRange) * 500);  
      stroke(109, 8, 8, 255 - (this.time / this.timeRange) * 255);
      rect(piston.x, centerAirTop, motorWidth - 10, centerAirHeight - 10);

      fill(0);
      text("4: Ausstoßen", textp.x, textp.y);
      break;
    }

    fill(255);
    stroke(0);
    circle(disk.x, disk.y, 100); //disk
    rect(piston.x, piston.y, pistonWidth, pistonHeight); //piston
    diskToEdge.show();
    circle(disk.x, disk.y, 15); //disk decor
    edgeToPiston.show(); //this is drawn later to simulate depth
    circle(edge.x, edge.y, 10); //edge decor
    circle(piston.x, piston.y, 10); //piston decor

    pop();
  }
}

class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  clone() {
    return new Point(x, y);
  }
  
  getX(){
    return this.x;
  }
  
  getY(){
    return this.y;
  }
}

class Line {
  constructor(x1, y1, x2, y2) {
    this.p1 = new Point(x1, y1);
    this.p2 = new Point(x2, y2);
  }
  
  rotateLine(a) {
    this.p1 = rotateAroundZero(p1, a);
    this.p2 = rotateAroundZero(p2, a);
    return this;
  }

  show() {
    line(this.p1.getX(), this.p1.getY(), this.p2.getX(), this.p2.getY());
  }
}

function rotateAroundZero(p, a) {
  let nx = p.y*cos(a) - p.x*sin(a);
  let ny = p.y*sin(a) + p.x*cos(a);
  let out = new Point(nx, ny);
  return out;
}
