int speed = 5;
Motor m1, m2, m3, m4;

void setup() {
  size(800, 400);
  pixelDensity(2);
  strokeWeight(2);
  m1 = new Motor(0, new Point(-(width/2) + 0.5*(width/4), -30));
  m2 = new Motor(1, new Point(-(width/2) + 1.5*(width/4), -30));
  m3 = new Motor(2, new Point(-(width/2) + 2.5*(width/4), -30));
  m4 = new Motor(3, new Point(-(width/2) + 3.5*(width/4), -30));
}

void draw() {
  rectMode(CENTER);
  textAlign(CENTER);
  textSize(20);
  background(255);
  translate(width / 2, height / 2);

  m1.simulate();
  m2.simulate();
  m3.simulate();
  m4.simulate();

  m1.show();
  m2.show();
  m3.show();
  m4.show();

  //saveFrame("OttoMotor####.png");
}

class Motor {
  float timeRange, time;
  Point pos;

  Motor() {
    this(0);
  }

  Motor(int step) {
    this(step, new Point(0, 0));
  }

  Motor(int step, Point pos) {
    this.timeRange = speed * 100;
    this.time = (timeRange / 4) * step;
    this.pos = pos;
  }

  void simulate() {
    time = (time + 1) % timeRange;
  }

  void show() {
    float motorHeight = 300; // length of motor (outer box)
    float motorWidth = 120; // width of motor box
    float motorOffset = -10; // vertical offset of motor box (visual only)
    float pistonHeight = 50;
    float pistonWidth = 100;
    float diskCenter = 100; // y offset to the center of the motor
    float pistonDistance = 130; // length of the axel that pushes away the piston
    float rot = radians((time / timeRange) * 720 - 90); // calculates the current position of the rotation
    int currentState = floor(map(time, 0, timeRange, 0, 4)); // determines the state the motor is currently in

    Point motor = new Point(0, 10);
    Point disk = new Point(0, diskCenter); // rotating disk at the bottom
    Point edge = rotateAroundZero(new Point(0, diskCenter / 2), rot); // the rotating point
    edge.y += diskCenter; // moves the point down by 50 to fit the circle, this is a little hacky
    Point piston = new Point(0, edge.y - pistonDistance); // the center of the piston that pushes up and down
    Point text = new Point(0, motorHeight / 2 + 30);
    Line diskToEdge = new Line(disk, edge); // line from center of disk to the edge
    Line edgeToPiston = new Line(edge, piston); // line from edge of the disk to the piston

    float centerAirTop = ((motor.y - motorHeight / 2) + (piston.y - pistonHeight / 2))/2; // top center between piston and motor (for gas etc)
    float centerAirHeight = dist(0, (motor.y - motorHeight / 2), 0, (piston.y - pistonHeight / 2));

    pushMatrix();
    translate(pos.x, pos.y);

    fill(255);
    stroke(0);
    rect(motor.x, motor.y, motorWidth, motorHeight); //outer box

    switch(currentState) { //draws state specific objects
    case 0: 
      fill(#ba91b8, (time / timeRange) * 300);
      stroke(#2d1c2d, (time / timeRange) * 255);
      rect(piston.x, centerAirTop, motorWidth - 10, centerAirHeight - 10);

      fill(0);
      text("1: Ansaugen", text.x, text.y);
      break;

    case 1:
      fill(#ba91b8, (time / timeRange) * 300);
      stroke(#2d1c2d, 50);
      rect(piston.x, centerAirTop, motorWidth - 10, centerAirHeight - 10);

      fill(0);
      text("2: Verdichten", text.x, text.y);
      break;

    case 2:
      fill(255, (time / timeRange) * 500 - 200, 0, 500 - (time / timeRange) * 500);
      stroke(#6d0808, 255 - (time / timeRange) * 255);
      rect(piston.x, centerAirTop, motorWidth - 10, centerAirHeight - 10);

      fill(0);
      text("3: Arbeiten", text.x, text.y);
      break;

    case 3:
      fill(255, (time / timeRange) * 500 - 200, 0, 500 - (time / timeRange) * 500);  
      stroke(#6d0808, 255 - (time / timeRange) * 255);
      rect(piston.x, centerAirTop, motorWidth - 10, centerAirHeight - 10);

      fill(0);
      text("4: Ausstoßen", text.x, text.y);
      break;
    }

    fill(255);
    stroke(0);
    circle(disk.x, disk.y, 100); //disk
    rect(piston.x, piston.y, pistonWidth, pistonHeight); //piston
    diskToEdge.show();
    circle(disk.x, disk.y, 15); //disk decor
    edgeToPiston.show(); //this is drawn later to simulate depth
    circle(edge.x, edge.y, 10); //edge decor
    circle(piston.x, piston.y, 10); //piston decor

    popMatrix();
  }
}

class Point {
  float x, y;
  Point(float x, float y) {
    this.x = x;
    this.y = y;
  }

  public Point clone() {
    return new Point(x, y);
  }

  public String toString() {
    return "(" + x + ", " + y +")";
  }
}

class Line {
  Point p1, p2;
  Line(float x1, float y1, float x2, float y2) {
    p1 = new Point(x1, y1);
    p2 = new Point(x2, y2);
  }

  Line(Point p1, Point p2) {
    this.p1 = p1.clone();
    this.p2 = p2.clone();
  }

  Line rotateLine(float a) {
    p1 = rotateAroundZero(p1, a);
    p2 = rotateAroundZero(p2, a);
    return this;
  }

  void show() {
    line(p1.x, p1.y, p2.x, p2.y);
  }
}

Point rotateAroundZero(Point p, float a) {
  float nx = p.y*cos(a) - p.x*sin(a);
  float ny = p.y*sin(a) + p.x*cos(a);
  Point out = new Point(nx, ny);
  return out;
}
