let margin; //Abstand zwischen Fragen und Antworten
let padding; //Abstand Text zum Boxenrand
let qheight; //Höhe der Fragen
let qwidth; //Breite der Fragen
let awidth; //Breite der Antworten
let aheight; //Höhe der Antworten

let richtige = 0; //Anzahl der richtig bestimmten Antworten
let falsche = 0; //Anzahl der falsch bestimmten Antworten

let fragen; 

let aktuelleFrage; 

let richtig;
let dauer = -1;

class Frage {

  constructor(fragenText, moeglicheAntworten, richtigeAntwort) {
    this.fragenText = fragenText;
    if (moeglicheAntworten.length == 4)
      this.antworten = moeglicheAntworten;
    if (richtigeAntwort >= 0 && richtigeAntwort < 4)
      this.richtigeAntwort = richtigeAntwort;
  }
}

function ladeFragen() {
  let frage1 = new Frage("Welches dieser Zeichen ist kein Java-Operator?", ["+","!", "_", "&&"], 2);
  let frage2 = new Frage("Welches ist eine korrekte for-Schleife die über ein array namens 'bar' iteriert?", 
    ["for (let i == 0; i >= bar.length; i++)", "for (let i = 0; i <= bar.length; i++)", "for (let i = 0; i < bar.length; i+=1)", "for (let i == 0; i < bar.length; i++)"], 2);
  fragen = [frage1, frage2];
}

function naechsteFrage() {
  let nf = fragen[floor(random(fragen.length))];
  if (nf == aktuelleFrage && fragen.length > 1)
    naechsteFrage();
  else
    aktuelleFrage = nf;
}

function setup() {
  ladeFragen();
  createCanvas(1000, 500);
  guiInit();
  naechsteFrage();
}

function guiInit() {
  qheight = (height-200)/3;
  qwidth = width-200;
  awidth = qwidth / 2;
  aheight = (height-qheight-200)/2;  
  margin = 100;
  padding = 10;
}

function draw() {
  background(0);    

  schreibeQuizFenster();

  if (dauer != -1)

    schreibeRichtigFalsch();

  schreibeErgebnisse();
}

function schreibeQuizFenster() {
  rect(margin, margin, qwidth, qheight);
  fill(0);
  text(aktuelleFrage.fragenText, margin+padding, margin+padding, qwidth-30, qheight-30);
  fill(255);
  let ax = 100; 
  let ay = 100+qheight; 
  rect(ax, ay, awidth, aheight); 
  schreibeAntwort(0, ax, ay, awidth, aheight);
  ax += awidth;
  rect(ax, ay, awidth, aheight); 
  schreibeAntwort(1, ax, ay, awidth, aheight);
  ax -= awidth;
  ay += aheight;
  rect(ax, ay, awidth, aheight); 
  schreibeAntwort(2, ax, ay, awidth, qheight);
  ax += awidth;
  rect(ax, ay, awidth, aheight);
  schreibeAntwort(3, ax, ay, awidth, qheight);
}

function schreibeErgebnisse() {
  text("Richtige: "+richtige, 0, 25);
  text("Falsche: "+falsche, 0, 50);
}

function schreibeRichtigFalsch() {
  console.log(dauer);
  if (dauer > 50)
    dauer = -1;
  else
  {
    if (richtig)
    {
      fill('#4B8B3B');
      text("RICHTIG!", 450, 50);
    } else
    {
      fill('#DE1738');
      text("FALSCH!", 450, 50);
    }
    fill(255);
    dauer++;
  }
}

function schreibeAntwort( i, ax, ay, aw, ah) {
  fill(0);
  text(aktuelleFrage.antworten[i], ax+padding, ay+padding, aw-2*padding, ah-2*padding);
  fill(255);
}

function mouseClicked() {
  let gewAntw = -1;
  let astarty = margin+qheight;
  if (mouseX > margin && mouseY > astarty) {
    if (mouseX < margin+awidth) {

      if (mouseY < astarty+aheight)
        gewAntw = 0;
      else if (mouseY < astarty+aheight*2)
        gewAntw = 2;
    } else if (mouseX < margin + awidth*2) {

      if (mouseY < astarty+aheight)
        gewAntw = 1;
      else if (mouseY < astarty+aheight*2)
        gewAntw = 3;
    }

    if (gewAntw != -1) {
      if (gewAntw == aktuelleFrage.richtigeAntwort) {
        dauer = 0;
        richtig = true;
        richtige++;
      } else {
        dauer = 0;
        richtig = false;
        falsche++;
      }
      naechsteFrage();
    }
  }
}
