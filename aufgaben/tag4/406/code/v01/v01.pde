PFont f; //Schriftart

float margin; //Abstand zwischen Fragen und Antworten
float padding; //Abstand Text zum Boxenrand
float qheight; //Höhe der Fragen
float qwidth; //Breite der Fragen
float awidth; //Breite der Antworten
float aheight; //Höhe der Antworten

int richtige = 0; //Anzahl der richtig bestimmten Antworten
int falsche = 0; //Anzahl der falsch bestimmten Antworten

Frage[] fragen; 

Frage aktuelleFrage; 

boolean richtig;
int dauer = -1;

class Frage {
  String fragenText;
  String[] antworten;
  int richtigeAntwort;

  Frage(String fragenText, String[] moeglicheAntworten, int richtigeAntwort) {
    this.fragenText = fragenText;
    if (moeglicheAntworten.length == 4)
      this.antworten = moeglicheAntworten;
    if (richtigeAntwort >= 0 && richtigeAntwort < 4)
      this.richtigeAntwort = richtigeAntwort;
  }
}

void ladeFragen() {
  Frage frage1 = new Frage("Welches dieser Zeichen ist kein Java-Operator?", new String[]{"+", "!", "_", "&&"}, 2);
  Frage frage2 = new Frage("Welches ist eine korrekte for-Schleife die über ein array namens 'bar' iteriert?", 
    new String[]{"for (int i == 0; i >= bar.length; i++)", "for (int i = 0; i <= bar.length; i++)", "for (int i = 0; i < bar.length; i+=1)", "for (int i == 0; i < bar.length; i++)"}, 2);
  fragen = new Frage[]{frage1, frage2};
}

void naechsteFrage() {
  Frage nf = fragen[(int) random(fragen.length)];
  if (nf == aktuelleFrage && fragen.length > 1)
    naechsteFrage();
  else
    aktuelleFrage = nf;
}

void setup() {
  ladeFragen();
  size(1000, 500);
  guiInit();
  f = createFont("Arial", 16, true);
  naechsteFrage();
}

void guiInit() {
  qheight = (height-200)/3;
  qwidth = width-200;
  awidth = qwidth / 2;
  aheight = (height-qheight-200)/2;  
  margin = 100;
  padding = 10;
}

void draw() {
  background(0);
  textFont(f, 20);    

  schreibeQuizFenster();

  if (dauer != -1)

    schreibeRichtigFalsch();

  schreibeErgebnisse();
}

void schreibeQuizFenster() {
  rect(margin, margin, qwidth, qheight);
  fill(0);
  text(aktuelleFrage.fragenText, margin+padding, margin+padding, qwidth-30, qheight-30);
  fill(255);
  float ax = 100; 
  float ay = 100+qheight; 
  rect(ax, ay, awidth, aheight); 
  schreibeAntwort(0, ax, ay, awidth, aheight);
  ax += awidth;
  rect(ax, ay, awidth, aheight); 
  schreibeAntwort(1, ax, ay, awidth, aheight);
  ax -= awidth;
  ay += aheight;
  rect(ax, ay, awidth, aheight); 
  schreibeAntwort(2, ax, ay, awidth, qheight);
  ax += awidth;
  rect(ax, ay, awidth, aheight);
  schreibeAntwort(3, ax, ay, awidth, qheight);
}

void schreibeErgebnisse() {
  text("Richtige: "+richtige, 0, 25);
  text("Falsche: "+falsche, 0, 50);
}

void schreibeRichtigFalsch() {
  println(dauer);
  if (dauer > 50)
    dauer = -1;
  else
  {
    if (richtig)
    {
      fill(#4B8B3B);
      text("RICHTIG!", 450, 50);
    } else
    {
      fill(#DE1738);
      text("FALSCH!", 450, 50);
    }
    fill(255);
    dauer++;
  }
}

void schreibeAntwort(int i, float ax, float ay, float aw, float ah) {
  fill(0);
  text(aktuelleFrage.antworten[i], ax+padding, ay+padding, aw-2*padding, ah-2*padding);
  fill(255);
}

void mouseClicked() {
  int gewAntw = -1;
  float astarty = margin+qheight;
  if (mouseX > margin && mouseY > astarty) {
    if (mouseX < margin+awidth) {

      if (mouseY < astarty+aheight)
        gewAntw = 0;
      else if (mouseY < astarty+aheight*2)
        gewAntw = 2;
    } else if (mouseX < margin + awidth*2) {

      if (mouseY < astarty+aheight)
        gewAntw = 1;
      else if (mouseY < astarty+aheight*2)
        gewAntw = 3;
    }

    if (gewAntw != -1) {
      if (gewAntw == aktuelleFrage.richtigeAntwort) {
        dauer = 0;
        richtig = true;
        richtige++;
      } else {
        dauer = 0;
        richtig = false;
        falsche++;
      }
      naechsteFrage();
    }
  }
}
