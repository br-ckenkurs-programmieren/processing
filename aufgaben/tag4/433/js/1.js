var isDarkTheme;

function setup() {
    initializeFields();
    createCanvas(400, 400);
    textSize(40);
}

function draw() {
    if (isDarkTheme) {
        background(30);
        fill(255);
    } else {
        background(255);
        fill(20);
    }
    text("Hallo Welt!", 100, 100);
}

function mouseClicked() {
    isDarkTheme = !isDarkTheme;
}

function initializeFields() {
    isDarkTheme = false;
}