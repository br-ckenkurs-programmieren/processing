boolean isDarkTheme = false;

void setup() {
  size(400, 400);
  textSize(40);
}

void draw() {
  if (isDarkTheme) {
    background(30);
    fill(255);
  } else {
    background(255);
    fill(20);
  }
  text("Hallo Welt!", 100, 100);
}

void mouseClicked() {
  isDarkTheme = !isDarkTheme;
}
