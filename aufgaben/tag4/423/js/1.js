// -x (x => 10)= Schlüssel für Tür x
// -1 = Ziel
// 0 = Luft
// 1 = Wand
// 2 = Spieler
// x (x => 10) = Tür x
let field = [
    [2,   1,   0,   0, -14,   0, 1,   1,  0, -16],
    [0,   0,   0,   1,   1,  15, 0,   0,  0,   1],
    [1,   0,   1,   0,   0,   1, 1,   0,  1,   1],
    [1,  14,   1,   1,   0,   1, 1,   0,  0,   1],
    [0,   0,   0,   1,   0, -10, 0,   1, 10,   0],
    [0,   1,   0,   1,   1,   0, 1, -15,  1,  16],
    [0,   1,  11,   1,   1,   0, 0,   0,  1,   0],
    [-11, 1,   0, -13,   1,   0, 0,   1,  0,   0],
    [1,   1,   0,  12,   0,   0, 1,   1,  0,   1], 
    [1, -12,  13,   1,   1,   0, 0,   1,  0,  -1]
  ];
  // Positionen der Spielfigur
  let x = 0, y = 0, sieg, squareHeight, squareWidth;
  
  function setup() {
    createCanvas(800, 800);
    textSize(100);
    textAlign(CENTER, CENTER);
    background(255);
    squareHeight = height / field.length;
    squareWidth = width / field[0].length;
  }
  
  function draw() {
    for (let i = 0; i<field.length; i++) {
      for (let j = 0; j<field[i].length; j++) {
        switch(field[i][j]) {
          //Zeichne für jeden Wert im Array ein Kästchen mit der entsprechenden Farbe
        case 0:
          drawRect(i, j, color(255));
          break;
        case 1:
          drawRect(i, j, color(0));
          break;
        case 2:
          drawRect(i, j, color(0, 0, 255));
          break;
        case -1:
          drawRect(i, j, color(0, 255, 0));
          break;
        default:
          if (field[i][j] >= 10) { // Zeichne Tür
            drawRect(i, j, color(255, 0, 0));
          } else if (field[i][j] <= -10) { // Zeichne Schlüssel
            drawRect(i, j, color(255));
            fill(255, 255, 0);
            circle(i*squareWidth+width/20, j*squareHeight+height/20, 30);
          }
          break;
        }
      }
    }
  
    if (sieg) {
      background(255);
      text("Gewonnen", width/2, height/2);
    }
  }
  
  function drawRect(x, y, c) {
    fill(c);
    rect(x*squareWidth, y*squareHeight, squareWidth, squareHeight);
  }
  
  // Bei Drücken der Pfeiltasten bewegt sich der Spieler in die entsprechende Richtung
  function keyPressed() {
    field[x][y] = 0; // Spieler löscht sich selbst
  
    // Bewegung nur möglich wenn Feld existiert und betretbar ist (Wert <= 0 also Ziel, Schlüssel oder Luft)
    switch(keyCode) {
    case UP_ARROW:
      if (y > 0 && field[x][y-1] <= 0) y--;
      break;
    case DOWN_ARROW:
      if (y < field.length-1 && field[x][y+1] <= 0) y++;
      break;
    case LEFT_ARROW:
      if (x > 0 && field[x-1][y] <= 0) x--;
      break;
    case RIGHT_ARROW:
      if (x < field[0].length-1 && field[x+1][y] <= 0) x++;
      break;
    }
  
    //Falls der Schlüssel eingesammelt wurde, wird die Tür geöffnet
    if (field[x][y] <= -10) unlock(field[x][y]);
  
    // Falls das Ziel erreicht wurde, wird der Siegbildschirm angezeigt
    if (field[x][y] == -1) sieg = true;
  
    field[x][y] = 2;
  }
  
  // Sucht die verschlossene Tür im Array und ändert den Wert von 3 auf 0 (Luft)
  function unlock(key) {
    for (let i = 0; i<field.length; i++) {
      for (let j = 0; j<field[i].length; j++) {
        if (field[i][j] == -key) {
          field[i][j] = 0;
        }
      }
    }
  }