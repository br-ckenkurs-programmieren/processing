Segment[] segmente = new Segment[]{new Segment(500, 0), new Segment(400, PI/4), new Segment(300, PI/8), new Segment(200, PI/16)};

int aktuellerMotor = 1;

class Segment {
  float laenge;
  float winkel;

  Segment(float laenge, float winkel) {
    this.laenge = laenge;
    this.winkel = winkel;
  }
}

void setup() {
  size(1920, 1080);
}

void draw() {
  background(255);
  noFill();
  //fill(255);
  translate(125, 950);
  for (Segment segment : segmente) {
    rotate(segment.winkel);
    rect(-25, 0, 50, -segment.laenge);
    translate(0, -segment.laenge);
  }
  line(-10, 0, -25, -25);
  line(-25, -25, -10, -50);
  line(10, 0, 25, -25);
  line(25, -25, 10, -50);
  println("aktueller Motor: " + aktuellerMotor);
  resetMatrix();
  square(1000, 1000-100, 100);
}

void keyPressed() {
  println(key + "|" + keyCode);
  if (keyCode == LEFT)
    segmente[aktuellerMotor].winkel -= PI/16;
  if (keyCode == RIGHT)
    segmente[aktuellerMotor].winkel += PI/16;
  if (key >= '1' && key <= '4')
    aktuellerMotor = key - '1';
}
