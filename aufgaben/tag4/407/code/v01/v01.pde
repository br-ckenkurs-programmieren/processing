Segment[] segments;
int selected;

void setup() {
  size(600, 600);
  selected = 0;
  
  segments = new Segment[]{
    new Segment(0, 200, 20), new Segment(0.7, 175, 18), 
    new Segment(0.9, 150, 16), new Segment(0.4, 100, 14), 
    new Segment(0.2, 0, 0) // unsichtbares Segment ermöglicht Bewegung der Hand
  };
}

void draw() {
  if (key - 48 >= 0 && key - 48 < segments.length)
    selected = key - 48;

  background(255);

  pushMatrix();
  translate(100, height);
  rotate(PI);

  for (Segment s : segments)
    s.show();

  triangle(0, 0, -10, 20, 10, 20); // Hand
  popMatrix();
}

void keyPressed() {
  segments[selected].move(keyCode);
}

class Segment {
  float angle;
  float l, w;

  Segment(float angle, float l, float w) {
    this.angle = angle;
    this.l = l;
    this.w = w;
  }

  void move(int key) {
    if (key == LEFT)
      angle -= 0.01;
    else if (key == RIGHT)
      angle += 0.01;
  }

  void show() {
    rotate(angle);
    rect(-w / 2, 0, w, l);
    translate(0, l);
  }
}
