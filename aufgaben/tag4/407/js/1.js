
var segments;

var selected;

function setup() {
    initializeFields();
    createCanvas(600, 600);
    selected = 0;
    segments = [new Segment(0, 200, 20), new Segment(0.7, 175, 18), new Segment(0.9, 150, 16), new Segment(0.4, 100, 14), new Segment(0.2, 0, 0)];
}

function draw() {
    switch (key) {
        case '1':
            selected = 0;
            break;
        case '2':
            selected = 1;
            break;
        case '3':
            selected = 2;
            break;
        case '4':
            selected = 3;
            break;
        case '5':
            selected = 4;
            break;
    }
    background(255);
    push();
    translate(100, height);
    rotate(PI);
    segments.forEach(handleSegments);
    triangle(0, 0, -10, 20, 10, 20); // Hand
    pop();
}

function handleSegments(s) {
    s.show();
}

function keyPressed() {
    segments[selected].move(keyCode);
}

class Segment {
    constructor(angle, l, w) {
        this.angle = angle;
        this.l = l;
        this.w = w;
    }
    move(key) {
        if (key == LEFT_ARROW)
            this.angle -= 0.03;
        else if (key == RIGHT_ARROW)
            this.angle += 0.03;
    }

    show() {
        rotate(this.angle);
        rect(-this.w / 2, 0, this.w, this.l);
        translate(0, this.l);
    }
}

function initializeFields() {
    segments = null;
    selected = 0;
}

