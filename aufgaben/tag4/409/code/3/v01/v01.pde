void rotatedRectangle(float xCoordinate, float yCoordinate, float xExtend, float yExtend, float angle) {
  //der Ortsvektor von (x,y) als float Array
  float[] a = new float[]{xCoordinate, yCoordinate};

  // die Richtungsvektoren von (x,y) zu den drei übrigen Ecken des Rechteckes:
  float[] b = new float[]{xExtend, 0};
  float[] c = new float[]{xExtend, yExtend};
  float[] d = new float[]{0, yExtend};

  // die Richtungsvektoren werden nun um den Wert von 'angle' gedreht
  b = rotate(b, angle);
  c = rotate(c, angle);
  d = rotate(d, angle);

  // jetzt wird auf die Richtungsvektoren der Ortsvektor (x,y) addiert
  b[0] += xCoordinate;
  b[1] += yCoordinate;
  c[0] += xCoordinate;
  c[1] += yCoordinate;
  d[0] += xCoordinate;
  d[1] += yCoordinate;

  // wir haben jetzt die vier Ecken unseres gedrehten Rechteckes. Dieses muss jetzt noch gezeichnet werden:
  quad(a[0], a[1], b[0], b[1], c[0], c[1], d[0], d[1]);
}

// rotate dreht einen Punkt um den Winkel angle. Zur mathematischen Berechnung siehe: Rotation in der Ebene
float[] rotate(float[] point, float angle) {
  float[] result = new float[2];
  result[0] = point[0] * cos(angle) - point[1] * sin(angle);
  result[1] = point[0] * sin(angle) + point[1] * cos(angle);
  return result;
}

void setup() {
  size(400, 400);
}

// hier werden die Winkel der Zeiger gespeichert
float stundenzeiger = 0;
float minutenzeiger = 0;

void draw() {
  background(255);
  strokeWeight(6);
  
  // Die Fläche der Uhr:
  fill(255);
  circle(200,200,375);
  
  
  // Beschriftung der Uhr:
  fill(0);
  textSize(25);
  text("12",187.5,40);
  text("3",360,200);
  text("6",200,380);
  text("9",30,200);
  
  // Die Zeiger:
  rotatedRectangle(200,200,8,150,stundenzeiger);
  rotatedRectangle(200,200,4,175,minutenzeiger);
  
  // Bei der standardmäßigen frameRate von 60 braucht der Stundenzeiger eine Minute für einen kompletten Kreis
  stundenzeiger += 2 * PI / 60;
  // ... und der minutenzeiger eine Sekunde
  minutenzeiger += 2 * PI / 3600;
}
