void rotatedRectangle(float xCoordinate, float yCoordinate, float xExtend, float yExtend, float angle) {
  //der Ortsvektor von (x,y) als float Array
  float[] a = new float[]{xCoordinate, yCoordinate};

  // die Richtungsvektoren von (x,y) zu den drei übrigen Ecken des Rechteckes:
  float[] b = new float[]{xExtend, 0};
  float[] c = new float[]{xExtend, yExtend};
  float[] d = new float[]{0, yExtend};

  // die Richtungsvektoren werden nun um den Wert von 'angle' gedreht
  b = rotate(b, angle);
  c = rotate(c, angle);
  d = rotate(d, angle);

  // jetzt wird auf die Richtungsvektoren der Ortsvektor (x,y) addiert
  b[0] += xCoordinate;
  b[1] += yCoordinate;
  c[0] += xCoordinate;
  c[1] += yCoordinate;
  d[0] += xCoordinate;
  d[1] += yCoordinate;

  // wir haben jetzt die vier Ecken unseres gedrehten Rechteckes. Dieses muss jetzt noch gezeichnet werden:
  quad(a[0], a[1], b[0], b[1], c[0], c[1], d[0], d[1]);
}

// rotate dreht einen Punkt um den Winkel angle. Zur mathematischen Berechnung siehe: Rotation in der Ebene
float[] rotate(float[] point, float angle) {
  float[] result = new float[2];
  result[0] = point[0] * cos(angle) - point[1] * sin(angle);
  result[1] = point[0] * sin(angle) + point[1] * cos(angle);
  return result;
}

// Beispiel
void setup() {
  size(500, 400);
}

void draw() {
  line(0, 200, 500, 200);
  line(200, 0, 200, 500);
  rotatedRectangle(200, 200, 150, 100, PI/4);
}
