// Lösung mit Hilfe der Funktionen translate() und roate():
// Beachte: Die Reihenfolge von translate() und roate() verändert das Ergebnis!
void rotatedRectangle(float xCoordinate, float yCoordinate, float xExtend, float yExtend, float angle) {
  translate(xCoordinate, yCoordinate);
  rotate(angle);
  rect(0, 0, xExtend, yExtend);

  /*
  Unser gedrehtes Recteck ist gezeichnet, nun müssen wir noch translate() und roate() rückgängig machen,
   sonst werden auch alle folgenden Zeichnungen verschoben und gedreht!
  */
  rotate(-angle);
  translate(-xCoordinate, -yCoordinate);
}

// Beispiel
void setup() {
  size(500, 500);
}

void draw() {
  line(0, 200, 500, 200);
  line(200, 0, 200, 500);
  rotatedRectangle(200, 200, 150, 100, PI/4);
}
