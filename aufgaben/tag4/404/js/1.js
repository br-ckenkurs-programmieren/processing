function  setup() {
    createCanvas(1000, 1000);
    frameRate(5);
  }
  
  function  draw() {
    drawRandomShape(random(width), random(height), randomColor());
  }
  
  function drawRandomShape(w, h, c) {
    fill(c);
    var choice = random(4);
    choice = (choice * 10 - ((choice * 10) % 10)) / 10;
    switch(choice) {
    case 0:
      circle(random(width), random(height), w);
      break;
    case 1:
      rect(random(width), random(height), w, h);
      break;
    case 2:
      triangle(500-w/2, 500+h/2, 500+h/2, 500+h/2, w/2, h-h/2);
      break;
    case 3:
      ellipse(w/2, h/2, w, h);
      break;
    }
  }
  
  function randomColor() {
    return color(random(255), random(255), random(255));
  }