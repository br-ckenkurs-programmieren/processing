class Ball {
  
    constructor(x, y, vx, vy) {
      this.x = x;
      this.y = y;
      this.vx = vx;
      this.vy = vy;
    }
  
    display() {
      fill(255);
      circle(this.x, this.y, 50);
    }
  
    move() {
      this.x += this.vx;  
      this.y += this.vy;
      if (this.x > width || this.x < 0)
        this.vx *= -1;
      if (this.y > height || this.y < 0)
        this.vy *= -1;
    }
  }
  
  var balls;
  var maxBalls;
  
  function setup()
  {
    createCanvas(400, 400);
    maxBalls = random(1,7);
    maxBalls = (maxBalls * 10 - (maxBalls * 10 % 10)) / 10;
    balls = [maxBalls];
    createBalls();
  }
  
  function createBalls() {
    for (var i = 0; i < maxBalls; i++) {
      var xdir = 1;
      var ydir = 1;
      if (random(1) > 0.5)
        xdir *= -1;
      if (random(1) > 0.5)
        ydir *= -1;
      balls[i] = new Ball(random(width), random(height), random(2, 10)*xdir, random(2, 10)*ydir);
    }
  }
  
  function draw() {
    background(0);
    for (var i = 0; i < maxBalls; i++) {
      balls[i].move();
      balls[i].display();
    }
  }
  
  function mouseClicked() {
    createBalls();
  }