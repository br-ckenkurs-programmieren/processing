void setup() {
  size(400, 300);
}

void draw() {
  drawRandomShape(random(width), random(height), randomColor());
}

void drawRandomShape(float x, float y, color col) {
  int shapeCount = 3; // anzahl der abgedeckten fällen für formen
  int shape = int(random(64000)) % shapeCount; // Zufälliger wert zwischen 0 und max(shapeCount, 64000)
  fill(col);
  switch(shape) {
  case 0: // Zeichne einen Kreis.
    circle(x, y, random(5, 40));
    break;
  case 1: // Zeichne ein Rechteck.
    rect(x, y, random(5, 40), random(5, 40));
    break;
  case 2: // Zeichne eine Ellipse
    ellipse(x, y, random(5, 40), random(5, 40));
    break;
  default:
    break;
  }
}

color randomColor() {
  return color(random(255), random(255), random(255));
}
