void  setup() {
  size(1000, 1000);
  frameRate(5);
}

void  draw() {
  drawRandomShape(random(width), random(height), randomColor());
}

void drawRandomShape(float w, float h, color c) {
  fill(c);
  int choice = (int) random(4);
  switch(choice) {
  case 0:
    circle(random(width), random(height), w);
    break;
  case 1:
    rect(random(width), random(height), w, h);
    break;
  case 2:
    triangle(500-w/2, 500+h/2, 500+h/2, 500+h/2, w/2, h-h/2);
    break;
  case 3:
    ellipse(w/2, h/2, w, h);
    break;
  }
}

color randomColor() {
  return color(random(255), random(255), random(255));
}
