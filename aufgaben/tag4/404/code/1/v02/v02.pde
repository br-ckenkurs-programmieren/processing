void setup() {
  size(400, 300);
}
void draw() {
  drawRandomShape(int(random(width)), int(random(height)), randomColor());
}

void drawRandomShape(int x, int y, int c) {
  fill(c);
  if (random(2) > 1) // Box oder Kreis?
    rect(x, y, 50, 50); 
  else
    ellipse(x, y, 40, 40);
}

color randomColor() {
  int r = int(random(256));
  int g = int(random(256));
  int b = int(random(256));
  return color(r, g, b);
}
