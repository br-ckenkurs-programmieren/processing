Ball ball1 = new Ball(350, 150, 5, 5);      // Erzeugen eines neuen Objekts ball1
Ball ball2 = new Ball(50, 100, 7, -3);     // Ball(xb, yb, vxb, vyb) diese Variablen werden nach unten übergeben

void setup() {
  size(600, 300);
  background(255);
}

void draw() {
  background(255);
  fill(60);
  ball1.display();      // Aufruf der Methode zur Darstellung des Objekts
  ball1.move();         // Aufruf der Methode zur Bewegung des Objekts
  ball2.display();      // Objekt.Methode ist nichts andres als ein Pfad damit man weis wo die Methode liegt
  ball2.move();         // Eine Methode ist theoretisch eine Funktion die an eine Klasse gebunden ist
}

class Ball {            // Definierung der Klasse | Bsp: rect(); ist die Funktion einer vordefinierten Klasse (Objekt)
  int x;                // Festlegung der Eigenschaften
  int y;
  int vx;
  int vy;

  Ball(int xb, int yb, int vxb, int vyb) {    // Construktor übergibt die Variablen von einzelnen Objekten z.B. ball1 zur Erstellung an die Klasse 
    x = xb;                        //xb als der 1. übergebene Wert von z.B. ball1 xb=300 wird auf den x-Wert der Klasse geschreiben
    y = yb;                        // in java würde es this.y heissen. In Processing kann man "this." weglassen.
    vx = vxb;
    vy = vyb;
  }

  // Methode zum Darstellen des Balls/Bälle
  void display() {
    ellipse(x, y, 50, 50);
  }

  // Methode zum bewegen des Balls/Bälle
  void move() {
    y = y + vy;
    x = x + vx;
    if (x >= width || x <= 0) {    // Ball soll das Fenster nicht verlassen
      vx *= (-1);
    } else if (y >= height || y <= 0) {
      vy *= (-1);
    }
  }
}
