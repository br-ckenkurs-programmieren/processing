class Ball {
  float x;
  float y;
  float vx;
  float vy;

  Ball(float x, float y, float vx, float vy) {
    this.x = x;
    this.y = y;
    this.vx = vx;
    this.vy = vy;
  }

  void display() {
    circle(x, y, 50);
  }

  void move() {
    x += vx;  
    y += vy;
    if (x > width || x < 0)
      vx *= -1;
    if (y > height || y < 0)
      vy *= -1;
  }
}

Ball[] balls = new Ball[(int) random(1, 7)];

void setup()
{
  size(1000, 1000);
  createBalls();
}

void createBalls() {
  for (int i = 0; i < balls.length; i++) {
    float xdir = 1;
    float ydir = 1;
    if (random(1.0) > 0.5)
      xdir *= -1;
    if (random(1.0) > 0.5)
      ydir *= -1;
    balls[i] = new Ball(random(width), random(height), random(2, 10)*xdir, random(2, 10)*ydir);
  }
}

void draw() {
  background(0);
  for (Ball ball : balls) {
    ball.move();
    ball.display();
  }
}

void mouseClicked() {
  createBalls();
}
