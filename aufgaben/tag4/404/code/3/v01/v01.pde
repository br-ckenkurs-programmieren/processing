Ball[] ballArr;
int ballAmount = 10;

void setup() {
  size(400, 300);
  // Erstelle die Bälle
  ballArr = new Ball[ballAmount];
  for (int i = 0; i < ballAmount; i++) {
    ballArr[i] = new Ball();
  }
}

color randomColor() {
  return color(random(255), random(255), random(255));
}

void draw() {
  background(200);
  // Zeichne und bewege alle Bälle
  for (int i = 0; i < ballAmount; i++) {
    ballArr[i].display();
    ballArr[i].move();
  }
}

// Aufgabenteil 2 & 3
class Ball {
  float x, y;
  float vx, vy;
  float radius;
  color col;

  // Setze zufällige startwerte für den Ball
  Ball() {
    radius = random(5, 40);
    x = random(radius, width - radius);
    y = random(radius, height - radius);
    vx = random(0.0, 6.0);
    // Aufgabenteil 3 Füge bewegung in der y Axis hinzu.
    vy = random(0.0, 6.0);
    col = randomColor();
  }

  // Zeige den Ball an.
  void display() {
    fill(col);
    circle(x, y, radius*2);
  }

  // Bewege den Ball
  void move() {
    // Prüfe ob der Linke oder Rechte rand erreicht wird.
    if (x + vx >= width - radius || x + vx <= 0 + radius)
    {
      // Drehe die Richung um
      vx *= -1;
    }

    // Aufgabenteil 3 Füge bewegung in der y Axis hinzu.
    // Prüfe ob der Obere oder Untere rand erreicht wird.
    if (y + vy >= height - radius || y + vy <= 0 + radius)
      // Drehe die Richung um
      vy *= -1;

    // addiere die Beschleunigungen vx und vy auf die Ball Position x und y
    x += vx;
    y += vy;
  }
}
