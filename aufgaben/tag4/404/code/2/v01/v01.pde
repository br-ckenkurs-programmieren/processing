Ball b = new Ball();

void setup() {
  size(400, 300);
  // Erstelle die Bälle
}

color randomColor() {
  return color(random(255), random(255), random(255));
}

void draw() {
  background(200);
  b.display();
  b.move();
}

class Ball {
  float x, y;
  float vx, vy;
  float radius;
  color col;

  // Setze zufällige startwerte für den Ball
  Ball() {
    radius = random(5, 40);
    x = random(radius, width - radius);
    y = random(radius, height - radius);
    vx = random(0.0, 6.0);
    col = randomColor();
  }

  // Zeige den Ball an.
  void display() {
    fill(col);
    circle(x, y, radius*2);
  }

  // Bewege den Ball
  void move() {
    // Prüfe ob der Linke oder Rechte rand erreicht wird.
    if (x + vx >= width - radius || x + vx <= 0 + radius)
      vx *= -1; // Drehe die Richung um

    // addiere die Beschleunigungen vx und vy auf die Ball Position x
    x += vx;
  }
}
