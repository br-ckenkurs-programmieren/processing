class Ball {
  float x;
  float y;
  float vx;

  Ball(float x, float y, float vx) {
    this.x = x;
    this.y = y;
    this.vx = vx;
  }

  void display() {
    circle(x, y, 50);
  }

  void move() {
    x += vx;  
    if (x > width || x < 0)
      vx *= -1;
  }
}

Ball ball;

void setup() {
  size(1000, 1000);
  ball = new Ball(150, 500, 5);
}

void draw() {
  background(0);
  ball.move();
  ball.display();
}
