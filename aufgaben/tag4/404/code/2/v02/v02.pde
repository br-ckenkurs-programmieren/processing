Ball ball1 = new Ball(300, 150, 5);

void setup() {
  size(600, 300);
  background(255);
}

void draw() {
  background(255);
  ball1.display();
  ball1.move();
}

class Ball {
  int x;
  int y;
  int vx;

  Ball(int xb, int yb, int vxb) {    // Construktor übergibt die Variablen von einzelnen Objekten z.B. ball1 zur Erstellung an die Klasse 
    x = xb;                        //xb als der 1. übergebene Wert von z.B. ball1 xb=300 wird auf den x-Wert der Klasse geschreiben
    y = yb;
    vx = vxb;
  }

  void display() {
    ellipse(x, y, 50, 50);
  }

  void move() {
    x = x + vx;
    if (x >= width || x <= 0) {
      vx *= (-1);
    }
  }
}
