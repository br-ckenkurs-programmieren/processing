float x;
float vx = 10;

float y;
float vy = 5;

int durchmesser = 30;

void setup() {
  size(400, 300);
  noStroke();
  frameRate(60);
  x = width/2.0;
  y = height/2.0;
}

void draw() {
  background(0);

  if ((x <= 0+durchmesser/2 || x >= width-durchmesser/2)) { 
    vx = vx * (-1);
    fill(random(100, 255), random(100, 255), random(100, 255));
  }  
  if ((y <= 0 + durchmesser/2 || y>= height - durchmesser/2)) {
    vy = vy * (-1);
    fill(random(100, 255), random(100, 255), random(100, 255));
  }
  if (dist(mouseX, mouseY, x, y) > durchmesser/2) {
    x = x + vx;
    y = y + vy;
  }

  ellipse(x, y, durchmesser, durchmesser);
}
