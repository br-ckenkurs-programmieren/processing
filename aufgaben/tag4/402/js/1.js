let gridSize = 20;
let preyCount = 30;
let hunterCount = 5;

class Animal {

  constructor( pos, type) {
    this.pos = pos;
    this.type = type;
    this.dead = false;
  }

 show() {
  if (this.dead)
  return;

    let dPos = posToGrid(this.pos);
    if (this.type == 2)
      fill(255, 0, 0, 150);
    else
      fill(0, 0, 255, 150);

    ellipse(dPos.x, dPos.y, width / gridSize, height / gridSize);
  }

  move() {
    let dir = int(floor(random(4)));

    switch(dir) {
    case 0: 
      this.pos.x += 1;
      break;
    case 1: 
      this.pos.y += 1;
      break;
    case 2: 
      this.pos.x -= 1;
      break;
    case 3: 
      this.pos.y -= 1;
    }

    if (this.pos.x < 0)
      this.pos.x = 0;
    if (this.pos.y < 0)
      this.pos.y = 0;
    if (this.pos.x >= this.gridSize)
      this.pos.x = this.gridSize - 1;
    if (this.pos.y >= this.gridSize)
      this.pos.y = this.gridSize - 1;
  }

   attack(preyArray) {
    for (let i = 0; i < preyArray.length ; i++) {
      if (preyArray[i].pos.equals(this.pos))
        preyArray[i].eat();
    }
  }

   eat() {
    this.dead = true;
  }
}

let prey;
let hunters;

function setup() {
  createCanvas(700, 700);
  frameRate(5);

   prey = [];
   hunters = [];

  for (let i = 0; i < preyCount; i++)
    prey.push(new Animal( createVector(round(random(gridSize)), round(random(gridSize))), 1));

  for (let i = 0; i < hunterCount; i++)
    hunters.push(new Animal( createVector(round(random(gridSize)), round(random(gridSize))), 2));
}

function draw() {
  background(255);

  for (let i = 0; i < prey.length; i++ ) {
    prey[i].move();
    prey[i].show();
  }

  for (let i = 0; i < hunters.length; i++) {
    hunters[i].move();
    hunters[i].attack(prey);
    hunters[i].show();
  }
}

function posToGrid(pos) {
  return createVector((width / gridSize) / 2 + pos.x * (width / gridSize), (height / gridSize) / 2 + pos.y * (height / gridSize));
}