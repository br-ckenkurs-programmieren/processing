int gridSize = 20;
int preyCount = 30;
int hunterCount = 5;

Animal[] prey;
Animal[] hunters;

void setup() {
  size(700, 700);
  frameRate(5);

  prey = new Animal[preyCount];
  hunters = new Animal[hunterCount];

  for (int i = 0; i < prey.length; i++)
    prey[i] = new Animal(new PVector(round(random(gridSize)), round(random(gridSize))), 1);

  for (int i = 0; i < hunters.length; i++)
    hunters[i] = new Animal(new PVector(round(random(gridSize)), round(random(gridSize))), 2);
}

void draw() {
  background(255);

  for (Animal p : prey) {
    p.move();
    p.show();
  }

  for (Animal h : hunters) {
    h.move();
    h.attack(prey);
    h.show();
  }
}

PVector posToGrid(PVector pos) {
  return new PVector((width / gridSize) / 2 + pos.x * (width / gridSize), (height / gridSize) / 2 + pos.y * (height / gridSize));
}

class Animal {
  PVector pos;
  int type;
  boolean dead;

  Animal(PVector pos, int type) {
    this.pos = pos;
    this.type = type;
    this.dead = false;
  }

  void show() {
    if (dead)
      return;

    PVector dPos = posToGrid(pos);
    if (type == 2)
      fill(255, 0, 0, 150);
    else
      fill(0, 0, 255, 150);

    ellipse(dPos.x, dPos.y, width / gridSize, height / gridSize);
  }

  void move() {
    int dir = floor(random(4));

    switch(dir) {
    case 0: 
      pos.x += 1;
      break;
    case 1: 
      pos.y += 1;
      break;
    case 2: 
      pos.x -= 1;
      break;
    case 3: 
      pos.y -= 1;
    }

    if (pos.x < 0)
      pos.x = 0;
    if (pos.y < 0)
      pos.y = 0;
    if (pos.x >= gridSize)
      pos.x = gridSize - 1;
    if (pos.y >= gridSize)
      pos.y = gridSize - 1;
  }

  void attack(Animal[] preyArray) {
    for (Animal p : preyArray) {
      if (p.pos.equals(pos))
        p.eat();
    }
  }

  void eat() {
    dead = true;
  }
}
