int animalSpeedMax = 10; //<>//
int animalSizeMin = 5;
int animalSizeMax = 10;


/* Simulationsobjekt anlegen */
Simulation simulation;

void setup() {
  size(1000, 1000);
  frameRate(5);

  simulation = new Simulation(100);
}

void draw() {
  background(255);
  simulation.moveAllAnimals();
  simulation.showAllAnimals();
  simulation.evaluate();
}

class Animal {
  float x, y;
  int type, speed;
  int size;

  /* 1. Konstruktor (randomisiert) */
  Animal() {
    this.x = int(random(width)); 
    this.y = int(random(height));
    this.type = int(random(0, 1+1));
    this.speed = int(random(-animalSpeedMax-1, animalSpeedMax+1));
    this.size = round(animalSizeMin + random(animalSizeMax - animalSizeMin));
  }

  /* 2. Konstruktor */
  Animal(float x, float y, int type, int speed, int size) {
    this.x = x; 
    this.y = y;
    this.type = type;
    this.speed = speed;
    this.size = size;
  }

  void moveX(int direction) {
    this.x += direction * this.speed;
  }

  void moveY(int direction) {
    this.y += direction * this.speed;
  }

  void show() {
    color c;
    if (this.type == 0) {
      c = color(0, 255, 0);
    } else {
      c = color(255, 0, 0);
    }
    fill(c);
    circle(this.x, this.y, this.size);
  }
}

class Simulation {
  Animal animals[];

  /* Konstruktor */
  Simulation(int count_of_animals) {
    animals = new Animal[count_of_animals];

    for (int i = 0; i < animals.length; i++)
      animals[i] = new Animal();
  }

  void moveAllAnimals() {

    for (int i = 0; i < animals.length; i++) {
      int directionX = int(random(0, 3))-1;
      int directionY = int(random(0, 3))-1;

      animals[i].moveX(directionX);
      animals[i].moveY(directionY);
    }
    
  }

  void showAllAnimals() {
    for (int i = 0; i < animals.length; i++)
      animals[i].show();
  }

  void evaluate() {
    for (int i = 0; i < animals.length; i++) {
      for (int j = i; j < animals.length; j++) {
        if (dist(animals[i].x, animals[i].y, animals[j].x, animals[j].y) < (animals[i].size/2 + animals[j].size/2)) {
          /* TODO: Bewertung des Aufeinandertreffens */
          if (animals[i].type == animals[j].type) {
          } else {
            if (animals[i].type > animals[j].type) {
              print("WAH! ");
            } else {
            }
          }
        }
      }
    }
  }
}
