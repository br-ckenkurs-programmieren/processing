float angle = 0.005; //<>//

void setup() {
  size(800, 600);
}

void draw() {
  background(51);
  stroke(255);
  translate(width / 2, height);
  branch(200);
}

void branch(float len) {
  line(0, 0, 0, -len);
  translate(0, -len);

  if (len > 2) {
    pushMatrix();

    rotate(angle * frameCount);
    branch(0.67 * len);

    popMatrix();
    pushMatrix();

    rotate(-angle * frameCount);
    branch(0.67 * len);

    popMatrix();
  }
}
