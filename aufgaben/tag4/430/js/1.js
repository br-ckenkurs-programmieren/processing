
var angle;

function setup() {
    initializeFields();
    createCanvas(800, 600);
}

function draw() {
    background(51);
    stroke(255);
    translate(width / 2, height);
    branch(200);
}

function branch(len) {
    line(0, 0, 0, -len);
    translate(0, -len);
    if (len > 6) {
        push();
        rotate(angle * frameCount);
        branch(0.67 * len);
        pop();
        push();
        rotate(-angle * frameCount);
        branch(0.67 * len);
        pop();
    }
}

function initializeFields() {
    angle = 0.005;
}

