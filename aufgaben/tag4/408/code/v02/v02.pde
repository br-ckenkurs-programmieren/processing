float gravitation = -0.09; //<>//
float airX, airY;
Cannon cannon;
boolean targetExist = false;
float x, y, radius = 20;

void setup() {
  size(750, 450, P3D);
  cannon = new Cannon();
}

void draw() {
  background(200);
  if (!targetExist) {
    // Create new Target
    x = random(radius, width - radius);
    y = random(radius, height - radius);
    // check if Circle will spawn in air display and if so reroll position
    while (x >= width - 80 - radius && y <= 40 + radius) {
      x = random(radius, width - radius);
      y = random(radius, height - radius);
    }
    targetExist = true;

    // Set new Random Air Speed
    airX = random(2);
    airX-= 1.0;
  } else {

    // Draw existing target
    fill(255, 0, 0);
    circle(x, y, radius*2);
    fill(255);
    circle(x, y, radius*2 * 0.7);
    fill(0);
    circle(x, y, radius*2 * 0.35);

    // Check if target got hit
    if (cannon.isShot && dist(x, y, cannon.x, cannon.y) <= max(radius*2, cannon.cannonBallRadius)) {      
      targetExist = false;
      cannon.isShot = false;
    }
  }

  // draw Air Speed display
  fill(255);
  quad(
    width - 80, 0, 
    width, 0, 
    width, 40, 
    width -80, 40
    );
  fill(255 * abs(airX), 255 * (1 - abs(airX)), 0);
  triangle(
    width - 40, 
    20, 
    width - 40 - (airX * 40), 
    20 + (10 * abs(airX)), 
    width - 40 - (airX * 40), 
    20 - (10 * abs(airX))
    );

  // fire cannon if mouse is Pressed
  if (mousePressed) 
    cannon.fire();


  // draw and update cannon and cannonball
  cannon.draw();
  cannon.update();
}

class Cannon {
  // cannonball values
  float x, y, vx, vy, ax, ay;
  //
  float powerAdjustment = 0.04;
  boolean isShot = false;
  int cannonBallRadius = 10;
  float power; // Power will be set by distance from muzzle to mousecursor capped by maxPower
  float maxPower = 300;

  // cannon values
  float cannonLength = 40;
  float cannonCenterX = 0;
  float cannonCenterY = height;

  void fire() {
    // only shoot if no cannon ball is currently flying
    if (!isShot)
    {
      // Find Aimed direction and multiply it with the distance between the muzzle and the mouse capped by maxPower
      PVector direction = findCannonDirectionNormalized();
      direction.mult(shotPower());
      direction.mult(powerAdjustment); // finally adjust power

      // apply to acceleration
      ax = direction.x;
      ay = direction.y;
      // reset the velocity
      vx = 0;
      vy = 0;

      // set position to the cannon muzzle
      PVector muzzle = findCannonMuzzle();
      x = muzzle.x;
      y = muzzle.y;

      // mark the cannon as Shot
      isShot = true;
    }
  }

  // returns the Direction the cannon is aimed at (not Normalized)
  private PVector findCannonDirection() {
    return new PVector(mouseX - cannonCenterX, height - mouseY - (height - cannonCenterY));
  }

  // returns the Direction the cannon is aimed at as a Normalized vector
  private PVector findCannonDirectionNormalized() {
    PVector dir = findCannonDirection();
    float normfactor = sqrt(pow(dir.x, 2) + pow(dir.y, 2));
    dir.mult(1/normfactor);
    return dir;
  }

  // returns the position of the cannons muzzle
  private PVector findCannonMuzzle() {
    PVector dir = findCannonDirectionNormalized();

    dir.mult(cannonLength);
    dir.y *= -1;

    dir.x += cannonCenterX;
    dir.y += cannonCenterY;

    return dir;
  }

  // returns the capped Power of the shot
  private float shotPower() {
    PVector muzzle = findCannonMuzzle();
    return min(dist(muzzle.x, muzzle.y, mouseX, mouseY), maxPower);
  }

  // draw all parts of the cannon, the cannonball and the power display
  void draw() {
    fill(0);
    strokeWeight(2);

    float power = shotPower();
    PVector muzzle = findCannonMuzzle();
    PVector dir = findCannonDirectionNormalized();

    float orthoA = -dir.x;
    float orthoB = -dir.y;

    float distX = muzzle.x + (dir.x * power);
    float distY = muzzle.y + (-dir.y * power);

    // draw the Power display
    stroke(0, 0, 0, 0);
    fill((power / maxPower) * 255, (1 - (power / maxPower)) * 255, 0, 150);
    triangle(
      muzzle.x, muzzle.y, // Point at the muzzle
      distX + (orthoB * 20 * (power / maxPower)), distY + (orthoA * 20 * (power / maxPower)), // Second Point
      distX - (orthoB * 20 * (power / maxPower)), distY - (orthoA * 20 * (power / maxPower)) // Third Point
      );

    // draw Cannon
    stroke(0);
    fill(0);
    line(cannonCenterX, cannonCenterY, muzzle.x, muzzle.y);
    circle(cannonCenterX, cannonCenterY, 20);

    // draw Cannonball if the cannon has shot
    if (isShot) {
      fill(0);
      stroke(0);
      circle(x, y, cannonBallRadius*2);
    }
  }

  // update cannonball
  void update() {
    if (isShot) {

      // Gravitation & air influence added to acceleration
      ax += -(airX * 0.1);
      ay += gravitation;
      // add accaleration to the velocity
      vx += ax;
      vy += -ay;
      // move position by current velocity
      x += vx;
      y += vy;
      // reset acceleration
      ax = 0;
      ay = 0;
      // clear cannonball if it leaves the screen
      if (x >= width + cannonBallRadius || x <= 0 - cannonBallRadius || y >= height + cannonBallRadius || y <= 0 - cannonBallRadius) {
        isShot = false;
        x = 0;
        y = 0;
      }
    }
  }
}
