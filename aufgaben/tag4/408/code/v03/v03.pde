float velocity = 500;        // Geschwindigkeit der Kugel
float gravity = - 220;       // Gravitation (negativ weil das Koordinatensystem umgedreht ist)
float angle;                 // Winkel der Kanone zur Maus
float cannonX = 50;          // Standpunkt der Kanone auf der X-Achse
float cannonY;               // Hoehe der Kanone
float cannon_length = 40;    // Entfernung des Kanonenende zum Drehpunkt
float shooting_X;            // X Koordinate an dem die Kugel die Kanone verlässt
float shooting_Y;            // Y Koordinate an dem die Kugel die Kanone verlässt
float target_size = 50;      // Hoehe der Zielscheibe
int score = 0;               // Aktueller Punktestand
int highscore = 0;           // Höchster Punktestand bisher
float wind;                  // Windstärke (beeinflusst die Geschwindigkeit)
float maxWind = 60;         // Maximale Windstärke
boolean shooting = false;    // Ist true solange sich eine Kugel in der Luft befindet
Cannonball ball;             // Kanonenkugel
Target target;               // Zielscheibe

void setup() {
  size(700, 400);
  cannonY = height - 15;                                    // Kanone steht auf dem unteren Rand
  target = new Target(width -50, height /2, target_size);   // Zielscheibe startet in der Mitte
  wind =  random(-maxWind, maxWind);                        // Initial-Windstärke
}  

void draw() {
  background(255);                             
  drawScore();         // Zeichnet Punkteanzeige
  drawWind();          // Zeichnet Windrichtung
  drawCannon();        // Zeichnet Kannone und Richtet sie gen maus aus   
  target.drawt();      // Zeichnet Zielscheibe
  if (shooting) {
    ball.update();     // Updated die Bewegung der Kugel solange diese fliegt
  }
}

void drawScore() {     // Zeichnet Punkteanzeige            
  fill(0);
  textSize(30);
  text("Score: " + score, width/5, height/4);
  textSize(15);
  text("Highscore: " + highscore, width/5, height/4+20);
}

void drawWind() {   // Zeichnet Windrichtung
  fill(#0000AA);
  textSize(10);
  text("Wind: " + round(wind), width/5, height/4+40);
  stroke(#0000AA);
  line(width/5, height/4+50, width/5 +30, height/4+50);
  int tmp;
  if (wind > 0) {                                                //Bestimmt die Richtung des anzuzeigenden Pfeils
    tmp = 30;
  } else {
    tmp = 0;
  }
  line(width/5+tmp, height/4+50, width/5 +15, height/4+45);
  line(width/5+tmp, height/4+50, width/5 +15, height/4+55);
}

void mouseMoved() {
  if (cannonX < mouseX &&  mouseY < cannonY) {                                 // Erlaub ein Zielen in 90°
    angle = acos((mouseX - cannonX)/dist(cannonX, cannonY, mouseX, mouseY))  ; // Berrechnet den Winkel der Kanone mit  acos(Ankathete/Hypotenuse)
  }
}

void mousePressed() {  
  if (!shooting) {                                  // Erlaubt kein doppeltes Schießen
    frameCount = 0;                                 // Setzt frameCount zurück um später mit Sekunden seit Klick zu rechnen 
    ball = new Cannonball(shooting_X, shooting_Y);  // Erschafft neues Kugel-Objekt am ende der Kanone
    shooting = true;                                // Kugel wird als fliegend betrachtet
  }
}

class Cannonball { // Klasse für Kanonenkugel Objekte                                    
  float startX;    // Start X-Position der Kugel
  float startY;    // start Y-Position der Kugel
  float xPos;      // X Position der Kugel
  float yPos;      // Y Position der Kugel
  float direction; //Richtung der Flugbahn

  Cannonball(float x, float y) {
    this.startX = x;
    this.startY = y;
    this.xPos = x;
    this.yPos = y;
    this.direction = - angle ;      // Setzt Flugrichtung der Kugel fest (negativ weil das Koordinatensystem umgedreht ist)
  }

  void update() {  // Errechnet die neue Position der Kugel
    if (shooting) {
      float seconds = (frameCount/frameRate); // Ergibt die Skunden seit dem Klick
      this.xPos = startX + (velocity+wind) * cos(direction) * seconds;                                 //das Zeit-Orts-Gesetz der gleichförmigen Bewegung in x-Richtung
      this.yPos = startY + ((velocity+wind) * sin(direction)) * seconds - 0.5 * gravity * sq(seconds); //das Zeit-Orts-Gesetz der gleichförmigen Bewegung in y-Richtung     
      fill(70);
      circle(this.xPos, this.yPos, 20);
      if (xPos > width && shooting) {          // Fängt den Fall des Verfehlens ab
        shooting = false;                      // Die Kugel wird nicht länger als "im Flug" betrachtet
        if (score > highscore) {
          highscore = score;
        }
        score = 0;
      }
    }
  }
}

void drawCannon() {                                 // Zeichnet Kannone und Richtet sie gen Maus aus 
  shooting_X = cannonX + cos(angle)*cannon_length;  // Berechnet durch  cos(α)/Hypotenuse das Ende der Kanone
  shooting_Y = cannonY - sin(angle)*cannon_length;  // Berechnet durch  cos(α)/Hypotenuse das Ende der Kanone
  fill(70);
  stroke(#9ea4a6);
  strokeWeight(7);
  line(cannonX -40, height, cannonX, cannonY -10);  // Optische Standhilfe der Kanone
  strokeWeight(20);
  line(cannonX, cannonY, shooting_X, shooting_Y );  // Rohr der Kanone in richtung Maus zeigend
  strokeWeight(2);
  circle(shooting_X, shooting_Y, 15 );              // Optische Öffnung des Rohrs
  stroke(#756244);
  circle(cannonX, cannonY, 30);                     // Optisches Rad der Kanone
  line(cannonX -15, cannonY, cannonX +15, cannonY); // Optische Speiche des Rads
  line(cannonX, cannonY -15, cannonX, height);      // Optische Speiche des Rads
  noStroke();
}

class Target { 
  float xPos;                                // X Position des Ziels
  float yPos;                                // Y Position des Ziels
  float size;                                // Größe des Ziels

  Target(float x, float y, float z) {   
    this.xPos = x;
    this.yPos = y;
    this.size = z ;
    this.drawt();
  }

  void drawt() {                      // Zeichnet das Ziel und prueft ob es getroffen wurde
    fill(#0000ff);
    ellipse(xPos, yPos, 20, size);
    fill(#ffffff);
    ellipse(xPos, yPos, 10, size/1.5);
    fill(#ff0000);
    ellipse(xPos, yPos, 5, size/3);    
    if (shooting && dist(xPos, yPos, ball.xPos, ball.yPos) < size) {  // Pruefung auf Treffer
      this.relocate();                                                // Verschiebt das Ziel
      shooting = false;                              
      score++;                                                        // Punktestand wird bei Treffern erhöht
    }
  }  
  void relocate() {                                                   // Verschiebt das Ziel                    
    this.yPos = random (50, height-50);                               // Wählt für die neue Y Koordinate zufällig einen Wert auf der gleichen X-Achse
    wind = random (-maxWind, maxWind);                                // Weist der Windstärke einen neuen zufälligen Wert zu
  }
}
