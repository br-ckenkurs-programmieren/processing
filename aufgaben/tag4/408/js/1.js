
var gravitation;

var airX, airY;

var cannon;

var targetExist;

var x, y, radius;

function setup() {
    initializeFields();
    createCanvas(750, 450);
    cannon = new Cannon();
}

function draw() {
    background(200);
    if (!targetExist) {
        // Create new Target
        x = random(radius, width - radius);
        y = random(radius, height - radius);
        // check if Circle will spawn in air display and if so reroll position
        while (x >= width - 80 - radius && y <= 40 + radius) {
            x = random(radius, width - radius);
            y = random(radius, height - radius);
        }
        targetExist = true;
        // Set new Random Air Speed
        airX = random(2);
        airX -= 1.0;
    } else {
        // Draw existing target
        fill(255, 0, 0);
        circle(x, y, radius * 2);
        fill(255);
        circle(x, y, radius * 2 * 0.7);
        fill(0);
        circle(x, y, radius * 2 * 0.35);
        // Check if target got hit
        if (cannon.isShot && dist(x, y, cannon.x, cannon.y) <= max(radius * 2, cannon.cannonBallRadius)) {
            targetExist = false;
            cannon.isShot = false;
        }
    }
    // draw Air Speed display
    fill(255);
    quad(width - 80, 0, width, 0, width, 40, width - 80, 40);
    fill(255 * abs(airX), 255 * (1 - abs(airX)), 0);
    triangle(width - 40, 20, width - 40 - (airX * 40), 20 + (10 * abs(airX)), width - 40 - (airX * 40), 20 - (10 * abs(airX)));
    // fire cannon if mouse is Pressed
    if (mouseIsPressed)
        cannon.fire();
    // draw and update cannon and cannonball
    cannon.draw();
    cannon.update();
}

class Cannon {

    constructor() {
        this.powerAdjustment = 0.04;
        this.isShot = false;
        this.cannonBallRadius = 10;
        this.maxPower = 300;
        this.cannonLength = 40;
        this.cannonCenterX = 0;
        this.cannonCenterY = height;
    }
    fire() {
        // only shoot if no cannon ball is currently flying
        if (!this.isShot) {
            let direction = this.findCannonDirectionNormalized();
            direction.mult(this.shotPower());
            direction.mult(powerAdjustment);
            this.ax = direction.x;
            this.ay = direction.y;
            this.vx = 0;
            this.vy = 0;
            let muzzle = this.findCannonMuzzle();
            this.x = muzzle.x;
            this.y = muzzle.y;
            this.isShot = true;
        }
    }

    // returns the Direction the cannon is aimed at (not Normalized)
    findCannonDirection() {
        return new p5.Vector(mouseX - this.cannonCenterX, height - mouseY - (height - this.cannonCenterY));
    }

    // returns the Direction the cannon is aimed at as a Normalized vector
    findCannonDirectionNormalized() {
        let dir = this.findCannonDirection();
        let normfactor = sqrt(pow(dir.x, 2) + pow(dir.y, 2));
        dir.mult(1 / normfactor);
        return dir;
    }

    // returns the position of the cannons muzzle
    findCannonMuzzle() {
        let dir = this.findCannonDirectionNormalized();
        dir.mult(this.cannonLength);
        dir.y *= -1;
        dir.x += this.cannonCenterX;
        dir.y += this.cannonCenterY;
        return dir;
    }

    // returns the capped Power of the shot
    shotPower() {
        let muzzle = this.findCannonMuzzle();
        return min(dist(muzzle.x, muzzle.y, mouseX, mouseY), this.maxPower);
    }

    // draw all parts of the cannon, the cannonball and the power display
    draw() {
        fill(0);
        strokeWeight(2);
        var power = this.shotPower();
        var muzzle = this.findCannonMuzzle();
        var dir = this.findCannonDirectionNormalized();
        var orthoA = -dir.x;
        var orthoB = -dir.y;
        var distX = muzzle.x + (dir.x * power);
        var distY = muzzle.y + (-dir.y * power);
        // draw the Power display
        stroke(0, 0, 0, 0);
        fill((power / this.maxPower) * 255, (1 - (power / this.maxPower)) * 255, 0, 150);
        triangle(
            muzzle.x,
            muzzle.y,
            distX + (orthoB * 20 * (power / this.maxPower)),
            distY + (orthoA * 20 * (power / this.maxPower)),
            distX - (orthoB * 20 * (power / this.maxPower)),
            distY - (orthoA * 20 * (power / this.maxPower)));
        stroke(0);
        fill(0);
        line(this.cannonCenterX, this.cannonCenterY, muzzle.x, muzzle.y);
        circle(this.cannonCenterX, this.cannonCenterY, 20);
        // draw Cannonball if the cannon has shot
        if (this.isShot) {
            fill(0);
            stroke(0);
            circle(this.x, this.y, this.cannonBallRadius * 2);
        }
    }

    update() {
        if (this.isShot) {
            // Gravitation & air influence added to acceleration
            this.ax += -(airX * 0.1);
            this.ay += gravitation;
            // add accaleration to the velocity
            this.vx += this.ax;
            this.vy += -this.ay;
            // move position by current velocity
            this.x += this.vx;
            this.y += this.vy;
            // reset acceleration
            this.ax = 0;
            this.ay = 0;
            // clear cannonball if it leaves the screen
            if (this.x >= width + this.cannonBallRadius || this.x <= 0 - this.cannonBallRadius || this.y >= height + this.cannonBallRadius || this.y <= 0 - this.cannonBallRadius) {
                this.isShot = false;
                this.x = 0;
                this.y = 0;
            }
        }
    }
}

function initializeFields() {
    gravitation = -0.09;
    airX = 0;
    airY = 0;
    cannon = null;
    targetExist = false;
    x = 0;
    y = 0;
    radius = 20;
    x = 0;
    y = 0;
    vx = 0;
    vy = 0;
    ax = 0;
    ay = 0;
    powerAdjustment = 0.04;
    isShot = false;
    cannonBallRadius = 10;
    power = 0;
    maxPower = 300;
    cannonLength = 40;
    cannonCenterX = 0;
    cannonCenterY = height;
}

