class Button {
  int x;
  int y;
  int buttonW;
  int buttonH;
  char label;
  color c = #212121;
  color cHover = #2e2e2e;
  color cClicked = #171717;
  
  Button(int x, int y, int buttonW, int buttonH, char label) {
    this.x = x;
    this.y = y;
    this.buttonW = buttonW;
    this.buttonH = buttonH;
    this.label = label;
  }
  Button(int x, int y, int size, char label) {
    this(x, y, size, size, label);
  }
  
  void drawButton(color c) {
    textAlign(CENTER);
    noStroke();
    fill(c);
    rect(x, y, buttonW, buttonH, 25);
    fill(255);
    textSize(45);
    text(label, x + buttonW / 2, y + 2 * buttonH / 3);
  }
  
  void drawButton() {
    this.drawButton(c);
  }
  
  void drawHover() {
    drawButton(cHover);
  }
  
  void drawClicked() {
    drawButton(cClicked);
  }
  
  boolean mouseOver() {
    if (mouseX >= this.x && mouseX <= this.x + this.buttonW && mouseY >= this.y && mouseY <= this.y + this.buttonH) return true;
    else return false;
  }
}
