// Ideen:
// Lambda Funktionen (scheinen nicht zu gehen)
// Enum + Abstrakte Funktionen (zu komplex)
// Einfach ausschreiben...
Button[] buttons = new Button[16];
int l1 = 15, l2 = 111, l3 = 207, l4 = 303, l5 = 399;
int r1 = 11, r2 = 107, r3 = 203, r4 = 299;
int x = 0, y = 0;
int bSize = 90;
char op = '%';


void setup() {
  size(400, 500);
  buttons[0] = new Button(r1,l5,bSize,'0');
  buttons[1] = new Button(r1,l4,bSize,'1');
  buttons[2] = new Button(r2,l4,bSize,'2');
  buttons[3] = new Button(r3,l4,bSize,'3');
  buttons[4] = new Button(r1,l3,bSize,'4');
  buttons[5] = new Button(r2,l3,bSize,'5');
  buttons[6] = new Button(r3,l3,bSize,'6');
  buttons[7] = new Button(r1,l2,bSize,'7');
  buttons[8] = new Button(r2,l2,bSize,'8');
  buttons[9] = new Button(r3,l2,bSize,'9');
  buttons[10] = new Button(r4,l2,bSize,'C');
  buttons[11] = new Button(r4,l3,bSize,'/');
  buttons[12] = new Button(r4,l4,bSize,'•');
  buttons[13] = new Button(r4,l5,bSize,'=');
  buttons[14] = new Button(r3,l5,bSize,'-');
  buttons[15] = new Button(r2,l5,bSize,'+');
  background(0);
}

void draw() {
  drawButtons();
  drawDisplay();
}

void drawDisplay() {
  fill(#212121);
  rect(r1, l1, width - 24, bSize, 25);
  fill(255);
  textAlign(RIGHT);
  text(x, r4 + 65, l1 + 2 * bSize / 3);
}

void drawButtons() {
  for (int i = 0; i < buttons.length; i++) {
    if (buttons[i].mouseOver() && mousePressed) buttons[i].drawClicked();
    else if (buttons[i].mouseOver() && !mousePressed) buttons[i].drawHover();
    else buttons[i].drawButton();
  }
}

void mousePressed() {
  if (mouseButton == LEFT) {
    for (int i = 0; i < buttons.length; i++) {
      if (buttons[i].mouseOver()) input(buttons[i].label);
    }
  }
}

void input(char c) {
  if (Character.getNumericValue(c) == -1 || Character.getNumericValue(c) == 12) {
    switch(c) {
      case 'C': 
        x = 0;
        y = 0;
        break;
      case '=': 
        x = operate();
        break;
      default:
        op = c;
        y = x;
        x = 0;
        break;
    }
  } else input(Character.getNumericValue(c));
}

void input(int i) {
  if (x == 0) x = i;
  else if (x < 10000) x = i + 10 * x;
}

int operate() {
  if (op == '+') return x + y;
  if (op == '-' && y != 0) return y - x;
  if (op == '•') return x * y;
  if (op == '/' && x != 0) return y / x;
  return 0;
}
