var buttons;
var x;
var y;
var op;
const l1 = 15;
const l2 = 111;
const l3 = 207;
const l4 = 303;
const l5 = 399;
const r1 = 11;
const r2 = 107;
const r3 = 203;
const r4 = 299;

function setup() {
    // background(0);
    initializeFields();
    createCanvas(400, 500);
    buttons[0] = new Button(r1, l5, 90, 90, "0");
    buttons[1] = new Button(r1, l4, 90, 90, "1");
    buttons[2] = new Button(r2, l4, 90, 90, "2");
    buttons[3] = new Button(r3, l4, 90, 90, "3");
    buttons[4] = new Button(r1, l3, 90, 90, "4");
    buttons[5] = new Button(r2, l3, 90, 90, "5");
    buttons[6] = new Button(r3, l3, 90, 90, "6");
    buttons[7] = new Button(r1, l2, 90, 90, "7");
    buttons[8] = new Button(r2, l2, 90, 90, "8");
    buttons[9] = new Button(r3, l2, 90, 90, "9");
    buttons[10] = new Button(r4, l2, 90, 90, "C");
    buttons[11] = new Button(r4, l3, 90, 90, "/");
    buttons[12] = new Button(r4, l4, 90, 90, "•");
    buttons[13] = new Button(r4, l5, 90, 90, "=");
    buttons[14] = new Button(r3, l5, 90, 90, "-");
    buttons[15] = new Button(r2, l5, 90, 90, "+");
}

function draw() {
    drawButtons();
    drawWindow();
    drawTxt();
}

function drawTxt() {
    fill(255);
    textAlign(RIGHT);
    text(x, r4 + 65, l1 + 2 * 90 / 3);
}

function drawWindow() {
    fill(color(0x21, 0x21, 0x21));
    rect(r1, l1, width - 24, 90, 25);
}

function drawButtons() {
    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i].mouseOver() && mouseIsPressed)
            buttons[i].drawClicked();
        else if (buttons[i].mouseOver() && !mouseIsPressed)
            buttons[i].drawHover();
        else
            buttons[i].drawButton();
    }
}

function mousePressed() {
    if (mouseButton == LEFT) {
        if (mouseX >= r1 && mouseX <= r1 + 90 && mouseY >= l2 && mouseY <= l2 + 90)
            inputI(7);
        else if (mouseX >= r2 && mouseX <= r2 + 90 && mouseY >= l2 && mouseY <= l2 + 90)
            inputI(8);
        else if (mouseX >= r3 && mouseX <= r3 + 90 && mouseY >= l2 && mouseY <= l2 + 90)
            inputI(9);
        else if (mouseX >= r4 && mouseX <= r4 + 90 && mouseY >= l2 && mouseY <= l2 + 90)
            inputC('c');
        else if (mouseX >= r1 && mouseX <= r1 + 90 && mouseY >= l3 && mouseY <= l3 + 90)
            inputI(4);
        else if (mouseX >= r2 && mouseX <= r2 + 90 && mouseY >= l3 && mouseY <= l3 + 90)
            inputI(5);
        else if (mouseX >= r3 && mouseX <= r3 + 90 && mouseY >= l3 && mouseY <= l3 + 90)
            inputI(6);
        else if (mouseX >= r4 && mouseX <= r4 + 90 && mouseY >= l3 && mouseY <= l3 + 90)
            inputC('/');
        else if (mouseX >= r1 && mouseX <= r1 + 90 && mouseY >= l4 && mouseY <= l4 + 90)
            inputI(1);
        else if (mouseX >= r2 && mouseX <= r2 + 90 && mouseY >= l4 && mouseY <= l4 + 90)
            inputI(2);
        else if (mouseX >= r3 && mouseX <= r3 + 90 && mouseY >= l4 && mouseY <= l4 + 90)
            inputI(3);
        else if (mouseX >= r4 && mouseX <= r4 + 90 && mouseY >= l4 && mouseY <= l4 + 90)
            inputC('*');
        else if (mouseX >= r1 && mouseX <= r1 + 90 && mouseY >= l5 && mouseY <= l5 + 90)
            inputI(0);
        else if (mouseX >= r2 && mouseX <= r2 + 90 && mouseY >= l5 && mouseY <= l5 + 90)
            inputC('+');
        else if (mouseX >= r3 && mouseX <= r3 + 90 && mouseY >= l5 && mouseY <= l5 + 90)
            inputC('-');
        else if (mouseX >= r4 && mouseX <= r4 + 90 && mouseY >= l5 && mouseY <= l5 + 90)
            inputC('=');
    }
}

function inputI(i) {
    if (x == 0)
        x = i;
    else if (x < 10000)
        x = i + 10 * x;
}

function inputC(c) {
    switch(c) {
        case 'c':
        {
            x = 0;
            y = 0;
            break;
        }
        case '=':
        {
            var temp = x;
            x = operate();
            if (op != '%')
                y = temp;
            op = '%';
            break;
        }
        default:
        {
            op = c;
            y = x;
            x = 0;
            break;
        }
    }
}

function operate() {
    if (op == '+')
        return x + y;
    if (op == '-' && y != 0)
        return y - x;
    if (op == '*')
        return x * y;
    if (op == '/' && x != 0)
        return y / x;
    return 0;
}


class Button{
    constructor(x, y, buttonW, buttonH, txt) {
        this.x = x;
        this.y = y;
        this.buttonW = buttonW;
        this.buttonH = buttonH;
        this.txt = txt;
    }


    drawButtonC(c) {
        textAlign(CENTER);
        noStroke();
        fill(c);
        rect(this.x, this.y, this.buttonW, this.buttonH, 25);
        fill(255);
        textSize(45);
        text(this.txt, this.x + this.buttonW / 2, this.y + 2 * this.buttonH / 3);
    }

    drawButton() {
        this.drawButtonC(color('#212121'));
    }

    drawHover() {
        this.drawButtonC(color('#2e2e2e'));
    }

    drawClicked() {
        this.drawButtonC(color('#171717'));
    }

    mouseOver() {
        if (mouseX >= this.x && mouseX <= this.x + this.buttonW && mouseY >= this.y && mouseY <= this.y + this.buttonH)
            return true;
        else
            return false;
    }
}


function initializeFields() {
    buttons = new Array(16);
    x = 0;
    y = 0;
    op = '%';
}
