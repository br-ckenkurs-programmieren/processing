var car;

var up, left, right;

function setup() {
    initializeFields();
    createCanvas(600, 600);
    rectMode(CENTER);
    car = new Car(new p5.Vector(width / 2, height / 2), 0);
}

function draw() {
    fill(255, 15);
    rect(0, 0, width * 2, height * 2);
    car.move(up, left, right);
    car.show();
}

function keyPressed() {
    switch (keyCode) {
        case UP_ARROW:
            up = true;
            break;
        case LEFT_ARROW:
            left = true;
            break;
        case RIGHT_ARROW:
            right = true;
            break;
    }
}

function keyReleased() {
    switch (keyCode) {
        case UP_ARROW:
            up = false;
            break;
        case LEFT_ARROW:
            left = false;
            break;
        case RIGHT_ARROW:
            right = false;
            break;
    }
}

class Car {
    constructor(pos, facing) {
        this.pos = pos;
        this.facing = facing;
    }

    move(forward, left, right) {
        if (forward) this.pos.add(p5.Vector.fromAngle(this.facing, 2));
        if (left) this.facing -= 0.02;
        if (right) this.facing += 0.02;
    }

    show() {
        push();
        translate(this.pos.x, this.pos.y);
        rotate(this.facing);
        fill(255, 50, 50);
        rect(0, 0, 40, 15);
        pop();
    }
}

function initializeFields() {
    car = null;
    up = false;
    left = false;
    right = false;
}
