Car car;
boolean up, left, right;

void setup() {
  size(600, 600);
  rectMode(CENTER);

  car = new Car(new PVector(width / 2, height / 2), 0);
}

void draw() {
  fill(255, 15);
  rect(0, 0, width * 2, height * 2);
  
  car.move(up, left, right);
  car.show();
}

void keyPressed() {
  if (key != CODED)
    return;

  switch(keyCode) {
  case UP:
    up = true;
    break;
  case LEFT:
    left = true;
    break;
  case RIGHT:
    right = true;
    break;
  }
}

void keyReleased() {
  if (key != CODED)
    return;

  switch(keyCode) {
  case UP:
    up = false;
    break;
  case LEFT:
    left = false;
    break;
  case RIGHT:
    right = false;
    break;
  }
}

class Car {

  PVector pos;
  float facing;

  Car(PVector pos, float facing) {
    this.pos = pos;
    this.facing = facing;
  }

  void move(boolean forward, boolean left, boolean right) {
    if (forward)
      pos.add(PVector.fromAngle(facing).setMag(2));

    if (left)
      facing -= 0.02;

    if (right)
      facing += 0.02;
  }

  void show() {
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(facing);

    fill(255, 50, 50);  
    rect(0, 0, 40, 15);

    popMatrix();
  }
}
