var x;

var y;

function setup() {
    initializeFields();
    createCanvas(400, 400);
}

function draw() {
    background(255);
    circle(x, y, 20);
}

function keyPressed() {
    switch(keyCode) {
        case UP_ARROW:
            y -= 20;
            break;
        case RIGHT_ARROW:
            x += 20;
            break;
        case DOWN_ARROW:
            y += 20;
            break;
        case LEFT_ARROW:
            x -= 20;
            break;
    } 
}

function initializeFields() {
    x = 200;
    y = 200;
}
