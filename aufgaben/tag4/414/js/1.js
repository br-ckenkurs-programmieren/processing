
var moles;

var score;

function setup() {
    initializeFields();
    createCanvas(300, 200);
    score = 0;
    textAlign(CENTER, CENTER);
    textSize(25);
    moles = new Array(6);
    moles[0] = new Mole(new p5.Vector(50, 50));
    moles[1] = new Mole(new p5.Vector(150, 50));
    moles[2] = new Mole(new p5.Vector(250, 50));
    moles[3] = new Mole(new p5.Vector(50, 150));
    moles[4] = new Mole(new p5.Vector(150, 150));
    moles[5] = new Mole(new p5.Vector(250, 150));
}

function draw() {
    background(255);
    moles.forEach(handleMole);
    fill(0);
    text(score, width / 2, height / 2);
}

function handleMole(m) {
    m.update();
    m.show();
}

function mouseReleased() {
    moles.forEach(checkHit);
}

function checkHit(m) {
    let mouse = new p5.Vector(mouseX, mouseY);
    if (m.pos.dist(mouse) < 50) {
        if (m.hit())
            score++;
    }
}

class Mole {

    constructor(pos) {
        this.active = false;
        this.counter = round(random(50));
        this.pos = pos;
    }

    update() {
        this.counter--;
        if (this.counter < 0) {
            this.active = !this.active;
            if (this.active)
                this.counter = round(random(10, 50));
            else
                this.counter = round(random(30, 400));
        }
    }

    hit() {
        if (!this.active)
            return false;
        this.counter = 0;
        return true;
    }

    show() {
        if (this.active)
            fill(255, 0, 0);
        else
            fill(200);
        circle(this.pos.x, this.pos.y, 50);
    }

}


function initializeFields() {
    moles = null;
    score = 0;
}

