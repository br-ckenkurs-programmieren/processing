Mole[] moles;
int score;

void setup() {
  size(300, 200);

  score = 0;

  textAlign(CENTER, CENTER);
  textSize(25);

  moles = new Mole[6];
  moles[0] = new Mole(50, 50);
  moles[1] = new Mole(150, 50);
  moles[2] = new Mole(250, 50);
  moles[3] = new Mole(50, 150);
  moles[4] = new Mole(150, 150);
  moles[5] = new Mole(250, 150);
}

void draw() {
  background(255);

  for (Mole m : moles) {
    m.update();
    m.show();
  }

  fill(0);
  text(score, width / 2, height / 2);
}

void mouseReleased() {
  for (Mole m : moles) {
    if (dist(mouseX, mouseY, m.x, m.y) < 50) {
      if (m.hit())
        score++;
    }
  }
}

class Mole {

  int x, y;
  boolean active;
  int counter;

  Mole(int x, int y) {
    active = false;
    counter = round(random(50));
    this.x = x;
    this.y = y;
  }

  void update() {
    counter--;
    if (counter < 0) {
      active = !active;
      if (active)
        counter = round(random(10, 50));
      else
        counter = round(random(30, 400));
    }
  }

  boolean hit() {
    if (!active)
      return false;

    counter = 0;
    return true;
  }

  void show() {
    if (active)
      fill(255, 0, 0);
    else
      fill(200);
    circle(x, y, 50);
  }
}
