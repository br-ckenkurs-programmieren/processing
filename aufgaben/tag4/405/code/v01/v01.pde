// Diese Lösung wurde durch nachzeichnen eines Bildes mit vertices erreicht //<>//
int i = 0;
float direct = 1.9; // Pumpgeschwindigkeit und richtung

void setup() {
  size(550, 550);
}

void draw() {
  background(255);
  stroke(0, 0, 0, 255);
  fill(255, 0, 0);

  beginShape();
  vertex(102, 18);
  vertex(88.5, 64.8);
  vertex(63.2, 117.5);
  vertex(52, 198.6);
  vertex(66.5, 264.8);

  // Bewege unteren bereich um einen faktor unseres zählers
  vertex(72.8 + (i * 0.1), 336.8 - (i * 0.1));
  vertex(97 + (i * 0.4), 390.8 - (i * 0.4));
  vertex(135 + (i * 0.65), 431.5 - (i * 0.65));
  vertex(188 + (i * 0.6), 467 - (i * 0.6));
  vertex(250 + (i * 0.4), 493.5 - (i * 0.4));
  vertex(312 + (i * 0.15), 514 - (i * 0.15));
  vertex(359 + (i * 0.1), 508 - (i * 0.1));
  vertex(396, 485);
  //

  // Bewege rechten bereich um einen faktor unseres zählers
  vertex(420 - (i * 0.4), 446);
  vertex(444 - (i * 0.8), 357);
  vertex(447 - (i * 0.7), 251);
  vertex(439 - (i * 0.6), 204);
  vertex(412 - (i * 0.3), 155);
  //

  vertex(372, 138);
  vertex(356, 123);
  vertex(331, 73);
  vertex(314, 57);
  vertex(280, 56);
  vertex(267, 43);
  vertex(232, 44);
  vertex(219, 53);
  vertex(210, 60);
  vertex(208, 42);
  vertex(149, 30);
  vertex(146, 46);
  vertex(139, 67);
  vertex(144, 25);
  endShape(CLOSE);

  i += direct;
  if (i > 60 || i < 0)
    direct *= -1;  // drehe zählrichtung
}
