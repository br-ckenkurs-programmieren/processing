String[] phases = new String[]{"groß", "klein", "groß", "klein"};
int[] phase_durations = new int[]{25, 25, 25, 100};
int cphase = 0;
int time;

void setup() {
  size(1000, 1000);
}

void draw() {
  background(255);
  if (time > phase_durations[cphase]) {
    cphase++;
    time = 0;
    
    if (cphase >= phases.length)
      cphase = 0;
  }
  if (phases[cphase].equals("groß"))
    grossesHerz();
  else
    kleinesHerz();
  time++;
}

void grossesHerz() {
  line(500, 800, 200, 600);
  line(500, 800, 800, 600);
  line(200, 600, 200, 200);
  line(800, 600, 800, 200);
  line(200, 200, 500, 300);
  line(800, 200, 500, 300);
}

void kleinesHerz() {
  line(500, 700, 300, 600);
  line(500, 700, 700, 600);
  line(300, 600, 300, 300);
  line(700, 600, 700, 300);
  line(300, 300, 500, 350);
  line(700, 300, 500, 350);
}
