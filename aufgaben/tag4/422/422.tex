\section{Logische Schaltkreise}
\expert{8 Z.\\35 Z.\\61 Z.\\103 Z.\\137 Z.}{\mathe \mouse \klassen}

Wir wollen einen logischen Schaltkreis programmieren.
Dank der Objektorientierung können wir uns dabei auf die sog. \enquote{Klassenerweiterung} verlassen.
Dazu werden Sie später in dieser Aufgabe mehr erfahren.

\begin{center}
	\imagebox{\img/Half_Adder.pdf}{.4\linewidth}
\end{center}

\clue{Diese Aufgabe ist sehr umfangreich und wird Stoff behandeln und erklären, welcher nicht in den Vorlesungen vorkommt.
Versuchen Sie sich gern!}

\subsection{Logisches Modul}

Ein logischer Schaltkreis kann aus vielen verschiedenen Modulen aufgebaut sein:

\begin{itemize}
	\item Ein \stress{Eingabesignal}, das dauerhaft entweder 1 oder 0 ausgibt.
	\item Ein \stress{NOT-Gatter}, das den Eingabewert umkehrt.
	\item Ein \stress{OR-Gatter}, das 1 ausgibt, sobald ein Eingang 1 ist.
	\item Ein \stress{AND-Gatter}, das nur 1 ausgibt, wenn beide Eingänge den Wert 1 haben. Sonst ist die Ausgabe 0.
	\item Ein \stress{Schalter}, den der/die Benutzer:in durch Mausklick an und ausschalten kann.
\end{itemize}

Was diese Elemente alle teilen:
Sie müssen mit den anderen Elementen verbunden werden.
Deswegen wollen wir zunächst eine abstrakte Klasse \code{class Module} formulieren, die nicht mehr als Positionsinformationen und die verbundenen Module speichert.

Speichern Sie die Position in einen Vektor \code{PVector pos} und die verbundenen Module in einem Array \code{Module[] outgoing} (Ja, sowas ist möglich!).
Wie der Name erahnen lässt, werden wir nur die Module in einer Instanz speichern, die von dieser ausgeht.

Schreiben Sie einen Konstruktor, welcher eine Position entgegennimmt und das Module-Array mit der Größe 10 initialisiert.

Ihr Ergebnis sollte also eine Klasse sein, welche theoretisch jede Art von Schalter oder Element sein könnte, wenn es etwas mehr Information speichern würde.

\pagebreak

\subsection{Die Klassenerweiterung}

Nun wollen wir unsere abstrakte Klasse verwenden, um alle Elemente zu erstellen. Dafür werden wir für jedes Element eine Klasse schreiben, welche die Klasse Modul \textit{erweitert}.
Zunächst werden wir das Konzept anhand des OR-Gatters erarbeiten.

Die Schreibweise einer solchen Klasse, z.B. im Fall des OR-Gatters ist:\\
\code{class OrGate extends Module \{ ... \}}.

Die Erweiterung funktioniert dabei so, wie man es erwarten würde: eine Instanz der Klasse \code{OrGate} wird implizit auch die Attribute \code{pos} und  \code{outgoing} haben - so sparen wir uns eine Menge an Schreibarbeit und müssen nicht für jede Art von Modul diese Variablen und später Methoden neu schreiben.

Damit das so reibungslos funktioniert, müssen wir allerdings der Klasse \code{Module} auch weiterhin die Information mitgeben, die es im Konstruktor benötigt - in unserem Fall die Position des Gates.
Für die Kommunikation mit einer sog. Oberklasse gibt es das \code{super}-Keyword.

So ungefähr sollte der Konstruktor Ihres OR-Gatters aussehen:

\begin{lstlisting}
OrGate(PVector pos) {
	super(pos);
}
\end{lstlisting}

Den Konstruktor selbst könnten wir jetzt natürlich ganz angepasst für das OR-Gatter festlegen.

Schreiben Sie nun in der Klasse \code{Module} die Methode \code{void input(boolean input)}.
Diese soll nur mit \code{println(s)} den Text \enquote{Modul kann keine Inputs aufnehmen!} ausgeben.

Nun überschreiben wir diese Methode in der Klasse \code{OrGate} - wir schreiben also genau so auch hier noch einmal eine Methode \code{void input(boolean input)}.
In dieser Methode sollen nun zwei boolsche Variablen \code{val1} und \code{val2} belegen.
Verwenden Sie einen Counter, damit der erste Aufruf von \code{input()} das erste und der zweite Aufruf das zweite Feld belegt.
Weitere Aufrufe sollen wieder mit einem \code{println} beantwortet werden.

Implementieren Sie zusätzlich in beiden Klassen eine \code{reset()}-Methode. In der Klasse \code{Modul} wird diese zunächst nichts tun - in \code{OrGate} hingegen soll sie den Counter sowie die beiden Felder zurücksetzen.

\subsection{Visualisierung}

Nun haben wir uns viel in einem sehr abstrakten Raum bewegt, ohne überhaupt zu sehen, was wir da programmieren.
Es ist an der Zeit, etwas Licht ins Dunkle zu bringen:

Schreiben Sie für beide Klassen eine Methode \code{void show()}, welche das Modul an der eigenen Position zeichnet.
Verwenden Sie dabei \code{square(x, y, d)}, um die Module zu zeichnen.
Verwenden Sie Text oder weitere Zeichenfunktionen, um OR-Gatter als solche erkennbar zu machen.

Schreiben Sie nun die altbekannten \code{setup()} und \code{draw()} Funktionen.
Speichern Sie dabei ein OR-Gate mithilfe von \code{Module m1 = new OrGate(new PVector(...))}.

\clue{Mithilfe von \code{rectMode(CENTER)} und \code{textAlign(CENTER)} können Sie Text und Elemente zentriert zeichnen.
	Mit \code{textSize(i)} können Sie die Textgröße festlegen.}

Instanziieren sie einige Gatter und positionieren Sie sie auf der Bildfläche. Stellen Sie sie mit \code{m1.show();} in der \code{draw()}-Funktion dar.

\pagebreak

\subsection{Mehr Tricksereien und Input}

Wir sind nun kurz davor, einen funktionierenden Schaltkreis darstellen zu können.
In unserem Fall ist das ein sog. \textit{Eingabesignal}, welches einen festen Wert in den Schaltkreis gibt.
Legen wir uns also zunächst eine Klasse \code{class Input extends Module} an, welche also im Konstruktor zusätzlich zur Position auch einen Wahrheitswert entgegennimmt und speichert.

Nun brauchen wir eine Methode, die das eigene Signal versendet.
Schreiben Sie zunächst die Methode \code{void send(boolean output)} in der Klasse \code{Module}.
Dort soll über das Array \code{outgoing} iteriert werden, und bei jedem der Module die Methode \code{m.input(output)} aufgerufen werden.
Prüfen Sie vor diesem Aufruf aber, ob das entsprechende Feld des Arrays \code{null} ist, und überspringen Sie den Methodenaufruf in diesem Fall.

Speichern Sie außerdem den Wahrheitswert der in \code{send} eingegeben wird in einer neuen Variable \code{boolean output}, anhand \code{this.output = output}.

Entscheiden Sie anhand dieser Variable, ob die Farbe des Moduls in \code{void show()} rot oder weiß sein soll.
Außerdem soll die Methode \code{send} durch \code{outgoing} iterieren, und bei jedem Modul \code{m.show()} aufrufen.
Achten Sie auch hier auf Felder, die \code{null} sind.
Lassen Sie auch eine Linie von \code{pos} zu \code{m.pos} zeichnen.

Schreiben Sie nun in allen Klassen die Methode \code{calculateSignal()}:
\begin{itemize}
	\item In \code{Module} soll ein \code{println(s)} mit dem Text \enquote{Modul kann kein Signal versenden!} aufgerufen werden.
	\item In \code{Input} soll diese Methode \code{send(b)} mit dem gespeicherten Wahrheitswert aufrufen.
	Das geht ohne Probleme, weil durch die Vererbung auch die Unterklassen nun die soeben geschriebene Methode haben.
	\item In \code{OrGate} wird es etwas komplizierter - das Gate kann nur ein Output senden, wenn beide Felder belegt wurden, der Zähler also genau bei 2 liegt.
	Ist er kleiner, sind nicht alle Felder belegt - ist er größer, wurde das Signal bereits versendet.
	Ist diese Bedingung erfüllt, soll das Gate ebenfalls \code{send(b)} aufrufen - mit dem Wahrheitswert, der dort zu erwarten ist.

\end{itemize}

Die Methode \code{calculateSignal()} soll in allen Unterklassen am Ende der Methode \code{input(b)} aufgerufen werden.
Später werden wir diese Methode \enquote{von Hand} in den \code{Input}-Instanzen aufrufen.

Zuletzt optimieren wir unsere \code{void show()} Methode der Unterklassen:
Rufen Sie als erstes \code{super.show()} auf - dadurch wird die Box bereits grundlegend gezeichnet, und außerdem werden so die nachfolgenden Module auch gezeichnet.
Nach diesem Aufruf sollen Sie weiterhin die Details zeichnen, die das Gate und das Input als solche erkennbar machen.

\pagebreak

\subsection{Der erste Schaltkreis}

Etwas fehlte uns noch:
Die Methode \code{void reset()} in \code{Module} soll nun für alle Werte von \code{outgoing} ebenfalls \code{m.reset()} aufrufen.
In Unterklassen müssen Sie nun auch hier jeweils \code{super.reset()} aufrufen.
In \code{Input} können wir uns die Implementation von \code{reset()} komplett sparen - dann übernehmen Instanzen dieser Unterklasse einfach die Methode von \code{Module}.

Nun wollen wir unseren Lorbeerkranz einsammeln und Schaltkreise darstellen:

Definieren Sie ein globales Array \code{Module[] baseModules}, welches alle Module speichern soll, welche nicht von anderen Modulen aufgerufen werden, also meist alle Eingabesignale und Schalter (welche Sie später noch entwickeln können, wenn sie denn möchten.)
Belegen Sie diese Array mit einigen Eingabesignalen.

Fügen Sie dann diesen Eingabesignalen neue Gates zu - da das etwas verwirrend sein könnte, hier ein Codebeispiel:
\begin{lstlisting}
baseModules[0] = new Input(new PVector(100, 100), true);
baseModules[1] = new Input(new PVector(100, 200), false);

Module or = new OrGate(new PVector(200, 150));

baseModules[0].outgoing[0] = or;
baseModules[1].outgoing[0] = or;
\end{lstlisting}

Nun sind Sie fast soweit!
Schreiben Sie in \code{draw()} nun drei for-each Schleifen - jede davon muss über alle Werten von \code{baseModules} iterieren und in dieser Reihenfolge diese Methoden aufrufen:

\begin{enumerate}
	\item \code{m.reset()}
	\item \code{m.calculateSignal()}
	\item \code{m.show()}
\end{enumerate}

Aufgrund den Abhängigkeiten von Modulen untereinander sind wir nicht in der Lage, diese Schleifen ohne weiteres zu vereinen.

Nun können Sie nach Interesse weitere der aufgelisteten Module realisieren und Schaltkreise bauen.
Ihrer Kreativität ist freien Lauf gelassen - solange Sie mit den gleichen Regeln der Klassenerweiterung arbeiten, könnten Sie sich auch eigene logische Module ausdenken.

\begin{center}
	\imagebox{\img/1.png}{.4\textwidth}
\end{center}
