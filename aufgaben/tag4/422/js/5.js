// Damit das Ganze bei Betrachtung nicht einfach nur einem Bild entspricht, wechselt das aktivierte Gate regelmässig.

let baseModules = [];
let time = 0;
let framesBetweenChanges = 45;

function setup() {
  createCanvas(500, 500);
  baseModules[0] = new Input(createVector(100, 100), false);
  baseModules[1] = new Input(createVector(100, 200), true);
  baseModules[2] = new Input(createVector(100, 300), false);
  baseModules[3] = new Input(createVector(100, 400), false);

  let or = new OrGate(createVector(200, 150));
  let or2 = new OrGate(createVector(200, 250));
  let or3 = new OrGate(createVector(200, 350));
  
  let or4 = new OrGate(createVector(300, 200));
  let or5 = new OrGate(createVector(300, 300));

  baseModules[0].outgoing[0] = or;
  baseModules[1].outgoing[0] = or;
  baseModules[1].outgoing[1] = or2;
  baseModules[2].outgoing[0] = or2;
  baseModules[2].outgoing[1] = or3;
  baseModules[3].outgoing[0] = or3;
  
  or.outgoing[0] = or4;
  or2.outgoing[0] = or4;
  or2.outgoing[1] = or5;
  or3.outgoing[0] = or5;

  rectMode(CENTER);
  textAlign(CENTER, CENTER);
  textSize(20);
}

function draw() {
  background(255);
  time = ((time + 1)% (framesBetweenChanges * 4));
  print(time);
  
  if(time < framesBetweenChanges * 1){
    baseModules[0].changeOutput(false);
    baseModules[1].changeOutput(true);
  } else if (time < framesBetweenChanges * 2){
    baseModules[1].changeOutput(false);
    baseModules[2].changeOutput(true);
  }else if (time < framesBetweenChanges * 3){
    baseModules[2].changeOutput(false);
    baseModules[3].changeOutput(true);
  }else if (time < framesBetweenChanges * 4){
    baseModules[0].changeOutput(true);
    baseModules[3].changeOutput(false);
  }

  for (let i = 0; i < baseModules.length; i++) {
    if (baseModules[i] == null)
      continue;

    baseModules[i].reset();
  }

  for (let i = 0; i < baseModules.length; i++) {
    if (baseModules[i] == null)
      continue;

    baseModules[i].calculateSignal();
  }

  for (let i = 0; i < baseModules.length; i++) {
    if (baseModules[i] == null)
      continue;

    baseModules[i].show();
  }
}

class Module {

  constructor(pos) {
    this.pos = pos;
    this.outgoing = [];
    this.output = false;
  }

  input(input) {
    print("Modul kann keine Inputs aufnehmen!");
  }

  reset() {
    for (let i = 0; i < this.outgoing.length; i++) {
      if (this.outgoing[i] == null)
        continue;

      this.outgoing[i].reset();
    }
  }

  show() {
    if (this.output)
      fill(255, 100, 100);
    else
      fill(255);

    square(this.pos.x, this.pos.y, 50);

    for (let i = 0; i < this.outgoing.length; i++) {
      if (this.outgoing[i] == null)
        continue;

      this.outgoing[i].show();
      line(this.pos.x, this.pos.y,  this.outgoing[i].pos.x,  this.outgoing[i].pos.y);
    }
  }

   send(output) {
    this.output = output;

    for (let i = 0; i < this.outgoing.length; i++) {
      if (this.outgoing[i] == null)
        continue;
        
      this.outgoing[i].input(output);
    }
  }

  calculateSignal() {
    print("Modul kann kein Signal versenden!");
  }
}

class OrGate extends Module {

  constructor(pos) {
    super(pos);
    this.counter = 0;
    this.val1 = false;
    this.val2 = false;
  }
  
  calculateSignal() {
    if (this.counter != 2)
      return;

    super.send(this.val1 || this.val2);
  }

  input(input) {
    if (this.counter == 0)
      this.val1 = input;
    else if (this.counter == 1)
      this.val2 = input;
    else
      print("Gate kann keine weiteren Inputs aufnehmen!");

    this.counter++;
    this.calculateSignal();
  }

  reset() {
    super.reset();

    this.val1 = false;
    this.val2 = false;
    this.counter = 0;
  }

  show() {
    super.show();

    fill(0);
    text("OR", this.pos.x, this.pos.y);
  }

}

class Input extends Module {

  constructor(pos, output) {
    super(pos);
    this.output = output;
  }
  
  changeOutput(newOutput){
    this.output = newOutput;
  }

  calculateSignal() {
    this.send(this.output);
  }

  show() {
    super.show();

    fill(0);
    text(this.output ? 1 : 0, this.pos.x, this.pos.y);
  }
}
