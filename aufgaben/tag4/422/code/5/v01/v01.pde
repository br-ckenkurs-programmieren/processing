Module[] baseModules;

void setup() {
  size(500, 500);
  baseModules = new Module[10];
  baseModules[0] = new Input(new PVector(100, 100), false);
  baseModules[1] = new Input(new PVector(100, 200), true);
  baseModules[2] = new Input(new PVector(100, 300), false);
  baseModules[3] = new Input(new PVector(100, 400), false);

  Module or = new OrGate(new PVector(200, 150));
  Module or2 = new OrGate(new PVector(200, 250));
  Module or3 = new OrGate(new PVector(200, 350));
  
  Module or4 = new OrGate(new PVector(300, 200));
  Module or5 = new OrGate(new PVector(300, 300));

  baseModules[0].outgoing[0] = or;
  baseModules[1].outgoing[0] = or;
  baseModules[1].outgoing[1] = or2;
  baseModules[2].outgoing[0] = or2;
  baseModules[2].outgoing[1] = or3;
  baseModules[3].outgoing[0] = or3;
  
  or.outgoing[0] = or4;
  or2.outgoing[0] = or4;
  or2.outgoing[1] = or5;
  or3.outgoing[0] = or5;

  rectMode(CENTER);
  textAlign(CENTER, CENTER);
  textSize(20);
}

void draw() {
  background(255);

  for (Module m : baseModules) {
    if (m == null)
      continue;

    m.reset();
  }

  for (Module m : baseModules) {
    if (m == null)
      continue;

    m.calculateSignal();
  }

  for (Module m : baseModules) {
    if (m == null)
      continue;

    m.show();
  }
}

class Module {

  PVector pos;
  Module[] outgoing;
  boolean output;

  Module(PVector pos) {
    this.pos = pos;
    this.outgoing = new Module[10];
  }

  void input(boolean input) {
    print("Modul kann keine Inputs aufnehmen!");
  }

  void reset() {
    for (Module m : outgoing) {
      if (m == null)
        continue;

      m.reset();
    }
  }

  void show() {
    if (output)
      fill(255, 100, 100);
    else
      fill(255);

    square(pos.x, pos.y, 50);

    for (Module m : outgoing) {
      if (m == null)
        continue;

      m.show();
      line(pos.x, pos.y, m.pos.x, m.pos.y);
    }
  }

  void send(boolean output) {
    this.output = output;

    for (Module m : outgoing) {
      if (m == null)
        continue;
      m.input(output);
    }
  }

  void calculateSignal() {
    print("Modul kann kein Signal versenden!");
  }
}

class OrGate extends Module {

  boolean val1, val2;
  int counter;

  OrGate(PVector pos) {
    super(pos);
    counter = 0;
  }

  void input(boolean input) {
    if (counter == 0)
      val1 = input;
    else if (counter == 1)
      val2 = input;
    else
      print("Gate kann keine weiteren Inputs aufnehmen!");

    counter++;
    calculateSignal();
  }

  void reset() {
    super.reset();

    val1 = false;
    val2 = false;
    counter = 0;
  }

  void show() {
    super.show();

    fill(0);
    text("OR", pos.x, pos.y);
  }

  void calculateSignal() {
    if (counter != 2)
      return;

    send(val1 || val2);
  }
}

class Input extends Module {

  boolean output;

  Input(PVector pos, boolean output) {
    super(pos);
    this.output = output;
  }

  void calculateSignal() {
    send(output);
  }

  void show() {
    super.show();

    fill(0);
    text(output ? 1 : 0, pos.x, pos.y);
  }
}
