class Module {

  PVector pos;
  Module[] outgoing;

  Module(PVector pos) {
    this.pos = pos;
    this.outgoing = new Module[10];
  }

  void input(boolean input) {
    print("Modul kann keine Inputs aufnehmen!");
  }
  
  void reset() {
  }
}

class OrGate extends Module {

  boolean val1, val2;
  int counter;

  OrGate(PVector pos) {
    super(pos);
    counter = 0;
  }

  void input(boolean input) {
    if (counter == 0)
      val1 = input;
    else if (counter == 1)
      val2 = input;
    else
      print("Gate kann keine weiteren Inputs aufnehmen!");

    counter++;
  }
  
  void reset() {
    val1 = false;
    val2 = false;
    counter = 0;
  }
}
