Module m1, m2, m3;

void setup() {
  size(750, 750);
  m1 = new Input(new PVector(100, 100), true);
  m2 = new OrGate(new PVector(500, 200));
  m3 = new OrGate(new PVector(400, 600));

  m1.outgoing[0] = m2;

  rectMode(CENTER);
  textAlign(CENTER, CENTER);
  textSize(20);
}

void draw() {
  background(255);

  m1.show();
  m2.show();
  m3.show();
}

class Module {

  PVector pos;
  Module[] outgoing;
  boolean output;

  Module(PVector pos) {
    this.pos = pos;
    this.outgoing = new Module[10];
  }

  void input(boolean input) {
    print("Modul kann keine Inputs aufnehmen!");
  }

  void reset() {
  }

  void show() {
    if (output)
      fill(255, 100, 100);
    else
      fill(255);

    square(pos.x, pos.y, 50);

    for (Module m : outgoing) {
      if (m == null)
        continue;
      m.show();
      line(pos.x, pos.y, m.pos.x, m.pos.y);
    }
  }

  void send(boolean output) {
    this.output = output;

    for (Module m : outgoing) {
      if (m == null)
        continue;
      m.input(output);
    }
  }

  void calculateSignal() {
    print("Modul kann kein Signal versenden!");
  }
}

class OrGate extends Module {

  boolean val1, val2;
  int counter;

  OrGate(PVector pos) {
    super(pos);
    counter = 0;
  }

  void input(boolean input) {
    if (counter == 0)
      val1 = input;
    else if (counter == 1)
      val2 = input;
    else
      print("Gate kann keine weiteren Inputs aufnehmen!");

    counter++;
    calculateSignal();
  }

  void reset() {
    val1 = false;
    val2 = false;
    counter = 0;
  }

  void show() {
    super.show();

    fill(0);
    text("OR", pos.x, pos.y);
  }

  void calculateSignal() {
    if (counter != 2)
      return;

    send(val1 || val2);
  }
}

class Input extends Module {

  boolean output;

  Input(PVector pos, boolean output) {
    super(pos);
    this.output = output;
  }

  void calculateSignal() {
    send(output);
  }

  void show() {
    super.show();

    fill(0);
    text(true ? 1 : 0, pos.x, pos.y);
  }
}
