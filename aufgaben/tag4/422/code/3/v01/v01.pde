Module m1, m2, m3;

void setup() {
  size(750, 750);
  m1 = new OrGate(new PVector(100, 100));
  m2 = new OrGate(new PVector(500, 200));
  m3 = new OrGate(new PVector(400, 600));
  
  rectMode(CENTER);
  textAlign(CENTER);
  textSize(20);
}

void draw() {
  background(255);
  
  m1.show();
  m2.show();
  m3.show();
}

class Module {

  PVector pos;
  Module[] outgoing;

  Module(PVector pos) {
    this.pos = pos;
    this.outgoing = new Module[10];
  }

  void input(boolean input) {
    print("Modul kann keine Inputs aufnehmen!");
  }
  
  void reset() {
  }
  
  void show() {
    fill(255);
    square(pos.x, pos.y, 50);
  }
}

class OrGate extends Module {

  boolean val1, val2;
  int counter;

  OrGate(PVector pos) {
    super(pos);
    counter = 0;
  }

  void input(boolean input) {
    if (counter == 0)
      val1 = input;
    else if (counter == 1)
      val2 = input;
    else
      print("Gate kann keine weiteren Inputs aufnehmen!");

    counter++;
  }
  
  void reset() {
    val1 = false;
    val2 = false;
    counter = 0;
  }
  
  void show() {
    fill(255);
    square(pos.x, pos.y, 50);
    
    fill(0);
    text("OR", pos.x, pos.y);
  }
}
