class Module {
  
  PVector pos;
  Module[] outgoing;
  
  Module(PVector pos) {
    this.pos = pos;
    this.outgoing = new Module[10];
  }
}
