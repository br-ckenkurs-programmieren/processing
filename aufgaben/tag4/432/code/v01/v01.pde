void setup() {
  Counter c = new Counter(40);
  c.count(); // Konsole: 40
  c.count(); // Konsole: 41
  println("Variable next speichert: " + c.next); // Konsole: Variable next speichert: 42
  c.count(); // Konsole: 42
}

class Counter {
  int next;

  Counter(int start) {
    this.next = start;
  }

  void count() {
    println(next);
    next = next + 1;
  }
}
