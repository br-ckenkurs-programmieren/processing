function setup() {
    var c = new Counter(40);
    // Konsole: 40
    c.count();
    // Konsole: 41
    c.count();
    // Konsole: Variable next speichert: 42
    print("Variable next speichert: " + c.next);
    // Konsole: 42
    c.count();
}

class Counter{
  constructor (start) {
      this.next = start;
  }

  count() {
    print(this.next);
    this.next = this.next + 1;
  }
}
