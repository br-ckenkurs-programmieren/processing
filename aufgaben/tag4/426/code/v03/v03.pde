//Game vars
boolean gameStart = true;
boolean gamePaused = false;
int highScore = 0;
int snakeScore = 0;

// WindowVars
int WindowHeight = 680;                           // Real Nutzbare Fensterhöhe
int barHeight = 60;

//snakeSetup vars
int startPosX = 200;                              // Startposition der Schlange
int startPosY = 200;
int snakeSize = 20;                               // Größe der einzelnen Körperteile                         
int snakeLength = 3;                              // Anfangszahl der einzelnen Körperteile     
String direction ="down";                         // Anfangsrichtung bei der ersten Runde
String lastDirection = direction;                 // Zwischenvariable um nicht in sich selbst zu gehen.
// [400] = Einzelne Elemente, [2] = x,y Pos
int [][] snake = new int [400][2];                //Array für die Positionen der einzelnen Körperteile

//Food vars
int foodX;                                        //Position des Futters auf der x-Achse
int foodY;                                        // Position des Futters auf der y-Achse

//int pic_id = 0;

void setup() {
  size(1000, 800);
  background(255);
  noStroke();                                     // Rahmenlinie deaktivieren

  //font setup  // Font nicht mitgeliefert -> Deaktiviert
  /*PFont font;
   font = createFont("ARCADECLASSIC.TTF", 32);
   textFont(font);*/
  frameRate(20);

  for (int i = 0; i < snakeLength; i++) {        // Erstellung des Schlangenkörpers
    snake[i][0] = 200 + i * snakeSize;           //Position auf der x-Achse
    snake[i][1] = 200;                           //Position auf der y-Achse
  }
  // Initiale Essensposition
  FoodPlacement();
}

void draw() { 
  if (!gameStart) {
    GameOverMenue();
    //saveFrame("snake_03_"+pic_id++ +".jpg");
    return;
  }
  if (gamePaused) {
    //saveFrame("snake_03_"+pic_id++ +".jpg");
    return;
  }
  // Bewegung
  Heritage();            //"Vererbung" der x- und y-Werte
  SnakeMove();           // Bewegung der Schlange
  OutOfScreen();         // Ereignis beim Verlassen des Fensters
  SnakeEatsPoint();      // Schlange isst Futter + Neuplazierung
  GameOverCheck();       // Ereignis bei Berührung zwischen Kopf und irgendeinem Körperteil

  // Drawing
  background(255);
  SnakeDraw();          // Zeichnen der Schlange und des Essens
  MenueDraw();          // Zeichnen des Menues und des Pauseknopfes
  //saveFrame("snake_03_"+pic_id++ +".jpg");
}

//Vererbung der Koordinaten, alle bis auf Kopf eins weiter
void Heritage() {
  for (int i = snakeLength - 1; i > 0; i--) {
    snake[i][0] = snake[i - 1][0];           //Position auf der x-Achse. Jeder Körperteil bekommt die Werten des vorherigen Körperteils
    snake[i][1] = snake[i - 1][1];           //Position auf der y-Achse. Jeder Körperteil bekommt die Werten des vorherigen Körperteils
  }
}

// Bewegung der Schlange, Kopfposition neu setzen
void  SnakeMove() {
  lastDirection = direction;
  if (direction == "up") {       //Bewegung auf der x-Achse
    snake[0][1] -= snakeSize;
  }
  if (direction == "down") {     //Bewegung auf der x-Achse
    snake[0][1] += snakeSize;
  }
  if (direction == "left") {     //Bewegung auf der y-Achse
    snake[0][0] -= snakeSize;
  }
  if (direction == "right") {    //Bewegung auf der y-Achse
    snake[0][0] += snakeSize;
  }
}

// Ereignis beim Verlassen des Fensters
void OutOfScreen() {
  //Beim Verlassen des Fensters an der linken Seite, erscheint die Schlange auf der rechten Seite wieder
  if (snake[0][0] < 0) {
    snake[0][0] = width - snakeSize - 0;
  }
  //Beim Verlassen des Fensters an der rechten Seite, erscheint die Schlange auf der linken Seite wieder
  if (width <= snake[0][0]) {
    snake[0][0] = 0;
  }
  //Beim Verlassen des Fensters an der oberen Seite, erscheint die Schlange auf der unteren Seite wieder
  if (snake[0][1] < barHeight) {
    snake[0][1] = height - snakeSize;
  }
  //Beim Verlassen des Fensters an der unteren Seite, erscheint die Schlange auf der oberen Seite wieder
  if (height <= snake[0][1]) {
    snake[0][1] = barHeight;
  }
}


//Anpassung der Körpergröße und -farbe und Zeichnung des Essens
void SnakeDraw() {

  // Farbe + Zeichnen der Körperteile
  int green = 70;
  int colorChange = 10;
  for (int i = 0; i < snakeLength; i++) {
    if (green > 205 - colorChange || green < 50 + colorChange) {
      colorChange *= -1;
    }
    green += colorChange;
    fill(0, green, 0);                            //Leichtes Fade-Effekt an der Körperfarbe
    square(snake[i][0], snake[i][1], snakeSize);  //Positionsänderung der einzelnen Körperteile
  }

  //Das Futter wir gezeichnet und hat die Größe eines einzelen Schlangenkörperteils
  fill(255, 0, 0);
  square(foodX, foodY, snakeSize);
}

// Zeichnen des Menues mit Highscore und Pausebutton/Screen
void MenueDraw() {

  // Oberen Balken Zeichnen
  fill(0);
  rect(0, 0, width, barHeight);

  //Highscore
  fill(255);
  textSize(20);
  text("Score " + snakeScore, 10, 30);          //aktueller Score
  text("Highscore  " + highScore, width - 250, 30 );    //Highscore

  // Pauseknopf
  if (!gamePaused) { // Zwei Balken / Pause
    fill(255);
    rect(width - 30, 10, 7.5, 25);
    rect(width - 15, 10, 7.5, 25);
  } else { // Resume
    triangle((width - 30), 10, (width - 30), 35, (width-5), 22.5);
  }

  // Cosmetics
  textSize(40);
  text("S n a k e", (width/2) - 100, 40);
}

//Essen und Positionsanpassung des Futters
void SnakeEatsPoint() {
  /*Wenn Schlangenkopf auf ein Futter trifft
   wird die Schlange um einen Körperteil verlängert und die Position des Futters geändert*/
  if (snake[0][0] == foodX && snake[0][1] == foodY) {

    // Letztes Feld wird doppelt plaziert und bald weiterbewegt
    snake[snakeLength][0] = snake[snakeLength-1][0];
    snake[snakeLength][1] = snake[snakeLength-1][1];

    // Länge erhöht
    snakeLength += 1;

    // Scoring
    snakeScore += 100;
    if (highScore < snakeScore) { // Highscore wird nur angepasst, wenn der aktuelle Score größer ist
      highScore = snakeScore;
    }

    // Neues Futter
    FoodPlacement();
  }
}

void FoodPlacement() {
  // Platz außerhalb der Schlange suchen
  boolean freeSpace = true;
  do {
    freeSpace = true;
    // Sicher gehen, dass das Futter im Raster der Schlange liegt, nicht leicht versetzt
    foodX = (int)random(0, width / snakeSize) * snakeSize; //Position des Futters auf der x-Achse
    foodY = (int)random(barHeight / snakeSize, height / snakeSize) * snakeSize; // Position des Futters auf der y-Achse

    for (int i = snakeLength - 1; i > 0; i--) { //Abfrage, ob der Kopf die gleiche Koordinaten hat wie irgendein Körperteil
      if (foodX == snake [i][0]
        && foodY == snake[i][1]) {
        freeSpace = false;
        println("Neue Koordinate gesucht");
      }
    }
  } while (!freeSpace); // Wiederholen bis freier Platz gefunden
}

//Ereignis bei Berührung zwischen Kopf und irgendeinem Körperteil
void GameOverCheck() {

  for (int i = 1; i < snakeLength; i++) { //Abfrage, ob der Kopf die gleiche Koordinaten hat wie irgendein Körperteil
    if (snake[0][0] == snake [i][0] 
      && snake[0][1] == snake[i][1]) {
      gameStart = false;                  // keine Freigabe mehr für den Spielstart
      snakeLength = 3;                    //Schlangenröße wird zurückgesetzt
      snake[0][0] = startPosX;            //Position auf der x-Achse wird zurückgesetzt
      snake[0][1]= startPosY;             //Position auf der y-Achse wird zurückgesetzt
      return;                             // Schleife beenden
    }
  }
}

// Zeichnen des GameOver Menues
void GameOverMenue() {

  // Game Over Menue
  background(0);              
  fill(#FFFFFF);                        //Schrift wird weiß
  textSize(52);
  text("GAME OVER", 350, (WindowHeight + 20) / 2 );
  textSize(28);
  text("Press Left Mouse  Button to start again", 175+30, (WindowHeight + 100) / 2 );
  text("Your Score: " + snakeScore, 175 + 200, (WindowHeight + 150) / 2 );
  if (highScore == snakeScore) {        // Highscore wird nur angepasst, wenn der aktuelle Score größer ist
    text("Congratulations you reached a new highscore", 175, (WindowHeight + 200) / 2);
  } else {
    text("Current Highscore: "+ highScore, 175 + 150, (WindowHeight + 200) / 2);
  }
}


// Richtungsänderung der Schlange
void keyPressed() {
  if (gamePaused) {
    return;
  }
  if (key == 'w' && lastDirection!= "down") {   // Bei einer vertikalen Bewegung, ändert sich die Richtung
    direction = "up";                  // der Schlange nur horizontal
  }

  if (key == 's' && lastDirection!="up") {      // Bei einer vertikalen Bewegung, ändert sich die Richtung
    direction = "down";                  // der Schlange nur horizontal
  }
  if (key == 'a' && lastDirection!= "right") {  // Bei einer horizontalen Bewegung, ändert sich die Richtung
    direction = "left";                  // der Schlange nur vertikal
  } 

  if (key == 'd' && lastDirection!= "left") {   // Bei einer horizontalen Bewegung, ändert sich die Richtung
    direction = "right";                 // der Schlange nur vertikal
  }
}

// Pausebutton klicken
void mousePressed() {
  if (mouseButton == LEFT) {
    if (gameStart == false) {
      gameStart = true;
      snakeScore = 0;
      return;
    }

    if (mouseX <= width-7.5 && mouseY <= 35) {
      if (mouseX >= width-30 && mouseY >= 10 ) {   
        gamePaused = !gamePaused;
      }
    }
  }
}
