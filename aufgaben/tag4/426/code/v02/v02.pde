import java.util.LinkedList;

// the position of a snake body part
class SnakeBodyPart {
  int x;
  int y;
  
  public SnakeBodyPart(int x, int y) {
    this.x = x;
    this.y = y;
  }
  
  public void draw() {
    rect(x*SQUARE_SIZE, y*SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE);
  }
}

// the position of a fruit
class Fruit {
  int x;
  int y;
  
  public Fruit(int x, int y) {
    this.x = x;
    this.y = y;
  }
  
  public void draw() {
    circle(x*SQUARE_SIZE+SQUARE_SIZE/2, y*SQUARE_SIZE+SQUARE_SIZE/2, SQUARE_SIZE);
  }
}

// constants
int GRID_SIZE = 10;
int SQUARE_SIZE = 50;

// playing field, mainly used for collision checks
int[][] grid;

// snake body parts and fruit
LinkedList<SnakeBodyPart> snake;
Fruit fruit;

// snake moving direction
int currentSnakeVelocityX;
int currentSnakeVelocityY;
int lastSnakeVelocityX;
int lastSnakeVelocityY;

// scores
int currentScore;
int lastScore;
int highScore;

// total frames and frames until the snake does its next move
int totalFrames;
int framesPerSnakeStep;


void settings() {
  size(GRID_SIZE*SQUARE_SIZE,GRID_SIZE*SQUARE_SIZE);
}

void setup() {
  frameRate(60);
  noStroke();
  resetGame();
  lastScore = 0;
  highScore = 0;
}

void keyPressed() { // arrow keys or WASD
  // save the last pressed key and save it for the next time the snake moves
    if (     key == 'w' || keyCode == UP) {
    setSnakeVelocity(0, -1);
  } else if (key == 'a' || keyCode == LEFT) {
    setSnakeVelocity(-1, 0);
  } else if (key == 's' || keyCode == DOWN) {
    setSnakeVelocity(0, 1);
  } else if (key == 'd' || keyCode == RIGHT) {
    setSnakeVelocity(1, 0);
  }
}

void draw() {
  // only update the screen when the snake moved
  if (totalFrames % framesPerSnakeStep == 0) {
    // move snake
    moveSnake();
    
    // draw grid background
    background(0,192,0);
    
    // draw fruit
    fill(192,0,0);
    fruit.draw();
    
    // draw snake
    float snakeBodyPartCounter = 0;
    for (SnakeBodyPart snakeBodyPart : snake) {
      // skip snake head
      if (snakeBodyPartCounter > 0) {
        // draw snake body part
        float clr = 55 + (200 * (snakeBodyPartCounter/snake.size()));
        fill(clr, clr, 255);
        snakeBodyPart.draw();
      }
      snakeBodyPartCounter++;
    }
    
    // draw scores
    fill(255);
    text(currentScore, 20, 20);
    text(lastScore, 20, 30);
    text(highScore, 20, 40);
  }
  
  // draw snake head with eyes in moving direction
  // always drawn in case the player pressed a button and the eyes move
  drawSnakeHead();
  
  // increase frame counter
  totalFrames++;
  
  // speed up snake over time
  if (totalFrames % 500 == 0 && framesPerSnakeStep > 10) { 
    framesPerSnakeStep--;
  }
}

void drawSnakeHead() {
  // draw head
  fill(0, 0, 255);
  SnakeBodyPart snakeHead = snake.get(0);
  rect(snakeHead.x*SQUARE_SIZE, snakeHead.y*SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE);
  
  // draw eyes in moving direction
  fill(255, 255, 255);
  int offset = SQUARE_SIZE/5;
  if (currentSnakeVelocityX == 1) {
    circle(snakeHead.x*SQUARE_SIZE+SQUARE_SIZE-offset, snakeHead.y*SQUARE_SIZE+SQUARE_SIZE/4,     SQUARE_SIZE/4);
    circle(snakeHead.x*SQUARE_SIZE+SQUARE_SIZE-offset, snakeHead.y*SQUARE_SIZE+3*SQUARE_SIZE/4+1, SQUARE_SIZE/4);
  } else if (currentSnakeVelocityX == -1) {
    circle(snakeHead.x*SQUARE_SIZE+offset, snakeHead.y*SQUARE_SIZE+SQUARE_SIZE/4,     SQUARE_SIZE/4);
    circle(snakeHead.x*SQUARE_SIZE+offset, snakeHead.y*SQUARE_SIZE+3*SQUARE_SIZE/4+1, SQUARE_SIZE/4);
  } else if (currentSnakeVelocityY == 1) {
    circle(snakeHead.x*SQUARE_SIZE+SQUARE_SIZE/4,     snakeHead.y*SQUARE_SIZE+SQUARE_SIZE-offset, SQUARE_SIZE/4);
    circle(snakeHead.x*SQUARE_SIZE+3*SQUARE_SIZE/4+1, snakeHead.y*SQUARE_SIZE+SQUARE_SIZE-offset, SQUARE_SIZE/4);
  } else if (currentSnakeVelocityY == -1) {
    circle(snakeHead.x*SQUARE_SIZE+SQUARE_SIZE/4,     snakeHead.y*SQUARE_SIZE+offset, SQUARE_SIZE/4);
    circle(snakeHead.x*SQUARE_SIZE+3*SQUARE_SIZE/4+1, snakeHead.y*SQUARE_SIZE+offset, SQUARE_SIZE/4);
  }
}


void setSnakeVelocity(int x, int y) {
  // this check prevents being able to do 180 degree turns
  if (   y == -1 && lastSnakeVelocityY !=  1
      || y ==  1 && lastSnakeVelocityY != -1
      || x == -1 && lastSnakeVelocityX !=  1
      || x ==  1 && lastSnakeVelocityX != -1) {
    // set velocity for next game tick
    currentSnakeVelocityX = x;
    currentSnakeVelocityY = y;
  }
}

void moveSnake() {
  // cache velocity for the 180 degree turn checks
  lastSnakeVelocityX = currentSnakeVelocityX;
  lastSnakeVelocityY = currentSnakeVelocityY;
  
  // select next position for the snake head
  SnakeBodyPart snakeHead = snake.peek();
  int x = snakeHead.x + currentSnakeVelocityX;
  int y = snakeHead.y + currentSnakeVelocityY;
  
  // teleport snake onto other side of the playing field at the edges
  x = checkBounds(x, GRID_SIZE);
  y = checkBounds(y, GRID_SIZE);
  
  // lose condition: snake head moved into snake body 
  if (isSnakeBodyAt(x, y)) { 
    gameOver();
    return;
  }
  
  // check if snake moved into fruit
  if (isFruitAt(x, y)) {
    collectFruit();
    // dont remove snake tail here to extend snake length
  } else {
    removeSnakeTail();
  }
  
  // move snake head to new position
  addSnakeBodyPartAt(x, y);
}

// adds a new body part to the snake
void addSnakeBodyPartAt(int x, int y) {
  snake.push(new SnakeBodyPart(x, y));
  grid[x][y] = 1;
}

// removes the last body part of the snail
void removeSnakeTail() {
  SnakeBodyPart snakeTail = snake.pollLast();
  grid[snakeTail.x][snakeTail.y] = 0;
}


void collectFruit() {
  // increase score by one
  currentScore++;
  
  // win condition: max snake length, all fruits collected
  if (currentScore >= GRID_SIZE*GRID_SIZE) {
    gameOver();
  }
  
  // add new fruit
  addRandomFruit();
}

// adds a fruits at a random but free spot
void addRandomFruit() {
  int x;
  int y;
  do { // search for a free spot
    x = (int)(Math.random()*GRID_SIZE);
    y = (int)(Math.random()*GRID_SIZE);
  } while (grid[x][y] != 0);
  
  // add fruit
  addFruitAt(x, y);
}

// add new fruit
void addFruitAt(int x, int y) {
  grid[x][y] = 2;
  fruit = new Fruit(x, y);
}


// checks if snake crossed the edges of the playing field
// teleports the snake to the other side
int checkBounds(int value, int max) {
  if (value < 0) {
    return max-1;
  } else if (value >= max) {
    return 0;
  } else {
    return value;
  }
}

// is there a snake body at the position (x,y)
boolean isSnakeBodyAt(int x, int y) {
  return grid[x][y] == 1;
}

// is there a fruit at the position (x,y)
boolean isFruitAt(int x, int y) {
  return grid[x][y] == 2;
}



// initializes a new game
void resetGame() {
  // clear the grid and snake
  grid = new int[GRID_SIZE][GRID_SIZE];
  snake = new LinkedList<SnakeBodyPart>();
  
  // spawn a snake
  int middle = (int)(GRID_SIZE/2);
  addSnakeBodyPartAt(middle-2, middle);
  addSnakeBodyPartAt(middle-1, middle);
  addSnakeBodyPartAt(middle,   middle);
  
  // set default movement to the right
  currentSnakeVelocityX = 1;
  currentSnakeVelocityY = 0;
  lastSnakeVelocityX = 1;
  lastSnakeVelocityY = 0;
  
  // set frames and snake speed
  totalFrames = 0;
  framesPerSnakeStep = 20;
  
  // add a fruit and set score to zero
  addRandomFruit();
  currentScore = 0;
}

// saves scores and resets the game
void gameOver() {
  // save current score
  lastScore = currentScore;
  // overwrite highscore if it was beaten
  if (currentScore > highScore) {
    highScore = currentScore;
  }
  // reset and start a new game
  resetGame();
}
