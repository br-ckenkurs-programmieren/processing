// Snake
// Autor: Elias Kupferschmitt

int nBoxesX, nBoxesY; // Größe des Spielfelds, wird aus der Fenstergröße bestimmt.
int boxSize = 8; // Größe eines Felds in Pixeln.

int[] none = new int[]{0, 0}; // Bewegungslose Schlange (nur am Anfang und bei Game Over).
int[] up = new int[]{0, -1}; // Bewegung nach oben.
int[] down = new int[]{0, 1}; // Bewegung nach unten.
int[] left = new int[]{-1, 0}; // Bewegung nach links.
int[] right = new int[]{1, 0}; // Bewegung nach rechts.

Snake snake; // Platzhalter für unsere Schlange.

// Diese Klasse stellt eine Schlange dar.
class Snake {
  int headX, headY; // Die Position des Kopfs der Schlange.
  int[][] body; // Der Körper der Schlange.
  int[] currentDirection; // Aktuelle Fortbewegungsrichtung der Schlange.
  int[] nextDirection; // Nächste Fortbewegungsrichtung der Schlange, wird durch Benutzereingaben gesetzt.
  int foodX, foodY; // Die Position des Futters.
  int nMeals; // Zähler, wie oft die Schlange gefressen hat.
  int speed; // Geschwindigkeit der Schlange, wird aus Framerate bestimmt.
  int moveCounter; // Wenn dieser Counter den Wert von speed erreicht, bewegt sich die Schlange.
  boolean isGameOver; // Bestimmt, ob das Spiel noch läuft.

  // Konstruktor für unsere Schlange. Es werden zufällige Startpositionen für Schlange und Futter gewählt.
  Snake() {
    headX = (int) random(nBoxesX); // Zufällige Startposition des Kopfs innerhalb des Spielfelds setzen.
    headY = (int) random(nBoxesY);
    foodX = (int) random(nBoxesX); // Same procedure für das Futter.
    foodY = (int) random(nBoxesY);
    body = new int[nBoxesX * nBoxesY - 1][]; // Wir benötigen maximal Breite * Höhe - 1 Felder für den Körper, da der Kopf seperat gespeichert wird. Die zweite Dimension bleibt zunächst leer, da die Schlange zu Beginn des Spiels nur einen Kopf hat.
    currentDirection = none; // Zu Beginn des Spiels bewegt sich die Schlange nicht.
    nextDirection = none;
    speed = ((int) frameRate) / 15; // Wir teilen die Framerate zur Bestimmung der Geschwindigkeit, damit die Schlange bei Änderung der Framerate ihre Geschwindigkeit beibehält.
  }
  
  // Zeichne Schlange und Futter.
  void display() {
    // Zeichne Kopf.
    int x = headX;
    int y = headY;
    rect(x * boxSize, y * boxSize, boxSize, boxSize);
    
    // Zeichne Körper.
    for (int i = 0; i < nMeals; i++) {
      x += body[i][0];
      y += body[i][1];
      rect(x * boxSize, y * boxSize, boxSize, boxSize);
    }
    
    // Zeichne Futter.
    fill(0, 255, 0);
    rect(foodX * boxSize + boxSize / 4, foodY * boxSize + boxSize / 4, boxSize / 2, boxSize / 2);
    fill(255);
  }
  
  // Bewege die Schlange.
  void move() {
    // Wir müssen hier nur etwas tun, wenn sich die Schlange auch bewegt.
    if (nextDirection != none) {
      // Game Over boolean setzen
      isGameOver = isGameOver();
      
      // Wenn das Spiel nicht mehr läuft, Schlange anhalten.
      if (isGameOver) {
        snake.currentDirection = none;
        snake.nextDirection = none;
      } else {
        // Prüfen, ob wir gerade fressen wollen.
        if (isEating()) {
          foodX = (int) random(nBoxesX); // Neue Position für Futter bestimmen.
          foodY = (int) random(nBoxesY);
          nMeals += 1; // Fresszähler inkrementieren, dadurch wird die letzte Position nicht überschrieben.
        }
        
        // Zuerst Körper verschieben, es ist leichter von hinten anzufangen, da die letzte Position überschrieben werden kann, sofern wir nicht gefressen haben.
        for (int i = nMeals; i > 0; i--) {
          // Alles im Array nach hinten verschieben.
          body[i] = body[i - 1];
        }
        
        // Gegenteil der Laufrichtung in das Array eintragen. Dadurch kann ausgehend vom Kopf der Körper der Schlange gespeichert werden.
        // Beispiel: <- OOO
        // Diese Schlange bewegt sich nach links, wenn wir vom Kopf ausgehend zeichnen wollen, müssen wir nach rechts.
        body[0] = getOppositeDirection();
        
        // Neuen Kopf berechnen.
        headX += nextDirection[0];
        headY += nextDirection[1];
        
        // Richtung aktualisieren.
        currentDirection = nextDirection;
      }
    }
  }
  
  // Prüfe, ob wir mit der nächsten Bewegung fressen werden.
  boolean isEating() {
    return headX + nextDirection[0] == foodX && headY + nextDirection[1] == foodY;
  }
  
  // Prüfe, ob wir mit der nächsten Bewegung verlieren werden.
  boolean isGameOver() {
    int nextHeadX = headX + nextDirection[0];
    int nextHeadY = headY + nextDirection[1];
    
    // Prüfe, ob wir in eine Wand laufen werden.
    if (nextHeadX < 0 || nextHeadX >= nBoxesX || nextHeadY < 0 || nextHeadY >= nBoxesY) {
      return true;
    }
    
    // Prüfe, ob wir uns selbst beissen werden.
    int x = headX;
    int y = headY;
    // Gespeicherte Richtungen entlanggehen und prüfen, ob der neue Kopf an einer dieser Positionen wäre.
    // Letzte Position im body-Array muss nicht angeguckt werden, da sich die Schlange nicht in das Schwanzende beissen kann.
    for (int i = 0; i < nMeals - 1; i++) {
      x += body[i][0];
      y += body[i][1];
      if (nextHeadX == x && nextHeadY == y) {
        return true;
      }
    }
    
    // Wenn wir hier ankommen, läuft das Spiel noch.
    return false;
  }
  
  // Gebe das Gegenteil der aktuellen Laufrichtung zurück.
  int[] getOppositeDirection() {
    if (nextDirection == up) return down;
    if (nextDirection == down) return up;
    if (nextDirection == left) return right;
    if (nextDirection == right) return left;
    return none;
  }
}

// Steuere die Schlange. Sowohl Pfeiltasten als auch WASD sind möglich. Die Schlange darf sich nur um 90° drehen und lässt sich nicht mehr steuern, sobald das Spiel vorbei ist.
void keyPressed() {
  if (!snake.isGameOver) {
    if ((key == 'W' || key == 'w' || (key == CODED && keyCode == UP)) && snake.currentDirection != down) {
      snake.nextDirection = up;
    } else if ((key == 'S' || key == 's' || (key == CODED && keyCode == DOWN)) && snake.currentDirection != up) {
      snake.nextDirection = down;
    } else if ((key == 'A' || key == 'a' || (key == CODED && keyCode == LEFT)) && snake.currentDirection != right) {
      snake.nextDirection = left;
    } else if ((key == 'D' || key == 'd' || (key == CODED && keyCode == RIGHT)) && snake.currentDirection != left) {
      snake.nextDirection = right;
    }
  }
}

void setup() {
  size(400, 400);
  pixelDensity(2);
  frameRate(60);
  nBoxesX = width / boxSize; // Spielfeldgröße = Fenstergröße, geteilt durch Größe eines Felds.
  nBoxesY = height / boxSize;
  snake = new Snake(); // Erzeuge Schlange.
}

void draw() {
  clear(); // Alten Frame löschen.
  // 60 Bewegungen pro Sekunde wären zu schnell. Sehr niedrige Frameraten wirken sich aber negativ auf die "smoothness" der Steuerung aus. Daher gibt es einen Counter. Nur wenn dieser sein Maximum erreicht, bewegt sich die Schlange.
  if (snake.moveCounter == snake.speed) {
    snake.moveCounter = 0;
    snake.move();
  } else {
    snake.moveCounter += 1;
  }
  snake.display(); // Schlange zeichnen lassen.
}
