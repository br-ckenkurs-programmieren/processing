float gravity = 0.08;
Particle p;

void setup() {
  size(250, 250);
  p = new Particle(width / 2, height / 2, color(255, 0, 0));
}

void draw() {
  background(255);
  p.show();
  p.move();
}

class Particle {
  float x, y;
  color c;
  float vx, vy;
  
  Particle(float x, float y, color c) {
    this.x = x;
    this.y = y;
    this.c = c;
    this.vy = random(-2, -0.5);
    this.vx = random(-2, 2);
  }
  
  void show() {
    fill(c);
    circle(x, y, 20);
  }
  
  void move() {
    x += vx;
    y += vy;
    vy += gravity;
  }
}
