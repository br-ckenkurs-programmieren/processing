Particle p;

void setup() {
  size(250, 250);
  p = new Particle(width / 2, height / 2, color(255, 0, 0)); // Partikel in der Mitte
}

void draw() {
  p.show(); // Methode aufrufen
}

class Particle {
  float x, y; // Position
  color c;
  float vx, vy; // Geschwindigkeit
  
  Particle(float x, float y, color c) { // Konstruktor
    this.x = x;
    this.y = y;
    this.c = c;
  }
  
  void show() {
    fill(c); // Farbe des Partikels anwenden
    circle(x, y, 20);
  }
}
