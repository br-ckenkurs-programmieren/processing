float gravity = 0.08;
int amount = 500;
Particle p;
Particle[] particles;

void setup() {
  size(500, 500);
  particles = new Particle[amount];
}

void draw() {
  background(0);
  
  for (Particle p : particles) {
    if (p == null)
      continue;

    p.show();
    p.move();
  }
  
  if(mousePressed)
    loadNewParticles(10);
}

void loadNewParticles(int n) {
  int c = 0;

  for (int i = 0; i < particles.length; i++) {

    if (particles[i] == null || particles[i].isOutside()) {
      particles[i] = new Particle(mouseX, mouseY, color(random(255), random(255), random(255)));
      c++;
    }

    if (c >= n)
      return;
  }
}

class Particle {
  float x, y;
  color c;
  float vx, vy;

  Particle(float x, float y, color c) {
    this.x = x;
    this.y = y;
    this.c = c;
    this.vy = random(-10, -0.5);
    this.vx = random(-5, 5);
  }

  void show() {
    fill(c);
    circle(x, y, 20);
  }

  void move() {
    x += vx;
    y += vy;
    vy += gravity;
  }

  boolean isOutside() {
    return x < 0 || x > width || y < 0 || y > height;
  }
}
