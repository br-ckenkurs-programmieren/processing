float gravity = 0.08;
int amount = 2000;
Particle p;
Particle[] particles;

void setup() {
  size(500, 500);
  particles = new Particle[amount];
}

void draw() {
  background(0);

  for (Particle p : particles) {
    if (p == null)
      continue;

    p.show();
    p.move();
  }

  if (mousePressed)
    loadNewParticles(10);
}

void loadNewParticles(int n) {
  int c = 0;

  for (int i = 0; i < particles.length; i++) {

    if (particles[i] == null || particles[i].isOutside()) {
      particles[i] = new Particle(new PVector(mouseX, mouseY), color(20, 20, 100 + random(150)));
      c++;
    }

    if (c >= n)
      return;
  }
}

class Particle {
  PVector pos;
  PVector vel;
  color c;

  Particle(PVector pos, int c) {
    this.pos = pos;
    this.c = c;

    vel = PVector.fromAngle(random(TWO_PI));
  }

  void show() {
    fill(c);
    circle(pos.x, pos.y, 20);
  }

  void move() {
    pos.add(vel);
  }

  boolean isOutside() {
    return pos.x < 0 || pos.x > width || pos.y < 0 || pos.y > height;
  }
}
