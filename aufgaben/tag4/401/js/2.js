
var amount;

var particles;

function setup() {
    initializeFields();
    createCanvas(500, 500);
    particles = new Array(amount);
}

function draw() {
    background(0);
    particles.forEach(handleParticle);
    if (mouseIsPressed)
        loadNewParticles(10);
}

function handleParticle(i) {
    if (i === undefined)
        return;
    i.show();
    i.move();
}

function loadNewParticles(n) {
    var c = 0;
    for (var i = 0; i < particles.length; i++) {
        if (particles[i] === undefined || particles[i] === null || particles[i].isOutside()) {
            particles[i] = new Particle(new p5.Vector(mouseX, mouseY), color(20, 20, 100 + random(150)));
            c++;
        }
        if (c >= n)
            return;
    }
}

function initializeFields() {
    amount = 2000;
    particles = null;
    c = null;
}


class Particle {
    constructor(pos, c) {
        this.pos = pos;
        this.c = c;
        this.vel = p5.Vector.fromAngle(random(TWO_PI));
    }

    show() {
        fill(this.c);
        circle(this.pos.x, this.pos.y, 20);
    }

    move() {
        this.pos.add(this.vel);
    }

    isOutside() {
        return this.pos.x < 0 || this.pos.x > width || this.pos.y < 0 || this.pos.y > height;
    }
}
