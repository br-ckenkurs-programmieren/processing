
var gravity;

var amount;

var p;

var particles;

function setup() {
    initializeFields();
    createCanvas(500, 500);
    particles = new Array(amount);
}

function draw() {
    background(0);
    particles.forEach(handleParticle);
    if (mouseIsPressed)
        loadNewParticles(10);
}

function handleParticle(i) {
    if (i === undefined)
        return;
    i.show();
    i.move();
}

function loadNewParticles(n) {
    var c = 0;
    for (var i = 0; i < particles.length; i++) {
        if (particles[i] === undefined || particles[i] === null || particles[i].isOutside()) {
            particles[i] = new Particle(mouseX, mouseY, color(random(255), random(255), random(255)));
            c++;
        }
        if (c >= n)
            return;
    }
}

function initializeFields() {
    gravity = 0.08;
    amount = 500;
    p = null;
    particles = null;
    x = 0;
    y = 0;
    c = null;
    vx = 0;
    vy = 0;
}


class Particle {
    constructor(x, y, c) {
        this.x = x;
        this.y = y;
        this.c = c;
        this.vy = random(-10, -0.5);
        this.vx = random(-5, 5);
    }

    show() {
        fill(this.c);
        circle(this.x, this.y, 20);
    }

    move() {
        this.x += this.vx;
        this.y += this.vy;
        this.vy += gravity;
    }

    isOutside() {
        return this.x < 0 || this.x > width || this.y < 0 || this.y > height;
    }
}
