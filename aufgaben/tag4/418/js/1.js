
var s1, s2, s3;

var selected;

function setup() {
    initializeFields();
    createCanvas(600, 300);
    s1 = new Stack();
    s2 = new Stack();
    s3 = new Stack();
    for (var i = 6; i > 0; i--) s1.push(i);
    fill(0);
    rectMode(CENTER);
}

function draw() {
    background(255);
    drawTower(s1, 100);
    drawTower(s2, 300);
    drawTower(s3, 500);
}

function drawTower(s, x) {
    var arr = s.toArray();
    for (var i = 0; i < arr.length; i++) rect(x, height - (30 + i * 25), arr[i] * 30, 20);
    rect(x, height / 2, 10, height - 20);
}

function mouseReleased() {
    if (selected == null) {
        if (mouseX < 200)
            selected = s1;
        else if (mouseX < 400)
            selected = s2;
        else
            selected = s3;
    } else {
        let target;
        if (mouseX < 200)
            target = s1;
        else if (mouseX < 400)
            target = s2;
        else
            target = s3;

        if (validMove(selected, target))
            target.push(selected.pop());
        else
            print("Invalid move!");
        selected = null;
    }
}

function validMove(from, to) {
    if (from.size <= 0 || to.size >= 10)
        return false;
    if (to.size <= 0)
        return true;
    return from.values[from.size - 1] < to.values[to.size - 1];
}

class Stack {

    constructor() {
        this.values = new Array(10);
        this.size = 0;
    }

    push(i) {
        this.values[this.size++] = i;
    }

    pop() {
        return this.values[--this.size];
    }

    toArray() {
        var out = new Array(this.size);
        for (let i = 0; i < this.size; i++) out[i] = this.values[i];
        return out;
    }

}


function initializeFields() {
    selected = null;
}