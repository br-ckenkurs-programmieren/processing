class Stack {
  int[] values;
  int size;
  
  Stack() {
    values = new int[10];
    size = 0;
  }

  void push(int i) {
    values[size++] = i;
  }

  int pop() {
    return values[--size];
  }

  int[] toArray() {
    int[] out = new int[size];
    for (int i = 0; i < size; i++)
      out[i] = values[i];
    return out;
  }
}
