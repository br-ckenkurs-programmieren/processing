Stack s1, s2, s3;
Stack selected;

void setup() {
  size(600, 300);
  s1 = new Stack();
  s2 = new Stack();
  s3 = new Stack();

  for (int i = 6; i > 0; i--)
    s1.push(i);

  fill(0);
  rectMode(CENTER);
}

void draw() {
  background(255);

  drawTower(s1, 100);
  drawTower(s2, 300);
  drawTower(s3, 500);
}

void drawTower(Stack s, int x) {
  int[] arr = s.toArray();
  for (int i = 0; i < arr.length; i++)
    rect(x, height - (30 + i * 25), arr[i] * 30, 20);

  rect(x, height / 2, 10, height - 20);
}

void mouseReleased() {
  if (selected == null) {
    if (mouseX < 200)
      selected = s1;
    else if (mouseX < 400)
      selected = s2;
    else
      selected = s3;
  } else {

    Stack target;
    if (mouseX < 200)
      target = s1;
    else if (mouseX < 400)
      target = s2;
    else
      target = s3;

    target.push(selected.pop());
    selected = null;
  }
}

class Stack {
  int[] values;
  int size;

  Stack() {
    values = new int[10];
    size = 0;
  }

  void push(int i) {
    values[size++] = i;
  }

  int pop() {
    return values[--size];
  }

  int[] toArray() {
    int[] out = new int[size];
    for (int i = 0; i < size; i++)
      out[i] = values[i];
    return out;
  }
}
