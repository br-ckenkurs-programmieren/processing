int widthOfStreets;
TrafficLight[][] lights = new TrafficLight[2][2]; //Zweidimensional, Position in der Matrix entspricht Position auf dem Feld zB oben-links == [0][0], oben-rechts == [0][1] 
Car[] cars = new Car[2];
color green = color(0, 255, 0);
color red = color(255, 0, 0);
boolean horizontalMayDrive;

void setup(){
 size(800, 800); //Fenstergroesse 800px*800px
 widthOfStreets = height / 4;
 horizontalMayDrive = true;
 initCars();
 initLights();
}

void draw(){
  showCrossroads(); // Kreuzung darstellen
  showTrafficLights();// Ampeln im jeweiligen Modus darstellen
  moveCars();
  showCars();
}

void initCars(){
  
 for (int i = 0; i < cars.length; i++){
   int radius = (widthOfStreets/4); //Radius eines Autos, sodass sein Umfang eine Fahrbahnspur füllt
   int xStartVertical = (width/2)-(radius); // Mittig auf der vertikal nach unten laufenden Fahrspur 
   int yStartHorizontal = (height/2)+(radius); // Mittig auf der horizontal nach rechts laufenden Fahrspur 
   
   if(i % 2 == 0){ //jedes zweite Auto senkrecht
     cars[i] = new Car( 0, +4, xStartVertical, radius, radius); 
   }else{
     cars[i] = new Car(+4, 0, radius, yStartHorizontal, radius);
   }
 }
}

void initLights(){
  int trafficLightDia = widthOfStreets/3; // Durchmesser fur Ampeln
  int offset = widthOfStreets/4; // Rückt die Ampel für bessere Zuordnung etwas naeher an die Strasse, zu der sie gehoert
  //Ampel 00, oben links
  lights[0][0]= new TrafficLight(((width/2)-widthOfStreets)+offset, height/2-widthOfStreets, trafficLightDia);
  
  //Ampel 01, oben rechts
  lights[0][1] = new TrafficLight(((width/2)+widthOfStreets), height/2-widthOfStreets+offset, trafficLightDia);
  
  //Ampel 10, unten links
  lights[1][0] = new TrafficLight(((width/2)-widthOfStreets), height/2+widthOfStreets-offset, trafficLightDia);
  
  //Ampel 11, unten rechts 
  lights[1][1] = new TrafficLight(((width/2)+widthOfStreets)-offset, height/2+widthOfStreets, trafficLightDia);
}
void showCars(){
 for( int i = 0; i < cars.length; i++){
   fill(cars[i].farbe);
   circle(cars[i].x, cars[i].y, cars[i].radius);
 }
}

void moveCars(){
  for( int i = 0; i < cars.length; i++){
   cars[i].move();
   cars[i].changeLaneIfAtBorder();
 }
}

void showCrossroads(){
  noStroke(); // Damit die Grenzen der ueberkreuzten Formen nicht sichtbar aufeinander liegen
  background(80,200,80); // grassgruener Hintergrund
  
  //Straßen
  fill(119, 122, 120);   // Asphaltgrau fuer Strassen
  rect(0, (height - widthOfStreets) / 2, width, widthOfStreets); // waagerechte Strasse
  rect((width - widthOfStreets) / 2, 0, widthOfStreets, height); // senkrechte Strasse
  
  // Markierlinien
  float widthOfMarks; // Breite der Markierungen
  widthOfMarks = widthOfStreets / 10; // Errechnet Breite in Anhaengigkeit von Strassenbreite
  float lengthOfHorizontalMarks; // Laenge der Markierungen fuer die waagerechte Strasse
  int nMarks = 8; // Anzahl der Markierungen pro Straße
  lengthOfHorizontalMarks = width / (nMarks * 2); // Errechnet die passende Laenge der Markierungen und Raeume zwischen ihnen
  float lengthOfVerticalMarks; // Laenge der Markierungen auf der senkrechten Strasse
  lengthOfVerticalMarks = height / (nMarks * 2); //Errechnet die passende Laenge der Markierungen und Raeume zwischen ihnen auf der senkrechten Strasse 
  fill(255); // Weiss fuer Strassenmakierungen
  for(int i = 0; i < nMarks; i++){
    // waaagerechte Markierungen
    rect((i * 2 * lengthOfHorizontalMarks)+(lengthOfHorizontalMarks/2), (height - widthOfMarks) / 2, lengthOfHorizontalMarks, widthOfMarks);
    // senkrechte Markierungen
    rect((width - widthOfMarks)/2, i * 2 * lengthOfVerticalMarks +(lengthOfVerticalMarks/2), widthOfMarks, lengthOfVerticalMarks);
  }
}

void showTrafficLights(){
  stroke(8);
  fill(50);
   color horizontalColor;
   color verticalColor;
  
    if(horizontalMayDrive){ // Ampelmodus
    horizontalColor = green;
    verticalColor = red;
    }else{
      horizontalColor = red;
     verticalColor = green;
    }
  fill(horizontalColor); // Färben der Ampeln oben links und unten rechts
  lights[0][0].showTrafficLight();
  lights[1][1].showTrafficLight();
  
  fill(verticalColor); // Färben der Ampeln oben rechts und unten links
  lights[0][1].showTrafficLight();
  lights[1][0].showTrafficLight();
 
}

void keyPressed(){ //Schaltet bei Tastendruck Ampeln um
  horizontalMayDrive = !horizontalMayDrive; // Verkehrt den Modus, wer gerade Fahren darf
}
