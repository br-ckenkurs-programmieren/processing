class Car {
  int vx;
  int vy;
  int x;
  int y;
  int radius;
  color farbe;

  Car(int vx, int vy, int x, int y, int carRadius) {
    this.vx = vx;
    this.vy = vy;
    this.x = x;
    this.y = y;
    this.radius = carRadius;
    this.farbe = color(50, 50, 170); //Dunkles Blau - natuerlich kann hier die Autofarbe auch zufällig festgelegt werden.
  }
  
  boolean horizontalDriving(){
    return (this.vx != 0);
  }

  void move() {

    if (horizontalMayDrive) {
      
      // horizontal fahrendes Auto?
      if (this.horizontalDriving()) { 
        // An einer der horizontal fahrende Autos stoppenden Ampeln?
        if (lights[1][0].inMyStopzone(this.x, this.y) || lights[0][1].inMyStopzone(this.x, this.y)) {
          //Do NOT move!!!
        } else {
          this.x = this.x + this.vx; // Nicht in Stoppzone: horizontale Bewegung weiterhin erlaubt
        }
      } else{
        //vertikal fahrende Autos duerfen fahren
        this.y = this.y + this.vy;
      }
    } else {

      // senkrecht fahrendes Auto?
      if (!this.horizontalDriving()) {
        //An einer der verikal fahrende Autos stoppenden Ampeln?
        if (lights[0][0].inMyStopzone(this.x, this.y) || lights[1][1].inMyStopzone(this.x, this.y)) {
          // Do NOT move!!!
        } else {
          this.y = this.y + vy; // Nicht in Stoppzone: vertikale Bewegung weiterhin erlaubt
        }
      } else{
        //horizontal fahren erlaubt
        this.x = this.x + this.vx;
      }
    }
  }
  
  void changeLaneIfAtBorder(){
    if(this.x <= -radius || this.x >= width+radius){ //Auto horizontal aus Bild gefahren?
      this.changeLane();
    }else if(this.y <= -radius || this.y >= height+radius){ //Auto senkrecht aus Bild gefahren?
      this.changeLane();
    }
  }
  
  void changeLane(){
    //Auto wechselt die Strassenseite und faehrt in entgegengesetzter Richtung weiter
    if(this.horizontalDriving()){
      this.vx = -(this.vx); // Kehre Fahrtrichtung um
      if(this.y < (height/2)){
        this.y = (height/2) + radius; // setze Auto auf untere Strassenseite
      } else{
         this.y = (height/2) - radius; // setze Auto auf obere Strassenseite
      }
    } else{
      this.vy = -(this.vy); // Kehre Fahrtrichtung um
      if(this.x < (width/2)){
        this.x = (width/2) + radius;  // setze Auto auf rechte Strassenseite
      }else{
        this.x = (width/2) - radius;  // setze Auto auf linke Strassenseite
      }
    }
  }
}
