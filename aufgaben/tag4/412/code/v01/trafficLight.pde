class TrafficLight{
  float xPos;
  float yPos;
  int diameter;

  TrafficLight(float x, float y, int d){
    this.xPos = x;
    this.yPos = y;
    this.diameter = d;
  }
  
  void showTrafficLight(){
   circle(this.xPos, this.yPos, diameter); 
  }
  
  boolean inMyStopzone(float carX, float carY){
   if(this.xPos < width/2 && carX < width/2){ // Ampel auf linker Seite und Auto auf linker Seite
     if( carY > ((height/2)-(widthOfStreets/2))-this.diameter && carY < (height/2)-(widthOfStreets/2)){ // in Stoppzone vor der Kreuzung links oben (vertikale Stoppzone)
        return true;
     } else if(carX > ((width/2)-(widthOfStreets/2))-this.diameter && carX < (width/2)-(widthOfStreets/2) && (carY > height/2)){ //in Stoppzone vor der Kreuzung links unten (horizontale Stoppzone)
       return true;
     }
   } else if (this.xPos > width/2 && carX > width/2){ // Ampel auf rechter Seite und Auto auf rechter Seite
     if(carY < ((height/2)+widthOfStreets/2)+this.diameter && carY > (height/2)-(widthOfStreets/2)){ // in Stoppzone vor der Kreuzung rechts unten (vertikale Stoppzone)
       return true;
     }else if (carX > ((width/2)-(widthOfStreets/2))-this.diameter && carX < (width/2)-(widthOfStreets/2) &&(carX >width/2)){ // in Stoppzone vor Kreuzung rechts oben (horizontale Stoppzone)
       return true;
     }
   }
   return false; // Wenn nicht vorher durch eine return true die Methode verlassen wurde, dann ist das Auto nicht in der entsprechenden Stopzone
  } 
}
