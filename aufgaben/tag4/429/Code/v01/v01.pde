//Eine Liste Aller Menschen/Zombies
ArrayList<Entity> entities = new ArrayList<Entity>();
//Die Größe der Entitäten
final int size = 32;

//Chance das ein Mensch einen Zombie tötet (0.3f => 30%)
float humanKillZombiePercent = 0.3f;

void setup() {
  size(1280, 720);
}

void draw() {
  background(255);

  //Erst alle Positionen/Aktionen der Entitäten berechnen, im Anschluss werden sie gezeichnet
  for (Entity ent : entities) {
    ent.update();
  }
  overlapCheck();
  for (Entity ent : entities) {
    ent.draw();
  }
}

void mousePressed() {
  //Platzieren von Menschen bei Linksklick
  if (mouseButton == LEFT) {
    entities.add(new Entity(mouseX, mouseY, size, size));
  }
  //Infizieren von Menschen bei Rechtsklick
  if (mouseButton == RIGHT) {
    for (Entity ent : entities) {
      if (ent.isMouseOver()) {
        ent.setInfected(true);
      }
    }
  }
}
//Findet die Entität mit der kleinsten Entfernung zur übergebenen Entität und dessen Status
Entity getClosestEntity(boolean infected, Entity self) {
  Entity closest = null;
  float closestDist = 999999999;
  for (Entity ent : entities) {
    if (ent != self && ent.isInfected() == infected) {
      float dist = dist(ent.x, ent.y, self.x, self.y);
      if (dist < closestDist) {
        closestDist = dist;
        closest = ent;
      }
    }
  }
  return closest;
}


void overlapCheck() {
outer:
  for (int i=0; i<entities.size(); i++) {
    for (int j=0; j<entities.size(); j++) {
      if (i!=j) {
        Entity ent1 = entities.get(i);
        Entity ent2 = entities.get(j);
        if (ent1.overlaps(ent2)) {
          boolean killed = random(1) < humanKillZombiePercent;
          if (ent1.isInfected() && !ent2.isInfected() ) {
            if (killed) {
              entities.remove(ent1);
              i--;
              continue outer;
            } else {
              ent2.setInfected(true);
            }
          }
        }
      }
    }
  }
}
