class Entity {

  //Position and Size of the Entity
  int x, y, w, h;
  //If the Entity is infected(Zombiefied)
  boolean infected = false;
  //Movement Speed
  float speed = 1f;

  //Current Target Position
  PVector moveTarget = new PVector();


  //Constructor
  public Entity(int x, int y, int w, int h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    moveTarget.x = random(0, width);
    moveTarget.y = random(0, height);
  }

  //Draw the Entity, color depending on if it is infected or not
  public void draw() {
    if (infected) {
      fill(color(0, 255, 0));
    } else {
      fill(color(0, 0, 0));
    }
    noStroke();
    ellipse(x, y, w, h);
  }

  //Set the Entity Infected
  public void setInfected(boolean infected) {
    this.infected = infected;
    Entity closest = getClosestEntity(false, this);
    if (closest != null) {
      if (dist(x, y, closest.x, closest.y) < 100) {
        moveTarget.x = closest.x;
        moveTarget.y = closest.y;
      } else {
        if (dist(x, y, moveTarget.x, moveTarget.y) < 10) {
          moveTarget.x = random(0, width);
          moveTarget.y = random(0, height);
        }
      }
    }
  }
  //Calculates the "AI" and the moves the Entity
  public void update() {
    calculateMovement();
    if (moveTarget.x > x) {
      x += speed;
    } else if (moveTarget.x < x) {
      x -= speed;
    }
    if (moveTarget.y > y) {
      y += speed;
    } else if (moveTarget.y < y) {
      y -= speed;
    }
  }
  //"AI" for the Entity depending on InfectionStatus, Distance to other Entities and recalculates if it reached the Target
  public void calculateMovement() {
    if (infected) {
      Entity closest = getClosestEntity(false, this);
      if (closest == null) return;
      if (dist(x, y, closest.x, closest.y) < 100) {
        moveTarget.x = closest.x;
        moveTarget.y = closest.y;
      } else {
        if (dist(x, y, moveTarget.x, moveTarget.y) < 10) {
          moveTarget.x = random(0, width);
          moveTarget.y = random(0, height);
        }
      }
    } else {
      if (dist(x, y, moveTarget.x, moveTarget.y) < 10) {
        moveTarget.x = random(0, width);
        moveTarget.y = random(0, height);
      }
    }
  }

  public boolean overlaps(Entity e1) {
    return (x < e1.x + e1.w &&
      x + w > e1.x &&
      y < e1.y + e1.h &&
      y + h > e1.y);
  }

  //If the mouse is over the entity AABB
  public boolean isMouseOver() {
    return mouseX > x-w/2 && mouseX < x+w/2 && mouseY > y-h/2 && mouseY < y+h/2;
  }
  //Returns whether the Entity is infected or not
  public boolean isInfected() {
    return infected;
  }
}
