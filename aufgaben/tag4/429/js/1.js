class Entity {

  //Constructor
  constructor(x, y, w, h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.speed = 1;
    this.infected = false;
    this.moveTarget = createVector(0,0);
    this.moveTarget.x = random(0, width);
    this.moveTarget.y = random(0, height);
  }

  //Draw the Entity, color depending on if it is infected or not
  draw() {
    if (this.infected) {
      fill(color(0, 255, 0));
    } else {
      fill(color(0, 0, 0));
    }
    noStroke();
    ellipse(this.x, this.y, this.w, this.h);
  }

  //Set the Entity Infected
  setInfected(infected) {
    this.infected = infected;
    let closest = getClosestEntity(false, this);
    if (closest != null) {
      if (dist(this.x, this.y, closest.x, closest.y) < 100) {
        this.moveTarget.x = closest.x;
        this.moveTarget.y = closest.y;
      } else {
        if (dist(this.x, this.y, this.moveTarget.x, this.moveTarget.y) < 10) {
          this.moveTarget.x = random(0, width);
          this.moveTarget.y = random(0, height);
        }
      }
    }
  }
  calculateMovement() {
    if (this.infected) {
      let closest = getClosestEntity(false, this);
      if (closest == null){ return;}
      if (dist(this.x, this.y, closest.x, closest.y) < 100) {
        this.moveTarget.x = closest.x;
        this.moveTarget.y = closest.y;
      } else {
        if (dist(this.x, this.y, this.moveTarget.x, this.moveTarget.y) < 10) {
          this.moveTarget.x = random(0, width);
          this.moveTarget.y = random(0, height);
        }
      }
    } else {
      if (dist(this.x, this.y, this.moveTarget.x, this.moveTarget.y) < 10) {
        this.moveTarget.x = random(0, width);
        this.moveTarget.y = random(0, height);
      }
    }
  }
  //Calculates the "AI" and the moves the Entity
  update() {
    this.calculateMovement();
    if (this.moveTarget.x > this.x) {
      this.x += this.speed;
    } else if (this.moveTarget.x < this.x) {
      this.x -= this.speed;
    }
    if (this.moveTarget.y > this.y) {
      this.y += this.speed;
    } else if (this.moveTarget.y < this.y) {
      this.y -= this.speed;
    }
  }
  //"AI" for the Entity depending on InfectionStatus, Distance to other Entities and recalculates if it reached the Target


  overlaps(e1) {
    return (this.x < e1.x + e1.w &&
        this.x + this.w > e1.x &&
        this.y < e1.y + e1.h &&
        this.y + this.h > e1.y);
  }

  //If the mouse is over the entity AABB
  isMouseOver() {
    return mouseX > this.x-this.w/2 && mouseX < this.x+this.w/2 && mouseY > this.y-this.h/2 && mouseY < this.y+this.h/2;
  }
  //Returns whether the Entity is infected or not
  isInfected() {
    return this.infected;
  }
}


//Eine Liste Aller Menschen/Zombies
let entities = [];
//Die Größe der Entitäten
const size = 32;

//Chance das ein Mensch einen Zombie tötet (0.3f => 30%)
let humanKillZombiePercent = 0.3;

function setup() {
  createCanvas(1280, 720);
}


function draw() {
  background(255);
  //Erst alle Positionen/Aktionen der Entitäten berechnen, im Anschluss werden sie gezeichnet
  entities.forEach(function(ent) {
    ent.update()
  }
  );

  overlapCheck();
  entities.forEach(function(ent) {
    ent.draw()
  }
  );
}
function mousePressed() {
  //Platzieren von Menschen bei Linksklick
  if (mouseButton == LEFT) {
    entities.push(new Entity(mouseX, mouseY, size, size));
  }
  //Infizieren von Menschen bei Rechtsklick
  if (mouseButton == RIGHT) {
    entities.forEach(function(ent) {
      entities.forEach(function(ent) {
        if (ent.isMouseOver()) {
          ent.setInfected(true);
        }
      }
      );
    }
    );
  }
}

  //Findet die Entität mit der kleinsten Entfernung zur übergebenen Entität und dessen Status
  function getClosestEntity(infected,self) {
    let closest = null;
    let closestDist = 999999999;
    entities.forEach(function(ent) {
      if (ent != self && ent.isInfected() == infected) {
        var dis = dist(ent.x, ent.y, self.x, self.y);
        if (dis < closestDist) {
          closestDist = dis;
          closest = ent;
        }
      }
    });
    return closest;
  }

  function overlapCheck() {
  outer:
    for (let i=0; i<entities.length; i++) {
      for (let j=0; j<entities.length; j++) {
        if (i!=j) {
          let ent1 = entities[i];
          let ent2 = entities[j];
          if (ent1.overlaps(ent2)) {
            let killed = random(1) < humanKillZombiePercent;
            if (ent1.isInfected() && !ent2.isInfected() ) {
              if (killed) {
                entities.splice(i,1)
                i--;
                continue outer;
              } else {
                ent2.setInfected(true);
              }
            }
          }
        }
      }
    }
  }
