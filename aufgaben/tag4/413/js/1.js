class Point {

  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  intersection (r1, p2, r2) {
    var dis = dist(this.x, this.y, p2.x, p2.y);

    var b = new Point(p2.x - this.x, p2.y - this.y);

    var a = (sq(r1) - sq(r2) + sq(dis)) / (2 * dis);

    var h = sqrt(r1 * r1 - a * a);

    var xm = this.x + a*b.x/dis;
    var ym = this.y + a*b.y/dis;

    var xs = xm + h*b.y/dis;
    var ys = ym - h*b.x/dis;

    return new Point(xs, ys);
  }
}

var d;
var mid;
var inter = [];

var s;

function setup() {
  createCanvas(400, 400);
  background(255);
  noFill();
  strokeWeight(3);
  stroke(0);
  noLoop();
  
  //Berechnen von Durchmesser und mittleren Kreis
  d = height/4;
  mid = new Point(width/2, height/2);
}

function draw() {
  
  
  //Zeichnen mittlerer Kreis
  ellipse(mid.x, mid.y, d, d);
  
  //Zeichnen innerer Ring
  innerCircle();
  
  //Zeichnen äußerer Ring
  outerCircle();
  outerCircle();
}

function innerCircle() {
  inter[0] = new Point(width/2, height/2 - d/2);
  circle(inter[0].x, inter[0].y, d);
  
  for (var i = 1; i < 6; i++) {
    inter[i] = mid.intersection(d/2, inter[i-1], d/2);
    circle(inter[i].x, inter[i].y, d);
  }
}

function outerCircle() {
  var j = inter.length - 1;

  var temp =  [];
  for (var i = 0; i < 6; i++) {
    temp[i] = inter[i].intersection(d/2, inter[j], d/2);
    circle(temp[i].x, temp[i].y, d);
    if (j == inter.length - 1) j = 0;
    else j++;
  }

  inter = temp;
}


