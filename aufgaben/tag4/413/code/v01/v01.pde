void setup() {
  size(400, 400);
  background(255);
  noFill();
  noLoop();
  counter = 0;
}

int counter;

void draw() {

  ellipse(200, 200, 100, 100);

  intersection(200, 200, 50, 200, 150, 50);
}

void intersection(float x1, float y1, float r1, float x2, float y2, float r2) {

  counter ++;

  float d = dist(x1, y1, x2, y2);

  float dx = x2 - x1;
  float dy = y2 - y1;

  float a = (sq(r1) - sq(r2) + sq(d)) / (2 * d);

  float h = sqrt(r1 * r1 - a * a);

  float xm = x1 + a*dx/d;  
  float ym = y1 + a*dy/d;

  float xs1 = xm + h*dy/d;
  float xs2 = xm - h*dy/d;

  float ys1 = ym - h*dx/d;
  float ys2 = ym + h*dx/d;

  circle(xs1, ys1, 100);
  if (counter < 6) intersection(xs1, ys1, 50, 200, 200, 50);
}
