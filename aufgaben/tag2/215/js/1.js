//      ---Variablen---
var s = 0;
var m = 0;
var h = 0;

var secOld = 0;  //Für das flackern der Punkte
var pointOnOff = false;

var displayOff, displayOn;

function setup() {
  createCanvas(1500, 500);
  displayOff = color(50, 0, 0);
  displayOn = color(255, 0, 0);
}

function draw() {
  //      ---Berechnung---
  getTime();  //Zeitvariablen aktuallisieren
  var hour10 = (h - (h % 10)) / 10;  //Vordere Ziffer von hour
  var hour1 = h % 10;   //Hintere Ziffer von hour

  var min10 = (m - (m % 10)) / 10;    //Vordere Ziffer von min
  var min1 = m % 10;     //Hintere Ziffer von min

  var sec10 = (s - (s % 10)) /10;
  var sec1 = s % 10;
  
  //      ---Zeichnen---
  //Uhr
  background(30, 120, 235);
  fill(0, 0, 0);
  stroke(255, 255, 255);
  var innerPad = 20 * 2;
  strokeWeight(innerPad/2);
  rectMode(CENTER);
  rect(width/2, height/2, width * 2/3, height * 2/3);

  //Ziffern
  var p = 40;  //Abstands Parameter
  var l = (height * 2/3 - p*2 - innerPad*2)/2; //Länge der Segmente parametriesiert

  //Ziffern Hintergrund
  drawNumeral(8, width/2 - l/2 - l*2 - p/2 - p*4, height/2, l, displayOff);
  drawNumeral(8, width/2 - l/2 - l - p/2 - p*3, height/2, l, displayOff);
  drawNumeral(8, width/2 + l/2 + p/2, height/2, l, displayOff);
  drawNumeral(8, width/2 - l/2 - p/2, height/2, l, displayOff);
  drawNumeral(8, width/2 + l/2 + l + p/2 + p*3, height/2, l, displayOff);
  drawNumeral(8, width/2 + l/2 + l*2 + p/2 + p*4, height/2, l, displayOff);

  //Ziffern
  drawNumeral(hour10, width/2 - l/2 - l*2 - p/2 - p*4, height/2, l, displayOn);
  drawNumeral(hour1, width/2 - l/2 - l - p/2 - p*3, height/2, l, displayOn);
  drawNumeral(min10, width/2 - l/2 - p/2, height/2, l, displayOn);
  drawNumeral(min1, width/2 + l/2 + p/2, height/2, l, displayOn);
  drawNumeral(sec10, width/2 + l/2 + l + p/2 + p*3, height/2, l, displayOn);
  drawNumeral(sec1, width/2 + l/2 + l*2 + p/2 + p*4, height/2, l, displayOn);
  
  //Doppelpunkte
  if (s != secOld) {
    pointOnOff = !pointOnOff;
    secOld = s;
  }
  if (pointOnOff) {
    fill(displayOn);
  } else {
    fill(displayOff);
  }

  circle(width/2 + l + p*2, height/2 - l/2, p * 2/3);
  circle(width/2 + l + p*2, height/2 + l/2, p * 2/3);
  circle(width/2 - l - p*2, height/2 - l/2, p * 2/3);
  circle(width/2 - l - p*2, height/2 + l/2, p * 2/3);
}


function getTime() { //Aktuallisieren der Zeitvariablen
  s = second();
  m= minute();
  h = hour();
}

function drawNumeral(numeral, x, y, l, c) {  //Zeichnen eines Displays
  var h = l * 1/8;  //Höhe der Segmente

  //Koordinaten für die einzelnen Segmente
  var xUp = x;
  var yUp = y -l;

  var xDown = x;
  var yDown = y + l;

  var xLeftUp = x - l/2;
  var yLeftUp = y + l/2;

  var xLeftDown = x - l/2;
  var yLeftDown = y - l/2;

  var xRightUp = x + l/2;
  var yRightUp = y + l/2;

  var xRightDown = x + l/2;
  var yRightDown =  y - l/2;

  noStroke();
  rectMode(CENTER);

  switch(numeral) {
  case 0:
    segmentUp(xUp, yUp, l, h, c);
    segmentDown(xDown, yDown, l, h, c);
    segmentLeft(xLeftUp, yLeftUp, l, h, c);
    segmentLeft(xLeftDown, yLeftDown, l, h, c);
    segmentRight(xRightUp, yRightUp, l, h, c);
    segmentRight(xRightDown, yRightDown, l, h, c);
    break;

  case 1:
    segmentRight(xRightUp, yRightUp, l, h, c);
    segmentRight(xRightDown, yRightDown, l, h, c);
    break;

  case 2:
    segmentUp(xUp, yUp, l, h, c);
    segmentDown(xDown, yDown, l, h, c);
    segmentLeft(xLeftUp, yLeftUp, l, h, c);
    segmentRight(xRightDown, yRightDown, l, h, c);
    segmentMid(x, y, l, h, c);
    break;

  case 3:
    segmentUp(xUp, yUp, l, h, c);
    segmentDown(xDown, yDown, l, h, c);
    segmentRight(xRightUp, yRightUp, l, h, c);
    segmentRight(xRightDown, yRightDown, l, h, c);
    segmentMid(x, y, l, h, c);
    break;

  case 4:
    segmentLeft(xLeftDown, yLeftDown, l, h, c);
    segmentRight(xRightUp, yRightUp, l, h, c);
    segmentRight(xRightDown, yRightDown, l, h, c);
    segmentMid(x, y, l, h, c);
    break;

  case 5:
    segmentUp(xUp, yUp, l, h, c);
    segmentDown(xDown, yDown, l, h, c);
    segmentLeft(xLeftDown, yLeftDown, l, h, c);
    segmentRight(xRightUp, yRightUp, l, h, c);
    segmentMid(x, y, l, h, c);
    break;

  case 6:
    segmentUp(xUp, yUp, l, h, c);
    segmentDown(xDown, yDown, l, h, c);
    segmentLeft(xLeftUp, yLeftUp, l, h, c);
    segmentLeft(xLeftDown, yLeftDown, l, h, c);
    segmentRight(xRightUp, yRightUp, l, h, c);
    segmentMid(x, y, l, h, c);
    break;

  case 7:
    segmentUp(xUp, yUp, l, h, c);
    segmentRight(xRightUp, yRightUp, l, h, c);
    segmentRight(xRightDown, yRightDown, l, h, c);
    break;

  case 8:
    segmentUp(xUp, yUp, l, h, c);
    segmentDown(xDown, yDown, l, h, c);
    segmentLeft(xLeftUp, yLeftUp, l, h, c);
    segmentLeft(xLeftDown, yLeftDown, l, h, c);
    segmentRight(xRightUp, yRightUp, l, h, c);
    segmentRight(xRightDown, yRightDown, l, h, c);
    segmentMid(x, y, l, h, c);
    break;

  case 9:
    segmentUp(xUp, yUp, l, h, c);
    segmentDown(xDown, yDown, l, h, c);
    segmentLeft(xLeftDown, yLeftDown, l, h, c);
    segmentRight(xRightUp, yRightUp, l, h, c);
    segmentRight(xRightDown, yRightDown, l, h, c);
    segmentMid(x, y, l, h, c);
    break;
  default:
    break;
  }
}

function segmentUp(x, y, l, h, c) {
  fill(c);
  rect(x, y, l, h);
  fill(0);
  triangle(x - l/2, y - h/2, x - l/2, y + h/2, x - l/2 + h, y + h/2);
  triangle(x + l/2, y - h/2, x + l/2, y + h/2, x + l/2 - h, y + h/2);
}

function segmentDown(x, y, l, h, c) {
  fill(c);
  rect(x, y, l, h);
  fill(0);
  noStroke();
  triangle(x - l/2, y - h/2, x - l/2, y + h/2, x - l/2 + h, y - h/2);
  triangle(x + l/2, y - h/2, x + l/2, y + h/2, x + l/2 - h, y - h/2);
}

function segmentLeft(x, y, l, h, c) {
  fill(c);
  rect(x, y, h, l);
  fill(0);
  triangle(x - h/2, y - l/2, x + h/2, y - l/2 + h, x - h/2 + h, y - l/2);
  triangle(x - h/2, y + l/2, x + h/2, y + l/2 - h, x - h/2 + h, y + l/2);
}

function segmentRight(x, y, l, h, c) {
  fill(c);
  rect(x, y, h, l);
  fill(0);
  triangle(x - h/2, y + l/2, x - h/2, y + l/2 - h, x - h/2 + h, y + l/2);
  triangle(x - h/2, y - l/2, x - h/2, y - l/2 + h, x - h/2 + h, y - l/2);
}

function segmentMid(x, y, l, h, c) {
  fill(c);
  rect(x, y, l, h);
  fill(0);
  triangle(x - l/2, y, x - l/2, y + h/2, x -l/2 + h/2, y +h/2);
  triangle(x - l/2, y, x - l/2, y - h/2, x -l/2 + h/2, y -h/2);
  triangle(x + l/2, y, x + l/2, y + h/2, x + l/2 - h/2, y +h/2);
  triangle(x + l/2, y, x + l/2, y - h/2, x + l/2 - h/2, y -h/2);
}