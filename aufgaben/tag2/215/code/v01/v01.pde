//      ---Variablen---
int sec = 0;
int min = 0;
int hour = 0;

//Für das flackern der Punkte
int secOld = 0; 
boolean pointBool = false;

void setup() {
  size(1500, 500);
}

void draw() {
  //      ---Berechnung---
  getTime();  //Zeitvariablen aktuallisieren
  int hour10 = hour / 10;  //Vordere Ziffer von hour
  int hour1 = hour % 10;   //Hintere Ziffer von hour

  int min10 = min / 10;    //Vordere Ziffer von min
  int min1 = min % 10;     //Hintere Ziffer von min
  
  int sec10 = sec /10;
  int sec1 = sec % 10;

  //      ---Zeichnen---
  //Uhr
  background(30, 120, 235);
  fill(0, 0, 0);
  stroke(255, 255, 255);
  int innerPad = 20 * 2;
  strokeWeight(innerPad/2);
  rectMode(CENTER);
  rect(width/2, height/2, width * 2/3, height * 2/3);

  //Ziffern
  int p = 40;  //Abstands Parameter
  int l = (height * 2/3 - p*2 - innerPad*2)/2; //Länge der Segmente parametriesiert
  
  //Ziffern Hintergrund ausgebleicht
  drawNumeral(8, width/2 - l/2 - l*2 - p/2 - p*4, height/2, l, true);
  drawNumeral(8, width/2 - l/2 - l - p/2 - p*3, height/2, l, true);
  drawNumeral(8, width/2 + l/2 + p/2, height/2, l, true);
  drawNumeral(8, width/2 - l/2 - p/2, height/2, l, true);
  drawNumeral(8, width/2 + l/2 + l + p/2 + p*3, height/2, l, true);
  drawNumeral(8, width/2 + l/2 + l*2 + p/2 + p*4, height/2, l, true);
  
  //Ziffern
  drawNumeral(hour10, width/2 - l/2 - l*2 - p/2 - p*4, height/2, l, false);
  drawNumeral(hour1, width/2 - l/2 - l - p/2 - p*3, height/2, l, false);
  drawNumeral(min1, width/2 + l/2 + p/2, height/2, l, false);
  drawNumeral(min10, width/2 - l/2 - p/2, height/2, l, false);
  drawNumeral(sec10, width/2 + l/2 + l + p/2 + p*3, height/2, l, false);
  drawNumeral(sec1, width/2 + l/2 + l*2 + p/2 + p*4, height/2, l, false);
  
  //Doppelpunkte
  if(sec > secOld){
    pointBool = !pointBool;
    secOld = sec;
  }
  if(pointBool){
    fill(255, 0, 0);
  }
  else{
    fill(50, 0, 0);
  }
  
  noStroke();
  circle(width/2 + l + p*2, height/2 - l/2, p * 2/3);
  circle(width/2 + l + p*2, height/2 + l/2, p * 2/3);
  circle(width/2 - l - p*2, height/2 - l/2, p * 2/3);
  circle(width/2 - l - p*2, height/2 + l/2, p * 2/3);
}


void getTime() {
  //Aktuallisieren der Zeitvariablen
  sec = second();
  min = minute();
  hour = hour();
}

void drawNumeral(int number, int x, int y, int l, boolean background) {
  int h = l * 1/8;
  
  int xUp = x;
  int yUp = y -l;
  
  int xDown = x;
  int yDown = y + l;
  
  int xLeftUp = x - l/2;
  int yLeftUp = y + l/2;
  
  int xLeftDown = x - l/2;
  int yLeftDown = y - l/2;
  
  int xRightUp = x + l/2; 
  int yRightUp = y + l/2; 
  
  int xRightDown = x + l/2;
  int yRightDown =  y - l/2;
  
  switch(number) {
  case 0:
    segmentUp(xUp, yUp, l, h, background);
    segmentDown(xDown, yDown, l, h, background);
    segmentLeft(xLeftUp, yLeftUp, l, h, background);
    segmentLeft(xLeftDown, yLeftDown, l, h, background);
    segmentRight(xRightUp, yRightUp, l, h, background);
    segmentRight(xRightDown,yRightDown, l, h, background);
    break;

  case 1:
    segmentRight(xRightUp, yRightUp, l, h, background);
    segmentRight(xRightDown,yRightDown, l, h, background);
    break;

  case 2:
    segmentUp(xUp, yUp, l, h, background);
    segmentDown(xDown, yDown, l, h, background);
    segmentLeft(xLeftUp, yLeftUp, l, h, background);
    segmentRight(xRightDown,yRightDown, l, h, background);
    segmentMid(x, y, l, h, background);
    break;

  case 3:
    segmentUp(xUp, yUp, l, h, background);
    segmentDown(xDown, yDown, l, h, background);
    segmentRight(xRightUp, yRightUp, l, h, background);
    segmentRight(xRightDown,yRightDown, l, h, background);
    segmentMid(x, y, l, h, background);
    break;

  case 4:
    segmentLeft(xLeftDown, yLeftDown, l, h, background);
    segmentRight(xRightUp, yRightUp, l, h, background);
    segmentRight(xRightDown,yRightDown, l, h, background);
    segmentMid(x, y, l, h, background);
    break;

  case 5:
    segmentUp(xUp, yUp, l, h, background);
    segmentDown(xDown, yDown, l, h, background);
    segmentLeft(xLeftDown, yLeftDown, l, h, background);
    segmentRight(xRightUp, yRightUp, l, h, background);
    segmentMid(x, y, l, h, background);
    break;

  case 6:
    segmentUp(xUp, yUp, l, h, background);
    segmentDown(xDown, yDown, l, h, background);
    segmentLeft(xLeftUp, yLeftUp, l, h, background);
    segmentLeft(xLeftDown, yLeftDown, l, h, background);
    segmentRight(xRightUp, yRightUp, l, h, background);
    segmentMid(x, y, l, h, background);
    break;

  case 7:
    segmentUp(xUp, yUp, l, h, background);
    segmentRight(xRightUp, yRightUp, l, h, background);
    segmentRight(xRightDown,yRightDown, l, h, background);
    break;

  case 8:
    segmentUp(xUp, yUp, l, h, background);
    segmentDown(xDown, yDown, l, h, background);
    segmentLeft(xLeftUp, yLeftUp, l, h, background);
    segmentLeft(xLeftDown, yLeftDown, l, h, background);
    segmentRight(xRightUp, yRightUp, l, h, background);
    segmentRight(xRightDown,yRightDown, l, h, background);
    segmentMid(x, y, l, h, background);
    break;

  case 9:
    segmentUp(xUp, yUp, l, h, background);
    segmentDown(xDown, yDown, l, h, background);
    segmentLeft(xLeftDown, yLeftDown, l, h, background);
    segmentRight(xRightUp, yRightUp, l, h, background);
    segmentRight(xRightDown,yRightDown, l, h, background);
    segmentMid(x, y, l, h, background);
    break;
  default:
    break;
  }
}

void segmentUp(int x, int y, int l, int h, boolean background) {
  if (background) {
    fill(50, 0, 0);
  } else {
    fill(255, 0, 0);
  }
  noStroke();
  rectMode(CENTER);
  rect(x, y, l, h);
  fill(0, 0, 0);
  triangle(x - l/2, y - h/2, x - l/2, y + h/2, x - l/2 + h, y + h/2);
  triangle(x + l/2, y - h/2, x + l/2, y + h/2, x + l/2 - h, y + h/2);
}

void segmentDown(int x, int y, int l, int h, boolean background) {
  if (background) {
    fill(50, 0, 0);
  } else {
    fill(255, 0, 0);
  }
  rectMode(CENTER);
  rect(x, y, l, h);
  fill(0, 0, 0);
  noStroke();
  triangle(x - l/2, y - h/2, x - l/2, y + h/2, x - l/2 + h, y - h/2);
  triangle(x + l/2, y - h/2, x + l/2, y + h/2, x + l/2 - h, y - h/2);
}

void segmentLeft(int x, int y, int l, int h, boolean background) {
  if (background) {
    fill(50, 0, 0);
  } else {
    fill(255, 0, 0);
  }
  noStroke();
  rectMode(CENTER);
  rect(x, y, h, l);
  fill(0, 0, 0);
  triangle(x - h/2, y - l/2, x + h/2, y - l/2 + h, x - h/2 + h, y - l/2);
  triangle(x - h/2, y + l/2, x + h/2, y + l/2 - h, x - h/2 + h, y + l/2);
}

void segmentRight(int x, int y, int l, int h, boolean background) {
  if (background) {
    fill(50, 0, 0);
  } else {
    fill(255, 0, 0);
  }
  noStroke();
  rectMode(CENTER);
  rect(x, y, h, l);
  fill(0, 0, 0);
  triangle(x - h/2, y + l/2, x - h/2, y + l/2 - h, x - h/2 + h, y + l/2);
  triangle(x - h/2, y - l/2, x - h/2, y - l/2 + h, x - h/2 + h, y - l/2);
}

void segmentMid(int x, int y, int l, int h, boolean background) {
  if (background) {
    fill(50, 0, 0);
  } else {
    fill(255, 0, 0);
  }
  noStroke();
  rectMode(CENTER);
  rect(x, y, l, h);
  fill(0, 0, 0);
  triangle(x - l/2, y, x - l/2, y + h/2, x -l/2 + h/2, y +h/2);
  triangle(x - l/2, y, x - l/2, y - h/2, x -l/2 + h/2, y -h/2);
  triangle(x + l/2, y, x + l/2, y + h/2, x + l/2 - h/2, y +h/2);
  triangle(x + l/2, y, x + l/2, y - h/2, x + l/2 - h/2, y -h/2);
}
