var time = 0;
  // Anzahl Trennlinien pro Achse
var num_lines = 5;
// die wievielte Trennlinie soll jeweils beschriftet werden
var label_distance = 2;
// Anzahl Längeneinheiten zwischen Trennlinien
var label_scale = 20;


// Berechnung Abstand zwischen Trennlinien
var x_offset; 
var y_offset;
var i = 0;

function setup() {
  createCanvas(600, 600);
  background('#FFFFFF');
  x_offset = width / num_lines;
  y_offset = height / num_lines;
}


function draw() {
	if (time == 100){
 		time = 0;
  	if ( i < num_lines){
  		// Zeichnen der Trennlinien
  		line(i * x_offset, 0, i * x_offset, height);
  		line(0, i * y_offset, width, i * y_offset);
  
  		if(i % label_distance == 0){
  		  // Beschriftung der Trennlinien
  		  fill(0);
  		  text(i * label_scale, i * x_offset + 10, 20);
  		  text(i * label_scale, 10, i * y_offset + 20);
  		}
  		i++
		}
	}
  time++;
}
