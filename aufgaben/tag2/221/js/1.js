
var secondsSinceStart, liftOffTime, lastSecond;

var acceleration, velocity, pos;

var rocket;

function setup() {
    initializeFields();
    createCanvas(600, 400);
    textAlign(CENTER);
    rectMode(CENTER);
    imageMode(CENTER);
    textSize(32);
    secondsSinceStart = 0;
    lastSecond = second();
    liftOffTime = 60;
    acceleration = 0.01;
    velocity = 0;
    pos = 0;
}

function draw() {
    if (lastSecond != second()) {
        lastSecond = second();
        secondsSinceStart++;
    }
    if (secondsSinceStart > liftOffTime)
        moveRocket();
    background(70);
    drawBoxes();
    drawText();
    drawRocket();
}

function drawBoxes() {
    stroke(0);
    strokeWeight(2);
    fill(color(0x2d, 0x00, 0xf7));
    rect(85, 25, 160, 40);
    fill(color(0x6a, 0x00, 0xf4));
    rect(85, 70, 160, 40);
    fill(color(0x89, 0x00, 0xf2));
    rect(85, 115, 160, 40);
    fill(color(0xa1, 0x00, 0xf2));
    rect(85, 160, 160, 40);
}

function drawText() {
    fill(255);
    text(nf(hour(), 2) + ":" + nf(minute(), 2) + ":" + nf(second(), 2), 85, 38);
    text("T" + nfp(secondsSinceStart - liftOffTime, 4), 85, 83);
    text("v: " + nf(velocity, 3, 2), 85, 128);
    text("p: " + nf(pos, 3, 2), 85, 173);
}

function drawRocket() {
    strokeWeight(4);
    fill(color(0xc4, 0xff, 0xf9));
    rect(width / 2 + 80, height / 2, width - 180, height - 20);
    noStroke();
    fill(color(0x2b, 0x93, 0x48));
    rect(width / 2 + 80, height / 2 + 150, width - 184, 80);
    image(rocket, width / 2 + 80, height - (85 + pos), 100, 100);
}

function moveRocket() {
    pos += velocity;
    velocity += acceleration;
}

function initializeFields() {
    secondsSinceStart = 0;
    liftOffTime = 0;
    lastSecond = 0;
    acceleration = 0;
    velocity = 0;
    pos = 0;
    rocket = null;
}

function preload() {
  rocket = loadImage("rocket.png");
}

