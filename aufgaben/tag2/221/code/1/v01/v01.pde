color rechteck = color(255, 0, 50);
color dreieck = color(0, 255, 0);
color kreis = color(0, 0, 255);

size(200, 200);
background(255);

fill(rechteck);
rect(50, 150, 100, 50);

fill(dreieck);
triangle(50, 150, 100, 50, 150, 150);

fill(kreis);
circle(100, 120, 50);
