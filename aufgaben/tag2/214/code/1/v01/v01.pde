void setup() {
  size(400, 400);
}

void draw() {
  background(255);
  drawModule(second(), 150, 100);
}

void drawModule(int value, int x, int y) {
  fill(255);
  rect(x, y, 100, 200);
  String binaryValue = binary(value, 8);

  for (int i = 0; i < 8; i++) {
    boolean isActive = binaryValue.charAt(i) == '1';
    if (isActive)
      fill(0);
    else
      fill(255);
      
    int xPos = x + (i < 4 ? 25 : 75); // Ternärer Operator
    int yPos = y + (i % 4) * 50 + 25;
    
    circle(xPos, yPos, 40);
  }
}
