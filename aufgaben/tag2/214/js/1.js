
function setup() {
    createCanvas(675, 300);
}

function draw() {
    background(255);
    drawModule(second(), 550, 50);
    drawModule(minute(), 425, 50);
    drawModule(hour(), 300, 50);
    drawModule(day(), 150, 50);
    drawModule(month(), 25, 50);
}

function drawModule(value, x, y) {
    fill(255);
    rect(x, y, 100, 200);
    var binaryValue = binary(value);
    for (var i = 0; i < 8; i++) {
        var isActive = binaryValue.charAt(binaryValue.length - i) == '1';
        if (isActive)
            fill(0);
        else
            fill(255);
        // Ternärer Operator
        var xPos = x + (i < 4 ? 25 : 75);
        var yPos = y + (i % 4) * 50 + 25;
        circle(xPos, yPos, 40);
    }
}

function binary(nMask) { // https://stackoverflow.com/a/24153275
    // nMask must be between -2147483648 and 2147483647
    for (var nFlag = 0, nShifted = nMask, sMask = ""; nFlag < 32; nFlag++, sMask += String(nShifted >>> 31), nShifted <<= 1);
    return sMask;
}
