//Erstelle 200 Schneeflocken
float[] posY = new float[200];

void setup() { 
  size(400, 400); 
  frameRate(100);
  //Platziere Schneeflocken an zufälliger Höhe 
  for (int i = 0; i < posY.length; i++) {
    posY[i] = random(height);
  }
}

void draw() {
  background(50);

  //Rechne die x-Position aus und platziere Kreise 
  for (int i = 0; i < posY.length; i++) {
    float posX = width * i / posY.length;
    circle(posX, posY[i], 10.0);

    posY[i]++;

    //Wenn Scheeflocken unten, starte neu
    if (posY[i] > height) {
      posY[i] = 0;
    }
  }
}
