float[][] snowflakes = new float[500][2];
int time = -500;

void setup() {
  size(500, 500);

  background(0);
  
  for (int i = 0; i < 500; i++) {
      snowflakes[i][0] = random(width); // zufälliger x-Wert
      snowflakes[i][1] = random(5);     // zufällige Dicke der Flocken
  }
  
  fill(255, 255, 255);
  stroke(255, 255, 255);
}

void draw() {
  background(0, 0, 0);
  
  for (int i = 0; i < 500; i++) {
    circle(snowflakes[i][0], (i + time) % 500, snowflakes[i][1]);
  }
  
  time++;
}