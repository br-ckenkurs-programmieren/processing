int h;
float[][] snowflakes;
int time = 0;

void setup() {
  size(500, 500);

  h = (int)height;
  snowflakes  = new float[h][3];

  background(0);
  
  for (int i = 0; i < 500; i++) {
      snowflakes[i][0] = random(width); // zufälliger x-Wert
      snowflakes[i][1] = 0;             // y = 0
      snowflakes[i][2] = random(5);     // zufällige Dicke der Flocken
  }
  
  fill(255, 255, 255);
  stroke(255, 255, 255);
}

void draw() {
  background(0, 0, 0);
  
  for (int i = 0; i < time; i++) {
    circle(snowflakes[i][0], snowflakes[i][1], snowflakes[i][2]);
    snowflakes[i][1] += 0.5 + snowflakes[i][2] / 4.5;
    snowflakes[i][1] %= h;
  }
  
  time = min(time + 1, h));
}