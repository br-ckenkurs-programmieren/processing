Drop[] drops  = new Drop[1000];        //Alle Regentropfen

public void settings() {
  size(640, 360);
}

public void setup() {
  for (int i = 0; i < drops.length; i++)
    drops[i] = new Drop();              //"befüllen" des Arrays
}

public void draw() {
  background(0);
  for (Drop d : drops) {
    d.show();
    d.fall();
  }
}

class Drop {
  float x = random(640);
  float y = random(-1000, -50);            //Die Tropfen starten irgendwo über dem Rand des Fensters, damit nicht alle gleichzeitig auf einer Linie oder mitten im Bild starten
  float z = random(0, 30);                //Simuliert die dritte Dimension indem es Tropfen dicker oder dünner sein lässt
  float len = map(z, 0, 30, 10, 20);      // map() kann eine neue Reichweite(range) definieren, in diesem Fall für z von 0-20 auf 10-20
  float yspeed = map(z, 0, 30, 1, 2);

  void fall() {
    y += yspeed;
    yspeed += 0.05;        //Wird immer schneller

    if (y > height) {
      y = random(-500, -50);            //Wenn unten Tropfen ankommen, wird deren y-Koordinate wieder auf -50 bis -500 gesetzt und die Fallgeschwindigkeit bekommst wieder einen Wert zwischen 4-10
      yspeed = map(z, 0, 20, 1, 2);
    }
  }
  void show() {
    float thick = map(z, 0, 20, 1, 3);          //Simuliert dritte Dimesion, Regentropfen "Dicke"
    strokeWeight(thick);
    stroke(255);
    line(x, y, x, y+len);
  }
}
