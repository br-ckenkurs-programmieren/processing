let h;
let snowflakes = [[]];
let time = 0;


function setup() {
  let canvas = createCanvas(500, 500);
  canvas.parent("p5js");
  h = int(height);
  

  background(0);

  for (let i = 0; i < 500; i++) {
    snowflakes.push([]);
    snowflakes[i].push(random(width)); // zufälliger x-Wert
    snowflakes[i].push(0);             // y = 0
    snowflakes[i].push( random(5));     // zufällige Dicke der Flocken
  }

  fill(255, 255, 255);
  stroke(255, 255, 255);
}


function draw() {
  
  background(0, 0, 0);
  
  for (let i = 0; i < time; i++) {
    circle(snowflakes[i][0], snowflakes[i][1], snowflakes[i][2]);
    snowflakes[i][1] += 0.5 + snowflakes[i][2] / 4.5;
    snowflakes[i][1] %= h;
  }
  
  time = min(time + 1, h);

}
