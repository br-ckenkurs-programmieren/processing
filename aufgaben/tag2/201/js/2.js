function setup() {
    createCanvas(400, 400);
    background(255);
    strokeWeight(3);
}

function draw() {
    for (let i = 0; i < 50; i++) {
        point(random(width), random(height));
    }
}
