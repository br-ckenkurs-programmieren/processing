function setup() {
    createCanvas(400, 400);
    background(255); // Hintergrundfarbe Weiß
    pixelDensity(2); // Dicke der Pixel
    x = width / 2;
    y = height / 2;
}

function draw() {
    for (let i = 0; i < 100; i++) {
        point(x, y);
        x += round(random(2)) - 1;
        y += round(random(2)) - 1;
    }
}
