var x, y;
var musterart;
var counter;

function setup() {
    initializeFields();
    createCanvas(200, 200);
    background(255);
    pixelDensity(2);
    x = width / 2;
    y = height / 2;
}

function draw() {
    for (var i = 0; i < 100; i++) {
        if (musterart == 0) {
            stroke(random(255));
        } else if (musterart == 1) {
            stroke(random(255), 0, random(255));
        } else if (musterart == 2) {
            if (counter >= 256)
                counter = 0;
            stroke(counter++);
        } else if (musterart == 3) {
            var step = 2000;
          
            if (counter >= step * 6)
                counter = 0;
          
            if (counter++ < step)
                stroke(255, (counter * 255) / step, 0);
            else if (counter < step * 2)
                stroke(255 - (counter * 255) / (step * 2), 255, 0);
            else if (counter < step * 3)
                stroke(0, 255, (counter * 255) / (step * 3));
            else if (counter < step * 4)
                stroke(0, 255 - (counter * 255) / (step * 4), 255);
            else if (counter < step * 5)
                stroke((counter * 255) / (step * 5), 0, 255);
            else
                stroke(255, 0, 255 - (counter * 255) / (step * 6));
            
        }
        point(x, y);
        x += round(random(2)) - 1;
        y += round(random(2)) - 1;
        if (x < 0)
            x++;
        if (x > width)
            x--;
        if (y < 0)
            y++;
        if (y > height)
            y--;
    }
}

function initializeFields() {
    x = 0;
    y = 0;
    musterart = 3;
    counter = 0;
}

function keyPressed() {
  if (keyCode == 37 && musterart > 0)
    musterart--;
  else if (keyCode == 39 && musterart < 3)
    musterart++;
  background(255);
}
