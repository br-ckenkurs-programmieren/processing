void setup() {
  size(400, 400); // Fenstergröße 400x400 Pixel
  background(255); // Hintergrundfarbe Weiß
  strokeWeight(3); // Dicke der Pixel
}

void draw() {
  for (int i = 0; i < 50; i++) { // 50 Wiederholungen
    point(random(width), random(height));
  }
}
