int x, y;

void setup() {
  size(200, 200); // Fenstergröße 400x400 Pixel
  background(255); // Hintergrundfarbe Weiß
  pixelDensity(2); // Dicke der Pixel
  x = width / 2;
  y = height / 2;
}

void draw() {
  for (int i = 0; i < 100; i++) {
    point(x, y);
    x += round(random(2)) - 1;
    y += round(random(2)) - 1;
  }
}
