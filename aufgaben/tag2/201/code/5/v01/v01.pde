int x, y;
int musterart = 3;
int counter = 0;

void setup() {
  size(200, 200); // Fenstergröße 400x400 Pixel
  background(255); // Hintergrundfarbe Weiß
  pixelDensity(2); // Dicke der Pixel
  x = width / 2;
  y = height / 2;
}

void draw() {
  for (int i = 0; i < 100; i++) {
    if (musterart == 0) {
      stroke(random(255));
      // zufälliger Grauton
    } else if (musterart == 1) {
      stroke(random(255), 0, random(255));
      // zufälliger Rot-Blau-Ton
    } else if (musterart == 2) {
      if (counter >= 256) {
        counter = 0;
      }
      stroke(counter++);
      // Farbverlauf Schwarz nach Weiß
    } else if (musterart == 3) {
      int step = 2000;

      if (counter >= step * 6)
        counter = 0;

      if (counter++ < step) {
        stroke(255, (counter * 255) / step, 0);
      } else if (counter < step * 2) {
        stroke(255 - (counter * 255) / (step * 2), 255, 0);
      } else if (counter < step * 3) {  
        stroke(0, 255, (counter * 255) / (step * 3));
      } else if (counter < step * 4) {
        stroke(0, 255 - (counter * 255) / (step * 4), 255);
      } else if (counter < step * 5) {
        stroke((counter * 255) / (step * 5), 0, 255);
      } else {
        stroke(255, 0, 255 - (counter * 255) / (step * 6));
      }
    }

    point(x, y);
    x += round(random(2)) - 1;
    y += round(random(2)) - 1;

    if (x < 0)
      x++;
    if (x > width)
      x--;
    if (y < 0)
      y++;
    if (y > height)
      y--;
  }
}
