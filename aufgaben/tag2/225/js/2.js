function setup() {
    createCanvas(400, 400);
    background(255);
    strokeWeight(2);
    noFill();
}

function draw() {
    background(255);
    // offset X (Verschiebung des Würfels)
    var oX = 50;
    // offset Y
    var oY = 50;
    // Weite und Höhe des Würfels
    var extent = 200;
    // foreground X (Wie "tief" der Würfel erscheint)
    var fX = mouseX - 100;
    // foreground Y
    var fY = mouseY - 100;
    // hinten
    square(oX, oY, extent);
    // oben links
    line(oX, oY, oX + fX, oY + fY);
    // oben rechts
    line(oX + extent, oY, oX + fX + extent, oY + fY);
    // unten links
    line(oX, oY + extent, oX + fX, oY + fY + extent);
    // unten rechts
    line(oX + extent, oY + extent, oX + fX + extent, oY + fY + extent);
    // vorne
    square(oX + fX, oY + fY, extent);
}