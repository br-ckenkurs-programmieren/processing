var x, y, r;

function setup() {
    initializeFields();
    createCanvas(400, 400);
}

function draw() {
    background(255);
    x = mouseX;
    y = mouseY;
    r = 100;
    if (r > x || r > y || r > width - x || r > height - y) {
        r = min([ x, y, width - x, height - y ]);
    }
    fill(255, 255, 0);
    circle(x, y, r * 2);
    fill(0, 0, 255);
    circle(x, y, r);
    fill(255, 0, 0);
    circle(x, y, r / 2);
}

function initializeFields() {
    x = 0;
    y = 0;
    r = 0;
}

