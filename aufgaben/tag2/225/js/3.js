
function setup() {
    createCanvas(400, 400);
}

function draw() {
    background(255);
    // Punkt 1
    var x1 = mouseX;
    var y1 = mouseY;
    // Punkt 2
    var x2 = 50;
    var y2 = 250;
    // https://www.mathsisfun.com/algebra/distance-2-points.html
    var dist = sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2));
    fill(255);
    // Linie zwischen den Punkten
    line(x1, y1, x2, y2);
    circle(x1, y1, 20);
    circle(x2, y2, 20);
    fill(0);
    textSize(20);
    // Angabe der Distanz
    text(dist, 5, 20);
}