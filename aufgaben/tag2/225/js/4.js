function setup() {
    createCanvas(400, 400);
}

function draw() {
    background(255);
    // Box 1:
    var x1 = mouseX;
    // Positionen
    var y1 = mouseY;
    // Weite
    var dx1 = 250;
    // Höhe
    var dy1 = 175;
    // Box 2:
    var x2 = 150;
    // Positionen
    var y2 = 50;
    // Weite
    var dx2 = 100;
    // Höhe
    var dy2 = 200;
    fill(255);
    rect(x1, y1, dx1, dy1);
    if (// 1
    x1 < x2 + dx2 && // 2
    x1 + dx1 > x2 && // 3
    y1 < y2 + dy2 && // 4
    y1 + dy1 > y2) {
        // -> es gibt eine Kollision!
        fill(color(255, 0, 0));
    } else {
        // keine Kollision
        fill(color(0, 255, 0));
    }
    rect(x2, y2, dx2, dy2);
}