void setup() {
  size(400, 400);
}

void draw() {
  background(255);
  
  int x1 = mouseX; // Punkt 1
  int y1 = mouseY;

  int x2 = 50; // Punkt 2
  int y2 = 250;

  float dist = sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2)); // https://www.mathsisfun.com/algebra/distance-2-points.html

  fill(255);
  line(x1, y1, x2, y2); // Linie zwischen den Punkten
  circle(x1, y1, 20);
  circle(x2, y2, 20);

  fill(0);
  textSize(20);
  text(dist, 5, 20); // Angabe der Distanz
}
