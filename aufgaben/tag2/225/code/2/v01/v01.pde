void setup() {
  size(400, 400);
  background(255);
  strokeWeight(2);
  noFill();
}

void draw() {
  background(255);
  int oX = 50; // offset X (Verschiebung des Würfels)
  int oY = 50; // offset Y
  int extent = 200; // Weite und Höhe des Würfels

  int fX = mouseX; // foreground X (Wie "tief" der Würfel erscheint)
  int fY = mouseY; // foreground Y

  square(oX, oY, extent); // hinten

  line(oX, oY, oX + fX, oY + fY); // oben links
  line(oX + extent, oY, oX + fX + extent, oY + fY); // oben rechts
  line(oX, oY + extent, oX + fX, oY + fY + extent); // unten links
  line(oX + extent, oY + extent, oX + fX + extent, oY + fY + extent); // unten rechts

  square(oX + fX, oY + fY, extent); // vorne
}
