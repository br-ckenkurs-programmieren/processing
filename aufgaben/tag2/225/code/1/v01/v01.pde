int x, y, r;

void setup() {
  size(400, 400);
}

void draw() {
  background(255);
  x = mouseX;
  y = mouseY;
  r = 100;

  if (r > x || r > y || r > width - x || r > height - y) {
    r = min(new int[]{x, y, width - x, height - y});
  }

  fill(255, 255, 0);
  circle(x, y, r * 2);
  fill(0, 0, 255);
  circle(x, y, r);
  fill(255, 0, 0);
  circle(x, y, r / 2);
}
