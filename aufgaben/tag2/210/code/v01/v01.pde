float len = 100; // Länge der Grundfläche
float br = 100;  // Breite der Grundfläche
float h = 100;  // Höhe des Gebäudes

float vol;  // Volumen des Gebäudes
float a;  // Oberfläche des Gebäudes

float beton; // Euro pro Kubikmeter (in Bezug auf das Gesamtvolumen)
float glas; // Euro pro Quadratmeter (in Bezug auf alle Flächen)


void setup() {
  size(1000, 1000);
}

void draw() {
  background(255);
  
  // Berechnung von Volumen und Oberfläche
  vol = br*len*h;
  a = 2*br*h+2*len*h+2*br*len;
  
  // Texte/ Überschriften/ Ergebnisse
  fill(0);
  textSize(20);
  
  // Grundfläche und Höhe
  textAlign(CENTER);
  text("Grundfläche " + br/10 + " m x " + len/10 + " m = " + br/10*len/10 + " m2", 200, 20); 
  text("Höhe " + h/10 + " m", 200, height/3);
  
  // Visuelle Teilung der Benutzeroberfläche
  line(width-410, 0, width-410, height);

  // Preise
  text("Betonpreis", width-300, 20);
  text(beton/20 + " €", width-375, 550-beton);
  text("Glaspreis", width-100, 20);
  text(glas/5 + " €", width-175, 550-glas);

  // Kostenvoranschlag
  textAlign(LEFT);
  
  textSize(30);
  text("Kostenvoranschlag", width-400, 600);
  line(width-400, 605, width-150, 605);
  
  textSize(20);
  text("Volumen:", width-400, 640);
  text(vol + " m3", width-400, 670);
  
  text("Oberfläche:", width-400, 710);
  text(a + " m2", width-400, 740);
  
  text("Betonkosten:", width-400, 780);
  text(vol*beton + " €", width-400, 810);
  
  text("Glaskosten:", width-400, 850);
  text(a*glas + " €", width-400, 880);

  scale();
  
  // Zeichnen der Grundfläche, der Säulen und des Hauses in Funktionen ausgelagert
  drawFloor();
 
  drawBars();
  
  drawHouse();
}

// Funktion für das Skalieren der Grundfläche und der Säulen für Höhe, Betonpreis und Glaspreis
void scale(){
  if (mousePressed){
    if(mouseX >= 0 && mouseX < width/3-50 && mouseY >= 0 && mouseY < height/3-50){
      len = min(max(50, mouseY), 200);
      br = min(max(100, mouseX), 200);
    }
    else if (mouseX >= 100 && mouseX < 300 && mouseY >= 400 && mouseY < height){
      h = min(max(50, height-mouseY), 500);
    }
    else if (mouseX > width-350  && mouseX < width-250 && mouseY >= 50 && mouseY < 550){
      beton = min(max(50, 550-mouseY), 500);
    }
    else if (mouseX > width-200 && mouseX < width && mouseY >= 50 && mouseY < 550){
      glas = min(max(50, 550-mouseY), 500);
    }   
  }
}

// Funktion zum Zeichnen der Grundfläche
void drawFloor(){
  // Setzen Ursprung für variable Grundfläche
  translate(200, 150);

  // Zeichnen der Grundfläche
  fill(150);
  rect(-br/2, -len/2, br, len);
  fill(255);

  // Zurücksetzen des Ursprungs
  translate(-200, -150);
}

// Funktion zum Zeichnen der Säulen für die Höhe des Hauses und die Preise für Beton und Glas
void drawBars(){
  fill(0);
  
  line(150, height-150, 250, height-150);
  line(width-350, 550, width-250, 550);
  line(width-150,550, width-50, 550);

  fill(150);
  rect(175, height-150 ,50, -h);
  
  fill(200);
  rect(width-325, 550 ,50, -beton);
  
  fill(0,0,250);  
  rect(width-125, 550 ,50, -glas);
}

// Funktion zum Zeichnen des Hauses
void drawHouse(){
  // Setzen Ursprung für Zeichnung des Hauses
  translate(450, height / 2 + 200);

  // Zeichnen des Hauses
  fill(255);
  // linke Wand
  quad(0, h/2, 0, -h/2, -br/2, -h/2 - br/4, -br/2, h/2 - br/4);
  // rechte Wand
  quad(0, h/2, 0, -h/2, len/2, -h/2 - len/4, len/2, h/2 - len/4);
  // Dach  
  quad(0, -h/2, len/2, -h/2 - len/4, 0, -h -len/2, -br/2, -h/2 - br/4);
  line(0, -h -len/2, 0, -h/2);
}
