// ##### Ball #####
// Ball Start-Positionen
float ball_x = 50; 
float ball_y = 150;
// Ball Durchmesser
int ball_durchmesser = 30;
// Ball Geschwindigkeit
float ball_vx = 2;
float ball_vy = 2;

// #### Paddle ####
// Paddle Positionen, in Draw gesetzt
float paddle_x;
float paddle_y;
// Größe
float paddle_width = 15;
float paddle_height = 70;

// ##### Erweitert: Scoring + Gameover ####
// Aktueller Counter
int counter = 0;
// Highscore
int highscore = 0;

// Erweitert: Initale Position von Ball, zufällig in y-achse
void zufaellig_startpos() {
  // Relativ in x Achse
  // Sicher gehen, dass der Bald nicht in der Wand spawned.
  ball_x = width / 20 + ball_durchmesser / 2;
  ball_y = random(0 + ball_durchmesser / 2, height - ball_durchmesser / 2);
}

void setup() {
  // initiale Spielfeld Setup
  size(600, 300); //Verhältnis 2:1 am besten
  background(0); // Hintergrund Schwarz
  frameRate(120); // Default 60, damit es flüssig läuft
  fill(255); // Initale Farbe Weiß

  // Erweitert
  zufaellig_startpos();
}




void draw() {

  // ## Erst die Logik, dann das Zeichnen ##

  // ## Paddle Position auslesen ##
  // Entfernung Rechts
  paddle_x = width - 50 - paddle_width;
  // Y Achse durch Maus Auslesen (Mitte des Paddles)
  paddle_y = mouseY - paddle_height / 2;

  // ## Erweitert: Paddle im Bildschirm behalten ##
  //oben testen
  paddle_y = max(0, paddle_y);
  //unten testen
  paddle_y = min(height - paddle_height, paddle_y);

  // Ball bewegen 
  ball_x = ball_x + ball_vx;
  ball_y = ball_y + ball_vy;

  // ## Ball Kollision Checken ##
  // Jeweils mit Abstand, damit der Ball nicht zur Hälfte in der Wand verschwindet
  // Kann auch zusammengefasst werden
  // Nach Links
  if (ball_x <= ball_durchmesser/2) {
    ball_vx *= -1;
  }
  // Nach Oben
  if (ball_y <= ball_durchmesser/2) {
    ball_vy *= -1;
  }
  // Nach Unten
  if (ball_y >= height - ball_durchmesser / 2) {
    ball_vy *= -1;
  }

  // Nach Rechts = Gameover
  // Komplett aus dem Bildschirm
  if (ball_x >= width + ball_durchmesser/2) {
    /*ball_x = 50;
     ball_y = 150; */
    // Erweitert:
    zufaellig_startpos();
    counter = 0;
  }

  // ## Kollision mit Paddle ##
  // + / - ball_durchmesser / 2 um die jeweiligen Flächen zu vergrößern, Ball nicht halb rein.
  // Zuerst Fläche nach Unten + Rechts Abspannen:
  if (ball_x + ball_durchmesser / 2 >= paddle_x && ball_y + ball_durchmesser / 2 >= paddle_y) {
    // Dann die Fläche nach Links + Oben begrenzen
    if (ball_x - ball_durchmesser / 2 <= (paddle_x + paddle_width) && ball_y - ball_durchmesser / 2 <= (paddle_y + paddle_height)) {
      // Bewegung umdrehen
      ball_vx = ball_vx * -1;
      ball_vy = ball_vy * 1;

      // Erweitert: Highscore setzen
      counter++;
      if (highscore < counter) {
        highscore = counter;
      }
    }
  }

  // ## Zeichnen ##
  background(0);

  // Paddle
  rect(paddle_x, paddle_y, paddle_width, paddle_height);

  // Score
  textSize(14);
  text("Treffer: " + counter, 20, 20);
  text("Highscore: " + highscore, width-100, 20);

  // Ball
  circle(ball_x, ball_y, ball_durchmesser);
}
