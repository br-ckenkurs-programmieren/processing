var diced;
var resString;
var results; // int[14]
var n;
var s;
var x;
var y;
var xN;
var xS;

function setup() {
    initializeFields();
    createCanvas(400, 400);
    textSize(20);
    textAlign(CENTER);
    x = width / 2;
    y = 2 * height / 3;
    xN = x / 2;
    xS = 3 * x / 2;
}

function draw() {
    background(color(0xFF, 0xFF, 0xFF));
    if (!diced)
        showStart();
    else
        showResult();
}

function showStart() {
    drawButtonMid("+", xN, y - 55, 30, 30, color(0x25, 0xcc, 0x6d)); // n, links, up
    drawButtonMid("", xN, y - 25, 30, 30, color(0xad, 0xad, 0xad)); // n, links, sum
    drawButtonMid("-", xN, y + 5, 30, 30, color(0xcc, 0x3e, 0x25)); // n, links, down
    drawButtonMid("+", xS, y - 55, 30, 30, color(0x25, 0xcc, 0x6d)); // s, rechts, up
    drawButtonMid("", xS, y - 25, 30, 30, color(0xad, 0xad, 0xad)); // s, rechts, sum
    drawButtonMid("-", xS, y + 5, 30, 30, color(0xcc, 0x3e, 0x25)); // s, rechts, down
    drawButtonMid("ok", x, y - 25, 30, 30, color(0xad, 0xad, 0xad)); // ok
    text(s, xS, y);
    text(n, xN, y);
    text("Select number of dices: n\nSelect number of sides: s", width / 2, height / 6);
    text("s:", xS, height / 2);
    text("n:", xN, height / 2);
}

function showResult() {
    drawButtonMid("back", x, y - 25, 50, 30, color(0xad, 0xad, 0xad)); // back
    text("Result for n: " + n + ", s: " + s + ":", width / 2, height / 6);
    text(resString, width / 2, 2 * height / 6);
}

function mousePressed() {
    if (mouseButton == LEFT) {
        print("press");
        if (!diced) {
            if (mouseX >= xN - 15 && mouseX <= xN + 15 && mouseY >= y - 55 && mouseY <= y - 25 && n < 14) n++; // n up
            else if (mouseX >= xN - 15 && mouseX <= xN + 15 && mouseY >= y + 5 && mouseY <= y + 35 && n > 0) n--; // n down
            else if (mouseX >= xS - 15 && mouseX <= xS + 15 && mouseY >= y - 55 && mouseY <= y - 25 && s < 50) s++; // s up
            else if (mouseX >= xS - 15 && mouseX <= xS + 15 && mouseY >= y + 5 && mouseY <= y + 35 && s > 0) s--; // s down
            else if (mouseX >= x - 15 && mouseX <= x + 15 && mouseY >= y - 25 && mouseY <= y + 5) {
                diced = true;
                calcRes();
            } // ok button
        } else if (diced) {
            if (mouseX >= x - 25 && mouseX <= x + 25 && mouseY >= y - 25 && mouseY <= y + 5) diced = false; // back
        }
    }
}

function calcRes() {
    resString = ""; // reset String to be empty for each calc
    for (var i = 0; i < n; i++) {
        results[i] = int(random(s)) + 1; // rand-value could be 0, but our dice can't -> +1
        resString += results[i] + ", ";
        if (n >= 8 && i + 1 == n / 2) resString += "\n"; // \n: line-break if there are too many results
    }
}

function drawButtonMid(txt, rectX, rectY, rectW, rectH, c) {
    fill(c);
    rect(rectX - rectW / 2, rectY, rectW, rectH);
    fill(0);
    text(txt, rectX, rectY + 5 * rectH / 6);
}

function initializeFields() {
    diced = false;
    resString = "";
    results = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
    n = 0;
    s = 0;
    x = 0;
    y = 0;
    xN = 0;
    xS = 0;
}