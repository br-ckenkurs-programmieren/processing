int currentTab = 0;
PImage ghost, kitty, dolphin;
String[] websiteTitles = new String[]{"BooTube", "Flipper", "PawHub"};

void setup() {
  size(600, 400);
  textAlign(CENTER);
  imageMode(CENTER);
  textSize(20);

  ghost = loadImage("ghost.png");
  kitty = loadImage("kitty.png");
  dolphin = loadImage("dolphin.png");
}

void draw() {
  // currentTab aktualisieren
  if (mouseY < 50) {
    if (mouseX < 200) currentTab = 0;
    else if (mouseX < 400) currentTab = 1;
    else currentTab = 2;
  }

  // Webseite zeichnen
  switch(currentTab) {
  case 0: // BooTube
    background(#212121);
    fill(0);
    rect(50, 100, 400, 200);
    
    image(ghost, width / 2 - 50, height / 2, 200, 200);
    
    fill(255, 0, 0);
    rect(70, 265, 360, 15);
    
    fill(230);
    rect(70, 310, 200, 15);
    
    fill(0);
    
    for(int y = 100; y < height; y += 50)
      rect(470, y, 100, 40);
      
    break;
  case 1: // Flipper
    background(#004e64);
    
    fill(#348aa7);
    rect(0, 0, 150, height);
    
    fill(#004e64);
    circle(75, 125, 130);
    image(dolphin, 75, 125, 100, 100);
    
    fill(#00a5cf);
    for(int y = 85; y < height + 50; y += 100) {
      rect(200, y, 350, 80);
    }
    
    break;
  case 2: // PawHub
    background(#1b1b1b);
    
    fill(0);
    rect(80, 100, 440, 200);
    
    image(kitty, width / 2, height / 2, 150, 150);
    
    fill(#ffa31a);
    rect(80, 80, 80, 15);
    
    fill(255);
    rect(80, 310, 200, 15);
  }

  // Tab-Leiste
  noStroke();
  
  for (int i = 0; i < 3; i++) {
    if (currentTab == i)
      fill(#ccc5b9);
    else
      fill(#fffcf2);
    rect(i * 200, 0, 200, 50);

    fill(50);
    text(websiteTitles[i], 100 + i * 200, 30);
  }
}
