
var currentTab;

var ghost, kitty, dolphin;

var websiteTitles;

function setup() {
    initializeFields();
    createCanvas(600, 400);
    textAlign(CENTER);
    imageMode(CENTER);
    textSize(20);
}

function draw() {
    // currentTab aktualisieren
    if (mouseY < 50) {
        if (mouseX < 200)
            currentTab = 0;
        else if (mouseX < 400)
            currentTab = 1;
        else
            currentTab = 2;
    }
    // Webseite zeichnen
    switch (currentTab) {
        case // BooTube
            0:
            background(color(0x21, 0x21, 0x21));
            fill(0);
            rect(50, 100, 400, 200);
            image(ghost, width / 2 - 50, height / 2, 200, 200);
            fill(255, 0, 0);
            rect(70, 265, 360, 15);
            fill(230);
            rect(70, 310, 200, 15);
            fill(0);
            for (let y = 100; y < height; y += 50) rect(470, y, 100, 40);
            break;
        case // Flipper
            1:
            background(color(0x00, 0x4e, 0x64));
            fill(color(0x34, 0x8a, 0xa7));
            rect(0, 0, 150, height);
            fill(color(0x00, 0x4e, 0x64));
            circle(75, 125, 130);
            image(dolphin, 75, 125, 100, 100);
            fill(color(0x00, 0xa5, 0xcf));
            for (let y = 85; y < height + 50; y += 100) {
                rect(200, y, 350, 80);
            }
            break;
        case // PawHub
            2:
            background(color(0x1b, 0x1b, 0x1b));
            fill(0);
            rect(80, 100, 440, 200);
            image(kitty, width / 2, height / 2, 150, 150);
            fill(color(0xff, 0xa3, 0x1a));
            rect(80, 80, 80, 15);
            fill(255);
            rect(80, 310, 200, 15);
    }
    // Tab-Leiste
    noStroke();
    for (var i = 0; i < 3; i++) {
        if (currentTab == i)
            fill(color(0xcc, 0xc5, 0xb9));
        else
            fill(color(0xff, 0xfc, 0xf2));
        rect(i * 200, 0, 200, 50);
        fill(50);
        text(websiteTitles[i], 100 + i * 200, 30);
    }
}

function initializeFields() {
    currentTab = 0;
    websiteTitles = ["BooTube", "Flipper", "PawHub"];
}

function preload() {
    ghost = loadImage("ghost.png");
    kitty = loadImage("kitty.png");
    dolphin = loadImage("dolphin.png");
}