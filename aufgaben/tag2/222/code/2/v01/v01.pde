void setup() {
  size(400, 400);
}

void draw() {
  background(255);
  translate(width / 2, height / 2);
  int fib = fibonacci(mouseX / 30);
  float rot = TWO_PI / fib;

  float y = 0;
  for (int i = 0; i < 250; i++) {
    rotate(mouseY / 100f);
    circle(0, y, 7);
    y += 0.4;
  }

  for (int i = 0; i < fib; i++) {
    rotate(rot);
    ellipse(0, 150, 100 / (fib + 1) + 50, 150);
  }
}

int fibonacci(int n) {
  if (n == 0 || n == 1) 
    return n;
  return fibonacci(n - 1) + fibonacci(n - 2);
}
