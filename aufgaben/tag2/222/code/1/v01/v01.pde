void setup() {
  size(400, 400);
}

void draw() {
  background(255);
  translate(width / 2, height / 2);
  int fib = fibonacci(mouseX / 30);
  float rot = TWO_PI / fib;

  for (int i = 0; i < fib; i++) {
    rotate(rot);
    ellipse(0, 100, 100 / (fib + 1) + 50, 200);
  }
}

int fibonacci(int n) {
  if (n == 0 || n == 1) 
    return n;
  return fibonacci(n - 1) + fibonacci(n - 2);
}
