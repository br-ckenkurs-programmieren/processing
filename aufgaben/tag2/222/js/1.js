function setup() {
    createCanvas(400, 400);
}

function draw() {
    background(255);
    translate(width / 2, height / 2);
    var fib = fibonacci(round(mouseX / 30));
    var rot = TWO_PI / fib;
    for (var i = 0; i < fib; i++) {
        rotate(rot);
        ellipse(0, 100, 100 / (fib + 1) + 50, 200);
    }
}

function fibonacci(n) {
    if (n < 2)
        return n;
    return fibonacci(n - 1) + fibonacci(n - 2);
}

