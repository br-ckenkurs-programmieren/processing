var x = 0;
var y = 0;

function setup() {
  createCanvas(400, 400);
  pixelDensity(2);
  background(255);
	
  x = width/2;
  y = height/2;
}

function draw() {

  var counter = 0;
  
  var  rand = floor (random(4));
  if(rand == 0 && x <= width){
  	x++;
  }else if(rand == 1 && x > 0){
  	x--;
  }else if(rand == 2 && y <= height){
  	y++;
  }else if(rand == 3 && y > 0){
  	y--;
  }
	
  point(x, y);
}
