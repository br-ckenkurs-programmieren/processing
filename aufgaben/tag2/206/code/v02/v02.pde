float x = 0;
float y = 0;

int counter = 0;

void setup(){
  size(200, 200);
  pixelDensity(2);
  background(255);
  
  x = width/2;
  y = height/2;
}

void draw(){
  
  int rand = (int) random(4);
  
  if(rand == 0 && x <= width){
      x++;
  }else if(rand == 1 && x > 0){
      x--;
  }else if(rand == 2 && y <= height){
      y++;
  }else if(rand == 3 && y > 0){
      y--;
  }
  
  point(x, y);
}
