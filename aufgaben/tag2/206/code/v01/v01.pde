// Position des Random Walks
int x;
int y;
// Schrittgröße
int step = 5;

void setup() {
  size(400, 400);
  // Startpunkt in der Mitte des Fensters
  x = width/2;
  y = height/2;
}

void draw() {
  // Zufällige Schrittgröße
  step = int(random(7));
  if (x < 1) { 
    x = x + 1;
  } else if (x >= width) {
    x = x - 1;
  } else {
    // Zufälliger Schritt in positive oder negative X-Richtung
    if(random(1) < 0.5) {
      x = x + step;
    } else {
      x = x - step;
    }
  }
  if (y < 1) { 
    y = y + 1;
  } else if (y >= height) {
    y = y - 1;
  } else {
    // Zufälliger Schritt in positive oder negative Y-Richtung
    if(random(1) < 0.5) {
      y = y + step;
    } else {
      y = y - step;
    }
  }
  // Zeichnen des Pfades des Random Walks
  rect(x,y,1,1);
}
