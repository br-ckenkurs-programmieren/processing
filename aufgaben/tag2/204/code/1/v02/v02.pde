// Dauer des Countdowns in Sekunden
int zahl = 11;
//String str_zahl = "10";
int t_size = 30;

void setup() {
  size(400,400);
  // Framerate auf 1, damit draw() nur sekündlich ausgeführt wird
  frameRate(1);
  // Text mittig ausrichten
  textAlign(CENTER,CENTER);
}

void draw() {
  background(0);
  // Solange der Countdown noch nicht abgelaufen ist, wird die restliche Zeit sekündlich um 1 reduziert
  if(zahl > 0) {
    zahl--;
    textSize(t_size);
    text(zahl, width/2, height/2);
  } else {
    // Ist 0 erreicht, erscheint ein Text
    textSize(50);
    text("Kaboom :-)", width/2-100, height/2);
  }
}
