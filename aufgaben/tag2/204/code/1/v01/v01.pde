int counter = 10;

void setup() {
  size(400, 400);
  textSize(50);
  textAlign(CENTER);

  frameRate(1);
}

void draw() {
  background(#FFFFFF);
  fill(0);

  text(counter, width / 2, height / 2);
  
  if (counter > 0)
    counter--;
}
