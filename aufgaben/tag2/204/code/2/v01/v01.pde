int counter = 10;
float x, y, v, a;

void setup() {
  size(400, 400);
  textSize(50);
  textAlign(CENTER);
  fill(0);

  frameRate(1);

  x = width / 2; // x-Pos.
  y = height * 0.75; // y-Pos.
  v = 0; // Vertikale Geschwindigkeit
  a = 0.8; // Vertikale Beschleunigung
}

void draw() {
  background(#FFFFFF);
  
  text(counter, x, y);

  if (counter > 0) {
    counter--;
  } else {
    fill(255, 0, 0);
    frameRate(30);
    y -= v;
    v += a;
  }
}
