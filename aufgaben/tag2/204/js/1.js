var counter;

var x, y, v, a;

function setup() {
    initializeFields();
    createCanvas(400, 400);
    
    textSize(50);
    textAlign(CENTER);
    fill(0);
    frameRate(1);

    x = width / 2;
    y = height * 0.75;
    v = 0;
    a = 0.8;
}

function draw() {
    background(color(0xFF, 0xFF, 0xFF));
    text(counter, x, y);

    if (counter > 0) {
        counter--;
    } else {
        fill(255, 0, 0);
        frameRate(30);
        y -= v;
        v += a;
    }
}

function initializeFields() {
    counter = 10;
    x = 0;
    y = 0;
    v = 0;
    a = 0;
}

