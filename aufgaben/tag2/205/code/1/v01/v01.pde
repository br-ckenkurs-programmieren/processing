size(800, 600);
background(#FFFFFF);
fill(0);
textSize(50);
textAlign(CENTER,CENTER);

// DNA-Sequenzen als String
String x_sequence = "ACGCTG";
String y_sequence = "ACGTGA";

float x_offset = width / (x_sequence.length() + 1);
float y_offset = height / (y_sequence.length() + 1);

// Dotplot
// Verschachtelte Schleife vergleicht jedes Element der einen Sequenz mit jedem Element der anderen Sequenz
for(int i = 0; i < y_sequence.length(); i++){
  for(int j = 0; j < x_sequence.length(); j++){
    if(x_sequence.charAt(i) == y_sequence.charAt(j)){
      // Bei gleichem Buchstaben wird ein Kreis (Dot) gezeichnet
      circle((i+1)*x_offset + x_offset/2, (j+1)*y_offset + y_offset/2, 20);
    }
  }
}


// Grid + Beschriftung

for(int i = 0; i <= x_sequence.length(); i++){
  line(i*x_offset, 0, i*x_offset, height);
  if(i < x_sequence.length()){
    text(x_sequence.charAt(i), (i+1)*x_offset + x_offset/2, y_offset/2);
  }
}

for(int i = 0; i <= y_sequence.length(); i++){
  line(0, i*y_offset, width, i*y_offset);
  if(i < y_sequence.length()){
    text(y_sequence.charAt(i), x_offset/2, (i+1)*y_offset + y_offset/2);
  }
}
