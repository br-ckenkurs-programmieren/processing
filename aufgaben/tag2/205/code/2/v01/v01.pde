size(800, 600);
background(#FFFFFF);
fill(0);
textSize(50);
textAlign(CENTER,CENTER);
int diff1 = 0;
int diff2 = 0;

// DNA-Sequenzen als String
String x_sequence = "ACGCTGATGC";
String y_sequence = "ACGTGA";

float x_offset = width / (x_sequence.length() + 1);
float y_offset = height / (y_sequence.length() + 1);


//Zusatz wenn die Sequenzlänge unterschiedlich lang ist
if (y_sequence.length() <= x_sequence.length()){
  diff1 = x_sequence.length() - y_sequence.length();
  for (int x = 0; x < diff1; x++){
    for(int j = 0; j < x_sequence.length()-diff1; j++){
      if(x_sequence.charAt(x_sequence.length()-x-1) == y_sequence.charAt(j)){
        // Bei gleichem Buchstaben wird ein Kreis (Dot) gezeichnet
        circle((x_sequence.length()-x)*x_offset + x_offset/2, (j+1)*y_offset + y_offset/2, 20);
      }
    } 
  }
}
else{
  diff2 = y_sequence.length() - x_sequence.length();
  for (int y = 0; y < diff2; y++){
    for(int i = 0; i < y_sequence.length()-diff2; i++){
      if(x_sequence.charAt(i) == y_sequence.charAt(y_sequence.length()-y-1)){
        // Bei gleichem Buchstaben wird ein Kreis (Dot) gezeichnet
        circle((i+1)*x_offset + x_offset/2, (y_sequence.length()-y)*y_offset + y_offset/2, 20);
      }
    } 
  }
}


// Dotplot
// Verschachtelte Schleife vergleicht jedes Element der einen Sequenz mit jedem Element der anderen Sequenz
for(int i = 0; i < y_sequence.length()-diff2; i++){
  for(int j = 0; j < x_sequence.length()-diff1; j++){
    if(x_sequence.charAt(i) == y_sequence.charAt(j)){
      // Bei gleichem Buchstaben wird ein Kreis (Dot) gezeichnet
      circle((i+1)*x_offset + x_offset/2, (j+1)*y_offset + y_offset/2, 20);
    }
  }
}


// Grid + Beschriftung

for(int i = 0; i <= x_sequence.length(); i++){
  line(i*x_offset, 0, i*x_offset, height);
  if(i < x_sequence.length()){
    text(x_sequence.charAt(i), (i+1)*x_offset + x_offset/2, y_offset/2);
  }
}

for(int i = 0; i <= y_sequence.length(); i++){
  line(0, i*y_offset, width, i*y_offset);
  if(i < y_sequence.length()){
    text(y_sequence.charAt(i), x_offset/2, (i+1)*y_offset + y_offset/2);
  }
}
