function setup() {
  createCanvas(1280, 720);
}

var rot = 0.0;

/* Ein Punkt in der Spirale ist quasi ein Eckpunkt eines rechtwinkligen Dreiecks
 * Zwischen diesem Punkt und dem Mittelpunkt der Spirale verläuft die Hypothenuse
 * Der Sinus bzw der Cosinus wird nun Stück für Stück erhöht und somit rotiert dieser Eckpunk um die Mitte,
 wodurch die Spirale entsteht
 */


function draw() {
  background(240);
  // Offsets, damit die Spirale mittig angeordnet wird
  var x= width/2;
  var y = height/2;
  var r = 61.80;
  for (var i=1; i<500; i+=1) {
    var xPos =(int)(i*cos(i*r +rot) +x );
    var yPos =(int)(i*sin(i*r +rot) +y );
    // Radius des Kreises wächst, je weiter außen der Punkt 
    var rad = 5.0 + i / 20.0;
    fill(0);
    circle(xPos, yPos, rad);
  }
  rot += 0.003;
}
