void setup() {
  size(1280, 720);
}

float rot = 0;

/* Ein Punkt in der Spirale ist quasi ein Eckpunkt eines rechtwinkligen Dreiecks
 * Zwischen diesem Punkt und dem Mittelpunkt der Spirale verläuft die Hypothenuse
 * Der Sinus bzw der Cosinus wird nun Stück für Stück erhöht und somit rotiert dieser Eckpunk um die Mitte,
 wodurch die Spirale entsteht
 */


void draw() {
  background(240);
  // Offsets, damit die Spirale mittig angeordnet wird
  int x= width/2;
  int y = height/2;
  float r = 61.80f;
  for (int i=1; i<500; i+=1) {
    int xPos =(int)(i*cos(i*r +rot) +x );
    int yPos =(int)(i*sin(i*r +rot) +y );
    // Radius des Kreises wächst, je weiter außen der Punkt 
    float rad = 5 + i / 20f;
    fill(0);
    circle(xPos, yPos, rad);
  }
  rot += 0.003f;
}
