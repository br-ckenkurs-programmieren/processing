boolean up = true; 
float pos_x = 200;
float pos_y = 500;

void setup() {
  size(400, 600);
  frameRate(1);
}

void draw() {
  background(255);
  
  // static lines
  line(pos_x - 75, pos_y, pos_x, pos_y - 100);
  line(pos_x + 75, pos_y, pos_x, pos_y - 100);
  line(pos_x, pos_y - 100, pos_x, pos_y - 300);
  circle(pos_x, pos_y - 300 - 50, 100);
  
  // arms
  if(up){
    line(pos_x, pos_y - 200, pos_x - 75, pos_y - 300);
    line(pos_x, pos_y - 200, pos_x + 75, pos_y - 300);
  }else{
    line(pos_x, pos_y - 200, pos_x - 75, pos_y - 100);
    line(pos_x, pos_y - 200, pos_x + 75, pos_y - 100);
  }
  
  up = !up;
  
}
