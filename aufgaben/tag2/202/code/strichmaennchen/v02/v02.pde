int x = 200;
int y = 200;
int h = 40;
boolean gehoben = true;
int arm_offset = 20;

void setup() {
  size(400, 400);
  background(255);
  frameRate(1);
}

void draw() {
  background(255);
  // Körper
  line(x, y, x, y+h);
  // Kopf 5,10,15
  ellipse(x, y-10, 18, 20);
  // Arme nach oben oder unten
  if (gehoben == true) {
    arm_offset = 20;
    gehoben = false;
  } else {
    arm_offset = -20;
    gehoben = true;
  }
  line(x, y+5, x-20, y+5+arm_offset);
  line(x, y+5, x+20, y+5+arm_offset);
  // Beine
  line(x, y+h, x-20, y+h+30);
  line(x, y+h, x+20, y+h+30);
}
