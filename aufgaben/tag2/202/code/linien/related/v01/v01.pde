void setup() {
  size(800, 400);
  background(255);
}

void draw() {
  // Bild 1
  float 
    x1 = 0, 
    y1 = 0, 
    x2 = 0, 
    y2 = height;

  while (x2 <= width) {
    stroke(0);
    line(x1, y1, x2, y2);
    x2 = x2 + 20;
  }

  // Bild 2
  float 
    x21 = width, 
    y21 = 0, 
    x22 = width, 
    y22 = height;
  for (int i = 0; i<100; i++) {
    i = i + 1; // Ich will 2, will aber i++ zum Verständnis behalten
    x22 = x22 - i;
    if (x22 <= 0) {
      break;
    }
    stroke(60, 200, 60);
    line(x21, y21, x22, y22);
  }

  // Bild 3
  float 
    x31 = width, 
    y31 = 0, 
    x32 = 0, 
    y32 = height;

  while (x32 <= width) {
    stroke(150, 30, 30);
    line(x31, y31, x32, y32);
    x31 = x31 - 20;
    x32 = x32 + 20;
  }
}
