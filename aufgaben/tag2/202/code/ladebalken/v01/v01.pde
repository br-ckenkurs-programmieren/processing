float len; // Breite der Lade-Anzeige
float h; // Höhe und Position der Lade-Anzeige
float x;
float y;
float lade; // Breite des Ladestatus
int duration = 3; // Ladedauer in Sekunden

void setup() {
  size(800, 400);
  len = width/2;
  h = height/4;
  x = width/2 - len/2;
  y = height/2 - h/2;
  lade = 0;
  // Berechnung, um wie viele Pixel der Ladebalken pro Sekunde breiter werden muss
  frameRate(len/duration);
}

void draw() {
  // Solange noch nicht fertig geladen wurde, wird der Ladebalken um 1 Pixel erweitert
  if (lade < len) {
    lade = lade + 1;
  }
  background(30);
  fill(200);
  rect(x, y, len, h);
  fill(0, 100, 0);
  rect(x, y, lade, h);
  fill(100, 0, 0);
  textSize(26);
  // Prozentsatz wird aus Breite des aktuellen Ladebalkens und der Breite der Anzeige berechnet
  text(round(lade/len*100) + " % geladen", width/2-30, height/2);
}
