float duration = 4.0;     // max duration of the bar
float elapsed = 0.0;      // time elapsed so far
float max_length = 600;

void setup() {
  size(800, 400);
  frameRate(20);
}

void draw() {
  background(255);
  fill(0);
  textSize(40);

  // (elapsed/duration) gives us the elapsed time 
  // as a fraction of the total duration. We use that 
  // to scale the bar
  float factor = (elapsed/duration);

  // draw the scaled bar
  rect(100, 200, max_length * factor, 100);

  text("" + (int)(factor * 100) + " %", 100, 350);

  // draw outline of the whole bar
  noFill();
  rect(100, 200, max_length, 100);

  // (1/frameRate) is the duration of 1 frame in seconds
  elapsed = elapsed + (1/frameRate);

  // reset bar
  if (elapsed >= duration) {
    elapsed = 0;
  }
}
