// Dauer in Millisekunden
float duration = 3000;
// Höhe und Breite des Ladebalkens
int h = 50;
int w = 300;
// Prozentzahl des Ladefortschrittes
float p = 0;

void setup() {
  size(400, 300);
  textSize(40);
  textAlign(CENTER, CENTER);
}

void draw() {
  background(255);
  noFill();
  rect(width/2 - w/2, height/2 - h/2, w, h);
  fill(0, 255, 0);
  rect(width/2 - w/2, height/2 - h/2, p*w, h);
  // millis() gibt seit Programmstart vergangene Millisekunden zurück
  if (millis()<duration) {
    // Anteil der vergangenen Zeit an der Ladedauer bestimmt den Anteil des Ladebalkens der gezeichnet wird
    p = millis()/duration;
  } else p = 1;
  fill(0);
  text("" + (int)(p * 100) + " %", width/2, height/2);
}
