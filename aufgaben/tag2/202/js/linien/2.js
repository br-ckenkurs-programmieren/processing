function setup(){
  createCanvas(800, 400);
  background(255);

  for (let i = 0; i < width; i += 10) {
    line(0, 0, i, height); // oben links
    line(width, 0, i, height); // oben rechts
  }
}