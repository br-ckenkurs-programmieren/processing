// Erzeuge Streifen in Sanduhrform
function setup(){
  createCanvas(800, 400);
  background(255);
}

function draw() {
  // schwarzer Bildanteil entspricht Bild 1
    let x1 = 0;
    let y1 = 0; 
    let x2 = 0; 
    let y2 = height;

  while (x2 <= width) {
    stroke(0);
    line(x1, y1, x2, y2);
    x2 = x2 + 20;
  }

  // schwarzer und grüner Bildanteil entspricht Bild 2 
    let x21 = width; 
   let y21 = 0; 
    let x22 = width; 
    let y22 = height;
  
  for (let i = 0; i<100; i++) {
    i = i + 1; // Ich will 2, will aber i++ zum Verständnis behalten
    x22 = x22 - i;
    if (x22 <= 0) {
      break;
    }
    stroke(60, 200, 60);
    line(x21, y21, x22, y22);
  }

  // roter Bildanteil entspricht Bild 3
  let x31 = width; 
  let y31 = 0;
  let x32 = 0; 
  let y32 = height;

  while (x32 <= width) {
    stroke(150, 30, 30);
    line(x31, y31, x32, y32);
    x31 = x31 - 20;
    x32 = x32 + 20;
  }
}