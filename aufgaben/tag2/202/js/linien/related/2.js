let steps = 10;

function setup() {
  createCanvas(1000, 1000);
  background(255);
  stroke(0);
  frameRate(30);
}

let i = 0;
let reverse = false;

function draw() {
  //background(0);
  if (i <= width/2) {
    line(i, 0, width-i, height);
    line(width-i, 0, i, height);
    i+= steps;
  } else {
    i = 0;
    if (reverse == true) {
      stroke(0);
      strokeWeight(1);
      reverse = false;
      print("Change Mode: Reverse!");
    } else {
      stroke(255);
      strokeWeight(3);
      print("Change Mode: Forwards!");
      reverse = true;
    }
  }
}