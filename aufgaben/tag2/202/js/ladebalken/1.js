// Dauer in Millisekunden
let duration = 3000.0;

// Höhe und Breite des Ladebalkens
let h = 50;
let w = 300;

// Prozentzahl des Ladefortschrittes
let p = 0.0;

function setup() {
  createCanvas(400, 300);
  textSize(40);
  textAlign(CENTER, CENTER);
}

function draw() {
  background(255);
  noFill();
  rect(width/2 - w/2, height/2 - h/2, w, h);
  fill(0, 255, 0);
  rect(width/2 - w/2, height/2 - h/2, p*w, h);

  // millis() gibt seit Programmstart vergangene Millisekunden zurück
  if (millis()<duration) {
    // Anteil der vergangenen Zeit an der Ladedauer bestimmt den Anteil des Ladebalkens der gezeichnet wird
    p = millis()/duration;

  } else { // vollständig geladen
	p = 1 
}
  fill(0);
  text("" + (int)(p * 100) + " %", width/2, height/2);
}