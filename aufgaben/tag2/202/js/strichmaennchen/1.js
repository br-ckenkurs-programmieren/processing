let wave = true;

function setup() {
  createCanvas(400, 400);
  frameRate(4);
}

function draw() {
  let mid = width / 2;
  background(255);
  
  circle(mid, mid-100, 50); // Kopf
  line(mid, mid-75, mid, mid+50); // Torso
  line(mid, mid+50, mid+50, mid+150); // Rechtes Bein
  line(mid, mid+50, mid-50, mid+150); // Linkes Bein

  if (wave) { // Oben
    line(mid, mid-25, mid+50, mid-75); // Rechter Arm
    line(mid, mid-25, mid-50, mid-75); // Linker Arm
    wave = false;
  } else { // Unten
    line(mid, mid-25, mid+50, mid+25); // Rechter Arm
    line(mid, mid-25, mid-50, mid+25); // Linker Arm
    wave = true;
  }
}