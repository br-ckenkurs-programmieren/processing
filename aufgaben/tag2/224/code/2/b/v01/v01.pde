void setup() {
  size(400, 400);
}

void draw() {
  background(#EAEAE0);
  strokeWeight(15);

  stroke(#1C4670);
  for (int i = -100; i < height + 100; i += 75) {
    line(0, i, width, i + 200);
  }
  
  stroke(#1DC690);
  for (int i = -100; i < width + 100; i += 40) {
    line(i - 100, height, i, 0);
  }
  
  noStroke();
  fill(#EAEAE0);
  rect(100, 100, 200, 200, 25);

  stroke(#278AB0);
  for (int i = -100; i < height + 100; i += 50) {
    line(0, i + 25, width, i);
  }
}
