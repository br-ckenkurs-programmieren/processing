void setup() {
  size(400, 400);
}

void draw() {
  background(#50514f);
  noStroke();

  int offset = -40;
  color[] colors = new color[]{#f25f5c, #ffe066, #247ba0, #70c1b3};

  for (color c : colors) {
    
    fill(c);
    for (int k = offset; k < height; k += 40) {
      for (int i = -100; i < width; i += random(0, 50)) {
        rect(i, k, random(0, 25), 40);
      }
    }
    
    offset += 10;
  }
  
  noLoop();
}
