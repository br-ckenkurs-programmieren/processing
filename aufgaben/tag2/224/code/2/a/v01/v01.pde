void setup() {
  size(400, 400);
}

void draw() {
  background(#F8D210);
  noStroke();

  for (int i = -200; i < width + 200; i += 10) {
    for (int j = -200; j < width + 200; j += 10) {

      if (i % 100 == 0 && j % 100 == 0) {

        pushMatrix();
        fill(#F51720);
        translate(i, j - i * 0.3f);
        triangle(0, 0, -25, -50, 25, -50);
        popMatrix();
      } else if (i % 100 == 50 && j % 100 == 50) {

        pushMatrix();
        fill(#2FF3E0);
        translate(i, j - i * 0.3f);
        triangle(0, 0, -25, 50, 25, 50);
        popMatrix();
      }
    }
  }
}
