void setup() {
  size(400, 400);
}

void draw() {
  background(#e4c1f9);
  noStroke();

  for (int i = 0; i < width; i += 100) {
    for (int j = 0; j < height; j += 100) {
      fill(#ff99c8);
      rect(i, j, random(0, 100), random(0, 100));
      fill(#fcf6bd);
      rect(i + 100, j, -random(0, 100), random(0, 100));
      fill(#d0f4de);
      rect(i + 100, j + 100, -random(0, 100), -random(0, 100));
      fill(#a9def9);
      rect(i, j + 100, random(0, 100), -random(0, 100));
    }
  }

  noLoop();
}
