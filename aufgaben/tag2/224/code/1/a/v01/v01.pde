void setup() {
  size(400, 400);
}

void draw() {
  background(255);
  
  stroke(13, 28, 137);
  strokeWeight(2);
  
  for(int i = -100; i < width; i += 10) {
    line(i, 0, i + 100, height);
  }
}
