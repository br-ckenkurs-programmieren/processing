void setup() {
  size(400, 400);
}

void draw() {
  background(255);
  
  noStroke();
  
  for(int i = -100; i < width; i += 15) {
    fill(82, 166, 6);
    square(i, (mouseX * i / 30f) % height, 30);
    fill(255, 25);
    square(0, 0, width);
  }
}
