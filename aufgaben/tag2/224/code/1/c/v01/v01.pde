void setup() {
  size(400, 400);
}

void draw() {
  background(98, 39, 198);

  noStroke();
  fill(198, 39, 159);

  for (int i = -50; i < width; i += 50) {
    for (int j = -50; j < height; j += 50) {

      if (i % 100 == 0) {
        fill(39, 140, 198);
        square(i, j + 20, 30);
      } else {
        fill(29, 60, 198);
        square(i, j, 30);
      }
    }
  }
}
