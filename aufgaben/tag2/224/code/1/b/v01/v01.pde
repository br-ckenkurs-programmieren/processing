void setup() {
  size(400, 400);
}

void draw() {
  background(255);
  
  noStroke();
  fill(198, 39, 159);
  
  for(int i = -100; i < width; i += 75) {
    for(int j = -100; j < height; j += 75) {
      circle(i, j, 50);
    }
  }
}
