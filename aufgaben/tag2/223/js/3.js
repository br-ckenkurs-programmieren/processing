function setup() {
    createCanvas(400, 400);
}

function draw() {
    background(255);
    translate(width / 2, height / 2);
    var a = mouseX / 500;
    var b = mouseY / 500;
    for (let t = 0; t < 200; t += 0.01) {
        var x = 150 * sin(a * t);
        var y = 150 * sin(b * t);
        point(x, y);
    }
}
