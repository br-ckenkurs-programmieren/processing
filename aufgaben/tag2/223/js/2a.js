
var pos;

function setup() {
    initializeFields();
    createCanvas(400, 400);
    background(255);
}

function draw() {
    translate(width / 2, height / 2);
    circle(150 * sin(pos * 2), 150 * sin(pos + PI / 2), 5);
    pos += 0.01;
}

function initializeFields() {
    pos = 0;
}

