float a, b;
float x, y;

void setup() {
  size(400, 400);
  /*
    Mit strokeWeight(); lässt sich die Größe der gezeichneten Punkte verändern.
    Für die in der Aufgabenstellung vorgegebene Schrittlänge 0.1 empfiehlt sich mindestens strokeWeight(2),
    da sonst nur einzelne Punkte und keine zusammenhängende Linien sichtbar sind.
  */
  strokeWeight(1);
}

void draw() {
  // in jedem Durchlauf der draw() Funktion wird der Hintergrund neu gezeichnet, da sich sonst die Lissajous Figuren überschneiden.
  background(255);
  
  a = mouseX/50.0;
  b = mouseY/50.0;
  
  /*
    Hier werden keine ~1000Punkte (wie in der Aufgabenstellung gefordert: 0 bis 100 in 0.1-Schritten) gezeichnet,
    sondern ~10000Punkte. Je mehr Punkte gezeichnet werden, umso besser sind die Linien zu erkennen.
  */
  for (float t = 0; t <= 100; t = t + 0.0) {
    x = width / 2 * sin(a * t);
    y = height / 2 * cos(b * t);

    /*
      Mit point(x,y) erscheint die Lissajous Figur um den Ursprung des Fensters
      und ist damit zu einem großen Teil nicht sichtbar. Indem width/2 bzw. height/2 addiert werden
      wird die Figur in den Mittelpunkt des Fensters verschoben
    */
    point(x + width/2, y + height/2);
  }
}
