void setup() {
  size(250, 250);
  pixelDensity(2); // Für bessere Sichtbarkeit
}

void draw() {
  background(255);
  fill(0);
  stroke(0);
  strokeWeight(2); // Pixel sind so klein...
  
  // Wir müssen das Zeichenfeld in die Mitte bewegen,
  // da sin auch negative Werte erzeugt.
  translate(width / 2, height / 2);
  
  // Ermittlung von a und be nach der Aufgabenstellung
  float a = mouseX/100.0;
  float b = mouseY/100.0;

  // Es ist besser, eine Schleife mit ganzen Zahlen zu machen,
  // weshalb wir t weiter unten durch 10 teilen.
  for (int t = 0; t < 1000; t++) {
    
    // Formeln für x und y sind vorgegeben. 
    float x = 100 * sin(a * (t / 10.0));
    float y = 100 * sin(b * (t / 10.0));
    
    // Einen Punkt, bitte!
    point(x, y);
  }
}
