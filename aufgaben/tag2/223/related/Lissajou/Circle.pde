class EnginCircle {

  private float x;  // x-Koordinate des Kreismittelpunkts im Koordinatensystem
  private float y;  // y-Koordinate des Kreismittelpunkts im Koordinatensystem

  private float diameter;  // Durchmesser des Kreises
  private float radius;  // Radius des Kreises 

  private color c;  //Farbe für Kreis, Punkt und Linie 

  private float dotX;  // x-Koordinate des Punkts relativ zum Kreismittelpunkt
  private float dotY;  // y-Koordinate des Punkts relativ zum Kreismittelpunkt
  private float dotPosX;  // x-Koordinate des Punkts im Koordinatensystem
  private float dotPosY;  // y-Koordinate des Punkts im Koordinatensystem

  private float angle;  // zeigt den aktuellen Fortschritt des Winkels an
  private float offset; // gibt an um welchen winkel der Punkt zum Start verschoben wird
  private float angularSpeed; // Winkelgeschwindigkeit mit der sich der Punkt um den Kreis bewegt, wenn die Geschwindigkeitsskalierung auf 1 steht. 
  private float angularSpeedFactor; // Geschwindigkeitsskalierung um die Grundgeschwindigkeit für idividuelle Punkte hoch und runter zu skalieren.
  
  private int baseSpeedRotations; //Gibt an wie häufig der Punkt den Kreis mit angularSpeedfactor=1 umrundet hat. 

  private boolean orientation; // Zeigt die Ausrichtung der Linie an: true = horizontal; false = vertikal

  public EnginCircle(float x, float y, float diameter, float angularSpeedFactor, boolean orientation) {
    this.x = x;
    this.y = y;

    this.diameter = diameter;
    this.radius = diameter/2;

    this.angle = 0; //Wird auf 0 gesetzt. Ist ein Zähler für den Winkelfortschritt
    this.angularSpeed = -0.01;
    this.angularSpeedFactor = angularSpeedFactor;
    
    this.baseSpeedRotations = 0;

    //Die Koordinaten eines Punkts auf dem Einheitskreis ergeben sich aus P(cos(ω)|sin(ω)); Winkel ω in Rad 
    //Punkt wird mit - HALF_PI nach oben auf den Kreis gesetzt; PI/2(rad) entsprechen 90°(grad)
    //Negativ = Gegen den Uhrzeigersinn; Postiv mit dem Uhrzeigersinn 
    this.offset = -1 * HALF_PI;
    this.dotX = radius*cos(angle*angularSpeedFactor + offset); 
    this.dotY = radius*sin(angle*angularSpeedFactor + offset);
    
    this.orientation = orientation;

    this.c = color(random(10, 255), random(10, 255), random(10, 255)); // Setzt einen zufälligen Farbwert. 10 als min-Wert um komplett schwarze Kreise auszuschließen. 
  }


  //Verändert den Winkel des Punkts um angularSpeed*angularSpeedFactor
  //Macht ein update auf Die x|y Position des Punkts im Koordinatensystem 
  public void MoveDot() {
    angle = angle + angularSpeed; //angularSpeed ist negativ -> Drehsinn gegen den Uhrzeigersinn 
    dotX = radius*cos(angle*angularSpeedFactor + offset);
    dotPosX = dotX + x;
    dotY = radius*sin(angle*angularSpeedFactor + offset);
    dotPosY = dotY + y;
    
    //Überprüft ob der Punkt den Kreis vollständig umgangen hat
    if(abs(angle)>TWO_PI){
      baseSpeedRotations++;
      angle = 0;
    }
  }


  //Zeichnet Kreis, Punkt und Linie
  public void DrawMeLikeOneOfYourFrenchFries() {
    noFill();
    strokeWeight(1);
    stroke(c);
    ellipse(x, y, diameter, diameter);

    strokeWeight(8);
    stroke(c);
    point(dotPosX, dotPosY);

    stroke(c, 150);
    strokeWeight(1);
    if (orientation) {
      line(dotPosX, 0, dotPosX, height); //Vertikal ausgerichtete Linie
    } else {
      line(0, dotPosY, width, dotPosY); //Horizontal ausgerichtete Linie
    }
  }
}
