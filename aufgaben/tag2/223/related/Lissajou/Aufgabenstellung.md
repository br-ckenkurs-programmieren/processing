# Übung: Lissajous Kurven (Anspruchsvoll)
Schreibe Ein Programm, das Lissajouskurven zeichnet.
![Lissajous](https://miro.medium.com/max/1000/1*o75epumclddyHiiJfCYNIg.gif)

Die folgenden Teilaufgaben sollen bei der Umstzung helfen.

## Teil 1: Kreis 

Zunächst einmal müssen die Kreise erstellt werden, die die Kurven zeichnen. Hierzu sollte die Klasse `Circle` erstellt werden, die die nötigen eigenschaften über den Kreis enthält. Die Klasse kann im Verlauf der Übung noch erweitert werden.

Nun müssen wir uns Überlegen wie der rotierende punkt dargestellt wird.

Die Kreisbewegung eines Teilchen lässt sich effizient in Polarkoordinaten darstellen. In kartesischen Koordinaten ist

$$ \vec r = \left( \begin{array}{r}
R * sin(\alpha)\\
R * cos(\alpha)\\
\end{array} \right) $$

Dabei bezeichnet $R$ den Abstand zwischen dem Ort des Teilchens und dem Ursprung, der das Zentrum der Kreisbewegung ist und $\alpha$ den Winkel zwischen der Verbindungslinie von Ursprung und Ort des Teilchens und der x-Achse. Im Fall der Kreisbewegung ist der Radius konstant. Quelle: [Wikipedia](https://de.wikipedia.org/wiki/Gleichf%C3%B6rmige_Kreisbewegung#Herleitung_%C3%BCber_Polarkoordinaten).

$R$ ist dann in unserem Fall der Radius unseres Kreises.

$\alpha$ kann von uns frei bestimmt werden und legt den Startpunkt des Punkts fest. Um eine Kreisbewegung zu erzeugen muss $\alpha$ verändert werden. 
Für uns kann das nun so aussehen:
```Java
float radius = 2;

float angle = 0;
float angularSpeed = 0.01;

float dotX = radius*sin(angle);
float dotY = radius*cos(angle);

angle = angle - angularSpeed;
```

Die Koordinaten des Punktes müssen noch auf den Kreismittelpunkt addiert werden `dotAbsoluteX = dotX + CircleX `dazu analog natürlich auch die Y-Werte.

### Tipps und Überlegungen 

- Die Klasse `Circle` sollte möglichst detailiert die Informationen über Kreis und Punkt enthalten.
- Die Bewegung des Punktes sollte möglichst von einer eigenen Methode übernommen werden
- Das Anzeigen des Kreises sollte möglichst von einer eigenen Methode übernommen werden.


## Teil 2 Kurven

Als erstes muss überlegt werden wie genau eine Kurve aufgebaut ist. 
$$
f(t) = \left(\begin{array}{r}
A_x*sin(\omega_1*t+\phi_1)\\
A_y*sin(\omega_2*t+\phi_2)\\
\end{array}\right) 
$$

$A$ ist der Radius des Kreises. 

$\omega*t$ ist unser Winkel $\alpha$

$\phi$ Ist die Phasen verschiebung also ein offset auf dem Winkel in unserem Fall gilt $\phi=0$

Die Schwingungen, von der die Kurven beschrieben werden ergibt sich aus der Y-Bewegung der Kreise auf der linken Seite und der X-Bewegung der Kreise oben 

Daraus ergibt sich:
$$
f(\alpha) = \left(\begin{array}{r}
R*sin(\alpha_x)\\
R*cos(\alpha_y)\\
\end{array}\right) 
$$

Um Vektoren abzuspeichern bietet Processing die Klasse ```PVector```. 
In processing kann der aktuellste Punkt der Kurve also wie folgt dargesellt werden:
```Java
PVector current = new PVector();
current.x = circleUp.abssoluteDotX;
current.y = circleLeft.absoluteDotY;
```
Die Kurve ergibt sich dann aus den verschiedenen Individuellen Punkten. In Processing kann das als Liste abgespeichert werden.
```Java
ArrayList<PVektor> path = new ArrayList<PVector>();
path.add(current);
```
Komplexe Formen werden in Processing mit Hilfe von ```beginShape()```, ```endShape() ``` und aufrufen von ```vertex()``` dazwischen realisiert.
```Java
beginShape();
    for (PVector v : path) {
      vertex(v.x, v.y); 
    }
    endShape();
```


### Tipps und Überlegungen 
- Am besten Erstellt man für Kurven eine Klasse, die den Pfad der Kurve, den aktuellsten Punkt so wie die beiden Kreise aus denen sich der Pfad ergibt zusammenfasst.
- Es hilft ungemein sich die Reference für ```PVector```,  ```beginShape()```, ```endShape() ```und ``` vertex() ``` anzuschauen. 