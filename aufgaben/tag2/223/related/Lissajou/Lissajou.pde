// Daniel Shiffman
// http://youtube.com/thecodingtrain
// http://codingtra.in

// Coding Challenge #116: Lissajous Curve Table
// https://youtu.be/--6eyLO78CY

// p5.js version:
// https://editor.p5js.org/codingtrain/sketches/BJbj5l3Y7

int w = 80; //Gibt die Breite/Höhe eines Quadrats im Grid an

int cols;
int rows;

Curve[][] curves; //2-Dimensionales Array um das Feld der Kurven zu realisieren
EnginCircle[] enginesCol; //Array der Kreise mit Koordinate Kreis(X|0)
EnginCircle[] enginesRow; //Array der Kreise mit Koordinate Kreis(0|Y)

void setup() {
  size(1200, 960);
  
  cols = width / w - 1; //Anzahl der Spalten; -1, da das Feld (0|0) frei bleibt 
  rows = height / w - 1; //Anzahl der Reihen; -1, da das Feld (0|0) frei bleibt 

  float diameter = w - 0.2*w; // Setzt einen kleinen Rand zwischen Quadrat im Grid und Kreis

  curves = new Curve[rows][cols];

  enginesRow = new EnginCircle[cols];
  enginesCol = new EnginCircle[rows];

  for (int j = 0; j < rows; j++) {
    float x = w/2; //Offset um den Kreismittelpunt in die mitte des Felds zu setzen
    float y = w + j * w + w / 2; //Offset um den Kreismittelpunt in die mitte des Felds zu setzen und Das Feld (0|0) frei zu lassen. lässt dann die y-koordinate wachsen. 
    enginesCol[j] = new EnginCircle(x, y, diameter, (float)j + 1, false); //angularspeed muss > 0 sein
  }

  for (int i = 0; i < cols; i++) {
    float x = w + i * w + w / 2; //Offset um den Kreismittelpunt in die mitte des Felds zu setzen und Das Feld (0|0) frei zu lassen. lässt dann die x-koordinate wachsen. 
    float y = w / 2; //Offset um den Kreismittelpunt in die mitte des Felds zu setzen
    enginesRow[i] = new EnginCircle(x, y, diameter, (float)i + 1, true); //angularspeed muss > 0 sein
  }

  for (int j = 0; j < rows; j++) {
    for (int i = 0; i < cols; i++) {
      curves[j][i] = new Curve(enginesCol[j], enginesRow[i]);
    }
  }
}

void draw() {
  background(0);

  //Geht alles Arrays durch und updated positionen 
  for (EnginCircle circle : enginesRow) {
    circle.DrawMeLikeOneOfYourFrenchFries();
    circle.MoveDot();
  }

  for (EnginCircle circle : enginesCol) {
    circle.DrawMeLikeOneOfYourFrenchFries();
    circle.MoveDot();
  }

  for (int j = 0; j < rows; j++) {
    for (int i = 0; i < cols; i++) {
      curves[j][i].updatePoint();
      curves[j][i].addPoint();
      curves[j][i].show();
    }
  }
  
  //Reset nach einer Umdrehung mit Baisgeschwindigkeit
  if (curves[0][0].enginRow.baseSpeedRotations > 0) {
    for (int j = 0; j < rows; j++) {
      for (int i = 0; i < cols; i++) {
        curves[j][i].reset();
      }
    }
    curves[0][0].enginRow.baseSpeedRotations = 0;
  }
  //saveFrame("../pics/lissajous####.png");
}
