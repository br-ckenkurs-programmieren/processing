class Curve {

  //PVector Datentyp für 2-oder 3-Dimensionale Vektoren; 2D in Unserem Fall
  ArrayList<PVector> path; //Liste an Vektoren aus denen unsere Kurve aufgebaut wird
  PVector current; //Vektor der als nächstes unserem Pfad hinzugefügt wird 

  EnginCircle enginCol; //Gibt an von welchem Kreis(X|0) die Kurve gezeichnet wird
  EnginCircle enginRow; //Gibt an von welchem Kreis(0|Y) die Kurve gezeichnet wird
  
  color c; //Farbe der Kurve

  Curve(EnginCircle enginCol, EnginCircle enginRow) {
    path = new ArrayList<PVector>();
    current = new PVector();

    this.enginCol = enginCol;
    this.enginRow = enginRow;
    
    //Farbe der Kurve wird von den Farben der beiden Kreise bestimmt
    this.c = color(((red(enginCol.c)+red(enginRow.c))/2),((green(enginCol.c)+green(enginRow.c))/2),((blue(enginCol.c)+blue(enginRow.c))/2)); 
  }

  void updatePoint() {
    current.x = enginRow.dotPosX; //x-koordinate des aktuellen Vektors wird vom Punkt des zeichneden Kreises(0|Y) bestimmt
    current.y = enginCol.dotPosY; //y-koordinate des aktuellen Vektors wird vom Punkt des zeichneden Kreises(X|0) bestimmt
  }

  //Fügt unserem Pfad den aktuellen Vektor hinzu
  void addPoint() {
    path.add(current); 
  }

  //Leert unseren Pfad
  void reset() {
    path.clear();
  }

  void show() {
    stroke(c);
    strokeWeight(1);
    noFill();
    
    //Mit beginShape() können komplexere Formen dagestellt werden.
    //Die Form wird durch das Verbinden verschiedener Punkte realisiert.
    //Die Verbindung Punkte wird mit dem aufruf von vertex() zwischen beginShape() und endShape() gemacht.
    beginShape();
    for (PVector v : path) {
      vertex(v.x, v.y); //Die Punkte der Form sind die Vektoren in unserem Pfad
    }
    endShape();

    strokeWeight(8);
    point(current.x, current.y); //Punkt der unsere Form "zeichnet"
    current = new PVector();
  }
}
