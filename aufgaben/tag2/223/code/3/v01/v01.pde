void setup() {
  size(400, 400);
}

void draw() {
  background(255);
  translate(width / 2, height / 2);
  float a = mouseX / 500f;
  float b = mouseY / 500f;
  
  for(float t = 0; t < 200; t += 0.01f) {
    float x = 150 * sin(a * t);
    float y = 150 * sin(b * t);
    point(x, y);
  }
}
