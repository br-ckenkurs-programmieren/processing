float pos = 0;

void setup() {
  size(400, 400);
  background(255);
}

void draw() {
  translate(width / 2, height / 2);
  circle(150 * sin(pos * 2), 150 * sin(pos * 1.5), 5);
  pos += 0.01f;
}
