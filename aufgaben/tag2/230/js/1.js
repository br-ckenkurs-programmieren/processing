var length = 10;
var cell = 1;
var counter = 0;
var array; //Deklaration des Integer-Arrays
// mögliche "manuelle" Initialisierung des Arrays
// in diesem Beispiel wird die Initialisierung mit einer for-Schleife bevorzugt zur Auseinandersetzung mit Schleifen
//int[] array = { 23,21,2,5,48,75,15,45,12,56};
var x_Kasten; //x-Koordinate der Kästen
var y_Kasten = 40.0; //y-Koordinate der Kästen

function setup() {
  createCanvas(500, 110);
  
  //Initialisierung des Arrays mit 10 zufälligen Werten mittels for-Schleife
  array = Array();
  
  for (var i = 0; i < length; i++) {
    array[i] = round(random(101));
  }
}

function draw() {
  
  //bei jeder neuen for-Schleife muss x_Kasten neu mit 20 belegt werden, damit Kästen immer dieselbe Position haben bei 
  //jedem neuen Durchgang und x_Kasten nicht unendlich groß wird durch die Zuweisung "x_Kasten += 40;"
  if (counter != 100){
      counter++;
  } else{
    cell++;
    counter = 0;
    background(200);
    for (var i = 0, x_Kasten = 20; i < length && i < cell; i++) { //int i ist unsere Index-Variable
    
      //Zeichnen der Kästen
      fill(255);
      strokeWeight(1);
      stroke(0);
      rect(x_Kasten, y_Kasten, 30, 30);
    
      //Zeichnen der Werte und Indizes
      fill(0);
      text(array[i], x_Kasten + 5, y_Kasten + 20);
      text(i, x_Kasten + 12, y_Kasten + 45);
    
      x_Kasten += 40; //x_Kasten so verändern, dass der nächste Kasten daneben rechts steht
    }
  }
}
