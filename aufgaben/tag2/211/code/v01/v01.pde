// Gravitation
float g = 0.8;
// Pendellänge
float l = 250;
// Winkel, Winkelgeschwindigkeit und Winkelbeschleunigung
float a,a1,a2;

void setup() {
  size(600,400);
  fill(0);
  translate(width/2, 0);
  a = HALF_PI;
}

void draw() {
  // Verschiebung des Ursprungs um die halbe Breite, damit das Pendel mittig aufgehängt und um diesen Punkt rotieren kann
  translate(width/2, 20);
  background(255);
  // Formeln aus der Aufgabenstellung
  // Winkelbeschleunigung
  a2 = g/l * sin(a);
  // Winkelgeschwindigkeit
  a1 = a1 + a2;
  // Winkel
  a  = a + a1;
  
  // Rotation um berechnetem Winkel
  rotate(a+PI);
  
  // Pendel an aktuelle Position zeichnen
  line(0,0,0,l);
  ellipse(0,l,30,30);
}
