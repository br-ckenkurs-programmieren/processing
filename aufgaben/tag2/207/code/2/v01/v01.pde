float x, y;
float radius = 60; // Größe des Kreises
int counter = 0; // Zähler für Treffer
int targetMillis; // Zeitpunkt, ab welchem der Kreis aktiv wird
boolean isActive; // Kreis aktiv
boolean mouseInside; // Maus im Kreis
float reactionTime; // Reaktionszeit
float totalReactionTime;

void setup() {
  size(500, 500);
  textSize(32);
  targetMillis = millis() + round(random(1000, 5000)); // Startzeit

  x = width / 2; // Position des Kreises
  y = height / 2;
  totalReactionTime = 0;

  background(255);
}

void draw() {
  noFill();

  float distX = abs(x - mouseX); // Abstand berechnen
  float distY = abs(y - mouseY); // abs() für Betrag

  mouseInside = distX < radius && distY < radius; // Maus im Kreis?
  isActive = millis() > targetMillis; // Kreis aktiv?

  if (mouseInside && isActive) { // Maus im Kreis & Kreis aktiv
    counter++; // Treffer zählen
    totalReactionTime += millis() - targetMillis; // Reaktionszeit zusammenzählen
    targetMillis = millis() + round(random(1000, 5000)); // Neue Zeit berechnen
    fill(0, 255, 0);
    
  } else if (mouseInside && !isActive) { // Maus im Kreis & Kreis nicht aktiv
    targetMillis = millis() + round(random(1000, 5000));
    
  } else if (isActive) { // Kreis aktiv, Maus nicht im Kreis
    fill(0, 0, 255);
    
  } else { // Kreis nicht aktiv, Maus nicht im Kreis
    fill(255);
  }

  circle(x, y, radius*2); // Kreis zeichnen

  if (counter >= 5) {
    background(255);
    fill(0);
    reactionTime = (totalReactionTime / 1000f) / counter; // Reaktionszeit berechnen
    text("Reaktionszeit: " + reactionTime, 20, height / 2);
    noLoop();
  }
}
