float x = 0; // Position des Kreises
float y = 0;
float radius = 60; // Größe des Kreises
int counter = 0; // Zähler für Treffer

void setup() {
  size(500, 500);
  textSize(32);

  x = random(width); // Zufällige Startposition
  y = random(height);
}

void draw() {
  background(255);

  float reactionTime = (millis() / 1000f) / counter;
  float distX = abs(x - mouseX); // Abstand berechnen
  float distY = abs(y - mouseY); // abs() für Betrag

  if (distX < radius // x/y-Abstand kleiner als der Radius?
    && distY < radius) {
    x = random(width); // Neue zufällige Position
    y = random(height);
    counter++; // Treffer zählen
  }

  fill(0);
  text("Reaktionszeit: " + reactionTime, 40, 80);

  fill(0, 255, 0); // Grün
  circle(x, y, radius*2); // Kreis zeichnen
}
