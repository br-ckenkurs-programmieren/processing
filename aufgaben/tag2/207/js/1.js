
// Position des Kreises
var x;

var y;

// Größe des Kreises
var radius;

// Zähler für Treffer
var counter;

function setup() {
    initializeFields();
    createCanvas(500, 500);
    textSize(32);
    // Zufällige Startposition
    x = random(width);
    y = random(height);
}

function draw() {
    background(255);
    var reactionTime = (millis() / 1000) / counter;
    // Abstand berechnen
    var distX = abs(x - mouseX);
    // abs() für Betrag
    var distY = abs(y - mouseY);
    if (// x/y-Abstand kleiner als der Radius?
        distX < radius && distY < radius) {
        // Neue zufällige Position
        x = random(width);
        y = random(height);
        // Treffer zählen
        counter++;
    }
    fill(0);
    text("Reaktionszeit: " + reactionTime, 40, 80);
    // Grün
    fill(0, 255, 0);
    // Kreis zeichnen
    circle(x, y, radius * 2);
}

function initializeFields() {
    x = 0;
    y = 0;
    radius = 60;
    counter = 0;
}

