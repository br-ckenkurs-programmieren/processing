
var x, y;

// Größe des Kreises
var radius;

// Zähler für Treffer
var counter;

// Zeitpunkt, ab welchem der Kreis aktiv wird
var targetMillis;

// Kreis aktiv
var isActive;

// Maus im Kreis
var mouseInside;

// Reaktionszeit
var reactionTime;

var totalReactionTime;

function setup() {
    initializeFields();
    createCanvas(500, 500);
    textSize(32);
    // Startzeit
    targetMillis = millis() + round(random(1000, 5000));
    // Position des Kreises
    x = width / 2;
    y = height / 2;
    totalReactionTime = 0;
    background(255);
}

function draw() {
    noFill();
    // Abstand berechnen
    var distX = abs(x - mouseX);
    // abs() für Betrag
    var distY = abs(y - mouseY);
    // Maus im Kreis?
    mouseInside = distX < radius && distY < radius;
    // Kreis aktiv?
    isActive = millis() > targetMillis;
    if (mouseInside && isActive) {
        // Maus im Kreis & Kreis aktiv
        // Treffer zählen
        counter++;
        // Reaktionszeit zusammenzählen
        totalReactionTime += millis() - targetMillis;
        // Neue Zeit berechnen
        targetMillis = millis() + round(random(1000, 5000));
        fill(0, 255, 0);
    } else if (mouseInside && !isActive) {
        // Maus im Kreis & Kreis nicht aktiv
        targetMillis = millis() + round(random(1000, 5000));
    } else if (isActive) {
        // Kreis aktiv, Maus nicht im Kreis
        fill(0, 0, 255);
    } else {
        // Kreis nicht aktiv, Maus nicht im Kreis
        fill(255);
    }
    // Kreis zeichnen
    circle(x, y, radius * 2);
    if (counter >= 5) {
        background(255);
        fill(0);
        // Reaktionszeit berechnen
        reactionTime = (totalReactionTime / 1000) / counter;
        text("Reaktionszeit: " + reactionTime, 20, height / 2);
        noLoop();
    }
}

function initializeFields() {
    x = 0;
    y = 0;
    radius = 60;
    counter = 0;
    targetMillis = 0;
    isActive = false;
    mouseInside = false;
    reactionTime = 0;
    totalReactionTime = 0;
}

