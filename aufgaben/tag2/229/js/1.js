//      ---Variablen---
//Simulieren der Zeit
var time = 0;
var timestep = 1; //Je größer timestep, je schneller die Umlaufgeschwindigkeit

//Radius der Kreislaufbahn
var radius = 150;

//Mittelpunkt
var x = 200;
var y = 200;

function setup() {
  createCanvas(400, 400);
}

function draw() {
  //      ---Berechnung---
  //Zeitschritte
  time += timestep;
  if (time > 360) {
    time = 0;
  }

  //Koodinaten
  var xRadian = x - (radius*cos(radians(time)));
  var yRadian = y - (radius*sin(radians(time)));

  //      ---Zeichen---
  //Hintergrund
  background(155);

  //Kreis
  fill(255, 0, 0); //rot
  circle(xRadian, yRadian, 30);
}