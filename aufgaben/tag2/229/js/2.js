//      ---Variablen---
//Simulieren der Zeit
let time = 0;
let timestep = 1; //Je größer timestep, je schneller die Umlaufgeschwindigkeit

//Radius der Kreislaufbahn
let radius = 150;

//Mittelpunkt
let x = 200;
let y = 200;

function setup() {
  createCanvas(400, 400);
}

function draw() {
  //      ---Berechnung---
  //Zeitschritte
  time += timestep;
  if (time > 360) {
    time = 0;
  }

  //Koodinaten
  let xRadian = x - (radius*cos(radians(time)));
  let yRadian = y - (radius*sin(radians(time)));

  //      ---Zeichen---
  //Hintergrund
  background(155);

   //Indikator
  fill(0, 0, 0);
  circle(xRadian, yRadian, 5);
  line(xRadian - 20, yRadian, xRadian + 20, yRadian);
  line(xRadian, yRadian - 20, xRadian, yRadian + 20);
  
  //Koordianten als Text
  textSize(15);
  text("x: " + xRadian, xRadian+10, yRadian - 18);
  text("y: " + yRadian, xRadian+10, yRadian - 5);
}