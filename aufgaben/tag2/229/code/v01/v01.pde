//      ---Variablen---
//Simulieren der Zeit
float time = 0;
float timestep = 1; //Je größer timestep, je schneller die Umlaufgeschwindigkeit

//Radius der Kreislaufbahn
float radius = 150;

//Mittelpunkt
float x = 200;
float y = 200;

void setup() {
  size(400, 400);
}

void draw() {
  //      ---Berechnung---
  //Zeitschritte
  time += timestep;
  if (time > 360) {
    time = 0;
  }

  //Koodinaten
  float xRadian = x - (radius*cos(radians(time)));
  float yRadian = y - (radius*sin(radians(time)));

  //      ---Zeichen---
  //Hintergrund
  background(155);

  //Kreis
  fill(255, 0, 0); //rot
  circle(xRadian, yRadian, 30);
}
