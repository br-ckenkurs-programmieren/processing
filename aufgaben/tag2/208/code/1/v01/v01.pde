void setup() {
  size(600, 600);
  textAlign(RIGHT); // Text an der rechten Seite verankern
  textSize(15); // Textgröße
}

void draw() {
  background(255); // Weißer Hintergrund
  translate(width / 2, height / 2); // Nullstelle zur Bildmitte
  fill(0); // Textfarbe Schwarz
  stroke(0); // Koordinatensystem Schwarz

  strokeWeight(3);
  line(-width, 0, width, 0); // x-Nullstelle
  line(0, -height, 0, height); // y-Nullstelle

  strokeWeight(1);
  for (int i = -width; i < width; i += 50) {
    line(i, -height, i, height); // Vertikale Linien
    text(i, i - 2, 15); // Beschriftung (15 für unterhalb der Mittellinie)
  }
  
  for (int i = -height; i < height; i += 50) {
    line(-width, i, width, i); // Horizontale Linien
    text(i, - 2, i - 2); // Beschriftung
  }
}
