void setup() {
  size(600, 600);
  textAlign(RIGHT); // Text an der rechten Seite verankern
  textSize(15); // Textgröße
}

void draw() {
  float a = mouseX - width / 2;
  float b = mouseY - height / 2;
  
  background(255); // Weißer Hintergrund
  translate(width / 2, height / 2); // Nullstelle zur Bildmitte
  fill(0); // Textfarbe Schwarz
  stroke(0); // Koordinatensystem Schwarz

  strokeWeight(3);
  line(-width, 0, width, 0); // x-Nullstelle
  line(0, -height, 0, height); // y-Nullstelle

  strokeWeight(1);
  for (int i = -width; i < width; i += 50) {
    line(i, -height, i, height); // Vertikale Linien
    text(i, i - 2, 15); // Beschriftung (15 für unterhalb der Mittellinie)
  }
  
  for (int i = -height; i < height; i += 50) {
    line(-width, i, width, i); // Horizontale Linien
    text(i, - 2, i - 2); // Beschriftung
  }
  
  strokeWeight(2);
  for(int x = -width; x < width; x++) {
    stroke(255, 0, 0);
    point(x, a * 2 + b);
    stroke(0, 255, 0);
    point(x, b * sin(x / 100f));
    stroke(0, 0, 255);
    point(x, 10 - pow(a, x / b));
  }
}
