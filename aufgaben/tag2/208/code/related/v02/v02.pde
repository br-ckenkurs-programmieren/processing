void setup() {
  size(1000, 800);
  background(255);
}

void draw() {
  background(255);

  strokeWeight(1);
  line(0, height/2, width, height/2);

  strokeWeight(10);

  float a = mouseX;
  float b = mouseY;

  for (int x = 0; x < width; x++) {

    float fx = a * sin(x/b) + 150;

    float gx = a + (b/100) * x;

    float hx = 100 + b/100 * pow(x, a/100);

    stroke(#FF0000);
    point(x, height/2 - fx);

    stroke(#00FF00);
    point(x, height/2 - gx);

    stroke(#0000FF);
    point(x, height/2 - hx);
  }
}
