void setup() {
  size(800, 600);
  background(255);
  textSize(15);
}

void draw() {
  background(255);
  //We don't want to start in the top left corner without any offset
  translate(50,50);
  
  // Draw x- and y-axis
  strokeWeight(1);
  stroke(0);
  line(0, 0, width, 0);
  line(0, 0, 0, height);
  
  // labeling of y-axis
  for(int i = 0; i < height; i+=25) {
    if(i%100==0) {
      text(i,-50,i+5);
      line(0,i,-20,i);
    }
    else {
      line(0,i,-10,i);
    }
    
  }
  
  // labeling of x-axis
  for(int i = 0; i < width; i+=25) {
    if(i%100==0) {
      text(i,i-10,-30);
      line(i,0,i,-20);
    }
    else {
      line(i,0,i,-10);
    }
    
  }
  
  // infotexts about the graphs
  fill(255,0,0);
  text("f(x) = a * sin(x/b)",10,height-120);
  
  fill(0,255,0); 
  text("g(x) = a + (b/100) * x",10,height-100);
  
  fill(0,0,255);
  text("h(x) = 100+b/100*x^a/100",10,height-80);

  strokeWeight(5);

  float a = mouseX/5;
  float b = mouseY/5;
  
  
  // Calculating f(x), g(x) and h(x)
  for (int x = 0; x < width-50; x++) {

    float fx = a * sin(x/b) + 150;

    float gx = a + (b/100) * x;

    float hx = 100 + b/100 * pow(x, a/100);
    
    // drawing the graph
    stroke(255,0,0);    
    point(x,fx);    

    stroke(0,255,0);
    point(x,gx);

    stroke(0,0,255);
    point(x,hx);
  }
}
