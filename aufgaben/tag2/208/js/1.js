function setup() {
    createCanvas(600, 600);
    // Text an der rechten Seite verankern
    textAlign(LEFT);
    // Textgröße
    textSize(15);
}

function draw() {
    var a = mouseX - width / 2;
    var b = mouseY - height / 2;
    // Weißer Hintergrund
    background(255);
    // Nullstelle zur Bildmitte
    translate(width / 2, height / 2);
    // Textfarbe Schwarz
    fill(0);
    // Koordinatensystem Schwarz
    stroke(0);
    strokeWeight(3);
    // x-Nullstelle
    line(-width, 0, width, 0);
    // y-Nullstelle
    line(0, -height, 0, height);
    strokeWeight(1);
    for (var i = -width; i < width; i += 50) {
        // Vertikale Linien
        line(i, -height, i, height);
        // Beschriftung (15 für unterhalb der Mittellinie)
        text(i, i - 2, 15);
    }
    for (var j = -height; j < height; j += 50) {
        // Horizontale Linien
        line(-width, j, width, j);
        // Beschriftung
        text(j, -2, j - 2);
    }
    strokeWeight(2);
    for (var x = -width; x < width; x++) {
        stroke(255, 0, 0);
        point(x, a * 2 + b);
        stroke(0, 255, 0);
        point(x, b * sin(x / 100));
        stroke(0, 0, 255);
        point(x, 10 - pow(a, x / b));
    }
}
