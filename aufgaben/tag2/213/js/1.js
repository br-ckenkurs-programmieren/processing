var mX = 250; // Mittelpunkt x
var mY = 250; // Mittelpunkt y
var r  = 200; // Radius

function setup() {
  frameRate(10);
  createCanvas(500, 500);
}

function draw() {
  background(220);
  // Ziffernblatt
  fill(255, 255, 255);
  stroke(127, 127, 127);
  
  strokeWeight(5);
  circle(250, 250, 400);
  
  strokeWeight(3);
  for (var i = 0; i < 2*PI; i += PI/6) {
      line(
        mX + sin(i) * r, 
        mY - cos(i) * r, 
        mX + sin(i) * 0.9*r,
        mY - cos(i) * 0.9*r
      );
  }
  
  strokeWeight(6);
  for (var i = 0; i < 2*PI; i += PI/2) {
      line(
        mX + sin(i) * r,
        mY - cos(i) * r,
        mX + sin(i) * 0.8*r, 
        mY - cos(i) * 0.8*r
      );
  }
  
  // Aktuelle Zeit abrufen
  var s = second() * PI / 30.0;
  var m = minute() * PI / 30.0;
   h = hour() * PI / 6.0;
  
  // Sekundenzeiger
  strokeWeight(1);
  line(
    mX, 
    mY, 
    mX + sin(s) * r, 
    mY - cos(s) * r
  );
  
  // Minutenzeiger
  strokeWeight(3);
  line(
    mX, 
    mY, 
    mX + sin(m) * 0.9*r, 
    mY - cos(m) * 0.9*r
  );
  
  // Stundenzeiger
  strokeWeight(5);
  line(
    mX, 
    mY, 
    mX + sin(h) * 0.6*r, 
    mY - cos(h) * 0.6*r
  );
}

