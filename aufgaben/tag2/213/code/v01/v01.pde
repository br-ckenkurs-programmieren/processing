float mX = 250; // Mittelpunkt x
float mY = 250; // Mittelpunkt y
float r  = 200; // Radius

void setup() {
  frameRate(10);
  background(0, 0, 0);
  size(500, 500);
  
  // Ziffernblatt
  fill(255, 255, 255);
  stroke(127, 127, 127);
  
  strokeWeight(5);
  circle(250, 250, 400);
  
  strokeWeight(3);
  for (float i = 0; i < 2*PI; i += PI/6) {
      line(
        mX + sin(i) * r, 
        mY - cos(i) * r, 
        mX + sin(i) * 0.9*r,
        mY - cos(i) * 0.9*r
      );
  }
  
  strokeWeight(6);
  for (float i = 0; i < 2*PI; i += PI/2) {
      line(
        mX + sin(i) * r,
        mY - cos(i) * r,
        mX + sin(i) * 0.8*r, 
        mY - cos(i) * 0.8*r
      );
  }
  
  // Aktuelle Zeit abrufen
  float s = second() * PI / 30;
  float m = minute() * PI / 30;
  float h = hour() * PI / 6;
  
  // Sekundenzeiger
  strokeWeight(1);
  line(
    mX, 
    mY, 
    mX + sin(s) * r, 
    mY - cos(s) * r
  );
  
  // Minutenzeiger
  strokeWeight(3);
  line(
    mX, 
    mY, 
    mX + sin(m) * 0.9*r, 
    mY - cos(m) * 0.9*r
  );
  
  // Stundenzeiger
  strokeWeight(5);
  line(
    mX, 
    mY, 
    mX + sin(h) * 0.6*r, 
    mY - cos(h) * 0.6*r
  );
}

void draw() {
  setup();
}
