// Grad der Rotation des Würfels
float rotation = 0;
// Geschwindigkeit der Rotation; um diesen Wert verändert sich die Variable "rotation"
float r_speed = 0;

void setup(){
  // Umstellen auf 3D
  size(400,400,P3D);
  // Verschieben des Ursprungs in die Mitte damit der Würfel um die eigene Achse rotieren kann
  translate(200,200,0);
  fill(255,0,0);
  frameRate(10);
}

void draw(){
  background(255);
  translate(200,200,0);
  // Würfel um die eigene Achse rotieren lassen
  rotateY(rotation);
  rotateX(rotation * 0.5);
  // Würfel zeichnen (box zeichnet immer an 0|0|0)
  box(100);
  
  // Reduzierung der Geschwindigkeit, damit der Würfel langsamer wird und irgendwann stehen bleibt
  if(r_speed>0) {
    r_speed-=0.05;
  }
  
  // Rotationswinkel updaten
  rotation += r_speed;
  
}

void mouseClicked() {
  // Mausklick setzt den Speed auf 1 und startet somit die Rotation
  r_speed = 1;
}
