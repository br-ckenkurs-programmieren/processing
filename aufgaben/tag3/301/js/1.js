
var x1, x2;

var finishLine;

function setup() {
    initializeFields();
    createCanvas(800, 350);
    finishLine = width - 45;
    textSize(50);
    textAlign(CENTER);
    strokeCap(PROJECT);
}

function draw() {
    background(color(0xfe, 0xe6, 0x9c));
    drawFinishLine();
    strokeWeight(1);
    fill(color(0xf8, 0x7a, 0x63));
    circle(x1, 100, 50);
    fill(color(0x42, 0x7a, 0xa1));
    circle(x2, height - 100, 50);
    if (x1 > finishLine) {
        fill(0, 150);
        rect(0, 0, width, height);
        fill(255);
        text("Pferd 1 (oben) hat gewonnen!", width / 2, height / 2);
        noLoop();
    } else if (x2 > finishLine) {
        fill(0, 150);
        rect(0, 0, width, height);
        fill(255);
        text("Pferd 2 (unten) hat gewonnen!", width / 2, height / 2);
        noLoop();
    }
}

function keyReleased() {
    if (key == 'a' || key == 'd')
        x1 += 5;
    else if (keyCode == LEFT_ARROW || keyCode == RIGHT_ARROW)
        x2 += 5;
}

function drawFinishLine() {
    stroke(255);
    strokeWeight(20);
    line(finishLine, 0, finishLine, height);
    strokeWeight(10);
    stroke(0);
    for (var i = 0; i < height + 10; i += 20) {
        point(finishLine - 5, i);
        point(finishLine + 5, i + 10);
    }
}

function initializeFields() {
    x1 = 50;
    x2 = 50;
    finishLine = 0;
}

