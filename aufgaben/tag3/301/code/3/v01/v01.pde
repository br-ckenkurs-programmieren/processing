int x1 = 50, x2 = 50;
int finishLine;

void setup() {
  size(800, 350);
  finishLine = width - 45;
  textSize(50);
  textAlign(CENTER);
  strokeCap(PROJECT);
}

void draw() {
  background(#fee69c);

  drawFinishLine();

  strokeWeight(1);
  fill(#f87a63);
  circle(x1, 100, 50);
  fill(#427aa1);
  circle(x2, height - 100, 50);

  if (x1 > finishLine) {
    fill(0, 150);
    rect(0, 0, width, height);
    fill(255);
    text("Pferd 1 (oben) hat gewonnen!", width / 2, height / 2);
    noLoop();
  } else if (x2 > finishLine) {
    fill(0, 150);
    rect(0, 0, width, height);
    fill(255);
    text("Pferd 2 (unten) hat gewonnen!", width / 2, height / 2);
    noLoop();
  }
}

void keyReleased() {
  if (key == 'a' || key == 'd')
    x1 += 5;
  else if (key == CODED && (keyCode == LEFT || keyCode == RIGHT))
    x2 += 5;
}

void drawFinishLine() {
  stroke(255);
  strokeWeight(20);
  line(finishLine, 0, finishLine, height);

  strokeWeight(10);
  stroke(0);
  for (int i = 0; i < height + 10; i += 20) {
    point(finishLine - 5, i);
    point(finishLine + 5, i + 10);
  }
}
