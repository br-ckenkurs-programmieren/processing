void setup() {
  size(800, 350);
}

void draw() {
  strokeCap(PROJECT);
  strokeWeight(1);
  background(#fee69c);

  fill(#f87a63);
  circle(50, 100, 50);
  fill(#427aa1);
  circle(50, height - 100, 50);

  drawFinishLine();
}

void drawFinishLine() {
  stroke(255);
  strokeWeight(20);
  line(width - 45, 0, width - 45, height);

  strokeWeight(10);
  stroke(0);
  for (int i = 0; i < height + 10; i += 20) {
    point(width - 50, i);
    point(width - 40, i + 10);
  }
}
