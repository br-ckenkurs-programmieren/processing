int x1 = 50, x2 = 50;

void setup() {
  size(800, 350);
}

void draw() {
  strokeCap(PROJECT);
  background(#fee69c);

  drawFinishLine();

  strokeWeight(1);
  fill(#f87a63);
  circle(x1, 100, 50);
  fill(#427aa1);
  circle(x2, height - 100, 50);
}

void keyPressed() {
  if (key == 'a' || key == 'd')
    x1 += 5;
  else if (key == CODED && (keyCode == LEFT || keyCode == RIGHT))
    x2 += 5;
}

void drawFinishLine() {
  stroke(255);
  strokeWeight(20);
  line(width - 45, 0, width - 45, height);

  strokeWeight(10);
  stroke(0);
  for (int i = 0; i < height + 10; i += 20) {
    point(width - 50, i);
    point(width - 40, i + 10);
  }
}
