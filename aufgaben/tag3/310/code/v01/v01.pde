int[] cells = new int[128]; //Die Zellen einer Generation
int generation = 0; //Der Index der Generation
int size = 8; //Die Größe jeder Zelle

//Das Regelset, was anhand der Grafik erstellt wurde. Kann beliebig bearbeitet werden
int[] rules = {0, 0, 0, 1, 1, 1, 1, 0}; 

void setup() {
  size(1000, 1000);
  cells[64] = 1 ; //Die "Startzelle" der Generation 0
}

void draw() {
  noStroke();
  if (generation >= cells.length)
    return; // Nur zeichnen, bis ein Quadrat gezeichnet wurde

  for (int i=0; i<cells.length; i++) {
    if (cells[i] == 1) 
      fill(0); //Lebende Zellen werden schwarz gezeichnet
    else
      fill(255);

    //Die generation gibt die Höhe an, wobei der Index im Array die Breit angibt
    rect(i * size, generation * size, size, size);
  }

  cells = makeNewGeneration();
}

int[] makeNewGeneration() {
  generation++;
  int[] next = new int[cells.length]; // nächste Generation

  for (int i=1; i<cells.length-1; i++) { //Anwendung der Regeln
    next[i] = getNextCell(cells[i-1], cells[i], cells[i+1]);
  }
  
  return next;
}

//Wendet die Regeln an, gibt je nach a b und c die nächste Zelle zurück
int getNextCell(int a, int b, int c) {
  switch(a + " " + b + " " + c) {
  case "1 1 1":
    return rules[0];
  case "1 1 0":
    return rules[1];
  case "1 0 1":
    return rules[2];
  case "1 0 0":
    return rules[3];
  case "0 1 1":
    return rules[4];
  case "0 1 0":
    return rules[5];
  case "0 0 1":
    return rules[6];
  case "0 0 0":
    return rules[7];
  default:
    return 0;
  }
}
