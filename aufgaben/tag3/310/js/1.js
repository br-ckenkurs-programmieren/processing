
// Die Zellen einer Generation
var cells;

// Der Index der Generation
var generation;

// Die Größe jeder Zelle
var size;

// Das Regelset, was anhand der Grafik erstellt wurde. Kann beliebig bearbeitet werden
var rules;

function setup() {
    initializeFields();
    createCanvas(1000, 1000);
    // Die "Startzelle" der Generation 0
    cells[64] = 1;
}

function draw() {
    noStroke();
    if (generation >= cells.length)
        // Nur zeichnen, bis ein Quadrat gezeichnet wurde
        return;
    for (var i = 0; i < cells.length; i++) {
        if (cells[i] == 1)
            // Lebende Zellen werden schwarz gezeichnet
            fill(0);
        else
            fill(255);
        // Die generation gibt die Höhe an, wobei der Index im Array die Breit angibt
        rect(i * size, generation * size, size, size);
    }
    cells = makeNewGeneration();
}

function makeNewGeneration() {
    generation++;
    // nächste Generation
    var next = new Array(cells.length);
    for (var i = 1; i < cells.length - 1; i++) {
        // Anwendung der Regeln
        next[i] = getNextCell(cells[i - 1], cells[i], cells[i + 1]);
    }
    return next;
}

// Wendet die Regeln an, gibt je nach a b und c die nächste Zelle zurück
function getNextCell(a, b, c) {
    switch (a + " " + b + " " + c) {
        case "1 1 1":
            return rules[0];
        case "1 1 0":
            return rules[1];
        case "1 0 1":
            return rules[2];
        case "1 0 0":
            return rules[3];
        case "0 1 1":
            return rules[4];
        case "0 1 0":
            return rules[5];
        case "0 0 1":
            return rules[6];
        case "0 0 0":
            return rules[7];
        default:
            return 0;
    }
}

function initializeFields() {
    cells = new Array(128).fill(0);
    generation = 0;
    size = 8;
    rules = [0, 0, 0, 1, 1, 1, 1, 0];
}

