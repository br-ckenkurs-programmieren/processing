let yTick = 0;
let xTick = 0;
let xAxisPos = 0;
let y = [3.0, 2.5, 3.5, 3.2, 3.3, 3.2];
let x = [2010, 2011, 2012, 2013, 2014, 2015];

let alpha = 0;
let beta = 0;

function linearRegression() {
  let sumX = 0;
  let sumY = 0;
  
  for (let i = 0; i < x.length; i++) {
    sumX = sumX+x[i];
  }
  
  for (let i = 0; i < y.length; i++) {
    sumY = sumY+y[i];
  }
  
  let meanX = sumX / x.length;
  let meanY = sumY / y.length;
  
  let betaNumerator   = 0;
  let betaDenominator = 0;
  
  for (let i = 0; i < y.length; i++) {
    betaNumerator   += (x[i] - meanX) * (y[i] - meanY);
    betaDenominator += pow(x[i] - meanX, 2);
  }
  
  beta  = betaNumerator / betaDenominator;
  alpha = meanY - beta * meanX;
}

function f(x) {
  return x * beta + alpha;
}

function xPosition(x) {
  return xTicks[x-2010];
}

function yPosition(y) {
  return height * (1 - (y - 2) / 1.8);
}

let xTicks = [];

function setup() {
  linearRegression();

  createCanvas(600, 400);
  background(255);

  // Dimensionen festlegen
  yTick = height / 9;
  xTick = width / 8;
  xAxisPos = height - yTick;
  
  for (let i = 0; i < y.length; i++) {
     xTicks[i] = xTick*(i+2);
  }
  
  koordinatenSystem();
  plotValues();
  plotRegression();
}

function displayValues() {
    stroke(127);
    let xPos = 0;
    
    for (let i = 1; i < xTicks.length; i++) {
      if (abs(xTicks[i] - mouseX) < abs(xTicks[xPos] - mouseX)) {
        xPos = i;
      }
    }
    
    line(xTicks[xPos], 0, xTicks[xPos], height);
    
    textAlign(RIGHT);
    
    stroke(31, 119, 180); // tab:blue
    fill(31, 119, 180); // tab:blue
    circle(xTicks[xPos], yPosition(y[xPos]), 6);
    text(y[xPos], xTicks[xPos] - 5, yPosition(y[xPos]));
    
    stroke(255, 127, 14); // tab:orange
    fill(255, 127, 14); // tab:orange
    circle(xTicks[xPos], yPosition(f(x[xPos])), 6);
    text(f(x[xPos]), xTicks[xPos] - 5, yPosition(f(x[xPos])));
}

function draw() {
  background(255);
  koordinatenSystem();
  plotValues();
  plotRegression();
  displayValues();
}

function plotValues() {
  stroke(31, 119, 180); // tab:blue
  for (let i = 1; i < 6; i++) {
    line(xTicks[i-1], yPosition(y[i-1]), xTicks[i], yPosition(y[i]));
  }
}

function plotRegression() {
  stroke(255, 127, 14); // tab:orange
  line(xTicks[0], yPosition(f(x[0])), xTicks[5], yPosition(f(x[5])));
}

function koordinatenSystem() {
  textAlign(RIGHT); // Text an der rechten Seite verankern
  textSize(15); // Textgröße
  stroke(0);
  fill(0);
  strokeWeight(2);
  line(xTick, 0, xTick, height);
  line(0, xAxisPos, width, xAxisPos);
  
  strokeWeight(1);
  
  for (let i = 0; i < 7; i++) {
    let y    = 2.4 + 0.2*i;
    let yPos = yPosition(y);
    
    line(xTick, yPos, xTick - 5, yPos);
    text(y, xTick - 10, yPos + 6);
  }
  
  textAlign(CENTER);
  for (let i = 0; i < 6; i++) {
    line(xTicks[i], xAxisPos, xTicks[i], xAxisPos + 5);
    text(2010 + i, xTicks[i], xAxisPos + 20);
  }
}