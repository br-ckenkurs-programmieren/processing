// Variablen für die Funktionen:
float[] a, b, c, d, e, f, p;

// Koordinaten des aktuellen Punktes:
float x, y;

// Hilfsvariablen um den nächsten Punkt zu berechnen
float newX, newY;

// Anzahl der gezeichneten Punkte:
int points = 50000;

// Faktor um den der Farn vergrößert wird (sonst ist das Bild winzig)
float factor = 50;

void setup() {
  size(400, 600);

 /* Für grüne Punkte auf schwarzem Hintergrund */
  background(0);
  stroke(0, 255, 0);
  
  /*
    Mit strokeWeight(); lässt sich die Punktgröße verändern, 
   um auch bei weniger gezeichneten Punkten ein gutes Ergebnis zu erzielen.
   */
  
  strokeWeight(2);

  //Variablen für den Farn: Schwarzstieliger Streifenfarn
  a = new float[]{0, 0.85, 0.2, -0.15};
  b = new float[]{0, 0.04, -0.26, 0.28};
  c = new float[]{0, -0.04, 0.23, 0.26};
  d = new float[]{0.16, 0.85, 0.22, 0.24};
  e = new float[]{0, 0, 0, 0};
  f = new float[]{0, 1.6, 1.6, 0.44};

  // Wahrscheinlichkeiten für die vier Funktionen
  p = new float[]{0.01, 0.85, 0.07, 0.07};

  // Variablen für Cyclosorus (Sumpffarngewächs):

  /*
   a = new float[]{0, 0.95, 0.035, -0.04};
   b = new float[]{0, 0.005, -0.2, 0.2};
   c = new float[]{0, -0.005, 0.16, 0.16};
   d = new float[]{0.25, 0.93, 0.04, 0.04};
   e = new float[]{0, -0.002, -0.09, 0.083};
   f = new float[]{-0.4, 0.5, 0.02, 0.12};
   p = new float[]{0.02, 0.84, 0.07, 0.07};
   */

  // Der Startpunkt ist (0,0)
  x = 0;
  y = 0;

  // In jedem Schleifendurchlauf wird ein Punkt gezeichnet:
  drawPoints(points);

  noLoop();
}

int chooseFunction(float i) {
  float max = 0;
  int index = -1;
  while (i > max) {
    max += p[++index];
  }
  return index;
}

void drawPoints(int points) {
  for (int i = 0; i < points; i++) {

    int index = chooseFunction(random(1));

    newX = a[index] * x + b[index] * y + e[index];
    newY = c[index] * x + d[index] * y + f[index];

    // Erst nachdem beide neuen Werte bestimmt wurden werden diese in x und y gespeichert
    x = newX;
    y = newY;

    /* 
     x und y werden mit den Faktor multipliziert (y mit -factor, da der Farn sonst nach unten wächst) und
     der Startpunkt mittig an den unteren Bildrand verlegt.
     */
    point(x * factor + width/2, y * -factor + height);
  }
}

void draw() {
}
