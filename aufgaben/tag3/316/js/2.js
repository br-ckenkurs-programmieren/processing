//***** Sumpffarngewächs Cyclosorus ******/


var a, b, c, d, e, f, p;

// Koordinaten des aktuellen Punktes:
var x, y;

// Hilfsvariablen um den nächsten Punkt zu berechnen
var newX, newY;

// Anzahl der gezeichneten Punkte:
var points;

// Faktor um den der Farn vergrößert wird (sonst ist das Bild winzig)
var factor;

function setup() {
    initializeFields();
    createCanvas(400, 600);
    background(0);
    /*
    Mit strokeWeight(); lässt sich die Punktgröße verändern, 
   um auch bei weniger gezeichneten Punkten ein gutes Ergebnis zu erzielen.
   */
    stroke(0, 255, 0);
    strokeWeight(2);
    
   // Variablen für Cyclosorus (Sumpffarngewächs):
   a = [0.0, 0.95, 0.035, -0.04];
   b = [0.0, 0.005, -0.2, 0.2];
   c = [0.0, -0.005, 0.16, 0.16];
   d = [0.25, 0.93, 0.04, 0.04];
   e = [0.0, -0.002, -0.09, 0.083];
   f = [-0.4, 0.5, 0.02, 0.12];
   p = [0.02, 0.84, 0.07, 0.07];

    // Wahrscheinlichkeiten für die vier Funktionen
    p = [ 0.01, 0.85, 0.07, 0.07 ];
    // Der Startpunkt ist (0,0)
    x = 0;
    y = 0;
    // In jedem Schleifendurchlauf wird ein Punkt gezeichnet:
    drawPoints(points);
    noLoop();
}

function chooseFunction(i) {
    var max = 0;
    var index = -1;
    while (i > max) {
        max += p[++index];
    }
    return index;
}

// noprotect
function drawPoints(points) {
    for (var i = 0; i < points; i++) {
        var index = chooseFunction(random(1));
        newX = a[index] * x + b[index] * y + e[index];
        newY = c[index] * x + d[index] * y + f[index];
        // Erst nachdem beide neuen Werte bestimmt wurden werden diese in x und y gespeichert
        x = newX;
        y = newY;
        /* 
     x und y werden mit den Faktor multipliziert (y mit -factor, da der Farn sonst nach unten wächst) und
     der Startpunkt mittig an den unteren Bildrand verlegt.
     */
        point(x * factor + width / 2, y * -factor + height);
    }
}

function draw() {
}

function initializeFields() {
    a = null;
    b = null;
    c = null;
    d = null;
    e = null;
    f = null;
    p = null;
    x = 0;
    y = 0;
    newX = 0;
    newY = 0;
    points = 50000;
    factor = 50;
}