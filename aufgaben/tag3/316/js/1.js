/******  Schwarzstieliger Streifenfarn ******/

var a, b, c, d, e, f, p;

// Koordinaten des aktuellen Punktes:
var x, y;

// Hilfsvariablen um den nächsten Punkt zu berechnen
var newX, newY;

// Anzahl der gezeichneten Punkte:
var points;

// Faktor um den der Farn vergrößert wird (sonst ist das Bild winzig)
var factor;

function setup() {
    initializeFields();
    createCanvas(400, 600);
    background(0);
    /*
    Mit strokeWeight(); lässt sich die Punktgröße verändern, 
   um auch bei weniger gezeichneten Punkten ein gutes Ergebnis zu erzielen.
   */
    stroke(0, 255, 0);
    strokeWeight(2);
    // Variablen für den Farn: Schwarzstieliger Streifenfarn
    a = [ 0, 0.85, 0.2, -0.15 ];
    b = [ 0, 0.04, -0.26, 0.28 ];
    c = [ 0, -0.04, 0.23, 0.26 ];
    d = [ 0.16, 0.85, 0.22, 0.24 ];
    e = [ 0, 0, 0, 0 ];
    f = [ 0, 1.6, 1.6, 0.44 ];
    // Wahrscheinlichkeiten für die vier Funktionen
    p = [ 0.01, 0.85, 0.07, 0.07 ];
    // Der Startpunkt ist (0,0)
    x = 0;
    y = 0;
    // In jedem Schleifendurchlauf wird ein Punkt gezeichnet:
    drawPoints(points);
    noLoop();
}

function chooseFunction(i) {
    var max = 0;
    var index = -1;
    while (i > max) {
        max += p[++index];
    }
    return index;
}

// noprotect
function drawPoints(points) {
    for (var i = 0; i < points; i++) {
        var index = chooseFunction(random(1));
        newX = a[index] * x + b[index] * y + e[index];
        newY = c[index] * x + d[index] * y + f[index];
        // Erst nachdem beide neuen Werte bestimmt wurden werden diese in x und y gespeichert
        x = newX;
        y = newY;
        /* 
     x und y werden mit den Faktor multipliziert (y mit -factor, da der Farn sonst nach unten wächst) und
     der Startpunkt mittig an den unteren Bildrand verlegt.
     */
        point(x * factor + width / 2, y * -factor + height);
    }
}

function draw() {
}

function initializeFields() {
    a = null;
    b = null;
    c = null;
    d = null;
    e = null;
    f = null;
    p = null;
    x = 0;
    y = 0;
    newX = 0;
    newY = 0;
    points = 50000;
    factor = 50;
}