
var p;

var v;

var pSize;

var sun;

var g;

var sunSize;

var mass;

function setup() {
    initializeFields();
    createCanvas(1000, 1000);
    strokeWeight(3);
}

function draw() {
    background(0, 50);
    var grav = g * (mass / pow(p5.Vector.dist(p, sun), 2));
    var dir = atan2(sun.y - p.y, sun.x - p.x);
    var result = p5.Vector.fromAngle(dir);
    result.setMag(grav);
    v.add(result);
    p.add(v);
    var line1 = p5.Vector.add(p, v.copy().mult(50));
    var line2 = p5.Vector.add(p, result.mult(2000));
    stroke(255, 0, 0);
    line(p.x, p.y, line1.x, line1.y);
    stroke(0, 0, 255);
    line(p.x, p.y, line2.x, line2.y);
    stroke(150);
    circle(sun.x, sun.y, sunSize);
    circle(p.x, p.y, pSize);
}

function initializeFields() {
    p = new p5.Vector(200, 500);
    v = new p5.Vector(0, 2.5);
    pSize = 30;
    sun = new p5.Vector(500, 500);
    g = 0.001;
    sunSize = 100;
    mass = 3000000;
}

