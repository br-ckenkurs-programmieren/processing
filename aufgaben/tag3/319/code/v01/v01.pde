PVector p = new PVector(200, 500);
PVector v = new PVector(0, 2.5);
float pSize = 30;

PVector sun = new PVector(500, 500);
float g = 0.001;
float sunSize = 100;
float mass = 3000000;

void setup() {
  size(1000, 1000);
  strokeWeight(3);
}

void draw() {
  background(0);

  float grav = g * (mass / pow(PVector.dist(p, sun), 2));
  float dir = atan2(sun.y - p.y, sun.x - p.x);

  PVector result = PVector.fromAngle(dir);
  result.setMag(grav);

  v.add(result);
  p.add(v);

  PVector line = PVector.add(p, v.copy().mult(50));
  PVector line2 = PVector.add(p, result.mult(2000));

  stroke(255, 0, 0);
  line(p.x, p.y, line.x, line.y);
  stroke(0, 0, 255);
  line(p.x, p.y, line2.x, line2.y);

  stroke(150);
  circle(sun.x, sun.y, sunSize);
  circle(p.x, p.y, pSize);
}
