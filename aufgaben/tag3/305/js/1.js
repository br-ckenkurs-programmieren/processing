var stripes;

function setup() {
    initializeFields();
    createCanvas(500, 200);
    noStroke();
    for (var i = 0; i < stripes.length; i++) stripes[i] = random(1) >= 0.5;
}

function draw() {
    for (var i = 0; i < stripes.length; i++) {
        if (stripes[i])
            fill(0);
        else
            fill(255);
        rect(i * 10, 0, 10, height);
    }
}

function mousePressed() {
    var index = floor(mouseX / 10.0);
    stripes[index] = !stripes[index];
}

function initializeFields() {
    stripes = new Array(50);
}