color y = 200;

void setup() {
  size(400, 400);
}

void draw() {
  background(255);
  circle(200, y, 20);
}

void mouseWheel(MouseEvent event) {
  y += event.getCount(); 
}
