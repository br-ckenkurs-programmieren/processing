var y;

function setup() {
    initializeFields();
    createCanvas(400, 400);
}

function draw() {
    background(255);
    circle(200, y, 20);
}

function mouseWheel(event) {
    y += event.deltaY * 0.02;
}

function initializeFields() {
    y = 200;
}
