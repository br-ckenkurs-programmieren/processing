// player nimmt zur Unterscheidung der Spieler die Werte -1 und 1 an
var player = 1;

/* 
In dem Array wird der Zustand der einzelnen Spielfelder gespeichert.
Dabe werden die Spielerwerte '1' für X und '-1' für O verwendet.
Die Felder werden wie folgt nummeriert:
0 1 2
3 4 5
6 7 8 
*/
var fields = [0, 0, 0, 0, 0, 0, 0, 0, 0];

// Abstand oben links zwischen Rand und Board
var oB = 50;

// größe des Spielbrettes
var boardS = 300;

// größe einer Zelle
var cW = boardS / 3;

// ist das Spiel vorbei (Sieg oder Unentschieden)?
var isOver = false;

function setup() {
  createCanvas(400, 400);
  background(0);
  strokeWeight(8);
}
 
function draw() {
  drawGrid();
  drawSigns();
  gameOver();
}

function drawGrid(){
  background(0);
  noFill();
  stroke(255);
  rect(oB, oB, boardS, boardS);
  line(oB, oB + cW, boardS + oB, oB + cW);
  line(oB, oB + cW * 2, boardS + oB, oB + cW * 2);
  line(oB + cW, oB, oB + cW, boardS + oB);
  line(oB + cW * 2, oB, oB + cW * 2, boardS + oB);
}

function drawSigns(){
  for (var x = 0; x < fields.length; x++) {
    if (fields[x] == 1) {
      stroke(255, 0, 0);
      line(65 + x%3 * cW, 65 + floor(x/3) * cW, 135 + x%3 * cW, 135 + floor(x/3) * cW);
      line(135 + x%3 * cW, 65 + floor(x/3) * cW, 65 + x%3 * cW, 135 + floor(x/3) * cW);
    } else if (fields[x] == -1) {
      stroke(50, 50, 255);
      circle((x%3 + 1) * cW, (floor(x/3) + 1) * cW, cW * 0.8);
    }
  }
}
 
function mouseClicked() {
  var pos = findTile(mouseX, mouseY);
  if ((pos != -1) && (fields[pos] == 0) && !isOver){
    fields[pos] = player;
    // Spieler wechseln
    player = -player
  }
}
 
function findTile(mX, mY) {
  if (mouseX < oB || mouseX > oB + boardS || mouseY < oB || mouseY > oB + boardS){
    // Maus nicht im Spielfeld
    return -1;
  } else {
    // mouseX und mouseY bekommen
    mX -= oB;
    // den linken Rand abgezogen
    mY -= oB;
    return floor(mX / cW) + floor(mY / cW) * 3;
  }
}
 
function findWin() {
  for(var i = 0; i < 3; i++){
    // prüfe Zeilen
    if(fields[i*3] != 0 && fields[i*3] == fields[i*3 + 1] && fields[i*3] == fields[i*3 + 2]){
      isOver = true;
      return fields[i*3];
    }
    // prüfe Spalten
    else if(fields[i] != 0 && fields[i] == fields[i+3] && fields[i] == fields[i+6]){
      isOver = true;
      return fields[i];  
    }
  }

  // prüfe Diagonalen
  if(fields[0] != 0 && fields[0] == fields[4] && fields[0] == fields[8]){
    isOver = true;
    return fields[0];
  }
  else if(fields[4] != 0 && fields[2] == fields[4] && fields[2] == fields[6]){
    isOver = true;
    return fields[2];
  }
  // alle Felder sind belegt (daher gibt es keine 0 mehr) -> Unentschieden
  else if(!fields.includes(0)){
    return 9;
  }
  else{
    return 0;
  }
}

function gameOver(){
  var winner = findWin();
  fill(255);
  stroke(0);
  textSize(35);
  if (winner == 1) {
    text("Game Over! X wins!", 0, 35);
  }
  else if (winner == -1) {
    text("Game Over! O wins!", 0, 35);
  }
  else if(winner == 9){
    text("Game Over! IT'S A DRAW", 0, 35);
  }
}