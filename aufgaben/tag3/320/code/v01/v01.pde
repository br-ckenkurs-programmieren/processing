// storing coordinates of all stars 
float [][] stars = new float [800][4];
float speed; 

void setup() {
  size(1000, 1000);
  background(0);
  noStroke();

  // creating all stars 
  for (int i = 0; i < stars.length; i++) {
    // creating x, y, z coordinates of the star
    float x = random(-width / 2, width / 2);
    float y = random(-height / 2, height / 2);
    float z = random(width);

    // storing coordinates in array
    stars[i][0] = x; 
    stars[i][1] = y; 
    stars[i][2] = z; 
    // storing previous z position
    stars[i][3] = z;
  }
}

void draw() {
  clear();

  // making animations happen according to center of the screen
  translate(width / 2, height / 2);

  // changing speed according to mouse moving
  speed = map(mouseX, 0, width, 0, 30);

  drawStars();
}

void drawStars() {
  for (int i = 0; i < stars.length; i++) {

    // extracting coordinates for readability reasons
    float x = stars[i][0];
    float y = stars[i][1];
    float z = stars[i][2];
    float pz = stars[i][3];
    
    fill(map(z, 0, width, 255, 0));

    // creating new x and y coordinates to have motion effect 
    // for every rerendering. x / z 
    float sx = map(x / z, 0, 1, 0, width);
    float sy = map(y / z, 0, 1, 0, height);

    // changing size of the star according to 
    // distance to the star
    float sz = map(z, 0, width, 5, 0);

    // drawing star with new coordinates 
    circle(sx, sy, sz);    

    // figuring out previous position of the star to draw the line
    float spx = map(x / pz, 0, 1, 0, width);
    float spy = map(y / pz, 0, 1, 0, height);

    // drawing line right after the star
    stroke(map(z, 0, width, 255, 0));
    strokeWeight(2);
    line(spx, spy, sx, sy);

    // storing previous z value
    stars[i][3] = z;

    // changing z coordinate to create move into effect
    stars[i][2] -= speed;

    // checking star is already out of the screen  
    if (stars[i][2] <= 0) {
      // recreating all coordinates to place new star on the screen
      stars[i][0] = random(-width / 2, width / 2);
      stars[i][1] = random(-height / 2, height / 2);
      stars[i][2] = random(width);
      stars[i][3] = stars[i][2];
    }
  }
}
