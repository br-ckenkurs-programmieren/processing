var femaleName;

var maleName;

var connective;

var surname;

var fullName;

function setup() {
    initializeFields();
    createCanvas(800, 150);
    textSize(35);
    textAlign(CENTER);
    fullName = getName();
}

function draw() {
    background(0);
    fill(255);
    text(fullName, width / 2, 100);
}

function keyPressed() {
    fullName = getName();
}

function getName() {
    if (random(2) > 1)
        return femaleName[int(random(femaleName.length))] + " die " + connective[int(random(connective.length))] + " " + surname[int(random(surname.length))];
    else
        return maleName[int(random(maleName.length))] + " der " + connective[int(random(connective.length))] + " " + surname[int(random(surname.length))];
}

function initializeFields() {
    femaleName = [ 'Anna', 'Emilia', 'Gudrun', 'Ursela', 'Olga', 'Maya', 'Gisela', 'Knort' ];
    maleName = [ "Olaf", "Peter", "Thor", "Nero", "Gnaruff", "Ronald", "Gregor", "Rüdiger" ];
    connective =  [ "Unermüdliche", "Schläfrige", "Hungrige", "Wütende", "Verschnupfte", "Blutende", "Unsportliche", "Schnelle", "Faule", "Fleißige", "Programmierende" ];
    surname =  [ "von und zu Kunz", "von Drenor", "", "von Gießen", "von Atlantis", "aus der Hölle" ];
    fullName = null;
}