String[] femaleName = new String[]{"Anna", "Emilia", 
  "Gudrun", "Ursela", "Olga", "Maya", "Gisela", "Knort"};
String[] maleName = new String[]{"Olaf", "Peter", "Thor", 
  "Nero", "Gnaruff", "Ronald", "Gregor", "Rüdiger"};
String[] connective = new String[]{"Unermüdliche", "Schläfrige", 
  "Hungrige", "Wütende", "Verschnupfte", "Blutende", "Unsportliche", 
  "Schnelle", "Faule", "Fleißige", "Programmierende"};
String[] surname = new String[]{"von und zu Kunz", "von Drenor", "", 
  "von Gießen", "von Atlantis", "aus der Hölle"};
String name;

void setup() {
  size(800, 150);
  textSize(35);
  textAlign(CENTER);
  name = getName();
}

void draw() {
  background(0);
  fill(255);
  text(name, width / 2, 100);
}

void keyPressed() {
  name = getName();
}

String getName() {
  if (random(2) > 1)
    return femaleName[(int) random(femaleName.length)] + " die " + 
      connective[(int) random(connective.length)] + " " + 
      surname[(int) random(surname.length)];
  else
    return maleName[(int) random(maleName.length)] + " der " + 
      connective[(int) random(connective.length)] + " " + 
      surname[(int) random(surname.length)];
}
