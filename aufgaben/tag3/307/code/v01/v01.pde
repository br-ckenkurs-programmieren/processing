PImage wheel;
float friction = 0.99;
float velocity = 0;
float rotation = 0;
String[] colors = new String[]{"Purple", "Blue", "Light Blue", 
  "Green", "Light Green", "Yellow", "Orange", "Red" };

void setup() {
  size(500, 500);
  wheel = loadImage("wheel.png");
  imageMode(CENTER);
  textAlign(CENTER);
  textSize(40);
}

void draw() {
  rotation = (rotation + velocity) % TWO_PI;
  velocity *= friction;

  background(255);

  pushMatrix();
  translate(width / 2, height / 2 - 40);
  rotate(rotation);

  image(wheel, 0, 0, 400, 400);

  popMatrix();

  fill(0);
  float rotStep = TWO_PI / colors.length;
  text(colors[floor(rotation / rotStep)], width / 2, height - 50);
}

void keyPressed() {
  velocity += 0.05;
}
