
var wheel;

var friction;

var velocity;

var rotation;

var colors;

function setup() {
    initializeFields();
    createCanvas(500, 500);
    imageMode(CENTER);
    textAlign(CENTER);
    textSize(40);
    // wheel = loadImage("https://i.imgur.com/D3fRMdD.png"); 
    wheel = loadImage("wheel.png");
}

function draw() {
    rotation = (rotation + velocity) % TWO_PI;
    velocity *= friction;
    background(255);
    push();
    translate(width / 2, height / 2 - 40);
    rotate(rotation);
    image(wheel, 0, 0, 400, 400);
    pop();
    fill(0);
    var rotStep = TWO_PI / colors.length;
    text(colors[floor(rotation / rotStep)], width / 2, height - 50);
}

function keyPressed() {
    velocity += 0.05;
}

function initializeFields() {
    wheel = null;
    friction = 0.99;
    velocity = 0;
    rotation = 0;
    colors = ["Purple", "Blue", "Light Blue", "Green", "Light Green", "Yellow", "Orange", "Red"];
}

