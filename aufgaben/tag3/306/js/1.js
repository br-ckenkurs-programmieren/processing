var students;

var gradePercentages;

var gradeValues;

function setup() {
    initializeFields();
    createCanvas(600, 400);
    calculateNewGrades();
    textSize(25);
}

function draw() {
    background(255);
    fill(0);
    text("Students: " + students, 25, 50);
    text("Average passing grade: " + nf(getAverageGrade(gradeValues), 1, 1), 25, 100);
    text("Average passing percentage: " + nf(getAveragePercentage(gradePercentages), 2, 0), 25, 150);
    text("sehr gut: " + countInRange(gradeValues, 0, 1.6), 25, 250);
    text("gut: " + countInRange(gradeValues, 1.61, 2.5), 25, 300);
    text("befriedigend: " + countInRange(gradeValues, 2.51, 3.5), 325, 250);
    text("ausreichend: " + countInRange(gradeValues, 3.51, 4), 325, 300);
    text("mangelhaft: " + countInRange(gradeValues, 5, 5), 150, 350);
}

function calculateNewGrades() {
    gradePercentages = new Array(students);
    gradeValues = new Array(students);
    for (var i = 0; i < students; i++) {
        gradePercentages[i] = round(random(100));
        gradeValues[i] = getGrade(gradePercentages[i]);
    }
}

function getGrade(percentage) {
    var grade = 4 - (percentage - 50) / (50 / 3.0);
    return grade > 4 ? 5 : grade;
}

function getAverageGrade(grades) {
    var gradeSum = 0;
    var passingGrades = 0;
    for (var i = 0; i < grades.length; i++) {
        if (grades[i] < 5) {
            gradeSum += grades[i];
            passingGrades++;
        }
    }
    return gradeSum / passingGrades;
}

function getAveragePercentage(percentages) {
    var percentSum = 0;
    var passingPercentages = 0;
    for (var i = 0; i < percentages.length; i++) {
        if (percentages[i] >= 50) {
            percentSum += percentages[i];
            passingPercentages++;
        }
    }
    return round(percentSum / passingPercentages);
}

function getFailingGrades(grades) {
    var failSum = 0;
    for (var i = 0; i < grades.length; i++) {
        if (grades[i] == 5)
            failSum++;
    }
    return failSum;
}

function countInRange(values, min, max) {
    var counter = 0;
    for (var i = 0; i < values.length; i++) {
        if (values[i] >= min && values[i] <= max)
            counter++;
    }
    return counter;
}

function keyPressed() {
    calculateNewGrades();
}

function initializeFields() {
    students = 200;
    gradePercentages = null;
    gradeValues = null;
}