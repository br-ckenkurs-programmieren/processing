PFont f;
int[] ergebnisse = new int[200];
float durchFallQuote;
int[] nva = new int[5];
float[] nvp = new float[5];
float durchschnittBestanden;
float standardAbweichung;

void setup()
{
    size(800, 120);
    background(0); 
    fill(255);
    f = createFont("Arial",16,true);
    textFont(f, 20); 
    
    float numDurch = 0;
    float summeNotenBestanden = 0;
    float summeNotenGroesserNull = 0;
    for (int i = 0; i < ergebnisse.length; i++)
    {
      int erg = (int) (random(1.0) * 100); 
      ergebnisse[i] = erg;
      
      if (erg >= 82) 
        nva[0] = nva[0] + 1;
      else if (erg >= 67)
        nva[1] = nva[1] + 1;
      else if (erg >= 52)
        nva[2] = nva[2] + 1;
      else if (erg >= 50)
        nva[3] = nva[3] + 1;
      else
        nva[4] = nva[4] + 1;
        
      if (erg > 0)
        summeNotenGroesserNull = summeNotenGroesserNull + erg;
      if (erg < 50)
        numDurch = numDurch + 1;
      else
        summeNotenBestanden += erg;
    }
    
    for (int i = 0; i < nva.length; i++)
      nvp[i] = (nva[i] / 200.0) * 100;
    
    durchFallQuote = (numDurch / 200)*100;
    float numBest = 200-numDurch;
    durchschnittBestanden = summeNotenBestanden / numBest;
    float durchschnitt = summeNotenGroesserNull / 200;
    float va = 0;
    for (int i = 0; i < ergebnisse.length; i++)
    {
      va += pow(ergebnisse[i]-durchschnitt, 2);  
    }
    va = va / durchschnitt;
    standardAbweichung = sqrt(va); 
}

void draw()
{
  clear();
  //das 200.0 ist wichtig, weil sonst eine Integer-Division ausgeführt wird und 
  //dann selbst 100 / 200 = 0 ergibt.
  text("Durchfallquote: "+durchFallQuote+"%!", 20, 20);
  text("Notenverteilung:", 20, 40);
  text("1.x: " + nva[0]+" (" + nvp[0] + "%), 2.x: " + nva[1] + " (" + nvp[1] + "%), 3.x: " + nva[2] + " (" + nvp[2] + "%), 4.0: " + nva[3] + " (" + nvp[3] + "%), 5.0: " + nva[4] + " (" + nvp[4] + "%)",
  20, 60);
  text("Durchschnittsnote der Bestehenden: "+durchschnittBestanden+"!", 20, 80);
  text("Standardabweichung größer Null: "+standardAbweichung+"!", 20, 100);
}

void mouseClicked()
{
    float numDurch = 0;
    float summeNotenBestanden = 0;
    float summeNotenGroesserNull = 0;
    for (int i = 0; i < ergebnisse.length; i++)
    {
      int erg = (int) (random(1.0) * 100); 
      ergebnisse[i] = erg;
      
      if (erg >= 82) 
        nva[0] = nva[0] + 1;
      else if (erg >= 67)
        nva[1] = nva[1] + 1;
      else if (erg >= 52)
        nva[2] = nva[2] + 1;
      else if (erg >= 50)
        nva[3] = nva[3] + 1;
      else
        nva[4] = nva[4] + 1;
        
      if (erg > 0)
        summeNotenGroesserNull = summeNotenGroesserNull + erg;
      if (erg < 50)
        numDurch = numDurch + 1;
      else
        summeNotenBestanden += erg;
    }
    
    for (int i = 0; i < nva.length; i++)
      nvp[i] = (nva[i] / 200.0) * 100;
    
    durchFallQuote = (numDurch / 200)*100;
    float numBest = 200-numDurch;
    durchschnittBestanden = summeNotenBestanden / numBest;
    float durchschnitt = summeNotenGroesserNull / 200;
    float va = 0;
    for (int i = 0; i < ergebnisse.length; i++)
    {
      va += pow(ergebnisse[i]-durchschnitt, 2);  
    }
    va = va / durchschnitt;
    standardAbweichung = sqrt(va); 
}
