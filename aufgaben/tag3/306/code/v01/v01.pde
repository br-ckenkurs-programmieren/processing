int students = 200;
int[] gradePercentages;
float[] gradeValues;

void setup() {
  size(600, 400);
  calculateNewGrades();
  textSize(25);
}

void draw() {
  background(255);
  fill(0);
  text("Students: " + students, 25, 50);
  text("Average passing grade: " + nf(getAverageGrade(gradeValues), 1, 1), 25, 100);
  text("Average passing percentage: " + nf(getAveragePercentage(gradePercentages), 2, 0), 25, 150);
  
  text("sehr gut: " + countInRange(gradeValues, 0, 1.6), 25, 250);
  text("gut: " + countInRange(gradeValues, 1.61, 2.5), 25, 300);
  text("befriedigend: " + countInRange(gradeValues, 2.51, 3.5), 325, 250);
  text("ausreichend: " + countInRange(gradeValues, 3.51, 4), 325, 300);
  text("mangelhaft: " + countInRange(gradeValues, 5, 5), 150, 350);
}

void calculateNewGrades() {
  gradePercentages = new int[students];
  gradeValues = new float[students];
  for (int i = 0; i < students; i++) {
    gradePercentages[i] = round(random(100));
    gradeValues[i] = getGrade(gradePercentages[i]);
  }
}

float getGrade(int percentage) {
  float grade = 4 - (percentage - 50) / (50 / 3f);
  return grade > 4 ? 5 : grade;
}


float getAverageGrade(float[] grades) {
  float gradeSum = 0;
  int passingGrades = 0;
  for (int i = 0; i < grades.length; i++) {
    if (grades[i] < 5) {
      gradeSum += grades[i];
      passingGrades++;
    }
  }
  return gradeSum / passingGrades;
}

int getAveragePercentage(int[] percentages) {
  float percentSum = 0;
  int passingPercentages = 0;
  for (int i = 0; i < percentages.length; i++) {
    if (percentages[i] >= 50) {
      percentSum += percentages[i];
      passingPercentages++;
    }
  }
  return round(percentSum / passingPercentages);
}

int getFailingGrades(float[] grades) {
  int failSum = 0;
  for (int i = 0; i < grades.length; i++) {
    if (grades[i] == 5)
      failSum++;
  }
  return failSum;
}

int countInRange(float[] values, float min, float max) {
  int counter = 0;
  for (int i = 0; i < values.length; i++) {
    if (values[i] >= min && values[i] <= max)
      counter++;
  }
  return counter;
}

void keyPressed() {
  calculateNewGrades();
}
