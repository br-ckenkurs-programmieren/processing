
var lamps;

var d;

function setup() {
    initializeFields();
    createCanvas(600, 600);
    textAlign(CENTER, CENTER);
    background(255);
    lamps = [[true, true, true, true, true],
    [true, true, true, true, true],
    [true, true, true, true, true],
    [true, true, true, true, true],
    [true, true, true, true, true]];
}

function draw() {
    background(74, 92, 102);
    for (var i = 0; i < lamps.length; i++) {
        for (var k = 0; k < lamps[i].length; k++) {
            if (lamps[i][k])
                fill(128, 168, 36);
            else
                fill(74, 92, 102);
            circle(width / 6 * (i + 1), height / 6 * (k + 1), d);
        }
    }
    if (hasWon())
        drawGameOver();
}

function hasWon() {
    for (let i = 0; i < lamps.length; i++) {
        for (var k = 0; k < lamps[i].length; k++) if (lamps[i][k])
            return false;
    }
    return true;
}

function drawGameOver() {
    background(128, 186, 36);
    textSize(100);
    text("Gewonnen", width / 2, height / 2);
    textSize(20);
    text("R drücken für Neustart", width / 2, height / 2 + 100);
}

function mousePressed() {
    if (hasWon())
        return;
    for (var i = 0; i < lamps.length; i++) {
        for (var k = 0; k < lamps[i].length; k++) {
            if (dist(width / 6 * (i + 1), height / 6 * (k + 1), mouseX, mouseY) <= d / 2) {
                lamps[i][k] = !lamps[i][k];
                if (i > 0)
                    lamps[i - 1][k] = !lamps[i - 1][k];
                if (i < lamps.length - 1)
                    lamps[i + 1][k] = !lamps[i + 1][k];
                if (k > 0)
                    lamps[i][k - 1] = !lamps[i][k - 1];
                if (k < lamps.length - 1)
                    lamps[i][k + 1] = !lamps[i][k + 1];
                return;
            }
        }
    }
}

function keyPressed() {
    if (key != 'r')
        return;
    for (var i = 0; i < lamps.length; i++) {
        for (var l = 0; l < lamps[i].length; l++) {
            lamps[i][l] = true;
        }
    }
}

function initializeFields() {
    lamps = null;
    d = 70;
}

