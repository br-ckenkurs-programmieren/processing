String[] words = new String[]{"Hello", "World", "Mystery", "Question", "Hangman", "Java", "Processing", "Secret", "Duck", "Frog", "Alpaca", "Lion", "Jazz", "Lasers", "Processing", "THM"};

char[] guesses;
char[] chars;
char[] display;
int x, guessCount;
String word;

void setup() {
  size(600, 600);
  background(255);

  x = 0;
  guesses = new char[26];
  word = words[(int) random(words.length)];
  chars = word.toLowerCase().toCharArray();
  display = new char[chars.length];
  for (int i = 0; i < display.length; i++)
    display[i] = '_';
}

void draw() {
  switch(x) {
  case 10:
    line(200, 200, 220, 240); // rechter Fuß
  case 9:
    line(200, 200, 180, 240); // linker Fuß
  case 8:
    line(200, 140, 230, 170); // rechte Hand
  case 7:
    line(200, 140, 170, 170); // linke Hand
  case 6:
    line(200, 120, 200, 200); // Körper
  case 5:
    ellipse(200, 100, 40, 40); // Kopf
  case 4:
    line(200, 50, 200, 80); // Seil
  case 3:
    line(100, 80, 130, 50); // Stützbalken
  case 2:
    line(100, 50, 200, 50); // oberer Balken
  case 1:
    line(100, 300, 100, 50); // linker Balken
  }
}
