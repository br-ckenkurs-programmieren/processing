PFont f;
String[] woerter = new String[]{"test", "Vogel", "blub"};
String wort = woerter[(int) random(woerter.length)];
boolean[] erraten = new boolean[wort.length()];
int falscheVersuche = 0;

boolean gameOver = false;
boolean won = false;

void setup()
{
  size(1000, 1000);  
  f = createFont("Arial", 16, true);
}

void draw()
{
  background(255);
  println(wort);
  fill(0);
  textFont(f, 32);
  String text = "";
  for (int i = 0; i < erraten.length; i++)
  {
    if (erraten[i])
      text += wort.charAt(i);
    else
      text += "_";
    text += " ";
  }
  text(text, 100, 100);
  //galgen-update
  if (falscheVersuche > 0)
    line(500, 250, 550, 200);
  if (falscheVersuche > 1)
    line(550, 200, 600, 250);
  if (falscheVersuche > 2)
    line(550, 200, 550, 150);
  if (falscheVersuche > 3)
    line(550, 150, 500, 150);
  if (falscheVersuche > 4)
    line(550, 150, 600, 150);
  if (falscheVersuche > 5)
    line(550, 150, 550, 100);
  fill(255);
  if (falscheVersuche > 6)
    circle(550, 100, 50);
  fill(0);
  if (falscheVersuche > 7)
    line(550, 75, 550, 25);
  if (falscheVersuche > 8)
    line(550, 25, 800, 25);
  if (falscheVersuche > 9)
    line(800, 25, 800, 300);
    
  if (gameOver)
  {
    textFont(f, 40);
    text("GAME OVER", 300, 500);
  }
  else if (won)
  {
    textFont(f, 40);
    text("YOU ARE WINNER!!1", 300, 500);
  }
}

void keyPressed()
{
  if (!gameOver)
  {
    println(key);
    boolean wrongGuess = true;
    for (int i = 0; i < erraten.length; i++)
    {
        if ((wort.charAt(i)+"").equalsIgnoreCase(key+""))
        {
           wrongGuess = false;
           if (!erraten[i])
           {
             erraten[i] = true;
           }
        }
    }
    if (wrongGuess)
    {
      falscheVersuche++;
      if (falscheVersuche > 9)
        gameOver = true;
    }
    boolean fertig = true;
    for (int i = 0; i < erraten.length; i++)
    {
      if (!erraten[i])
        fertig = false;
    }
    if (fertig)
      won = true;
  }
}
