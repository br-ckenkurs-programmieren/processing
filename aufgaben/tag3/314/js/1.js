
var words;

var guesses;

var chars;

var display;

var x, guessCount;

var word;

function setup() {
    initializeFields();
    createCanvas(600, 600);
    textSize(80);
    textAlign(CENTER);
    strokeWeight(2);
    x = 0;
    guesses = new Array(26);
    word = words[int(random(words.length))];
    chars = Array.from(word.toLowerCase());
    display = new Array(chars.length);
    for (var i = 0; i < display.length; i++) display[i] = '_';
}

function draw() {
    background(255);
    fill(0);
    text(display.join(" "), width / 2, height * 0.75);
    fill(255);
    switch (x) {
        case 10:
            // rechter Fuß
            line(200, 200, 220, 240);
        case 9:
            // linker Fuß
            line(200, 200, 180, 240);
        case 8:
            // rechte Hand
            line(200, 140, 230, 170);
        case 7:
            // linke Hand
            line(200, 140, 170, 170);
        case 6:
            // Körper
            line(200, 120, 200, 200);
        case 5:
            // Kopf
            ellipse(200, 100, 40, 40);
        case 4:
            // Seil
            line(200, 50, 200, 80);
        case 3:
            // Stützbalken
            line(100, 80, 130, 50);
        case 2:
            // oberer Balken
            line(100, 50, 200, 50);
        case 1:
            // linker Balken
            line(100, 300, 100, 50);
    }
    if (x >= 10) noLoop();
}

function isInArray(c, array) {
    return array.includes(c);
}

function keyReleased() {
    if (isInArray(key, guesses))
        return;
    if (isInArray(key, chars)) {
        for (var i = 0; i < chars.length; i++) {
            if (key == chars[i])
                display[i] = key;
        }
        guesses[guessCount] = key;
    } else {
        x++;
        guesses[guessCount] = key;
    }
}

function initializeFields() {
    words = ["Hello", "World", "Mystery", "Question", "Hangman", "Java", "Processing", "Secret", "Duck", "Frog", "Alpaca", "Lion", "Jazz", "Lasers", "Processing", "THM"];
    guesses = null;
    chars = null;
    display = null;
    x = 0;
    guessCount = 0;
    word = null;
}

