PShape triangle; //Object for the random figure
PVector v; // Vector Object to generate random points
int scale = 200;

void setup() {
  size(800, 800);

  // create a random figure with 4 points
  triangle = createShape();
  triangle.beginShape();
  for (int i = 0; i < 4; ++i) {
    //generate random values for the vector and store it in the array to access it
    v = PVector.random2D();
    //vector has values between -1 and 1 so to make it bigger its scaled
    triangle.vertex(v.x * scale, v.y * scale);
  }
  triangle.endShape(CLOSE);
}

void draw() {
  //translate the figure into the middle of the plane, random vectors can be negative and could be out of the window if we don't
  translate(400, 400);
  triangle.setFill(color(230, 130, 30));
  shape(triangle);
}
