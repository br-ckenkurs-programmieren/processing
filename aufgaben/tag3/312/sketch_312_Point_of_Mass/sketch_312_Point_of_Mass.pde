PShape triangle; //Object for the random figure
PVector v; // Vector Object to generate random points
int scale = 200;

PVector[] medians = new PVector[3];
PVector centerOfMass;

void setup() {
  size(800, 800);
  background(180);

  // create a random figure with 4 points
  triangle = createShape();
  triangle.beginShape();
  for (int i = 0; i < 4; ++i) {
    //generate random values for the vector and store it in the array to access it
    v = PVector.random2D();
    //vector has values between -1 and 1 so to make it bigger its scaled
    triangle.vertex(v.x * scale, v.y * scale);
  }
  triangle.endShape(CLOSE);

  calculateMedian(triangle);
  calculateCenterOfMass(medians);
}

void draw() {
  //translate the figure into the middle of the plane, random vectors can be negative and could be outside the window
  translate(400, 400);
  triangle.setFill(color(30, 230, 30));
  shape(triangle);

  // draw the medians of the sides as red dots
  for (int i = 0; i < medians.length; ++i) {
    fill(255, 0, 0);
    ellipse(medians[i].x, medians[i].y, 10, 10);
  }

  // draw the center of Mass as blue dot
  fill(50, 50, 150);
  ellipse(centerOfMass.x, centerOfMass.y, 10, 10);
}

void calculateMedian(PShape triangle) {
  for (int i = 0; i < triangle.getVertexCount(); ++i) {
    int j = i + 1;
    // after the last point, we have to go back to the first
    if ( j == 3 ) {
      j = 0;
    }
    PVector v1 = triangle.getVertex(i);
    PVector v2 = triangle.getVertex(j);
    PVector median = PVector.add(v1, v2);
    medians[i] = median.div(2);
  }
}

void calculateCenterOfMass(PVector[] med) {
  centerOfMass = PVector.add(med[0], med[1]);
  centerOfMass.add(med[2]);
  centerOfMass.div(3);
}
