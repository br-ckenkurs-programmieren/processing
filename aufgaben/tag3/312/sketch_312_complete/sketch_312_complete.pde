PShape triangle; //object for the random triangle
PVector v; // vector object to generate random points
int scale = 200;

boolean pickedUp = false, noAmplitude = true;
PVector trans;
int timeDangled = 0;
float amplitude;
float omega = 0.1;

PVector[] medians = new PVector[3];
PVector centerOfMass;

void setup() {
  size(1100, 1100);
  background(180);

  frameRate(60);
  // create a random figure with 4 points
  triangle = createShape();
  triangle.beginShape();
  for (int i = 0; i < 3; ++i) {
    //generate random values for the vector and store it in the array to access it
    v = PVector.random2D();
    //vector has values between -1 and 1 so to make it bigger its scaled
    triangle.vertex(v.x*scale, v.y*scale);
  }
  triangle.endShape(CLOSE);

  calculateMedian(triangle);
  calculateCenterOfMass(medians);
}

void draw() {
  background(180);
  drawTriangle();
}

void drawTriangle() {
  //translate to the middle of the screen, necessary throug PVector.random2D()
  translate(width/2, height/2);
  //push();
  if (pickedUp) {
    float angle;
    translate((mouseX-width/2), (mouseY-height/2));
    if (noAmplitude) {
      calcAmplitude();
    }
    ++timeDangled;
    //some damping from task description
    float damping = exp(-timeDangled/100.0);
    //formular for damped oscillation
    angle = amplitude * damping * cos((omega * timeDangled));
    
    // shape "jumps" some degrees, this is a gross try to compensate this. (seems to work xD)
    rotate(amplitude * -1.0);
    
    //the rotation should be around the y-Axis downwards, but it's not always... thats the main issue here
    rotate(angle);
  }
  
  
  triangle.setFill(color(30, 230, 30));
  shape(triangle);

  // draw the medians of the sides as red dots
  calculateMedian(triangle);
  for (int i = 0; i < medians.length; ++i) {
    fill(255, 0, 0);
    ellipse(medians[i].x, medians[i].y, 10, 10);
  }
  // draw the center of Mass as blue dot
  calculateCenterOfMass(medians);
  fill(50, 50, 150);
  ellipse(centerOfMass.x, centerOfMass.y, 10, 10);

  //pop();
}

// calculates the angle bewtewwn a lot Vector thats directed downwards and the vector to the center Of Mass
float calcAmplitude() {
  PVector lot = new PVector(0, 100);
  float sign = 1.0;
  if (centerOfMass.x > 0) {
    sign *= -1;
  }
  amplitude = PVector.angleBetween(centerOfMass, lot);
  println(degrees(amplitude));
  // the Amplitude has to be calculatet once at the beginning of every oscillation. to control this the noAmplitude bool is used
  noAmplitude = false;
  return sign * amplitude;
}

// transforms the Points of the shape to the current null position and recalculates the points of interest in relation
void mousePressed() {
  if (mouseInBoundsOfPoint(triangle)) {
    trans = new PVector((mouseX-width/2), (mouseY-height/2));
    for (int i = 0; i < triangle.getVertexCount(); ++i) {
      triangle.setVertex(i, triangle.getVertex(i).sub(trans));
    }
    calculateMedian(triangle);
    calculateCenterOfMass(medians);
    pickedUp = true;
  }
}

//resets everything to initial state
void mouseReleased() {
  if (pickedUp) {
    pickedUp = false;
    timeDangled = 0;
    noAmplitude = true;
    for (int i = 0; i < triangle.getVertexCount(); ++i) {
      triangle.setVertex(i, triangle.getVertex(i).add(trans));
    }
  }
}

// formular in task description
void calculateMedian(PShape triangle) {
  PVector v1;
  PVector v2;
  PVector median;
  for (int i = 0; i < triangle.getVertexCount(); ++i) {
    int j = i + 1;
    // after the last point, we have to go back to the first
    if ( j == 3 ) {
      j = 0;
    }
    v1 = triangle.getVertex(i);
    v2 = triangle.getVertex(j);
    median = PVector.add(v1, v2);
    medians[i] = median.div(2);
  }
}

void calculateCenterOfMass(PVector[] med) {
  centerOfMass = PVector.add(med[0], med[1]);
  centerOfMass.add(med[2]);
  centerOfMass.div(3);
}

// checks if mouse is in bounds for one of the points of the triangle.
boolean mouseInBoundsOfPoint(PShape triangle) {
  boolean inBounds = false;

  PVector v1 = triangle.getVertex(0);
  PVector v2 = triangle.getVertex(1);
  PVector v3 = triangle.getVertex(2);

  // the translation in y and x in draw() have to be comensated by the same amount
  if (dist(mouseX-width/2, mouseY-height/2, v1.x, v1.y) < 10) {
    inBounds = true;
  }

  if (dist(mouseX-width/2, mouseY-height/2, v2.x, v2.y) < 10) {
    inBounds = true;
  }

  if (dist(mouseX-width/2, mouseY-height/2, v3.x, v3.y) < 10) {
    inBounds = true;
  }

  return inBounds;
}
