function setup() {
    createCanvas(200, 200);
    frameRate(1);
}

function draw() {
    background(255);
    var arr = [random(0, width), random(0, height), random(0, width), random(0, height), random(0, width), random(0, height)];
    triangle(arr[0], arr[1], arr[2], arr[3], arr[4], arr[5]);
}