void setup() {
  int x = 11;
  int y = 310;
  int z = 67;
  println("Numbers : " + x + ", " + y + ", " + z);
  println("Max: " + maxOfThree(11, 310, 67));
}

int maxOfThree(int a, int b, int c) {
  if (a > b) {
    if (a > c) {
      return a;
    }
    return c;
  } else {
    if (b > c) {
      return b;
    }
    return c;
  }
}
