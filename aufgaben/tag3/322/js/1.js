function setup() {
  let x = 11;
  let y = 310;
  let z = 67;
  print("Numbers: " + x + ", " + y + ", " + z);
  print("Max: " + maxOfThree(11, 310, 67));
}

function maxOfThree(a, b, c) {
  if (a > b) {
    if (a > c) {
      return a;
    }
    return c;
  } else {
    if (b > c) {
      return b;
    }
    return c;
  }
}
