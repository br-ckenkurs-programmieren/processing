
//Schriften für normalen ablauf und für den "Victory Screen"
PFont f;
PFont f2;

//Bildwiederholungsrate
int fps = 10;

//Fängt ab, wenn die richtige Farbe getroffen wurde
boolean is_right = false;

// Pos und Radius der angezeigten Kreise
int num_cicrle = 5;
float[] x_circle = new float[num_cicrle];
float[] y_circle = new float[num_cicrle];
float circle_size = 100;

// Farben, die ein Kreis haben kann bzw. die gesucht werden können
int[] colours = new int[] {#FE0000, #FCBA03, #28FC03, #000000, #002FFF};
String[] colour_names = new String[] {"rot", "orange", "grün", "schwarz", "blau"};

// Farbe der aktuell dargestellten Kreises und die gesuchte Farbe
int circle_colour;
int wanted_colour;

// Zeit bis neue Kreise erzeugt werden
int time_left = 0;

// Zähler für Richtige und Falsche Treffer
int right = 0;
int wrong = 0;

//Zeit bis die richtige Farbe gefunden wurde
int timer;


void setup() {
  frameRate(fps);
  background(255);
  fullScreen();
  f = createFont("Arial", 16, true);
  f2 = createFont("Arial", 1000, true);
  // Gesuchte Farbe wird zufällig aus den vordefinierten Farben ausgewählt
  wanted_colour = (int) random(colours.length);
}

void draw() {

  //wenn vorher die richtige Farbe gefunden wurde, wird alles angehalten
  if (is_right) {
    background(255);
    textFont(f2, 100);
    text("Du hast " + timer + "ms gebraucht ;p", width/4, height/2);
  } else {
    // Solange die Zeit noch nicht abgelaufen ist, bleiben die Kreise. Danach werden neue generiert
    if (time_left <= 0) {
      for (int i = 0; i < num_cicrle; i++) {
        circle_colour = i;
        x_circle[i] = random(circle_size / 2, width-circle_size / 2);
        y_circle[i] = random(circle_size / 2, height-circle_size / 2);
      }

      time_left = (int) random(15, 50);
    } else time_left = time_left - 1;
    background(255);
    for (int i = 0; i< num_cicrle; i++) {
      fill(colours[i]);
      circle(x_circle[i], y_circle[i], circle_size);
    }

    //Zeit wird abhängig von der Bildrate erhöht
    timer = timer +(int) 1000/fps;

    // Scoreboard

    fill(colours[wanted_colour]);
    textFont(f);
    text("Finde den Kreis mit der Farbe "+colour_names[wanted_colour]+"!", 30, 30);
    text("Treffer: "+right, 30, 60);
    text("Fehler: "+wrong, 30, 90);
    text("Timer:" +timer +"ms", 30, 120);
  }
}
void mouseReleased() {

  if (is_right) {
    is_right = false;
    timer = 0;
    delay(50);
  } else {
    for (int i = 0; i < num_cicrle; i++) {
      if (dist(mouseX, mouseY, x_circle[i], y_circle[i]) < circle_size / 2) {
        // Falls der richtige Kreis angeclickt wurde,
        // wird eine neue gesuchte Farbe ausgewählt und man kriegt einen Punkt.
        // Falls ein falscher Kreis angeclickt wurde, kriegt man einen Fehler und das Spiel geht weiter.
        if (i == wanted_colour) {
          time_left = 0;
          right = right +1;
          wanted_colour = (int) random(colours.length);
          is_right = true;
        } else wrong = wrong + 1;
      }
    }
  }
}
