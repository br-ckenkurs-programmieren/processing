PFont f;

float xbild;
float ybild;
float groesse_bild = 100;

PImage[] bilder;
String[] bilder_namen = new String[] {"PC", "kaffee", "Rose"};
int[] reaktionszeitenBilder = new int[bilder_namen.length];
int[] treffer_bilder = new int[bilder_namen.length];

int momentanes_bild;
int gesuchtes_bild;

int zeitUebrig = 0;

int treffer = 0;
int fehler = 0;

int zeitBisFund = 0;

boolean paused = false;

void setup()
{
  background(255);
  size(1000, 1000);  
  f = createFont("Arial",16,true);
  bilder = new PImage[] {loadImage("pc.jpg"), loadImage("coffee.jpg"), loadImage("rose.jpg")};
  gesuchtes_bild = (int) random(bilder.length);
}

void draw()
{
  if (!paused)
  {
    if (zeitUebrig <= 0)
    {
      momentanes_bild = (int) random(bilder.length); 
      xbild = random(groesse_bild / 2, width-groesse_bild / 2);
      ybild = random(groesse_bild / 2, width-groesse_bild / 2);
      zeitUebrig = (int) random(30, 100);
    }
    else zeitUebrig = zeitUebrig - 1;
    background(255);
    image(bilder[momentanes_bild], xbild, ybild, groesse_bild, groesse_bild);
    
    fill(0);
    text("Finde das "+bilder_namen[gesuchtes_bild]+" Bild!", 30, 30);
    text("Drücke ENTER um dir Statistiken anzeigen zu lassen!", 30, 60);
    text("Treffer: "+treffer, 30, 90);
    text("Fehler: "+fehler, 30, 120);
    zeitBisFund++;
  }
}

void mouseClicked()
{
  if (mouseX > xbild && mouseX < xbild + groesse_bild && mouseY > ybild && mouseY < ybild + groesse_bild)
  {
    if (momentanes_bild == gesuchtes_bild)
    {
      momentanes_bild = (int) random(bilder.length); 
      xbild = random(0, width-groesse_bild);
      ybild = random(0, height-groesse_bild);
      zeitUebrig = (int) random(30, 100);
      
      treffer = treffer + 1;
      reaktionszeitenBilder[gesuchtes_bild] += zeitBisFund;
      treffer_bilder[gesuchtes_bild] += 1; 
      zeitBisFund = 0;
      gesuchtes_bild = (int) random(bilder.length);
    }
    else fehler = fehler + 1;
  }
}

void keyPressed()
{
  if (key == ENTER)
  {
    paused = true;
    background(255);
    fill(0);
    int stats = 0;
    for (int i = 0; i < bilder_namen.length; i++)
    {
        //circle(100, 100, 50); 
        //text("TEST", 60, 60);
        println("rb: "+reaktionszeitenBilder[i]+" tb: "+treffer_bilder[i]);
        if (reaktionszeitenBilder[i] != 0 && treffer_bilder[i] != 0)
        {
          text("Bild "+bilder_namen[i]+" Reaktionszeit: "+(reaktionszeitenBilder[i] / treffer_bilder[i]) / frameRate + " Sekunden", 30, 30*(i+1));
          stats++;
        }
    }
    if (stats == 0)
      text("Du musst Bilder erfolgreich finden bevor du dir Statistiken anzeigen lassen kannst!", 30, 30);
  }
}
