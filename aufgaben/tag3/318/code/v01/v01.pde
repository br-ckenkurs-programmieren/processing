void setup() {
  size(1000, 1000);
}

void draw() {
  circle(mouseX, mouseY, 50);
}

/*Damit auch während der Bewegung der Maus die Farbe
 geändert werden kann.*/
void mouseDragged() {
  fill(random(255), random(255), random(255));
}

/*Wird nur ausgelöst, wenn die Maus sich beim Click nicht
 bewegt*/
void mouseClicked() {
  fill(random(255), random(255), random(255));
}
