PDF Dateien von den `..all`-Dateien werden nicht benötigt, also bitte einfach löschen. 

Diese Dateien können nun auch von diesem Verzeichnis kompiliert werden und müssen nicht verschoben werden.
