// Heiligtümer des Todes mit exakter Berechnung
// Formeln von https://de.wikipedia.org/wiki/Gleichseitiges_Dreieck#Berechnung_und_Konstruktion

size(400,400);
background(255);
noFill();

// Radius des Kreises (vorgegeben)
float r = 100;

// Berechnung der Seitenlänge
float a = r * 2 * sqrt(3);
// Berechnung der Höhe
float h = (sqrt(3)/2) * a;

ellipse(200,250,200,200);

// Berechung der Eckpunkte des Dreiecks mit Hilfe der zuvor berechneten Werte a und h
float x1 = 200 - a/2;
float y1 = 250 + r;

float x2 = 200 + a/2;
float y2 = 250 + r;

float x3 = 200;
float y3 = 250 + r - h;

triangle(x1,y1,x2,y2,x3,y3);
line(x3,y3,x3,250+r);
