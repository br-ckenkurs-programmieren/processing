// Kantenlänge der Würfel
int a = 30;
color blau1 = color(26,245,255);
color blau2 = color(185,252,255);
color hellbraun = color(144,89,47);
color dunkelbraun = color(93,42,4);

size(600,600,P3D);
noStroke();
translate(150,100);

rotateZ(0.2);
rotateX(-0.5);
rotateY(-0.2);

// Spitze
fill(hellbraun);
box(a);
translate(a,0,0);
fill(dunkelbraun);
box(a);
translate(-a,a,0);
box(a);
translate(a,0,0);
fill(hellbraun);
box(a);

//Mitte
translate(a,a,0);
fill(blau2);
box(a);

// rechte Seite
pushMatrix(); // Wir wollen später einfach wieder zur Mitte zurück
translate(0,-a,0);
box(a);
translate(a,0,0);
box(a);
translate(0,-a,0);
box(a);
for(int i = 0; i<5; i++){
  if(i == 4) fill(blau1);
  translate(a,0,0);
  box(a);
}
// Oberer Rand
pushMatrix();
translate(0,-a,0);
for(int i = 0; i<5; i++){
  translate(-a,0,0);
  box(a);
}
translate(-a,a,0);
box(a);
popMatrix();
// Unterer Rand
translate(0,a,0);
for(int i = 0; i<4; i++){
  translate(-a,0,0);
  box(a);
}
popMatrix(); // Zurück zur Mitte

// Linke Seite
pushMatrix();
fill(blau2);
translate(-a,0,0);
box(a);
translate(0,a,0);
box(a);
translate(-a,0,0);
box(a);
for(int i = 0; i<5; i++){
  if(i == 4) fill(blau1);
  translate(0,a,0);
  box(a);
}
pushMatrix();
translate(-a,0,0);
for(int i = 0; i<5; i++){
  translate(0,-a,0);
  box(a);
}
translate(a,-a,0);
box(a);
popMatrix();
translate(a,0,0);
for(int i = 0; i<4; i++){
  translate(0,-a,0);
  box(a);
}
popMatrix(); // Zurück zur Mitte

// Stiel
for(int i = 0;i<9;i++) {
  translate(a,0,0);
  fill(dunkelbraun);
  box(a);
  translate(-a,a,0);
  box(a);
  translate(a,0,0);
  fill(hellbraun);
  box(a);
}
