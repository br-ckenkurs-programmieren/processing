size(500, 500);
background(255);
rectMode(CENTER);
translate(250, 250);
noFill();

stroke(255, 0, 0);
rect(0, 0, 250, 250);

rotate(QUARTER_PI);
stroke(0);
rect(0, 0, 250, 250);

save("rotvis_2.png");
