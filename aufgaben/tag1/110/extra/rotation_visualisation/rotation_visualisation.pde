size(500, 500);
background(255);
translate(300, 250);

noFill();
strokeWeight(2);

stroke(255, 0, 0);
rect(0, 0, 175, 175);

stroke(0, 0, 255);
arc(0, 0, 350, 350, HALF_PI, QUARTER_PI + PI);

rotate(QUARTER_PI + HALF_PI);
stroke(0);
fill(255, 200);
rect(0, 0, 175, 175);

save("rotation_visualisation.png");
