size(400, 400);
background(255);
noFill();

// Ursprung in die Mitte verschieben, damit um den Mittelpunkt des Fensters rotiert werden kann
translate(width / 2, height / 2);

// Rotation um einen Achtelkreis (45°)
rotate(PI/4);
ellipse(0, 0, 200, 100);
