size(400, 400);
background(255);

translate(width / 2, height / 2);

pushMatrix(); // Ablegen der Matrix

rotate(PI/4);
ellipse(0, 0, 200, 100);

popMatrix(); // Abrufen der Matrix

rect(-100, -25, 200, 50);
