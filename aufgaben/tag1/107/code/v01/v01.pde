// Fenstergröße 400x400
size(400, 400);

// Hintergrund Weiß
background(255);

boolean watching = false;

triangle(50, 350, 200, 90, 350, 350);

if (watching) {
  ellipse(200, 250, 170, 80);
  ellipse(200, 250, 60, 60);

  // Füllfarbe Schwarz für Mitte des Auges
  fill(0);
  ellipse(200, 250, 30, 30);
} else {
  line(115, 250, 285, 250); // Stelle geschlossenes Auge durch Linie da
}
