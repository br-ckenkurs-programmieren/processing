//Lösung wie Variante 01, nur mit 'arc()'

size(400, 400);
background(255);

boolean watching = true;

triangle(50, 350, 200, 90, 350, 350);

arc(200, 250, 170, 80, 0, PI);  // untere Augenhälfte (auch sichtbar bei geschlossenem Auge)

if ( watching) {
  arc(200, 250, 170, 80, PI, TWO_PI); //obere Augenhälfte
  ellipse(200, 250, 60, 60); // Iris
  fill(0);
  ellipse(200, 250, 30, 30); //Pupille
}
