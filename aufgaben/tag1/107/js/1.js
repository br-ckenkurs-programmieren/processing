function setup() {
  createCanvas(400, 400);
}

function draw() {
  watching = false;
  
   background(255);
// Erstelle gleichseitiges Dreieck
  triangle(50, 300, 350, 300, 200, 300 - (sqrt(3)/2)*300 )
  
  if(watching){
// Augenrand
  ellipse(200, 200, 175, 75)
// Iris
  circle(200, 200, 75)
// schwarz gefüllte Pupille
  fill(0)
  circle(200, 200, 37)
  fill(255)
  } else {
// geschlossenes Auge für watching == false
    arc(200, 200, 175, 50, 0, PI)
  }
}