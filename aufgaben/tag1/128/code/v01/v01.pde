size(400, 400);

boolean secretBoolean = false;
char secretCharacter = 'g';
String secretString = "theSecretRecipe";
int secretInt = 42;
float secretFloat = 4.8;

boolean combinationWrong = false;

if(secretBoolean == true) {
  combinationWrong = true;
}

if(secretCharacter != 'g') {
  combinationWrong = true;
}

if(!secretString.equals("theSecretRecipe")) {
  combinationWrong = true;
}

if(secretInt != 42) {
  combinationWrong = true;
}

if(secretFloat < 4 || secretFloat > 5) {
  combinationWrong = true;
}

rectMode(CENTER);
if(combinationWrong) {
  fill(255, 0, 0);
} else {
  fill(0, 255, 0);
}
rect(width / 2, height / 2, 100, 100);
