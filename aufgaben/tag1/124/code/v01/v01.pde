size(120, 220);

// line() wird zu einem Rechteck
strokeCap(SQUARE);
// Strichstärke / Dicke
strokeWeight(15);

// x ist die angezeigte Ziffer
int x = 4;

// Es wird entweder überprüft welche Ziffern den entsprechenden
// Balken haben oder nicht haben (je nach dem welche Varainte
// kürzer ist)

// a
if (x != 1 && x != 4) {
  line(10,10,110,10);
}

//b
if(x != 5 && x != 6){
 line(110,10,110,110); 
}

//c
if(x != 2){
 line(110,110,110,210); 
}

//d
if(x != 1 && x != 4 && x != 7 && x != 9){
 line(110,210,10,210); 
}

//e
if(x == 2 || x == 6 || x == 8 || x == 0){
 line(10,210,10,110); 
}

//f
if(x != 1 && x != 2 && x != 3 && x != 7){
 line(10,110,10,10); 
}

//g
if(x != 1 && x != 7 && x != 0){
 line(10,110,110,110); 
}
