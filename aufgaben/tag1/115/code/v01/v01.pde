size(500,600);
background(255);
strokeWeight(2);

//Raum
int room_w = 400; //Raumbreite
int room_h = 500; //Raumlänge
rect(50, 50, room_w, room_h);

//Bett
boolean bed = true;
int bed_w = 180; //Bettbreite
int bed_h = 200; //Bettlänge

if (bed){
  rect(room_w + 50 - bed_w, 50, bed_w, bed_h);
}

//Schreibtisch
boolean desk = true;
int desk_w = 80; //Schreibtischbreite
int desk_h = 120; //Schreibtischlänge

if (desk){
  rect(50, 50, desk_w, desk_h);
}

//Tisch 
boolean table = true;
int table_w = 160; //Tischbreite
int table_h = 180; //Tischlänge

if (table){
  ellipse(room_w - 50, room_h - 50, table_w, table_h);
}

//Stühle
boolean chair1 = true;
boolean chair2 = true;
boolean chair3 = true;
boolean chair4 = true;
int chair = 40; //Radius des Stuhls

if (chair1){
  circle(50 + desk_h, 50 + 0.75*desk_w, chair); //Schreibtischstuhl
}

if (chair2){
  circle(room_w - 160, room_h - 30, chair);
}

if (chair3){
  circle(room_w - 150, room_h - 100, chair);
}

if (chair4){
  circle(room_w - 100, room_h - 160, chair);
}

//Schrank
boolean cab = true;
int cab_w = 60; //Schrankbreite
int cab_h = 180; //Schranklänge

if (cab){
  rect(50, room_h + 50 - cab_h, cab_w, cab_h);
}
