size(800, 800, P3D);
background(255);

translate(width / 2, height / 2);
noFill();
sphere(300);

rotateY(QUARTER_PI * 0.75);
rotateX(-QUARTER_PI / 2);
fill(0, 0, 255);
strokeWeight(5);
box(300);
