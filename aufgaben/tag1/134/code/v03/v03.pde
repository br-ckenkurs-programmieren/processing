size(800, 800, P3D);
background(0);
stroke(255, 0, 0);
strokeWeight(3);
fill(0);

translate(width * 0.25, height / 2);
sphere(150);

translate(width / 2, 0);
box(200);
