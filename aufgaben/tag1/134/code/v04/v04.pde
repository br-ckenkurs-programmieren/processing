size(800, 800, P3D);
background(0);
stroke(255);
strokeWeight(3);
fill(0);

translate(width / 2, height / 2);
sphere(700);

translate(0, height * 0.2);
stroke(0, 255, 0);
rotateY(QUARTER_PI * 0.75);
box(250);
