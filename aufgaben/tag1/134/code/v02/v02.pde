size(800, 800, P3D);
background(255);

translate(width / 2, height / 2);
fill(255, 0, 0);
sphere(300);

fill(0, 0, 255, 50);
rotateX(QUARTER_PI);
rotateY(QUARTER_PI);
strokeWeight(5);
box(400);
