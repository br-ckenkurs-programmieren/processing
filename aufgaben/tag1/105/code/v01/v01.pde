size(400, 400);
background(255);

// oben links
fill(255);
stroke(0);
circle(40, 40, 40);
circle(40, 80, 40);

fill(0);
circle(80, 40, 40);

// unten rechts
fill(255);
stroke(0);
square(340, 340, 40);

fill(0);
square(300, 340, 40);
fill(255);
square(340, 300, 40);

// oben rechts
stroke(0);
strokeWeight(4);
line(360, 20, 360, 60);

stroke(255, 0, 0);
line(360, 60, 360, 100);

strokeWeight(1);
stroke(0);
line(320, 20, 320, 60);

// unten links
stroke(0);
strokeWeight(4);
line(20, 360, 60, 360);

stroke(255, 0, 0);
line(60, 360, 100, 360);

stroke(0);
strokeWeight(1);
line(20, 320, 60, 320);
