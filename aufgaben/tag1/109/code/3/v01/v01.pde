size(400, 400);
background(255);

float dFamily = 45;
float dNormal = 32;
float pFamily = 11;
float pNormal = 7;

float aFamily = PI * pow(dFamily/2, 2);
float aNormal = PI * pow(dNormal/2, 2);

float rFamily = aFamily/pFamily;
float rNormal = aNormal/pNormal;

if (aFamily > 2*aNormal) {
  fill(255, 0, 0);
} else {
  noFill();
}
circle(100, 200, dFamily * 4);
if (aFamily < 2*aNormal) {
  fill(255, 0, 0);
} else {
  noFill();
}
circle(300, 100, dNormal * 4);
circle(300, 300, dNormal * 4);

fill(0);
text(dFamily+" cm", 100, 200);
text(dNormal+" cm", 300, 100);
text(dNormal+" cm", 300, 300);
text(round(aFamily)+" cm²", 100, 220);
text(round(aNormal)+" cm²", 300, 120);
text(round(aNormal)+" cm²", 300, 320);
if (rFamily > rNormal) {
  fill(255, 0, 0);
} else {
  fill(0);
}
text(round(rFamily)+" cm²/€", 100, 20);
if (rFamily < rNormal) {
  fill(255, 0, 0);
} else {
  fill(0);
}
text(round(rNormal)+" cm²/€", 300, 20);
save("pizza.png");
