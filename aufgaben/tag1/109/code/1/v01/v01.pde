size(450,450);

float dFamily = 45;
float dNormal = 32;

//  Faktor: 4Pixel pro Zentimeter
float factor = 4;

//  Familienpizza zeichnen:
circle(1/4.0 * width, 1/2.0 * height, dFamily * factor);

//  Normale Pizzen zeichnen:
circle(3/4.0 * width, 1/4.0 * height, dNormal * factor);
circle(3/4.0 * width, 3/4.0 * height, dNormal * factor);

// Pizzen beschriften:
fill(0);
text(dFamily + "cm",1/4.0 * width, 1/2.0 * height);
text(dNormal + "cm", 3/4.0 * width, 1/4.0 * height);
text(dNormal + "cm", 3/4.0 * width, 3/4.0 * height);
