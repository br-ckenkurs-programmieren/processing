size(450, 450);

float dFamily = 45;
float dNormal = 32;

//  Faktor: 4Pixel pro Zentimeter
float factor = 4;

//  Brechnung des Flächeninhaltes
float aFamily = PI * dFamily/2 * dFamily/2;
float aNormal = PI * dNormal/2 * dNormal/2;

if (aFamily > 2 * aNormal) {
  //  Familienpizza zeichnen:
  fill(255, 0, 0);
  circle(1/4.0 * width, 1/2.0 * height, dFamily * factor);

  //  Normale Pizzen zeichnen:
  fill(255);
  circle(3/4.0 * width, 1/4.0 * height, dNormal * factor);
  circle(3/4.0 * width, 3/4.0 * height, dNormal * factor);
} else {
  //  Familienpizza zeichnen:
  circle(1/4.0 * width, 1/2.0 * height, dFamily * factor);

  //  Normale Pizzen zeichnen:
  fill(255, 0, 0);
  circle(3/4.0 * width, 1/4.0 * height, dNormal * factor);
  circle(3/4.0 * width, 3/4.0 * height, dNormal * factor);
}

// Pizzen beschriften:
fill(0);
/*
  Der Flächeninhalt eines Kreies ist meist keine ganze Zahl.
  Mit (int) wandelt man einen Float in einen Int um und schneidet dabei die Nachkommastellen weg.
 */
text(dFamily + "cm\n" + (int)aFamily +"cm²", 1/4.0 * width, 1/2.0 * height);
text(dNormal + "cm\n" + (int)aNormal + "cm²", 3/4.0 * width, 1/4.0 * height);
text(dNormal + "cm\n" + (int)aNormal + "cm²", 3/4.0 * width, 3/4.0 * height);
