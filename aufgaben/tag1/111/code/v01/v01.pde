size(400, 400);
background(255);

rect(190, 0, 20, 400);

pushMatrix(); //speichern des derzeitigen Koordinatensystems

// Es wird ein "zweites Koordinatensystem über das andere gelegt
translate(200, 200); // 0|0 ist jetzt da, vor zuvor 200|200 war

rotate(PI/4); // es wird um einen Achtelkreis gedreht (2 PI = volle Umdrehung)

ellipse(0, 0, 200, 100);

popMatrix(); // Rückkehr zum alten Koordinatensystem

rect(0, 190, 400, 20);
