size(200, 200);
strokeWeight(14);
background(0);

stroke(255);
line(0, 0, 100, 100);

noFill();
stroke(0, 0, 255);
circle(100, 100, 100);

stroke(255);
line(100, 100, 200, 200);
