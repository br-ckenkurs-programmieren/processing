size(200, 200);
strokeWeight(14);
background(0);

stroke(0, 255, 0);
line(0, 200, 200, 0);

stroke(255, 0, 0);
line(0, 0, 200, 200);

noFill();
stroke(255);
circle(100, 100, 100);

stroke(0, 255, 0);
line(200, 0, 125, 75);
line(0, 200, 75, 125);

stroke(255, 0, 0);
line(80, 80, 120, 120);
