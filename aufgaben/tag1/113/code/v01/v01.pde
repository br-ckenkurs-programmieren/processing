size(400,150);
textSize(20);
background(255);
fill(0);

float a = 7;
float b = 3;  


// Die Strings werden mit einem + verkettet
text("a = " + a + "  b = " + b, 20, 20);
text("a + b  = " + (a + b), 20, 40);
text("a - b  = " + (a - b), 20, 60);
text("a * b  = " + (a * b), 20, 80);
text("a / b  = " + (a / b), 20, 100);

/*
  Wenn man mit Floats versucht durch 0 zu teilen,
  ist das Ergebnis Infinity bzw. - Infinity.
  Bei 0 / 0 ist das Ergebnis NaN (not a number).
  Wenn Intergers verwendet werden,
  löst eine Division durch 0 einen Fehler aus: ArithmeticException.
*/
