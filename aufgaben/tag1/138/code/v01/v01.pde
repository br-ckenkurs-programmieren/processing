int x = 12;
println("x ist " + x + ".");

if (x < 100) {
  println("x ist kleiner als 100.");
} else {
  println("x ist mindestens 100.");
}

if (x % 2 == 0) {
  println("x ist gerade.");
} else {
  println("x ist ungerade.");
}

if (x * 50 < 100) {
  println("x * 5 ist kleiner als 50.");
} else {
  println("x * 5 ist mindestens 50.");
}
