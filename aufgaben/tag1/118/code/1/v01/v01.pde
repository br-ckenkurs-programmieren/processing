size(400, 250);
background(255);

int patients = 3;

fill(150, 200, 255);
stroke(0);

rect(10, 10, 150, 100);
rect(240, 10, 150, 100);
rect(85, 120, 75, 100);

fill(50, 100, 255);
if (patients > 0) {
  ellipse(50, 50, 20, 20);
}
if (patients > 3) {
  ellipse(120, 50, 20, 20);
}
if (patients > 1) {
  ellipse(280, 50, 20, 20);
}
if (patients > 4) {
  ellipse(350, 50, 20, 20);
}
if (patients > 2) {
  ellipse(120, 170, 20, 20);
}
