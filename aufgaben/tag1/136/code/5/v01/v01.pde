size(200, 200);
strokeWeight(14);
background(0);

stroke(255, 0, 0);
line(0, 0, 200, 200);

stroke(255);
noFill();
circle(100, 100, 100);

stroke(0, 255, 0);
line(200, 0, 100, 100);
line(0, 200, 100, 100);

stroke(255, 0, 0);
line(75, 75, 125, 125);
