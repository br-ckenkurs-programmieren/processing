size(400,400);
strokeWeight(2);

// Punkt 1
float p1x = 50;
float p1y = 50;

// Punkt 2
float p2x = 350;
float p2y = 50;

// Winkel: 45Grad entsprechen 1/4 * PI
float winkel = 1/4.0 * PI;

// Richtungsvektoren vx und vy von p1 zu p2:
float vx = p2x - p1x;
float vy = p2y - p1y;

/*
  Mithilfe der Drehmatrix ergibt sich:
  x' = x * cos(alpha) - y * sin(alpha)
  y' = x * sin(alpha) + y * cos(alpha)
*/

float vx2 = vx * cos(winkel) - vy * sin(winkel);
float vy2 = vx * sin(winkel) + vy * cos(winkel);

//Linie von Punkt 1 zu Punkt 2
line(p1x,p1y,p2x,p2y);

//Linie von Punkt 1 zu Punkt 2'
line(p1x, p1y, p1x + vx2, p1y + vy2);
