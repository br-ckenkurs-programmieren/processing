size(600,600);
strokeWeight(2);

float p1x = 100;
float p1y = 100;

float p2x, p2y;

float vx = 150;
float vy = 0;

float winkel = 2*PI/6;

//Hilfsvariablen für den Richtungsvektor:
float vxTemp, vyTemp;

// 1.Linie:
p2x = p1x + vx;
p2y = p1y + vy;
line(p1x,p1y,p2x,p2y);

// 2.Linie
p1x = p2x;
p1y = p2y;

/*
  Mithilfe der Drehmatrix ergibt sich:
  x' = x * cos(alpha) - y * sin(alpha)
  y' = x * sin(alpha) + y * cos(alpha)
*/

vxTemp = vx * cos(winkel) - vy * sin(winkel);
vyTemp = vx * sin(winkel) + vy * cos(winkel);
vx = vxTemp;
vy = vyTemp;

p2x = p1x + vx;
p2y = p1y + vy;
line(p1x,p1y,p2x,p2y);

// 3.Linie
p1x = p2x;
p1y = p2y;

vxTemp = vx * cos(winkel) - vy * sin(winkel);
vyTemp = vx * sin(winkel) + vy * cos(winkel);
vx = vxTemp;
vy = vyTemp;

p2x = p1x + vx;
p2y = p1y + vy;
line(p1x,p1y,p2x,p2y);

// 4.Linie
p1x = p2x;
p1y = p2y;

vxTemp = vx * cos(winkel) - vy * sin(winkel);
vyTemp = vx * sin(winkel) + vy * cos(winkel);
vx = vxTemp;
vy = vyTemp;

p2x = p1x + vx;
p2y = p1y + vy;
line(p1x,p1y,p2x,p2y);

// 5.Linie
p1x = p2x;
p1y = p2y;

vxTemp = vx * cos(winkel) - vy * sin(winkel);
vyTemp = vx * sin(winkel) + vy * cos(winkel);
vx = vxTemp;
vy = vyTemp;

p2x = p1x + vx;
p2y = p1y + vy;
line(p1x,p1y,p2x,p2y);

// 6.Linie
p1x = p2x;
p1y = p2y;

vxTemp = vx * cos(winkel) - vy * sin(winkel);
vyTemp = vx * sin(winkel) + vy * cos(winkel);
vx = vxTemp;
vy = vyTemp;

p2x = p1x + vx;
p2y = p1y + vy;
line(p1x,p1y,p2x,p2y);
