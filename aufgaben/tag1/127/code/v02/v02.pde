int number;
final String one = "I";
final String five = "V";
final String ten = "X";
final String fifty = "L";
final String hundred = "C";

PFont font1; // for the normal numeral 
PFont font2; // for the roman numeral

void setup() {
  size(800,500);
  background(0);
  number = 1;
  font1 = createFont("Serif", 100);
  font2 = createFont("Serif", 200);
}


void draw() {
   textFont(font1);
  textAlign(CENTER, CENTER);
  text(number, width / 2, (height / 2)/2);
  textFont(font2);
  text(getRomanNumeral(number), width / 2, height / 2);
}

/**
* @brief Number spliter
* @note This function divides a number into several pieces of 10
* and stores them in the table, which will then be returned at the end,
* if the modulo of this name not 10 is different from zero the table is
* created with one more place to be able to store the remainder
*
* @param num The number that will be split
* @retval returns an array of integer
*/
int[] splitNumber(int num) {
  int size = num / 10;
  boolean hasRest = num % 10 != 0;
  int[] res = new int[hasRest ? size + 1 : size];
  int i = 0;
  int rest = num % 10;
  while(num >= 10) {
    res[i] = 10;
    i += 1;
    num -= 10;
  }
  if(hasRest)
    res[i] = rest;
   
  return res;
}

/**
* @note This function is able to convert all integer n
* umbers from 1 to 100 into roman numbers
*
* @param The number that will be converted to Roman numerals
* @retval returns the number converted in Roman numerals
*/
String getRomanNumeral(int num){
  int[] nums = splitNumber(num);
  String res = "";
  int checker = 0;
  for(int i = 0; i < nums.length; i++ ) {
    checker += nums[i];
    if (checker == 40) {
      res = ten + fifty;
      continue;
    }
    if(checker == 50) {
      res = fifty;
      continue;
    }
    if(checker == 90) {
      res = ten + hundred;
      continue;
    }
    if(checker == 100){
      res = hundred;
      continue;
    }
    res += getRomanString(nums[i]);
     
  }
  return res;
}

/**
* @note This function is able to convert all integer n
* umbers from 1 to 10 into roman numbers 

* @param The number that will be converted to Roman numerals
* @retval returns the number converted in Roman numerals
*/
String getRomanString(int num) {
  String res = "";
  boolean numLE3 = num <= 3;
  boolean numGE4ANDLE8 = num >= 4 && num <= 8;
  boolean numGE9ANDLE10 = num >= 9 && num <= 10;
  
  if(numLE3) {
    res = romanLE3(num);
  }
  if(numGE4ANDLE8) {
    if ((num % 5) == num)
      res = romanLE3(5 - num) + five;
    else
      res = five + romanLE3(num % 5);
   }
   if(numGE9ANDLE10) {
     if ((num % 10) == num)
       res = romanLE3(10 - num) + ten;
     else
       res = ten + romanLE3(num % 10);
   }
  
  return res;
}

/**
* @note This function generates for the numbers 
* inferior or equal to 3 their roman numeral
*
* @param The number that will be converted to Roman numerals
* @retval returns the number converted in Roman numerals
*/
String romanLE3(int n) {
  String res = "";
  for (int i = 0; i < n; i++)
      res += one;
  return res;
}

void keyPressed() {
  background(0);
  if (keyCode == UP)
      number = (number + 1) % 101;
  if(keyCode == DOWN)
      number -= number > 1 ? 1 : 0;
}
