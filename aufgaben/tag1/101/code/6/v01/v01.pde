size(200, 200);
background(255);

int x, y, r;
x = 150;
y = 50;
r = 50;

if (r <= x && r <= y && r <= width - x && r <= height - y) {
  fill(255, 255, 0);
  circle(x, y, r * 2);
  fill(0, 0, 255);
  circle(x, y, r);
  fill(255, 0, 0);
  circle(x, y, r / 2);
}

save("step6.png");
