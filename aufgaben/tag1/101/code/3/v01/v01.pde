size(200, 200);
background(255);

int x, y, r;
x = 80;
y = 110;
r = 75;

circle(x, y, r * 2);
circle(x, y, r);
circle(x, y, r / 2);

save("step3.png");
