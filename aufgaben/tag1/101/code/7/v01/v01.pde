size(200, 200);
background(255);

int x, y, r;
x = 150;
y = 50;
r = 57;

if (r > x || r > y || r > width - x || r > height - y) {
  r = min(new int[]{x, y, width - x, height - y});
}

fill(255, 255, 0);
circle(x, y, r * 2);
fill(0, 0, 255);
circle(x, y, r);
fill(255, 0, 0);
circle(x, y, r / 2);
