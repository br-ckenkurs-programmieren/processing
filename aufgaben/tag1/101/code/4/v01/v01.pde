size(200, 200);
background(255);

int x, y, r;
x = 100;
y = 100;
r = 75;

fill(255, 255, 0);
circle(x, y, r * 2);
fill(0, 0, 255);
circle(x, y, r);
fill(255, 0, 0);
circle(x, y, r / 2);

save("step4.png");
