size(500, 500);
float e = 100;
float offsetX = 50;
float offsetY = 50;

square(offsetX + 0, offsetY + 0, e);
square(offsetX + e + e / 2, offsetY + 0, e);
square(offsetX + e + e / 2, offsetY + e + e / 2, e);
