size(500, 500);
background(255);
fill(128, 186, 36);

float e = 75;
float offsetX = 100;
float offsetY = 50;

square(offsetX + e + e / 2, offsetY, e);
square(offsetX + 2 * ( e + e / 2), offsetY, e);

offsetY += e + e / 2;
square(offsetX + 0, offsetY, e);
square(offsetX + e + e / 2, offsetY, e);
square(offsetX + 2 * ( e + e / 2 ), offsetY, e);

offsetY += e + e / 2;
square(offsetX + 0, offsetY, e);
square(offsetX + e + e / 2, offsetY, e);
square(offsetX + 2 * ( e + e / 2 ), offsetY, e);

offsetY += e + e / 2;
square(offsetX + 0, offsetY, e);
square(offsetX + e + e / 2, offsetY, e);
square(offsetX + 2 * ( e + e / 2 ), offsetY, e);
