size(400, 400);
background(255);
// Breite der Quadrate
float w = 50;
// Abstand der Quadrate
float d = 20;
// Position des Logos
float x = 110;
float y = 80;

//1 = Gießen, 2 = Friedberg, 3 = Wetzlar
int location = 1;
// FB Nummer (06 = MNI)s
int faculty = 06;

// Anlegen der Farbtöne
color green = color(128,186,36);
color grey = color(74,92,102);

// Berechnungen mit Modulorechnung, ob Quadrat in der Binärdarstellung benötigt wird
// Einfärben dementsprechend in Grau oder Grün
// Formel aus Aufgabe "Binärcode"

//Reihe 1
fill((faculty/10)%16>=8?green:grey);
square(x + w + d, y, w);
fill((faculty%10)%16>=8?green:grey);
square(x + 2 * (w + d), y, w);

//Reihe 2
fill(location==3?green:grey);
square(x, y + (w + d), w);
fill((faculty/10)%8>=4?green:grey);
square(x + w + d, y + (w + d), w);
fill((faculty%10)%8>=4?green:grey);
square(x + 2 * (w + d), y + (w + d), w);

//Reihe 3
fill(location==2?green:grey);
square(x, y + 2 * (w + d), w);
fill((faculty/10)%4>=2?green:grey);
square(x + w + d, y + 2 * (w + d), w);
fill((faculty%10)%4>=2?green:grey);
square(x + 2 * (w + d), y + 2 * (w + d), w);

//Reihe 4
fill(location==1?green:grey);
square(x, y + 3 * (w + d), w);
fill((faculty/10)%2>=1?green:grey);
square(x + w + d, y + 3 * (w + d), w);
fill((faculty%10)%2>=1?green:grey);
square(x + 2 * (w + d), y + 3 * (w + d), w);
