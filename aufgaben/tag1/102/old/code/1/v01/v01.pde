size(400, 400);
background(255);

fill(0);

//Reihe 1
square(60, 0, 50);
square(60+60, 0, 50);

//Reihe 2
square(0, 60, 50);
square(60, 60, 50);
square(120, 60, 50);

//Reihe 3
square(0, 120, 50);
square(60, 120, 50);
square(120, 120, 50);

//Reihe 4
square(0, 180, 50);
square(60, 180, 50);
square(120, 180, 50);
