/*
 * Satz des Pythagoras
 */

noStroke();

color red   = color(255, 0, 0);
color green = color(0, 255, 0);
color blue  = color(0, 0, 255);
color yellow = color(255, 255, 0);

float a = 200;
float b = 250;
float c = sqrt((a*a) + (b*b));

float alpha = asin(a / c);
float beta = asin(b / c);

float A_x = 400;
float A_y = 600;

float B_x = A_x + b;
float B_y = A_y - a;

float C_x = A_x + b;
float C_y = A_y;

// Fenstergröße einstellen
size(1000, 1000);
background(0);

textSize(40);
text("a = " + a + "\n" + "b = " + b + "\n" + "c = " + c, 20, 40);

//triangle
fill(yellow);
triangle(A_x, A_y, B_x, B_y, C_x, C_y);

//rect A
fill(red);
rect(B_x, B_y, a, a);

//rect B
fill(green);
rect(A_x, A_y, b, b);

//rect C
fill(blue);
translate(A_x, A_y);
rotate(- alpha - 0.5*PI);
rect(0, 0, c, c);
