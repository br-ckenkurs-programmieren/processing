size(600,600);

float a = 150;
float b = 100;

translate(100,400);

// Quadrat a
fill(255,0,0);
rect(0,0,a,a);

// Quadrat b
fill(0,255,0);
rect(a, -b, b, b);

// Dreieck
fill(255,255,0);
triangle(0, 0, a, -b, a, 0);

/*
  Quadrat c:
  Zwei Punkte des Quadrats sind bekannt:
  Der Punkt an dem das Quadrat a aufgespannt ist,
  und der Punkt an dem das Quadrat b aufgespannt ist.
  
  Die anderen beiden Punkte erhalten wir,
  wenn wir die Hypotenuse als Richtungsverktor um 90 Grad drehen.
  
  Mithilfe der Drehmatrix ergibt sich:
  x' = x * cos(alpha) - y * sin(alpha)
  y' = x * sin(alpha) + y * cos(alpha) 
*/

float v1 = a * cos(1/2.0 * PI) - b * sin(1/2.0 * PI);
float v2 = a * sin(1/2.0 * PI) + b * cos(1/2.0 * PI);
// Nun wird der Vektor (v1,-v2) auf die beiden bekannten Punkte 
// addiert um die verbleibenden Punkte zu erhalten:
fill(0,0,255);
quad(0, 0, a, -b, a + v1, -b - v2, v1, -v2);
