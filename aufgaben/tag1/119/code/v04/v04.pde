void setup() {
  size(1024, 768);
  noStroke();
}

void draw() {
  background(255);
  
  // Längen a und b
  float a = (mouseX/(float)width) * 250;
  float b = (mouseY/(float)height) * 200;
  
  // A
  float aX = (width / 2) - (a / 2);   // -(a/2), damit unsere Zeichnungen auf der X-Achse zentriert sind
  float aY = (height / 2) + (b / 2);  // +(b/2), damit unsere Zeichnungen auf der Y-Achse zentriert sind
  fill(0, 255, 0);
  square(aX, aY, a);                  // Die Koordinaten bestimmten die obere linke Ecke des Quadrats
  
  // B
  float bX = aX + a;
  float bY = aY - b;
  fill(0, 0, 255);
  square(bX, bY, b);
  
  // Dreieck
  fill(255, 255, 0);
  triangle(aX, aY, bX, bY, aX+a, aY);
  
  // C
  float c = sqrt((a * a) + (b * b));       // Hier kommt der Satz des Pythagoras zum Einsatz um die Kantenlänge bzw. Hypotenuse zu bestimmen
  float alpha = asin(b / c);               // Simple Trigonometrie um den Winkel zu bestimmen (Processing nutzt den Bogenmaß/Radiant)
  fill(255, 0, 0);
  pushMatrix();
  translate(aX, aY);                       // Da die Rotation um die Achse des Ursprungs (0, 0) geschieht, müssen wir ihn zu A verschieben
  rotate(-HALF_PI - alpha);                // Erst muss das Quadrat um PI/2 gedreht werden,
                                           // dann kann der eigentliche Winkel dazukommen (minus, da wir gegen den Uhrzeigersinn drehen wollen)
  square(0, 0, c);
  popMatrix();                             // Durch push- und popMatrix() ist nur der dazwischenliegende Inhalt von translate() und rotate() beinflusst
  
  // Beschriftungen
  textSize(24);
  fill(0);
  
  text("a: " + a, width / 20, height / 20);
  text("b: " + b, width / 20, height / 20 + 32);
  text("c: " + c, width / 20, height / 20 + 64);
  
  text("a", aX + a/2 - 8, aY - 8);
  text("a²", aX + a/2 - 8, aY + a/2);
  text("b", aX + a - 16, aY - b/2 + 8);
  text("b²", aX + a + b/2 - 8, aY - b/2);
  translate(aX, aY);
  rotate(-HALF_PI - alpha - PI); 
  text("c", 4, -c/2);
  text("c²", -c/2, -c/2);
}
