// Seitenlänge a + b
float a = 150;
float b = 100;

size(600,600);
  
background(255);
  
// Zeichnen von a² und b²  
fill(0,0,255);
rect(300+b/2,300-a/2,a,a);  
fill(0,255,0);
rect(300-b/2,300+a/2,b,b);

// Zeichnen vom innenliegenden Dreieck mit den Seiten a,b und c
fill(255,255,0);
triangle(300+b/2,300-a/2,300+b/2,300+a/2,300-b/2,300+a/2);

// Berechnen von c mit der (umgestellten) Formel a² + b² = c²
float c = sqrt(a*a+b*b);
// Berechnung von Sin des Winkels um den rotiert werden soll
float sin = a/c;

// Verschiebung von 0|0 zu dem Punkt, um den rotiert werden soll
translate(300-b/2,300+a/2);
rotate(asin(-sin));

// Zeichnen von c²
fill(255,0,0);
rect(0,0-c,c,c);
