function setup() {
    createCanvas(500, 500);
  }
  
function draw() {
    background(255);

    let a = (mouseX/width) * 200;
    let b = (mouseY/height) * 200;

    let aX = (width / 2) - (a / 2);
    let aY = (height / 2) + (b / 2);
    fill(0, 255, 0);
    square(aX, aY, a);

    let bX = aX + a;
    let bY = aY - b;
    fill(0, 0, 255);
    square(bX, bY, b);

    fill(255, 255, 0);
    triangle(aX, aY, bX, bY, aX+a, aY);

    let c = sqrt((a * a) + (b * b));
    let alpha = asin(b / c);
    fill(255, 0, 0);
    translate(aX, aY);
    rotate(-HALF_PI - alpha);
    square(0, 0, c);
}