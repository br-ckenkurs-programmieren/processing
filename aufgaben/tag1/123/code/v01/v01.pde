// Dicke der Platte und der Beine
int d = 30;

// Länge der Tischplatte
float lt = width-100;

size(600,400,P3D);
rotateX(-0.5);
rotateY(1);

fill(125);
pushMatrix();

// Tischplatte
translate(width/2,150,0);
box(lt,d,200);

//Tischbein vorne Links
translate(-lt/2+d/2,100+d/2,85);
box(d,200,d);

//Tischbein hinten Links
translate(0,0,-200+d);
box(d,200,d);

//Tischbein hinten Rechts
translate(lt-d,0,0);
box(d,200,d);

//Tischbein vorne Rechts
translate(0,0,200-d);
box(d,200,d);

popMatrix();
fill(100,0,0);

//Stuhl Sitzfläche
translate(width/2,250,250);
box(lt/3,d,lt/3);

// Stuhlbein hinten links
translate(-lt/6+d/2,50+d/2,-lt/6+d/2);
box(d,100,d);

// Stuhlbein vorne links
translate(0,0,lt/3-d);
box(d,100,d);

//Stuhlbein vorne rechts
translate(lt/3-d,0,0);
box(d,100,d);

// Stuhlbein hinten rechts
translate(0,0,-lt/3+d);
box(d,100,d);

// Stuhllehne
translate(0,-50-d-75,lt/3-d);
box(d,150,d);
translate(-lt/3+d,0,0);
box(d,150,d);
translate(lt/6-d/2,-75-d/2,0);
box(lt/3,d,d);
