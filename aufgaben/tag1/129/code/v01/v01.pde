size(400, 400);
background(255);
strokeWeight(2);
noFill();

int oX = 100; // offset X (Verschiebung des Würfels)
int oY = 100; // offset Y
int extent = 150; // Weite und Höhe des Würfels

int fX = 50; // foreground X (Wie "tief" der Würfel erscheint)
int fY = 50; // foreground Y

square(oX, oY, extent); // hinten

line(oX, oY, oX + fX, oY + fY); // oben links
line(oX + extent, oY, oX + fX + extent, oY + fY); // oben rechts
line(oX, oY + extent, oX + fX, oY + fY + extent); // unten links
line(oX + extent, oY + extent, oX + fX + extent, oY + fY + extent); // unten rechts

square(oX + fX, oY + fY, extent); // vorne
