// Anfangsposition
var x;
var y;

// Anfangsgeschwindigkeit
var ySpeed;

var xSpeed;

// Ballradius
var radius;

function setup() {
    initializeFields();
    // Fenstergröße festlegen
    createCanvas(600, 600);
    // Keine Outlines
    noStroke();
}

function draw() {
    // Hintergrund zeichnen
    background(100);
    // Fallbeschleunigung  (Erhöhe Geschwindigkeit nach unten)
    ySpeed += 1;
    // Position je nach Geschwindigkeit verändern, um Bewegung zu simulieren
    x += xSpeed;
    y += ySpeed;
    // Geschwindigkeit verringern, um Reibung zu simulieren
    xSpeed *= 0.99;
    ySpeed *= 0.99;
    // Abprallen vom Boden
    if (y > height - radius) {
        // Wenn in Berührung mit Boden
        // Korrigiere Position, falls Ball "im" Boden ist
        y = height - radius;
        if (// Falls Geschwindigkeit noch nicht invertiert
        ySpeed > 0)
            // Invertiere horizontale Geschwindigkeit(gleichzeitig wird die Geschwindigkeit reduziert)
            ySpeed *= -0.8;
    }
    // Abprallen von der rechten Wand
    if (x > width - radius) {
        // Wenn in Berührung mit rechter Wand
        // Korrigiere Position, falls Ball "in" der Wand ist
        x = width - radius;
        if (// Falls Geschwindigkeit noch nicht invertiert
        xSpeed > 0)
            // Invertiere horizontale Geschwindigkeit(gleichzeitig wird die Geschwindigkeit reduziert)
            xSpeed *= -0.8;
    }
    // Abprallen von der linken Wand
    if (x < radius) {
        // Wenn in Berührung mit linker Wand
        // Korrigiere Position, falls Ball "in" der Wand ist
        x = radius;
        if (// Falls Geschwindigkeit noch nicht invertiert
        xSpeed < 0)
            // Invertiere horizontale Geschwindigkeit(gleichzeitig wird die Geschwindigkeit reduziert)
            xSpeed *= -0.8;
    }
    // Falls Maustaste gedrückt ist
    if (mouseIsPressed) {
        // In Richtung Maus beschleunigen
        xSpeed = (mouseX - x) / 3;
        ySpeed = (mouseY - y) / 3;
    }
    // Ball an aktueller Position zeichnen
    circle(x, y, 2 * radius);
}

function initializeFields() {
    x = 300;
    y = 100;
    ySpeed = 0;
    xSpeed = 12;
    radius = 40;
}