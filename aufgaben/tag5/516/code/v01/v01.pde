<<<<<<< HEAD
// This Code results in boids simulating swarm behaviour without a set direction

int boidAmount = 35;
Boid[] boids = new Boid[boidAmount];
float boidPerceptionRange = 150;
int maxOneDimensionalBoidSpeed = 5;
float maxSpeed = sqrt(maxOneDimensionalBoidSpeed*maxOneDimensionalBoidSpeed + maxOneDimensionalBoidSpeed+maxOneDimensionalBoidSpeed);
float cohesionWeight = 1.0;
float alignmentWeight = 1.0;
float separationWeight = 0.5;
float affectOldMovementBy = 0.2;
float margin = 100.0;
boolean drawFieldOfVision = false;

void setup() {
  size(1800, 1000);
  background(255);
  // initalise boids
  for (int i = 0; i < boidAmount; i++) {
    boids[i] = randomValueBoid();
  }
}

void draw() {
  background(255);
  for (int i = 0; i < boidAmount; i++) {
    Boid myBoid = boids[i];
    ArrayList<Boid> neighbours = new ArrayList<Boid>();
    neighbours = getNeighbours(boids, i);
    // deal with neighbours
    if (neighbours.size() >= 1) {
      calculateMovement(neighbours, myBoid);
    }
    myBoid.limitSpeed();
    boidReactToEnvironment(i);
    // show boids
    boids[i].show();
  }
}

// create a new Boid with random values inside the window. Make sure it is not too fast
Boid randomValueBoid() {
  return new Boid(random(0, width), random(0, height), random(-maxOneDimensionalBoidSpeed, maxOneDimensionalBoidSpeed), random(-maxOneDimensionalBoidSpeed, maxOneDimensionalBoidSpeed));
}

// moves the boid and makes sure it stays in the window 
void boidReactToEnvironment(int i) {
  respawnLostBoids(i);
  boids[i].turnIfOutOfBounds(margin, width-margin, margin, height-margin);
  boids[i].move();
}

// if a boid ended up out of frame, respawn it in the window
void respawnLostBoids(int index) {
  if (boids[index].boidIsLost(0, width, 0, height)) {
    boids[index] = randomValueBoid();
  }
}

// calculates the future movement of the boid
void calculateMovement(ArrayList<Boid> neighbours, Boid myBoid) {
  PVector alignment, separation, cohesion;
  separation = calculateSeparation(neighbours, myBoid);
  // cohesion
  cohesion = calculateCohesion(neighbours, myBoid);
  // alignment
  alignment = calculateAlignment(neighbours, myBoid);
  // weight the input of the three rules
  myBoid.weighVectors(separation, cohesion, alignment);
}

// rule "alignment": move in the average dicrection your neighbours are moving. This should result in flock members usually moving in similar directions
PVector calculateAlignment(ArrayList<Boid> neighbours, Boid currentBoid) {
  PVector sum = new PVector(0, 0);
  for (int n = 0; n < neighbours.size(); n++) {
    sum = vAddition(neighbours.get(n).getMovement(), sum);
  }
  PVector avg = skalarMultiplication(1.0/neighbours.size(), sum);
  return vSubtraction(avg, currentBoid.getMovement());
}

// rule "separation": move away from your neighbours. If they are close, move more to avoid them. This should result in a healthy distance between members of the flock.
PVector calculateSeparation(ArrayList<Boid> neighbours, Boid currentBoid) {
  PVector sum = new PVector(0, 0);
  for (int n = 0; n < neighbours.size(); n++) {
    // to make sure that the movement away from a neighbour is stronger the closer it is, use the inverseDistance: calculate the distance between neighbour and the end of your range of vision.
    PVector inverseDistance = calculateInverseDistanceVector(currentBoid, neighbours.get(n));
    // combine your vectors
    sum = vAddition(sum, inverseDistance);
  }
  // divide through number of neighbours for average
  return skalarMultiplication(1.0/neighbours.size(), sum);
}

// rule "cohesion": move towards the center of your neighbours. This should in members usually staying with the flock
PVector calculateCohesion(ArrayList<Boid> neighbours, Boid currentBoid) {
  PVector sum = new PVector(0, 0);
  // get the center of the Neighbours by calculationg an average of their positions
  for (int n = 0; n < neighbours.size(); n++) {
    sum = vAddition(neighbours.get(n).getPosition(), sum);
  }
  PVector centerOfNeighboursPositions = skalarMultiplication(1.0/neighbours.size(), sum);
  // calculate a vector describing the path from the current boid to the neighbours' center
  return vSubtraction(centerOfNeighboursPositions, currentBoid.position);
}

// finds all boids in the percetion range of boids[boidIndex] and returns them as ArrayList
ArrayList<Boid> getNeighbours(Boid[] boids, int boidIndex) {
  ArrayList<Boid> neighbours = new ArrayList<Boid>();
  for (int j = 0; j < boidAmount; j++) {
    // don't treat a boid as its own neighbour!
    if (j != boidIndex) {
      // use local variables for easier access
      Boid otherBoid = boids[j];
      PVector myPos = boids[boidIndex].getPosition();
      PVector otherPos = otherBoid.getPosition();
      
      // in perception range?
      if (dist(myPos.x, myPos.y, otherPos.x, otherPos.y) < boidPerceptionRange) {
        neighbours.add(otherBoid);
      }
    }
  }
  return neighbours;
}


PVector calculateInverseDistanceVector(Boid myBoid, Boid otherBoid) {
  // use local variables for easier access
  PVector myPos = myBoid.getPosition();
  PVector otherPos = otherBoid.getPosition();
  // get distance between boids
  float dist = dist(myPos.x, myPos.y, otherPos.x, otherPos.y);
  float partsOfRange = (boidPerceptionRange / dist);
  // get vector to get from otherPos to myPos -> if applied to myPos, moves it away from otherPos
  PVector fromMyToOtherVector = vSubtraction(myPos, otherPos);
  PVector prod = skalarMultiplication(partsOfRange - 1, fromMyToOtherVector);
  return prod;
=======
int blockHeight, blockWidth; // block size
int paddleHeight, paddleWidth, paddleOffset; // paddle size

// Array aus Steinen, die getroffen werden müssen
Brick[] bricks;
// Farben für Steine
color[] colors;
//Position des Balls
float xPos, yPos;
//Position des Balls im vorherigen Frame
float xPosOld, yPosOld;
//Vertikale und horizontale Geschwindigkeit des Balls
float vx, vy;
//Durchmesser, Radius des Balls
float d, r;

boolean running;

void setup() {
  size(800, 600);
  textSize(30);

  bricks = flag();
  colors = new color[10];

  vx = 0.5;
  vy = 1.3;
  d = 10;
  r = d / 2;
  xPos = width/2;
  yPos = 500;

  blockHeight = 25;
  blockWidth = width/12;

  paddleHeight = 20;
  paddleWidth = 200;
  paddleOffset = 50;

  running = false;

  // Farben für Regenbogenmuster in den Steinen
  colors[0] = color(255, 0, 0);
  colors[1] = color(255, 153, 0);
  colors[2] = color(255, 255, 0);
  colors[3] = color(152, 255, 0);
  colors[4] = color(41, 255, 0);
  colors[5] = color(0, 255, 150);
  colors[6] = color(0, 150, 255);
  colors[7] = color(0, 0, 255);
  colors[8] = color(120, 0, 255);
  colors[9] = color(190, 0, 255);
}

void draw() {
  int paddleY = height - paddleOffset;
  int paddleX;
  if (pmouseX > width - paddleWidth/2) {
    paddleX = width - paddleWidth;
  } else if (pmouseX < paddleWidth/2) {
    paddleX = 0;
  } else {
    paddleX = pmouseX - paddleWidth/2;
  }

  background(100);
  fill(255);
  rect(paddleX, paddleY, paddleWidth, paddleHeight);

  for (int i = 0; i< bricks.length; i++) {
    fill(colors[i/10]);
    bricks[i].display();
  }

  fill(152, 255, 0);
  circle(xPos, yPos, 10);

  //Abprallen von Wänden (links, rechts, oben)
  if ((xPos < r && vx < 0) || (xPos > width - r && vx > 0)) vx *= -1;
  if (yPos < r && vy < 0) vy *= -1;

  //Abprallen von Schläger
  if ((xPos + r > paddleX && xPos + r < paddleX + paddleWidth
    || xPos - r > paddleX && xPos - r < paddleX + paddleWidth)
    && yPos + r > paddleY) {
    //Trefferpunkt ermittlen
    float hitpoint = (xPos - mouseX) / 100;
    vx += hitpoint;
    //Winkel sollte nicht zu flach sein
    if (vx > 1.3) {
      vx = 1.3;
    } else if (vx < -1.3) {
      vx = -1.3;
    }

    if (vy > 0) vy *= -1; //Ball nur nach oben abprallen lassen
  }

  // Wenn der Ball am Schläger vorbei fliegt ist das Spiel vorbei
  if (yPos > 600) {
    running = false;
    text("Game Over, press r to reset", 250, 300);
    return;
  }


  //Abprallen vom Blöcken
  for (int i = 0; i < bricks.length; i++) {
    int hit = bricks[i].collide(xPos, yPos, xPosOld, yPosOld, r);
    if (hit == 1) {
      vx *= -1;
      bricks[i].active = false;
    }
    if (hit == 2) {
      vy *= -1;
      bricks[i].active = false;
    }
  }

  if (victory()) {
    running = false;
    text("Sieg", 400, 300);
  }

  if (running) {
    xPosOld = xPos;
    yPosOld = yPos;

    xPos += vx * 3;
    yPos += vy * 3;
  } else {
    text("Click to start", width/2 - 300, height/2 + 100);
    text("Use mouse to move paddle", width/2 - 100, height/2+50);
    text("Use key 'r' to reset", width/2 - 100, height/2+100);
    text("Use keys 1-4 to change map", width/2 - 100, height/2+150);
  }
}

// Repräsentiert einen Block mit seiner Position und seinem Status
class Brick {

  float posX;
  float posY;
  boolean active;

  Brick(float posX, float posY) {
    this.posX = posX;
    this.posY = posY;
    this.active = true;
  }
  // Zeichnet Block
  void display() {
    if (this.active) {
      rect(posX, posY, blockWidth, blockHeight);
    }
  }

  /* Prüft ob der Ball die Box berührt
   Alte Position wird benutzt, um Richtung des Balls zu bestimmen
   */
  int collide(float x, float y, float xOld, float yOld, float r) {
    if (!active) return 0; // Wenn der Block nicht aktiv ist, kann er nicht berührt werden

    // Wenn der Ball die Box berührt, wird die Richtung des Balls bestimmt
    if (x + r > posX // rechts
      && x - r < posX + blockWidth // links
      && y + r > posY // unten
      && y - r < posY + blockHeight // oben
      ) {
      if (xOld + r < posX  // links
        || xOld - r > posX + blockWidth) { // rechts
        return 1;
      } else {
        return 2;
      }
    }

    return 0;
  }
}


// R drücken um Spiel zu reseten
void keyPressed() {
  if (key == 'r' || key == '1' || key == '2' || key == '3' || key == '4') {
    if (key == '1') {
      bricks = flag();
    } else if (key == '2') {
      bricks = rectangle();
    } else if (key == '3') {
      bricks = circle();
    } else if (key == '4') {
      bricks = triangle();
    } else {
      for (int i = 0; i< bricks.length; i++) {
        bricks[i].active = true;
      }
    }
    running = false;

    vx = 0.5;
    vy = 1;
    xPos = width/2;
    yPos = 500;
  }
}

// Klicken um das Spiel zu starten und den Ball zu aktivieren
void mouseClicked() {
  running = true;
}

// Berechnung ob das Spiel vorbei ist bzw. der Spieler gewonnen hat
boolean victory() {
  for (int i = 0; i < bricks.length; i++) {
    if (bricks[i].active) return false;
  }
  return true;
}

Brick[] rectangle() {
  Brick[] bricks = new Brick[100];
  for (int i = 0; i < bricks.length; i++) {
    bricks[i] = new Brick(width/12 + i%10*width/12, int(i/10)*25 + 50);
  }
  return bricks;
}

Brick[] circle() {
  Brick[] bricks = new Brick[100];
  int centerX = width/2;
  int centerY = height/2;
  int radius = 250;
  for (int i = 0; i < bricks.length; i++) {
    float angle = i * TWO_PI / bricks.length;
    float x = centerX + cos(angle) * radius;
    float y = centerY + (sin(angle) * radius) / 2;
    bricks[i] = new Brick(x, y);
  }
  return bricks;
}

Brick[] triangle() {
  Brick[] bricks = new Brick[100];
  int centerX = width/2;
  int centerY = height/2 - 200;
  int oneThird = int(bricks.length / 3);
  int twoThirds = int((bricks.length / 3) * 2);
  int step = 10;
  for (int i = 0; i < oneThird; i += 2) { // Balken unten
    bricks[i] = new Brick(centerX - i * step, centerY + 330);
    bricks[i + 1] = new Brick(centerX + i * step, centerY + 330);
  }
  for (int i = oneThird; i < twoThirds; i++) { // linke Seite
    bricks[i] = new Brick(centerX - (oneThird - i) * step, centerY - (oneThird - i) * step);
  }
  for (int i = twoThirds; i < bricks.length; i++) { // rechte Seite
    bricks[i] = new Brick(centerX + (twoThirds - i) * step, centerY + (i - twoThirds) * step);
  }
  return bricks;
}

Brick[] flag() {
  Brick[] bricks = new Brick[100];
  for (int i = 0; i < bricks.length; i++) {
    bricks[i] = new Brick(
      width / 12 + (i % 10) * (width / 12),
      (int(i / 10) * 25 + 50) + 15 * sin((i % 10) / 1.5));
  }
  return bricks;
>>>>>>> e4ba3d8f196ce51fec31a2b9e6ef701339fb0457
}
