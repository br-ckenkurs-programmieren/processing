class Boid {
  PVector position;
  PVector movement;

  Boid(float positionX, float positionY, float movementX, float movementY) {
    this.position = new PVector(positionX, positionY);
    this.movement = new PVector(movementX, movementY);
  }
  
  void limitSpeed(){
  float speed = dist(this.movement.x, this.movement.y, 0, 0);
    // ensure that maxSpeed is not exceeded.
    // keep direction
    if (speed > maxSpeed) {
      this.movement = skalarMultiplication(maxSpeed, skalarMultiplication(1/speed, this.movement));
    }
  }

  void move() {
    // change Boids position
    this.position = vAddition(this.position, this.movement);
  }


  void show() {
    // Seeing the field of vision of the boids can be interesting when experimenting
    if (drawFieldOfVision) {
      noFill();
      circle(this.position.x, this.position.y, 2 * boidPerceptionRange);
      fill(0);
    }
    // show boid as circle and orientation as line
    circle(this.position.x, this.position.y, 5);
    line(this.position.x, this.position.y, this.position.x + this.movement.x, this.position.y + this.movement.y);
  }

  // apply the weights to control the input of the rules on the boids path 
  void weighVectors(PVector separation, PVector cohesion, PVector alignment) {
    // one vector represing each of the three rules and apply weights
    separation = skalarMultiplication(separationWeight, separation);
    cohesion = skalarMultiplication(cohesionWeight, cohesion);
    alignment = skalarMultiplication(alignmentWeight, alignment);
    // combine rules
    PVector rulesCombined = vAddition(separation, vAddition(cohesion, alignment));
    // combine the weightened new and old movement for more fluid behaviour
    PVector newMovementPart = skalarMultiplication(affectOldMovementBy, rulesCombined);
    this.movement = vAddition(this.movement, newMovementPart);
  }

  // inverse direction of movement if position of boid is out of the area definied via arguments
  void turnIfOutOfBounds(float minX, float maxX, float minY, float maxY) {
    float turnBy = 0.5;
    if (this.position.x < minX) {
      this.movement.x = this.movement.x + turnBy;
    }
    if (this.position.x > maxX) {
      this.movement.x = this.movement.x - turnBy;
    }
    if (this.position.y < minY ) {
      this.movement.y = this.movement.y  + turnBy;
    }
    if (this.position.y > maxY ) {
      this.movement.y = this.movement.y  - turnBy;
    }
  }

  // check if a boid is outside the window
  boolean boidIsLost(float minX, float maxX, float minY, float maxY) {
    if (this.position.x < minX || this.position.x > maxX) {
      return true;
    } else if (this.position.y < minY || this.position.y > maxY) {
      return true;
    } else {
      return false;
    }
  }

  void setPosition(float x, float y) {
    this.position = new PVector(x, y);
  }

  PVector getPosition() {
    return this.position;
  }

  PVector getMovement() {
    return this.movement;
  }
}

PVector vAddition(PVector a, PVector b) {
  return new PVector(a.x + b.x, a.y + b.y);
}

PVector vSubtraction(PVector a, PVector b) {
  return new PVector(a.x - b.x, a.y - b.y);
}


PVector skalarMultiplication(float skalar, PVector a) {
  return new PVector( skalar * a.x, skalar * a.y);
}
