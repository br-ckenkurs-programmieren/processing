int boidAmount = 35;
Boid[] boids = new Boid[boidAmount];
float boidPerceptionRange = 100;
int maxInitialBoidSpeed = 5;

// seperation does not work yet
void setup() {
  size(1800, 1000);
  background(255);
  // initalise boids
  for (int i = 0; i < boidAmount; i++) {
    boids[i] = new Boid(random(0, width), random(0, height), random(-maxInitialBoidSpeed, maxInitialBoidSpeed), random(-maxInitialBoidSpeed, maxInitialBoidSpeed));
  }
}

void draw() {
  background(255);
  for (int i = 0; i < boidAmount; i++) {
    PVector adhesion, separation, coherence;
    Boid myBoid = boids[i];
    ArrayList<Boid> neighbours = new ArrayList<Boid>();
    for (int j = 0; j < boidAmount; j++) {
      if (j != i) {
        // in perception range?
        Boid otherBoid = boids[j];
        PVector myPos = myBoid.getPosition();
        PVector otherPos = otherBoid.getPosition();
        if (dist(myPos.x, myPos.y, otherPos.x, otherPos.y) < boidPerceptionRange) {
          neighbours.add(otherBoid);
        }
      }
    }
    // deal with neighbours
    if (neighbours.size() > 1) {
      PVector sepSum = new PVector(0, 0);
      for (int n = 0; n < neighbours.size(); n++) {
        PVector inverseDistance = calculateInverseDistanceVector(myBoid, neighbours.get(n));
        sepSum = vAddition(sepSum, inverseDistance);
      }
      separation = skalarMultiplication(1.0/neighbours.size(), sepSum);
      // coherence
      PVector coSum = new PVector(0, 0);
      for (int n = 0; n < neighbours.size(); n++) {
        coSum = vAddition(neighbours.get(n).getPosition(), coSum);
      }
      PVector coAvgPos = skalarMultiplication(1.0/neighbours.size(), coSum);
      coherence = vSubtraction(coAvgPos, myBoid.position);
      // adhesion
      PVector adSum = new PVector(0, 0);
      for (int n = 0; n < neighbours.size(); n++) {
        adSum = vAddition(neighbours.get(n).getMovement(), adSum);
      }
      adhesion = skalarMultiplication(1.0/neighbours.size(), adSum);
      boids[i].weighVectors(separation, coherence, adhesion);
    }
    boids[i].move();
    // spaw out of bounds boids on left frame side
    if (boids[i].outOfBounds(0.0, width, 0.0, height)) {
      
      boids[i] = new Boid(random(0, width), random(0, height), random(-maxInitialBoidSpeed, maxInitialBoidSpeed), random(-maxInitialBoidSpeed, maxInitialBoidSpeed));
    }
    // show boids
    boids[i].show();
  }
}


PVector calculateInverseDistanceVector(Boid myBoid, Boid otherBoid) {
  PVector myPos = myBoid.getPosition();
  PVector otherPos = otherBoid.getPosition();
  float dist = dist(myPos.x, myPos.y, otherPos.x, otherPos.y);
  float partsOfRange = (boidPerceptionRange / dist);
  PVector fromMyToOtherVector = vSubtraction(myPos, otherPos);
  PVector prod = skalarMultiplication(partsOfRange - 1, fromMyToOtherVector);
  prod.x = max(min(prod.x, 50), -50);
  prod.y = max(min(prod.y, 50), -50);
  return prod;
}
