class Player {

  int xPosition;
  int speed;
  int direction;

  public Player() {
    xPosition = 250;
    speed = 5;
    direction = 0;
  }

  void show() {
    fill(255);
    stroke(0);
    rect(xPosition, 450, 12, 40);
  }

  void move() {
    if (xPosition <= 480 && direction == 1) {
      xPosition = xPosition + direction * speed;
    } else if (xPosition >= 10 && direction == -1) {
      xPosition = xPosition + direction * speed;
    } 
  }

  void setDirection(int dir) {
    direction = dir;
  }

  int getXPosition() {
    return xPosition;
  }
}
