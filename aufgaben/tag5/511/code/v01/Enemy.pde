class Enemy {

  int xPosition, yPosition;
  int dir;

  Enemy(int x, int y) {
    xPosition = x;
    yPosition = y;
    dir = 1;  
  }

  void show() {
    fill(180, 30, 30);
    stroke(0);
    circle(xPosition, yPosition, 25);
  }
  
  void move(){   
    xPosition += dir;
}
  
  void changeDirection(){
    dir *= -1;
  }
  
  void down(){
    yPosition += 20;
  }
  
  boolean bottom(){
    return (yPosition > 420);
  }
  
  boolean edge(){
    return (xPosition > 450 || xPosition < 50); 
  }

  int getX() {
    return xPosition;
  }

  int getY() {
    return yPosition;
  }
}
