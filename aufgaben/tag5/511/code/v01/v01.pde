//Hier stehen die Variablen die wir global anlegen.
//Dazu haben wir eine Variable vom Typ "Player", "Bullet" und eine Liste vom Typ "Enemy".
//Die Typen sind selbstangelegte Klassen.

Player player;
Bullet bullet;
ArrayList<Enemy> enemies;

//Diese Methode wird beim Start des Programms einmal ausgeführt. 
//Das eignet sich für initalisierung von Variablen sehr gut.
void setup() {
  //Größe des Programms in (x, y) Pixel
  size(500, 500); 
 
  //Initialisierung der Variablen
  player = new Player();
  bullet = new Bullet();
  enemies = new ArrayList<Enemy>(); 
  
  //Schleife um neue Objekte vom Typ "Enemy" in die Liste "enemies" einzufügen.
  //Pro Schleifendurchlauf werden 2 Objekte eingefügt und die Koordinaten des "Enemy"
  //werden als Parameter übergeben (Die Zahlen in den Klammern). 
  for (int i=0; i<6; i++) {
    enemies.add(new Enemy(75 + 50*i, 100));
    enemies.add(new Enemy(75 + 50*i, 150));
  }
}


//Diese Methode wird regelmäßig aufgerufen, eignet sich um Informationen 
//dauerhaft abzurufen und alles auf die graphische Oberfläche abzubilden
void draw() {
  //Farbe des Hintergrunds
  background(50);

  //Methoden der Player-Klasse um den Spieler anzuzeigen und ggf. zu bewegen.
  player.show();
  player.move();
  
  //Schleife durch die Liste der "Enemies" um alle Objekte anzuzeigen und zu bewegen.
  //  Die .size() Methode einer Liste gibt die Anzahl der enthaltenen Elemente zurück
  //  Die Liste.get(x) Methode gibt das Objekt der Liste an der stelle x zurück.
  for (int i=0 ; i<enemies.size() ; i++) {
    enemies.get(i).show();
    enemies.get(i).move();
    
  //Die If-Abfrage wird benötigt um zu überprüfen ob eines der "Enemy"-Objekte sich am Rand
  //des Programms befindet: 
  //  Sollte das der Fall sein wird nochmal eine Schleife durch alle Elemente der Liste durchlaufen,
  //  sodass jeder "Enemy" die Richtung wechselt und sich nach unten bewegt.
    if (enemies.get(i).edge()) {
      for (int k = 0 ; k<enemies.size() ; k++) {
        enemies.get(k).changeDirection();
        enemies.get(k).down();
      }
    }
    
  //Diese If-Abfrage überprüft bei jedem Schleifendurchlauf für jedes Element, ob es sich am unterem Rand
  //des Programms befindet: Sollte das passieren hat man Verloren und das Programm wird beendet.
    if (enemies.get(i).bottom()) {
      exit();
    }
  }

  //Diese If-Abfrage überprüft, ob das "Bullet"-Objekt geschossen wurde:
  //Wenn ja, dann wird das Objekt angezeigt und auch bewegt.
  if (bullet.isShooting()) {
    bullet.show();
    bullet.move();

  //Die Schleife wird genutzt als ein Kollisions erkenner.
  //Sollte die Distanz der "Bullet" kleiner 20 sein zu einem der "Enemies", dann
  //wird das "Enemy"-Objekt aus der Liste entfernt und die Methode .isShoot(false) aus 
  //der Bullet-Klasse aufgrufen um das Objekt zu "deaktivieren"
    //Kreis-kreis abstand berechnung
    for (int i=0; i< enemies.size(); i++) {
      if (dist(bullet.getX(), bullet.getY(), enemies.get(i).getX(), enemies.get(i).getY()) < 20) {
        enemies.remove(i);
        bullet.reset();
      }
    }
  }
}

//Diese Methode wird immer aufgerufen wenn eine Taste gedrückt wird
void keyPressed() {
  //Für die Taste 'a' wird die Richtung des Spielers auf "-1" gesetzt.
  if (key == 'a') {
    player.setDirection(-1);
  }
  //Für die Taste 'd' wird die Richtung des Spielers auf "1" gesetzt.  
  if (key == 'd') {
    player.setDirection(1);
  }
  //Für die Taste ' ' (Leertaste) wird überprüft ob bereits geschossen wurde und
  //schießt dementsprechend eine neuen Schuss an der X-Koordinate des Spieler kreiert.
  if (key == ' ') {
    if (!bullet.isShooting()) {
      bullet.shoot(player.getXPosition());
    }
  }
}

//Diese Methode wird immer aufgerufen wenn eine Taste losgelassen wird.
//Ist die Taste ein 'a' oder 'd'  wird die Richtung des Spieler auf 0 gesetzt. 
void keyReleased() {
  if (key == 'a' || key == 'd') {
    player.setDirection(0);
  }
}
