class Bullet {

  int xPosition, yPosition;
  int speed;
  boolean isShooting;

  Bullet() { 
    yPosition = 460;
    speed = 3;
    isShooting = false;
  }

  void show() {
    fill(250);
    stroke(0);
    circle(xPosition + 6, yPosition, 10);
  }

  void move() {
    yPosition -= speed;
    if (yPosition < -10) {
      reset();
    }
  }

  void reset(){
    isShooting = false;
    yPosition = 460;
  }
  
  void shoot(int x) {
    xPosition = x;
    isShooting = true;
  }

  boolean isShooting() {
    return isShooting;
  }

  int getX() {
    return xPosition;
  }

  int getY() {
    return yPosition;
  }
}
