\section{Space Invaders}
\easy{226 Z.}{\keyboard \methoden \klassen}

\textit{Space Invaders} war ein Arcade-Spiel, in welchem Spieler:innen sich als Raumschiff  am unterem Spielfeld horizontal bewegen konnten, und mit einer Kanone versuchten, die Aliens abzuschießen.
Die Aliens bewegen sich dabei nach links und nach rechts.
Sobald die Aliens den Rand des Spielfelds erreicht haben, ändern sie die Richtung und bewegen sich ein Stück nach unten.
Erreichten die Aliens den unteren Rand des Spielfelds bevor alle getroffen wurden, verlor der/die Spieler:in.

\subsection{Grundlagen}

Was benötigen wir für unser Spiel?

\begin{enumerate}
	\item Wir benötigen natürlich ein Raumschiff, welches sich bewegen und schießen können muss.
	Dazu werden wir eine Klasse namens \code{class Player} anlegen, in welche wir die notwendigen Methoden zum Anzeigen, Bewegen und Schießen implementieren.
	\item Die Gegner werden als eine neue Klasse \code{class Enemy} implementiert und auch
	mit Funktionalität (Anzeigen, Bewegen) ausgestattet.
	\item Da wir auch die Kugeln welche vom Raumschiff geschossen werden darstellen wollen, benötigen wir zusätzlich eine Klasse \code{class Bullet}, welche Positionsdaten und Geschwindigkeit speichern soll, und ebenso eine Methode zum Anzeigen besitzen soll.
\end{enumerate}

Zu diesen Klassen werden einige wichtige Verhalten benötigt:

\begin{enumerate}
	\item Das Raumschiff soll eine Kugel schießen können.
	\item Eine Kugel soll, wenn sie einen Gegner trifft, diesen und sich selbst löschen.
	\item Eine Kugel soll, falls sie außerhalb des Bildes ist, sich selbst löschen.
	\item Alle Gegner sollen synchron bewegt werden.
	\item Spieler:innen sollen das Raumschiff durch Tasten bewegen können.
	\item Spielverlust und -Gewinn sollten erkannt und angezeigt werden.
\end{enumerate}

Dies sollte Ihnen genug Tipps geben, das Spiel zu realisieren.

Falls Sie irgendwo hängen bleiben sollten und nicht weiter wissen, können Sie in den nächsten Abschnitten nachschauen.
Dort wird es allerdings Lösungsansätze und nicht nur Tipps geben. \\
\textbf{Versuchen Sie es also gerne erst alleine!}

\vspace{12em}

\textit{Auf der nächsten Seite sind Lösungsansätze zu finden...}

\pagebreak

\subsection{Variablen und Klassen}

\clue{Hier und später werden wir Variablen verwenden, welche in der Aufgabenstellung nicht definiert werden.
	Extrahieren Sie anhand des Namens der Variable den Sinn dahinter, und implementieren Sie sie.
	Die meisten dieser Variablen sollten global angelegt werden, und können schon direkt bei der Deklarierung mit einem Wert belegt werden.}

Um Instanzen von Gegnern und Kugeln zu speichern, ist es sinnvoll, Listen statt Arrays zu verwenden.
So können dynamisch mehr Gegner und Kugeln hinzugefügt oder vom Spielbrett entfernt werden.

\begin{lstlisting}
Player player;
ArrayList<Bullet> bullets;
ArrayList<Enemy> enemies;
\end{lstlisting}

Diese Variablen werden wie gewohnt in \code{setup()} initialisiert:

\begin{lstlisting}
void setup() {
	size( ... );
	player = new Player();

	bullets = new ArrayList<>(); // Zunächst leer!
	enemies = new ArrayList<>();

	for(int i = 0; i < enemyCount; i++) { // enemyCount muss definiert werden
		enemies.add(new Enemy()); // Hier müssen später noch Informationen eingefügt werden
	}
}
\end{lstlisting}

Mit einem ähnlichen, bekannten Prinzip können wir daraufhin alle vorliegenden Instanzen anzeigen und ggf. aktualisieren:

\begin{lstlisting}
void draw() {
	background( ... );
	player.show();

	for(Bullet b : bullets) { // Über Listen mit for-each iterieren...
		b.show();
		// ... (Bewegung?)
	}

	for(int i = 0; i < enemies.size(); i++) { // ...oder normalem for
		enemies.get(i).show();
		// ... (Bewegung?)
	}
}
\end{lstlisting}

\pagebreak

\subsection{Spielerbewegungen}

Für die Bewegungen des Spielers wird die Implementierung eines Events benötigt.
Hierfür eignet sich \code{void keyPressed()}, da dieses beim Gedrückt-halten auch mehrmals aufgerufen wird.

Eine mögliche Implementierung des Events:
\begin{lstlisting}
void keyPressed() {
	switch (key) {
		case 'a':
			player.moveLeft(); // müssen noch implementiert werden!
			break;
		case 'd':
			player.moveRight();
			break;
	}
}
\end{lstlisting}

Entsprechend müssen Informationen zur Position des Spielers in der Klasse \code{Player} gespeichert werden.

\subsection{Gegnerbewegungen}

Charakteristisch für \textit{Space Invaders} ist das synchrone nach links und rechts-Bewegen der Gegner, welche dann die Richtung wechseln und einen Schritt nach vorne machen, bevor sie sich wieder weiterbewegen.
Demzufolge werden wir in der Klasse \code{Enemy} auch Information zu Position sowie Richtung speichern.
Zudem sollten die Schritte in einer Variable zählen, sodass wir wissen, wann ein Gegner die Richtung umkehren sollte.

Der Kopf der Klasse könnte ungefähr so aussehen:

\begin{lstlisting}
class Enemy {
	int x, y;
	int direction;
	int steps;

	Enemy(int x, int y) {
		this.x = x;
		this.y = y;
		direction = 1;
	}
}
\end{lstlisting}

... und die Methode \code{move()} so:

\begin{lstlisting}
void move() {
	if(steps >= maxSteps) { // maxSteps muss implementiert werden!
		steps = 0;
		direction = -direction;
		y += stepY; // muss implementiert werden!
	} else {
		steps++;
		x += stepX * direction;
	}
}
\end{lstlisting}

\pagebreak

\subsection{Schießen}

Die Implementierung von der Klasse \code{Bullet} ist nicht viel anders als die anderen, sodass sie hier nicht weiter erläutert wird.
Besonders ist vielmehr die Interaktion zwischen Spieler und Kugel:
Hier macht es Sinn, in das \code{keyPressed()}-Event eine weitere Bedingung einzufügen:

\begin{lstlisting}
void keyPressed() {
	switch (key) {
		...
		case 'w':
			if (shotCountdown <= 0) { // Implementierung fehlt
				bullets.add(new Bullet(player.x, player.y));
				shotCountdown = ticksUntilNextShot; // hier auch
			}
			break;
	}
}
\end{lstlisting}

\subsection{Kollisionen}

Die Bestimmung von Kollisionen ist mit der Suche nach einer überlappenden Instanz von \code{Bullet} mit einer solchen von \code{Enemy} gleichzustellen.
Es herrscht Überlappung, wenn die Distanz zwischen den Objekten kleiner als die Summe der Radien ist.

Eine Funktion, welche Kollisionen prüft, könnte so aussehen:
\begin{lstlisting}
void checkForCollisions() {
	for(Bullet b : bullets) {
		for(Enemy e : enemies) {
			if(dist(b.x, b.y, e.x, e.y) < bulletSize + enemySize) {
				enemies.remove(e);
				bullets.remove(b);
				break;
			}
		}
	}
}
\end{lstlisting}

\subsection{Erweiterungsideen}

Falls Sie das Grundkonstrukt des Spieles erfolgreich programmiert haben, können Sie es gerne noch erweitern.
Hier sind einige Anregungen dazu:

\begin{itemize}
	\item Spieler:innen sollten sich nur innerhalb des Fensters bewegen können.
	\item Kann man die Formen der Aliens und des Spielers durch Bilder von Raumschiffen ersetzen?
	\item Die Geschwindigkeit des Spieles könnte sich durch bestimmte Ereignisse verändern.
	\item \enquote{Neu starten} oder \enquote{Zurücksetzen}-Funktionalität.
	\item Wie wäre es mit Punktzahlen und High-Scores?
\end{itemize}
