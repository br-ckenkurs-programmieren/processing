let player;
let bullet;
let enemies;

class Bullet{

    constructor() {
        this.yPosition = 460;
        this.speed = 3;
        this.isShooting = false;
    }
    
    show() {
        fill(250);
        stroke(0);
        circle(this.xPosition + 6, this.yPosition, 10);
    }
  
    reset() {
        this.isShooting = false;
        this.yPosition = 460;
    }
  
    move() {
        this.yPosition -= this.speed;
        if (this.yPosition < -10) {
            this.reset();
        }
    }
    
    
    
    shoot(x) {
        this.xPosition = x;
        this.isShooting = true;
    }
    
    /*isShooting() {
        return this.isShooting;
    }*/
    
    getX() {
        return this.xPosition;
    }
    
    getY() {
        return this.yPosition;
    }
}


class Enemy {

    constructor(x, y) {
        this.xPosition = x;
        this.yPosition = y;
        this.dir = 1;
    }
    
    show() {
        stroke(0);
        fill(180, 30, 30);
        circle(this.xPosition, this.yPosition, 25);
    }
    
    move() {
        this.xPosition += this.dir;
    }
    
    changeDirection() {
        this.dir *= -1;
    }
    
    down() {
        this.yPosition += 20;
    }
    
    bottom() {
        return (this.yPosition > 420);
    }
    
    edge() {
        return (this.xPosition > 450 || this.xPosition < 50);
    }
    
    getX() {
        return this.xPosition;
    }
    
    getY() {
        return this.yPosition;
    }
}

class Player {

    constructor() {
        this.xPosition = 250;
        this.speed = 5;
        this.direction = 0;
    }
    
    show() {
        fill(255);
        stroke(0);
        rect(this.xPosition, 450, 12, 40);
    }
    
     move() {
        if (this.xPosition <= 480 && this.direction == 1) {
            this.xPosition = this.xPosition + this.direction * this.speed;
        } else if (this.xPosition >= 10 && this.direction == -1) {
            this.xPosition = this.xPosition + this.direction * this.speed;
        }
    }
    
    setDirection(dir) {
        this.direction = dir;
    }
    
    getXPosition() {
        return this.xPosition;
    }
}

function setup() {
//Größe des Programms in (x, y) Pixel
 createCanvas(500, 500); 
    
 //Initialisierung der Variablen
 player = new Player();
 bullet = new Bullet();
 enemies = []; 

    //Schleife um neue Objekte vom Typ "Enemy" in die Liste "enemies" einzufügen. 
    for (let i=0; i<6; i++) {
      enemies.push(new Enemy(75 + 50*i, 100));
      enemies.push(new Enemy(75 + 50*i, 150));
    }
}

function draw() {
     //Farbe des Hintergrunds
  background(50);

  //Methoden der Player-Klasse um den Spieler anzuzeigen und ggf. zu bewegen.
  player.show();
  player.move();
  
  //Schleife durch die Liste der "Enemies" um alle Objekte anzuzeigen und zu bewegen.
  for (let i=0 ; i<enemies.length ; i++) {
    enemies[i].show();
    enemies[i].move();
    
  //Die If-Abfrage wird benötigt um zu überprüfen ob eines der "Enemy"-Objekte sich am Rand
  //des Programms befindet
    if (enemies[i].edge()) {
      for (let k = 0 ; k<enemies.length ; k++) {
        enemies[k].changeDirection();
        enemies[k].down();
      }
    }
    
  //Diese If-Abfrage überprüft bei jedem Schleifendurchlauf für jedes Element, ob es sich am unterem Rand
  //des Programms befindet: Sollte das passieren hat man Verloren und das Programm wird beendet.
    if (enemies[i].bottom()) {
      textSize(32);
      text('GAME OVER', 150, 300);
      noLoop();
    }
  }

  //Diese If-Abfrage überprüft, ob das "Bullet"-Objekt geschossen wurde:
  //Wenn ja, dann wird das Objekt angezeigt und auch bewegt.
  if (bullet.isShooting) {
    bullet.show();
    bullet.move();

  //Die Schleife wird zur Kollisionserkennung genutzt.
    for (let i=0; i< enemies.length; i++) {
      if (dist(bullet.getX(), bullet.getY(), enemies[i].getX(), enemies[i].getY()) < 20) {
        enemies.splice(i,1);
        bullet.reset();
      }
    }
  }
}

//Diese Methode wird immer aufgerufen wenn eine Taste gedrückt wird
function keyPressed() {
    //Für die Taste 'a' wird die Richtung des Spielers auf "-1" gesetzt.
    if (key == 'a') {
      player.setDirection(-1);
    }
    //Für die Taste 'd' wird die Richtung des Spielers auf "1" gesetzt.  
    if (key == 'd') {
      player.setDirection(1);
    }
    //Für die Taste ' ' (Leertaste) wird überprüft ob bereits geschossen wurde und
    //schießt dementsprechend eine neuen Schuss an der X-Koordinate des Spieler kreiert.
    if (key == ' ') {
      if (!bullet.isShooting) {
        bullet.shoot(player.getXPosition());
      }
    }
  }
  
  //Diese Methode wird immer aufgerufen wenn eine Taste losgelassen wird.
  //Ist die Taste ein 'a' oder 'd'  wird die Richtung des Spieler auf 0 gesetzt. 
  function keyReleased() {
    if (key == 'a' || key == 'd') {
      player.setDirection(0);
    }
  }