// Game of Life
const resolution = 10;
const size = 100;
let genCounter = 0;

var board1 = new Array(size);
var board2 = new Array(size);

function setup() {
    createCanvas(1000, 1000);
    noStroke();
    frameRate(32);
    background(125);

    for (var x = 0; x < board1.length; x++) {
        board1[x] = new Array(size);
    }
    for (var y = 0; y < board2.length; y++) {
        board2[y] = new Array(size);
    }

    for (let i = 0; i < size; i++) {
        for (let j = 0; j < size; j++) {
            board1[i][j] = (random(1) >= 0.5);
        }
    }
}

function draw() {
    if(genCounter % 2 == 0) {
        drawBoard(board1);
        nextGen(board1, board2);
    }
    else {
        drawBoard(board2);
        nextGen(board2, board1);
    }
    genCounter += 1;
}

function drawBoard(board) {
    for (let i = 0; i < size; i++) {
        for (let j = 0; j < size; j++) {
            if(board[i][j]) fill(0);
            else fill(255);
            rect(i * resolution, j * resolution, resolution, resolution);
        }
    }
}

function nextGen(boardOld, boardNew) {
    for (let i = 0; i < size; i++) {
        for (let j = 0; j < size; j++) {
            if (boardOld[i][j] && (neighbours(boardOld, i, j) == 2 || neighbours(boardOld, i, j) == 3)) boardNew[i][j] = true; // lebendig && 2 || 3 -> true
            else boardNew[i][j] = neighbours(boardOld, i, j) == 3;
        }
    }
}

function neighbours(board, i, j) {
    var counter = 0;
    if (i - 1 >= 0 && board[i - 1][j]) counter ++; // oben
    if (i + 1 < size && board[i + 1][j]) counter ++; // unten
    if (j - 1 >= 0 && board[i][j - 1]) counter ++; // links
    if (j + 1 < size && board[i][j + 1]) counter ++; // rechts
    if (i - 1 >= 0 && j - 1 >= 0 && board[i - 1][j - 1]) counter ++; // oben-links
    if (i - 1 >= 0 && j + 1 < size && board[i - 1][j + 1]) counter ++; // oben-rechts
    if (i + 1 < size && j - 1 >= 0 && board[i + 1][j - 1]) counter ++; // unten-links
    if (i + 1 < size && j + 1 < size && board[i + 1][j + 1]) counter ++; // unten-rechts
    return counter;
}
