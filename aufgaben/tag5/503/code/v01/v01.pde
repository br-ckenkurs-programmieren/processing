/**
 Game Of Life by
 @author Jonas Reitz
 
 Die Zellen werden in einem 2 Dimensionalen boolean Array angelegt und durch true = lebend und false = tot dargestellt.  
 */

//Die Anzahl der Spalten und Reihen des Rasters
final int rows = 50;
final int columns = 50;

//Die Pixelanzahl pro Zelle
final int resolution = 20;

//Das Raster selber
boolean[][] grid;

//Eine Methode um eine neues 2 Dimensionales Array zu erstellen
boolean[][] makeNewArray() {
  //Das Neue Array, welches rows anzahl an reihen und coumns anzahl an spalten hat. Werte sind standartmäßig "false"
  boolean[][] ret = new boolean[columns][rows]; 
  return ret;
}

//Wird zum Start des Programms aufgerufen
void setup() {
  //Die Größe des Fensters muss mindestens Spalten * resolution Breit und Reihen* resolution hoch sein.
  size(1000, 1000);

  //Die Framerate begerenzt den Aufruf der update() pro sekunde, hier auf 10, um das game of life besser anzuschauen, da es sonst zu schnell ist
  frameRate(10);

  //Erstellen des Initial "Grids"/Rasters
  grid = makeNewArray();


  stroke(0);
  //Die 2 folgenden Schleifen sind geschachtelt um jeder einzelnen Zelle einen Wert zuzuweisen
  //Für Jede Spalte
  for (int x=0; x<columns; x++) {
    //Für Jede Reihe
    for (int y=0; y<rows; y++) {
      //Für Jede Zelle
      //Zufallszahl zwischen 0 und 2
      //Wenn die Zufallszahl größer oder gleich 1 ist, wird der initialwert der Zelle auf true gesetzt, so bekommt jede Zelle einen zufällgien Startwert
      if (random(2) >=1) {
        grid[x][y] = true;
      }
    }
  }
}

void draw() {
  //Wieder für Jede Zelle
  for (int x=0; x<columns; x++) {
    for (int y=0; y<rows; y++) {
      //Wenn die Zelle = true ist, wird die Farbe auf Schwarz (RGB Code = 0,0,0) oder wenn nicht, dann auf Weiß(RGB Code = 255,255,255);
      if (grid[x][y]) {
        fill(0, 0, 0);
      } else {
        fill(255, 255, 255);
      }
      //Zeichnen der Zelle an der Stelle x,y und der Größe der resolution variable als höhe und breite
      rect(x*resolution, y*resolution, resolution-1, resolution-1);
    }
  }

  makeNewGeneration();
}
//Erstellen der nächsten Generation
void makeNewGeneration() {
  //Erstellen eines neuen Arrays, um die nächste Generation zu speichern, ohne die momentane zu verändern, da die Regeln sonst nicht funktionieren
  boolean[][] next = makeNewArray();

  //Wieder für Jede Zelle : 
  for (int x=0; x<columns; x++) {
    for (int y=0; y<rows; y++) {

      //Der Zellenstatus
      int state = isAlive(grid, x, y);
      //Zähle die lebenden Nachbarn
      int neiCount = countNeighbors(grid, x, y, state);


      //Die angewendeten Regeln
      if (state == 0 && neiCount == 3) {
        next[x][y] = true;
      } else if (state == 1 && neiCount < 2 || neiCount > 3) {
        next[x][y] = false;
      } else {
        next[x][y] = grid[x][y];
      }
    }
  }
  //Übertragung der nächsten Generation auf das richtige Raster
  grid = next;
}


//Methode um lebende Nachbarn zu zählen
int countNeighbors(boolean[][] g, int x, int y, int selfStatus) {
  //Zählt die Summe der umliegenden Nachbarn, addiert 1 für lebend und 0 für tot
  int sum = 0;
  //Für jede umliegende Zelle
  /*
        (x-1,y-1)|(x,y-1)|(x+1,y-1)
   (x-1,y)  |(x,y)  |(x+1,y)
   (x-1,y+1)|(x,y+1)|(x+1,y+1)
   */
  sum += isAlive(g, x+1, y, selfStatus);
  sum += isAlive(g, x+1, y+1, selfStatus);
  sum += isAlive(g, x+1, y-1, selfStatus);
  sum += isAlive(g, x-1, y, selfStatus);
  sum += isAlive(g, x-1, y+1, selfStatus);
  sum += isAlive(g, x-1, y-1, selfStatus);
  sum += isAlive(g, x, y+1, selfStatus);
  sum += isAlive(g, x, y-1, selfStatus);
  return sum;
}


//Methode um zu überprüfen ob die Zelle lebt und gibt 1 für lebend und 0 für tot zurück anstatt einem true oder false, um die Zählung der Nachbarn zu vereinfachen, da die Zahlen dann einfach addiert werden können
int isAlive(boolean[][] g, int x, int y) {
  //Falls die Zelle aushalb des Rasters "grid" ist, wird in diesem Beispiel die Zelle als tot gezählt und somit 0 zurückgegeben
  if (x<0 || x>=columns || y<0 || y>= rows) {
    return 0;
  }
  if (g[x][y]) {
    return 1;
  } else {
    return 0;
  }
}
//Methode um zu überprüfen ob die Zelle lebt und gibt 1 für lebend und 0 für tot zurück anstatt einem true oder false, um die Zählung der Nachbarn zu vereinfachen, da die Zahlen dann einfach addiert werden können
int isAlive(boolean[][] g, int x, int y, int self) {
  //Falls die Zelle aushalb des Rasters "grid" ist, wird in diesem Beispiel die Zelle gleich wie der Nachbar gezählt
  if (x<0 || x>=columns || y<0 || y>= rows) {
    return self;
  }
  if (g[x][y]) {
    return 1;
  } else {
    return 0;
  }
}
