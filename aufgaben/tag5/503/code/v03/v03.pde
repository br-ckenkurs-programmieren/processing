// Game of Life

int resolution = 5;
int size = 200;
int genCounter = 0;

// alternierender Ansatz, Speicherplatz-freundlicher
boolean[][] board1 = new boolean[size][size];
boolean[][] board2 = new boolean[size][size];


void setup() {
  size(1000, 1000); // resolution * size
  noStroke();
  frameRate(32);
  
  // das erste Board einmal mit zufälligen Werten füllen
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      board1[i][j] = random(1) >= 0.5;
    }
  }
}

void draw() {
  if (genCounter % 2 == 0) { // beim ersten Durchlauf, und sonst jeder zweite
    drawBoard(board1);
    nextGen(board1, board2);
  } else { // ab dem zweiten Durchlauf, jeder zweite
    drawBoard(board2);
    nextGen(board2, board1);
  }
  drawText();
  genCounter ++;
}

void drawText() {
  fill(120, 120, 120);
  rect(width / 2 - 60, 5, 120, 60);
  textSize(45); 
  textAlign(CENTER);
  fill(0);
  text(genCounter, width / 2, 50);
}

// Visualisierung des Boards
void drawBoard(boolean[][] board) {
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      if (board[i][j]) fill(0); // anhand des boolean wird entschieden welche Farbe verwendet wird, true = schwarz
      else fill(255);
      rect(i * resolution, j * resolution, resolution, resolution);
    }
  }
}

// das neue Brett wird beladen anhand der Daten aus dem alten Brett
void nextGen(boolean[][] boardOld, boolean[][] boardNew) {
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      if (boardOld[i][j] && (neighbours(boardOld, i, j) == 2 || neighbours(boardOld, i, j) == 3)) boardNew[i][j] = true; // lebendig && 2 || 3 -> true
      else if (neighbours(boardOld, i, j) == 3) boardNew[i][j] = true; // tod && 3 -> true
      else boardNew[i][j] = false;
    }
  }
}

// Berechnung der Anzahl an Nachbarn um die Regeln des Game of Life anzuwenden
// um einen IndexOutOfBounds zu vermeiden prüfen wir zuerst ob die werte zwischen 0 und size liegen
// continue wird verwendet um direkt in den nächsten for-Durchlauf zu gehen
int neighbours(boolean[][] board, int i, int j) {
  int counter = 0; 
  for (int a = -1; a < 2; a++) {
    if (!(i + a < size && i + a >= 0)) continue; 
    for (int b = -1; b < 2; b++) {
      if (!(j + b < size && j + b >= 0)) continue;
      if (board[i + a][j + b] && !(a == 0 && b == 0)) counter++;
    }
  }
  return counter;
}
/*
int neighbours2(boolean[][] board, int i, int j) {
  int counter = 0;
  if (i - 1 >= 0 && board[i - 1][j]) counter ++; // oben
  if (i + 1 < size && board[i + 1][j]) counter ++; // unten
  if (j - 1 >= 0 && board[i][j - 1]) counter ++; // links
  if (j + 1 < size && board[i][j + 1]) counter ++; // rechts
  if (i - 1 >= 0 && j - 1 >= 0 && board[i - 1][j - 1]) counter ++; // oben-links
  if (i - 1 >= 0 && j + 1 < size && board[i - 1][j + 1]) counter ++; // oben-rechts
  if (i + 1 < size && j - 1 >= 0 && board[i + 1][j - 1]) counter ++; // unten-links
  if (i + 1 < size && j + 1 < size && board[i + 1][j + 1]) counter ++; // unten-rechts
  return counter;
}*/
