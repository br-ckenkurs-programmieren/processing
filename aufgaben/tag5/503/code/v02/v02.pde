/* //<>//
 * Game Of Life (ohne Objektorientierung)
 * (c) Jakob Czekansky, 2020
 */

/*===================================================================
 *
 * Einstellungen
 *
 *===================================================================*/
int count_of_fields = 200; // ! needs to be a divider of the width and the height
color field_color = color(255, 0, 0);
color text_color = color(255, 0, 0);
color positioning_color = color(0, 255, 255);

/*===================================================================
 *
 * Spiel-Mechanik-Variablen
 *
 *===================================================================*/
boolean simulation_running = true;
int iterations = 0;

/*===================================================================
 *
 * Spielfeld-Angaben
 *
 *===================================================================*/
int field_size;
int[][] field1 = new int[count_of_fields+2][count_of_fields+2];
int[][] field2 = new int[count_of_fields+2][count_of_fields+2];
int[][] currentField = field1;
int[][] nextField = field2;

/*===================================================================
 *
 * Spezielle, spawnbare Figuren
 *
 *===================================================================*/
int[][] block_1x1 = 
  {{1}};
int[][] block_2x2 = 
  {{1, 1}, 
  { 1, 1}};
int[][] gleiter = 
  {{0, 1, 0}, 
  { 0, 0, 1}, 
  { 1, 1, 1}};
int[][] segler_lwss = 
  {{0, 1, 1, 1, 1}, 
  { 1, 0, 0, 0, 1}, 
  { 0, 0, 0, 0, 1}, 
  { 1, 0, 0, 1, 0}};
int[][] segler_mwss = 
  {{0, 1, 1, 1, 1, 1}, 
  { 1, 0, 0, 0, 0, 1}, 
  { 0, 0, 0, 0, 0, 1}, 
  { 1, 0, 0, 0, 1, 0}, 
  { 0, 0, 1, 0, 0, 0}};
int[][] segler_hwss = 
  {{0, 1, 1, 1, 1, 1, 1}, 
  { 1, 0, 0, 0, 0, 0, 1}, 
  { 0, 0, 0, 0, 0, 0, 1}, 
  { 1, 0, 0, 0, 0, 1, 0}, 
  { 0, 0, 1, 1, 0, 0, 0}};

int active_figure_index = 0;

int[][][] figures = {block_1x1, block_2x2, gleiter, segler_lwss, segler_mwss, segler_hwss};

/*===================================================================*/

/*
 * Processing Setup Funktion
 */
void setup() {
  size(1200, 1300);
  noStroke();
  field_size = width/count_of_fields;
  initializeFieldBorder(0);
  initializeRandomField(0.5);
  colorizeField();
  frameRate(50);
}

/*
 * Processing Draw Funktion
 */
void draw() {
  if (simulation_running == true) {
    checkConditions();
    switchFields();
    colorizeField();
    showText("Conways Game Of Life - Iterationen: " + iterations, 10);

    iterations++;
  }
}

void initializeFieldBorder(int value) {
  for (int i = 0; i <= count_of_fields+1; i++) {
    currentField[0][i] = value; 
    currentField[i][0] = value; 
    currentField[count_of_fields+1][0] = value; 
    currentField[0][count_of_fields+1] = value;

    nextField[0][i] = value; 
    nextField[i][0] = value; 
    nextField[count_of_fields+1][0] = value; 
    nextField[0][count_of_fields+1] = value;
  }
}

void initializeRandomField(float chance_for_living_cell) {
  int value = 0;
  for (int i = 1; i <= count_of_fields; i++) {
    for (int j = 1; j <= count_of_fields; j++) {
      if (random(0, 1) < chance_for_living_cell ) {
        value = 1;
      } else {
        value = 0;
      }
      currentField[i][j] = value;
    }
  }
}

void colorizeField() {
  color c; 
  for (int i = 1; i <= count_of_fields; i++) {
    for (int j = 1; j <= count_of_fields; j++) {
      if (currentField[i][j] == 0) {
        c = color(0);
      } else {
        c = field_color;
      }
      fill(c); 
      rect((i-1)*field_size, (j-1)*field_size, field_size, field_size);
    }
  }
}

void colorizeSection(int start_idx_x, int start_idx_y, int stop_idx_x, int stop_idx_y) {
  color c; 
  if (start_idx_x < 1) {
    start_idx_x = 1;
  }
  if (start_idx_y < 1) {
    start_idx_y = 1;
  }
  if (stop_idx_x > count_of_fields) {
    stop_idx_x = count_of_fields;
  }
  if (stop_idx_y > count_of_fields) {
    stop_idx_y = count_of_fields;
  }  



  for (int i = start_idx_x; i <= stop_idx_x; i++) {
    for (int j = start_idx_y; j <= stop_idx_y; j++) {
      if (currentField[i][j] == 0) {
        c = color(0);
      } else {
        c = field_color;
      }
      fill(c); 
      rect((i-1)*field_size, (j-1)*field_size, field_size, field_size);
    }
  }
}

void checkConditions() {
  int neighbours = 0; 
  for (int i = 1; i <= count_of_fields; i++) {
    for (int j = 1; j <= count_of_fields; j++) {
      neighbours = currentField[i-1][j-1] + currentField[i][j-1] + currentField[i+1][j-1] + 
        currentField[i-1][j]   + 0                    + currentField[i+1][j]   + 
        currentField[i-1][j+1] + currentField[i][j+1] + currentField[i+1][j+1]; 
      //print(neighbours + ", ");             
      if (neighbours < 2 || neighbours > 3) {
        nextField[i][j] = 0;
      } else if (neighbours == 2) {
        nextField[i][j] = currentField[i][j];
      } else if (neighbours == 3) {
        nextField[i][j] = 1;
      }
    }
  }
}

void switchFields() {
  if (currentField == field1) {
    currentField = field2;
    nextField = field1;
  } else {
    currentField = field1;
    nextField = field2;
  }
}

void preshowFigure(int[][] figure, int x, int y) {
  fill(positioning_color); 
  for (int i = 0; i < figure.length; i++) {
    for (int j = 0; j < figure[i].length; j++) {
      if (figure[i][j] == 1 && (y*field_size+figure[i].length) < height-100) {
        rect((x+j)*field_size, (y+i)*field_size, field_size, field_size);
      }
    }
  }
}

void setFigure(int[][] figure, int x, int y) {
  for (int i = 0; i < figure.length; i++) {
    for (int j = 0; j < figure[i].length; j++) {
      if (figure[i][j] == 1) { 
        if ((x+j <= count_of_fields) && (y+i <= count_of_fields)) {
          if (currentField[x+j][y+i] == 0) {
            currentField[x+j][y+i] = 1;
          } else {
            currentField[x+j][y+i] = 0;
          }
        }
      }
    }
  }
}

void showText(String message, int x_offset) {
  fill(150);
  rect(0, height-100+field_size, width, 100-field_size);
  fill(text_color);
  textSize(24);
  textAlign(CENTER);
  text(message, width/2, height - (100-field_size)/2 + x_offset);
}


/*===================================================================
 *
 * EVENT Funktionen
 *
 *===================================================================*/
/*
 * mousePressed Event Funktion
 */
void mousePressed() {
  switch(mouseButton) {
  case LEFT:
    if (simulation_running == false) {
      int field_x = int(mouseX/field_size)+1;
      int field_y = int(mouseY/field_size)+1;
      setFigure(figures[active_figure_index], field_x, field_y);
      colorizeField();
    } else {
    }


    break;

  case RIGHT:
    break;

  default:
  }
}

/*
 * keyPressed Event Funktion
 */
void keyPressed() {
  switch(key) {
  case ' ': 
    if (simulation_running == true) {
      simulation_running = false;
      showText("Select objects to spawn with LEFT and RIGHT key.\n Set a position with the LEFT MOUSE BUTTON.", -10);
    } else {
      simulation_running = true;
    }
  }
  switch(keyCode) {
  case UP:
    break;
  case DOWN:
    break;
  case RIGHT:
    if (simulation_running == false) {
      active_figure_index = (active_figure_index + 1 ) % figures.length;
    }
    break;
  case LEFT: 
    if (simulation_running == false) {
      active_figure_index = (active_figure_index - 1   ) % figures.length;
      if (active_figure_index < 0) {
        active_figure_index = figures.length-1;
      }
    }
    break;
  default:
  }
  mouseMoved();
}

/*
 * mouseMoved Event Funktion
 */
void mouseMoved() {
  if (simulation_running == false) {
    int range = 20;
    int field_x = int(mouseX/field_size)+1;
    int field_y = int(mouseY/field_size)+1;
    int figure_width = figures[active_figure_index].length;
    int figure_height = figures[active_figure_index][0].length;

    colorizeSection(field_x-range, field_y-range, field_x + figure_width+range, field_y + figure_height+range);
    preshowFigure(figures[active_figure_index], int(mouseX/field_size), int(mouseY/field_size));
  }
}
