const powerUpChance = 0.1;

class Ball {

  constructor(xPos,yPos,d, r, vx, vy) {
    this.xPos = xPos;
    this.yPos = yPos;
    this.d = d;
    this.r = r;
    this.vx = vx;
    this.vy = vy;
    this.speed = 3;
    this.xPosOld = xPos;
    this.yPosOld= yPos;
  }

  draw(){
    fill(152, 255, 0);
    circle(this.xPos, this.yPos, this.r*2);
  }
}
class Brick {

   

  constructor(posX,  posY, life = 1) {
    this.posX = posX;
    this.posY = posY;
    this.active = true;
    this.life = life;

  }
  // Zeichnet Block
  display() {
    if (this.active) {
      rect(this.posX, this.posY, blockWidth, blockHeight);
      fill(0, 0, 0);
      //text(""+life, posX, posY+blockHeight);
    }
  }

  /* Prüft ob der Ball die Box berührt
   Alte Position wird benutzt, um Richtung des Balls zu bestimmen
   */
  collide(x,  y,  xOld,  yOld,  r) {
    if (!this.active) return 0; // Wenn der Block nicht aktiv ist, kann er nicht berührt werden

    // Wenn der Ball die Box berührt, wird die Richtung des Balls bestimmt
    if (x + r > this.posX // rechts
      && x - r < this.posX + blockWidth // links
      && y + r > this.posY // unten
      && y - r < this.posY + blockHeight // oben
      ) {
      if (xOld + r < this.posX  // links
        || xOld - r > this.posX + blockWidth) { // rechts
        return 1;
      } else {
        return 2;
      }
    }

    return 0;
  }
    //Bei jedem Treffer wird die Lebenanzahl um eins reduziert
  hit() {
    this.life--;
    score++;
    if (this.life<=0) {
      this.active = false;
    }
    if(random(1)<powerUpChance) {
      powerups.push(new Powerup(this.posX,this.posY,int(random(4))));
    }
  }
}

class Paddle {
  

  constructor( paddleHeight,  paddleWidth,  paddleOffset) {
    this.paddleHeight = paddleHeight;
    this.paddleWidth = paddleWidth;
    this.paddleOffset = paddleOffset;
  }
  
  draw( x, y){
    fill(255);
    rect(x, y, paddle.paddleWidth, paddle.paddleHeight);
  }
}
class Powerup{

  
  
  constructor( inWorldPosX, inWorldPosY, t){
    this.inWorldPosX = inWorldPosX+blockWidth/2;
    this.inWorldPosY = inWorldPosY+blockHeight/2;
    this.pickedUp = false;
    this.r=10;
    this.type = t;
  }
  
  
  update(p, paddleX, paddleY){
    this.inWorldPosY += 1;

    if ((this.inWorldPosX + this.r > paddleX && this.inWorldPosX + this.r < paddleX + p.paddleWidth
        || this.inWorldPosX - this.r > paddleX && this.inWorldPosX - this.r < paddleX + p.paddleWidth)
        && this.inWorldPosY + this.r > paddleY) {
        this.pickedUp = true;
        // PADDLE_WIDTH,BALL_SPEED,BALL_SIZE,BALL_COUNT
        switch(this.type){
          case Type.PADDLE_WIDTH:
            p.paddleWidth += 10;
            break;
          case Type.BALL_SPEED:
            for(let i=0;i<balls.length;i++){
              balls[i].speed += 1;
            }
            break;
          case Type.BALL_SIZE:
            for(let i=0;i<balls.length;i++){
              balls[i].r += 3;
            }
            break;
          case Type.BALL_COUNT:
            balls.push(new Ball(paddleX+p.paddleWidth/2, 500, 10, 5, 0.5, 1.3));
            break;
        }
     }
  }
  
  draw(){
    if(!this.pickedUp){
      fill(203,91,240);
      ellipse(this.inWorldPosX,this.inWorldPosY,this.r,this.r);
    }
  }
}
//Die Verschiedenen Powerup-Typen

const Type = {
PADDLE_WIDTH: 0,
BALL_SPEED: 1,
BALL_SIZE: 2,
BALL_COUNT: 3
};

let paddle;
let balls = [];
let blockHeight, blockWidth;
let powerups = [];
// Array aus Steinen, die getroffen werden müssen
let bricks = [];
// Farben für Steine
let colors = [];

let score = 0;


let running;

function setup() {
  createCanvas(800, 600);
  textSize(30);

  bricks = flag();
  colors = [];
  balls.push(new Ball(width/2, 500, 10, 5, 0.5, 1.3));

  blockHeight = 25;
  blockWidth = width/12;

  paddle = new Paddle(20, 200, 50);

  running = false;

  // Farben für Regenbogenmuster in den Steinen
  colors[0] = color(255, 0, 0);
  colors[1] = color(255, 153, 0);
  colors[2] = color(255, 255, 0);
  colors[3] = color(152, 255, 0);
  colors[4] = color(41, 255, 0);
  colors[5] = color(0, 255, 150);
  colors[6] = color(0, 150, 255);
  colors[7] = color(0, 0, 255);
  colors[8] = color(120, 0, 255);
  colors[9] = color(190, 0, 255);
  colors[10] = color(190, 0, 255);
}

function draw() {
  let paddleY = height - paddle.paddleOffset;
  let paddleX;
  if (pmouseX > width - paddle.paddleWidth / 2) {
    paddleX = width - paddle.paddleWidth;
  } else if (pmouseX < paddle.paddleWidth / 2) {
    paddleX = 0;
  } else {
    paddleX = pmouseX - paddle.paddleWidth / 2;
  }

  background(100);
  paddle.draw(paddleX, paddleY);

  for (let i = 0; i < bricks.length; i++) {
    fill(colors[int(i / 10)]);
    
    bricks[i].display();
  }
  for (let i = 0; i < balls.length; i++) {
    balls[i].draw();
  }
  for (let i = 0; i < powerups.length; i++) {
    let p = powerups[i];
    p.update(paddle, paddleX, paddleY);
    if (p.pickedUp) {
      powerups.splice(p,1);
    }
    p.draw();
  }

  for (let j = 0; j < balls.length; j++) {
    let ball = balls[j];
    //Abprallen von Wänden (links, rechts, oben)
    if ((ball.xPos < ball.r && ball.vx < 0) || (ball.xPos > width - ball.r && ball.vx > 0)) ball.vx *= -1;
    if (ball.yPos < ball.r && ball.vy < 0) ball.vy *= -1;

    //Abprallen von Schläger
    if ((ball.xPos + ball.r > paddleX && ball.xPos + ball.r < paddleX + paddle.paddleWidth
            || ball.xPos - ball.r > paddleX && ball.xPos - ball.r < paddleX + paddle.paddleWidth)
        && ball.yPos + ball.r > paddleY) {
      //Trefferpunkt ermittlen
      let hitpoint = (ball.xPos - mouseX) / 100;
      ball.vx += hitpoint;
      //Winkel sollte nicht zu flach sein
      if (ball.vx > 1.3) {
        ball.vx = 1.3;
      } else if (ball.vx < -1.3) {
        ball.vx = -1.3;
      }

      if (ball.vy > 0) ball.vy *= -1; //Ball nur nach oben abprallen lassen
    }
    if (ball.yPos > 600) {
      balls.splice(ball,1);
      if (balls.length <= 0) {
        running = false;
        text("Game Over, press r to reset", 250, 300);
        return;
      }
    }

    //Abprallen von Blöcken
    for (let i = 0; i < bricks.length; i++) {
      let hit = bricks[i].collide(ball.xPos, ball.yPos, ball.xPosOld, ball.yPosOld, ball.r);
      if (hit == 1) {
        ball.vx *= -1;

        bricks[i].hit();
      }
      if (hit == 2) {
        ball.vy *= -1;
        bricks[i].hit();
      }
    }
  }




  if (victory()) {
    running = false;
    text("Sieg", 400, 300);
  }

  if (running) {
    for (let i = 0; i < balls.length; i++) {
      let ball = balls[i];
      ball.xPosOld = ball.xPos;
      ball.yPosOld = ball.yPos;

      ball.xPos += ball.vx * ball.speed;
      ball.yPos += ball.vy * ball.speed;
    }
    text("Score: " + score, 10, 30);
  } else {
    text("Click to start", width/2 - 300, height/2 + 100);
    text("Use mouse to move paddle", width/2 - 100, height/2+50);
    text("Use key 'r' to reset", width/2 - 100, height/2+100);
    text("Use keys 1-5 to change map", width/2 - 100, height/2+150);
  }
}

// Repräsentiert einen Block mit seiner Position und seinem Status



// R drücken um Spiel zu reseten
function keyPressed() {
  if (key === 'r' || key === '1' || key === '2' || key === '3' || key === '4'|| key === '5') {
    if (key === '1') {
      bricks = flag();
    } else if (key === '2') {
      bricks = rectangle();
    } else if (key === '3') {
      bricks = circleM();
    } else if (key === '4') {
      bricks = triangleM();
    }else if (key === '5') {
      bricks = rectangle2();
    } else {
      for (let i = 0; i< bricks.length; i++) {
        bricks[i].active = true;
      }
    }
    running = false;
    balls = [];
    powerups = [];
    balls.push(new Ball(width/2, 500, 10, 5, 0.5, 1.3));
    
    score =0;
  }
}

// Klicken um das Spiel zu starten und den Ball zu aktivieren
function mouseClicked() {
  running = true;
}

// Berechnung ob das Spiel vorbei ist bzw. der Spieler gewonnen hat
function victory() {
  for (let i = 0; i < bricks.length; i++) {
    if (bricks[i].active) return false;
  }
  return true;
}

function rectangle() {
  let bricks = [];
  for (let i = 0; i < 100; i++) {
    bricks[i] = new Brick(width/12 + i%10*width/12, int(i/10)*25 + 50);
  }
  return bricks;
}

function rectangle2() {
  let bricks = [];
  for (let i = 0; i < 100; i++) {
    bricks[i] = new Brick(width/12 + i%10*width/12, int(i/10)*25 + 50,((int(i/10)*25 + 50)/(height/3) * 2)+1);
  }
  return bricks;
}

function circleM() {
  let bricks = [];
  let centerX = width/2;
  let centerY = height/2;
  let radius = 250;
  for (let i = 0; i < 100; i++) {
    let angle = i * TWO_PI / 100;
    let x = centerX + cos(angle) * radius;
    let y = centerY + (sin(angle) * radius) / 2;
    bricks.push(new Brick(x, y));
  }
  return bricks;
}

function triangleM() {
  let bricks =  [];
  let centerX = width/2;
  let centerY = height/2 - 300;
  let oneThird = int(100 / 3);
  let twoThirds = (int(100 / 3) * 2);
  let step = 10;
  for (let i = 0; i < oneThird; i += 2) { // Balken unten
    bricks.push(new Brick(centerX - i * step, centerY + 330));
    bricks.push( new Brick(centerX + i * step, centerY + 330));
  }
  for (let i = oneThird; i < twoThirds; i++) { // linke Seite
    bricks.push(new Brick(centerX - (oneThird - i) * step, centerY - (oneThird - i) * step));
  }
  for (let i = twoThirds; i < 100; i++) { // rechte Seite
    bricks.push(new Brick(centerX + (twoThirds - i) * step, centerY + (i - twoThirds) * step));
  }
  return bricks;
}

function flag() {
  let bricks = [];
  for (let i = 0; i < 100; i++) {
    bricks[i] = new Brick(
      width / 12 + (i % 10) * (width / 12),
      ((i / 10) * 25 + 50) + 15 * Math.sin(int((i % 10) / 1.5)));
  }
  return bricks;
}
