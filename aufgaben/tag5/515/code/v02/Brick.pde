class Brick {
    //Die Chance das ein powerup spawnen soll
   final float powerUpChance = 0.1f;
  float posX;
  float posY;
  boolean active;
    //Anzahl der momentanen leben
  int life;

  Brick(float posX, float posY) {
    this(posX, posY, 1);
  }

  Brick(float posX, float posY, int life) {
    this.posX = posX;
    this.posY = posY;
    this.active = true;
    this.life = life;
  }
  // Zeichnet Block
  void display() {
    if (this.active) {
      rect(posX, posY, blockWidth, blockHeight);
      fill(0, 0, 0);
      //text(""+life, posX, posY+blockHeight);
    }
  }

  /* Prüft ob der Ball die Box berührt
   Alte Position wird benutzt, um Richtung des Balls zu bestimmen
   */
  int collide(float x, float y, float xOld, float yOld, float r) {
    if (!active) return 0; // Wenn der Block nicht aktiv ist, kann er nicht berührt werden

    // Wenn der Ball die Box berührt, wird die Richtung des Balls bestimmt
    if (x + r > posX // rechts
      && x - r < posX + blockWidth // links
      && y + r > posY // unten
      && y - r < posY + blockHeight // oben
      ) {
      if (xOld + r < posX  // links
        || xOld - r > posX + blockWidth) { // rechts
        return 1;
      } else {
        return 2;
      }
    }

    return 0;
  }
    //Bei jedem Treffer wird die Lebenanzahl um eins reduziert
  void hit() {
    life--;
    score++;
    if (life<=0) {
      active = false;
    }
    if(random(1)<powerUpChance) {
      powerups.add(new Powerup(posX,posY,Type.values()[(int)random(Type.values().length)]));
    }
  }
}
