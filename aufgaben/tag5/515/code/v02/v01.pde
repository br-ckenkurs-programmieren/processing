Paddle paddle;
ArrayList<Ball> balls = new ArrayList<Ball>();
float blockHeight, blockWidth;
ArrayList<Powerup> powerups = new ArrayList<Powerup>();
// Array aus Steinen, die getroffen werden müssen
Brick[] bricks;
// Farben für Steine
color[] colors;

int score = 0;


boolean running;

void setup() {
  size(800, 600);
  textSize(30);

  bricks = flag();
  colors = new color[10];
  balls.add(new Ball(width/2, 500, 10, 5, 0.5f, 1.3f));

  blockHeight = 25;
  blockWidth = width/12;

  paddle = new Paddle(20, 200, 50);

  running = false;

  // Farben für Regenbogenmuster in den Steinen
  colors[0] = color(255, 0, 0);
  colors[1] = color(255, 153, 0);
  colors[2] = color(255, 255, 0);
  colors[3] = color(152, 255, 0);
  colors[4] = color(41, 255, 0);
  colors[5] = color(0, 255, 150);
  colors[6] = color(0, 150, 255);
  colors[7] = color(0, 0, 255);
  colors[8] = color(120, 0, 255);
  colors[9] = color(190, 0, 255);
}

void draw() {
  int paddleY = height - paddle.paddleOffset;
  int paddleX;
  if (pmouseX > width - paddle.paddleWidth/2) {
    paddleX = width - paddle.paddleWidth;
  } else if (pmouseX < paddle.paddleWidth/2) {
    paddleX = 0;
  } else {
    paddleX = pmouseX - paddle.paddleWidth/2;
  }

  background(100);
  paddle.draw(paddleX, paddleY);

  for (int i = 0; i< bricks.length; i++) {
    fill(colors[i/10]);
    bricks[i].display();
  }
  for (Ball b : balls) {
    b.draw();
  }
  for (int i=0;i<powerups.size();i++) {
    Powerup p=powerups.get(i);
    p.update(paddle, paddleX, paddleY);
    if(p.pickedUp){
      powerups.remove(p);
    }
    p.draw();
  }

  for (int j=0;j< balls.size() ; j++) {
    Ball ball = balls.get(j);
    //Abprallen von Wänden (links, rechts, oben)
    if ((ball.xPos < ball.r && ball.vx < 0) || (ball.xPos > width - ball.r && ball.vx > 0)) ball.vx *= -1;
    if (ball.yPos < ball.r && ball.vy < 0) ball.vy *= -1;

    //Abprallen von Schläger
    if ((ball.xPos + ball.r > paddleX && ball.xPos + ball.r < paddleX + paddle.paddleWidth
      || ball.xPos - ball.r > paddleX && ball.xPos - ball.r < paddleX + paddle.paddleWidth)
      && ball.yPos + ball.r > paddleY) {
      //Trefferpunkt ermittlen
      float hitpoint = (ball.xPos - mouseX) / 100;
      ball.vx += hitpoint;
      //Winkel sollte nicht zu flach sein
      if (ball.vx > 1.3) {
        ball.vx = 1.3;
      } else if (ball.vx < -1.3) {
        ball.vx = -1.3;
      }

      if (ball.vy > 0) ball.vy *= -1; //Ball nur nach oben abprallen lassen
    }
    if (ball.yPos > 600) {
      balls.remove(ball);
      if (balls.size()<=0) {
        running = false;
        text("Game Over, press r to reset", 250, 300);
        return;
      }
    }

    //Abprallen von Blöcken
    for (int i = 0; i < bricks.length; i++) {
      int hit = bricks[i].collide(ball.xPos, ball.yPos, ball.xPosOld, ball.yPosOld, ball.r);
      if (hit == 1) {
        ball.vx *= -1;

        bricks[i].hit();
      }
      if (hit == 2) {
        ball.vy *= -1;
        bricks[i].hit();
      }
    }
  }




  if (victory()) {
    running = false;
    text("Sieg", 400, 300);
  }

  if (running) {
    for (Ball ball : balls) {
      ball.xPosOld = ball.xPos;
      ball.yPosOld = ball.yPos;

      ball.xPos += ball.vx * ball.speed;
      ball.yPos += ball.vy * ball.speed;
    }
    text("Score: " + score, 10, 30);
  } else {
    text("Click to start", width/2 - 300, height/2 + 100);
    text("Use mouse to move paddle", width/2 - 100, height/2+50);
    text("Use key 'r' to reset", width/2 - 100, height/2+100);
    text("Use keys 1-5 to change map", width/2 - 100, height/2+150);
  }
}

// Repräsentiert einen Block mit seiner Position und seinem Status



// R drücken um Spiel zu reseten
void keyPressed() {
  if (key == 'r' || key == '1' || key == '2' || key == '3' || key == '4'|| key == '5') {
    if (key == '1') {
      bricks = flag();
    } else if (key == '2') {
      bricks = rectangle();
    } else if (key == '3') {
      bricks = circle();
    } else if (key == '4') {
      bricks = triangle();
    }else if (key == '5') {
      bricks = rectangle2();
    } else {
      for (int i = 0; i< bricks.length; i++) {
        bricks[i].active = true;
      }
    }
    running = false;
    balls.clear();
    balls.add(new Ball(width/2, 500, 10, 5, 0.5f, 1.3f));
    
    score =0;
  }
}

// Klicken um das Spiel zu starten und den Ball zu aktivieren
void mouseClicked() {
  running = true;
}

// Berechnung ob das Spiel vorbei ist bzw. der Spieler gewonnen hat
boolean victory() {
  for (int i = 0; i < bricks.length; i++) {
    if (bricks[i].active) return false;
  }
  return true;
}

Brick[] rectangle() {
  Brick[] bricks = new Brick[100];
  for (int i = 0; i < bricks.length; i++) {
    bricks[i] = new Brick(width/12 + i%10*width/12, int(i/10)*25 + 50);
  }
  return bricks;
}

Brick[] rectangle2() {
  Brick[] bricks = new Brick[100];
  for (int i = 0; i < bricks.length; i++) {
    bricks[i] = new Brick(width/12 + i%10*width/12, int(i/10)*25 + 50,(int)(((i/10f)*25f + 50f)/(height/3) * 2f)+1);
  }
  return bricks;
}

Brick[] circle() {
  Brick[] bricks = new Brick[100];
  int centerX = width/2;
  int centerY = height/2;
  int radius = 250;
  for (int i = 0; i < bricks.length; i++) {
    float angle = i * TWO_PI / bricks.length;
    float x = centerX + cos(angle) * radius;
    float y = centerY + (sin(angle) * radius) / 2;
    bricks[i] = new Brick(x, y);
  }
  return bricks;
}

Brick[] triangle() {
  Brick[] bricks = new Brick[100];
  int centerX = width/2;
  int centerY = height/2 - 200;
  int oneThird = int(bricks.length / 3);
  int twoThirds = int((bricks.length / 3) * 2);
  int step = 10;
  for (int i = 0; i < oneThird; i += 2) { // Balken unten
    bricks[i] = new Brick(centerX - i * step, centerY + 330);
    bricks[i + 1] = new Brick(centerX + i * step, centerY + 330);
  }
  for (int i = oneThird; i < twoThirds; i++) { // linke Seite
    bricks[i] = new Brick(centerX - (oneThird - i) * step, centerY - (oneThird - i) * step);
  }
  for (int i = twoThirds; i < bricks.length; i++) { // rechte Seite
    bricks[i] = new Brick(centerX + (twoThirds - i) * step, centerY + (i - twoThirds) * step);
  }
  return bricks;
}

Brick[] flag() {
  Brick[] bricks = new Brick[100];
  for (int i = 0; i < bricks.length; i++) {
    bricks[i] = new Brick(
      width / 12 + (i % 10) * (width / 12),
      (int(i / 10) * 25 + 50) + 15 * sin((i % 10) / 1.5));
  }
  return bricks;
}
