class Paddle {
  int paddleHeight, paddleWidth, paddleOffset;

  Paddle(int paddleHeight, int paddleWidth, int paddleOffset) {
    this.paddleHeight = paddleHeight;
    this.paddleWidth = paddleWidth;
    this.paddleOffset = paddleOffset;
  }
  
  void draw(int x,int y){
    fill(255);
    rect(x, y, paddle.paddleWidth, paddle.paddleHeight);
  }
}
