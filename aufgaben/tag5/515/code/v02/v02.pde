// Boids are moving from the left to the right side of the window. While doing so, they usually show vaguely triangle shaped formations. 

int boidAmount = 35; 
Boid[] boids = new Boid[boidAmount];
float boidPerceptionRange = 200;
int maxInitialBoidSpeed = 5;
float maxSpeed = 10;

// seperation does not work yet
void setup() {
  size(1800, 1000);
  background(255);
  // initalise boids
  for (int i = 0; i < boidAmount; i++) {
    boids[i] = new Boid(random(0, width), random(0, height), random(-maxInitialBoidSpeed, maxInitialBoidSpeed), random(-maxInitialBoidSpeed, maxInitialBoidSpeed));
  }
}

void draw() {
  background(255);
  for (int i = 0; i < boidAmount; i++) {
    PVector alignment, separation, cohesion;
    Boid myBoid = boids[i];
    ArrayList<Boid> neighbours = new ArrayList<Boid>();
    for (int j = 0; j < boidAmount; j++) {
      if (j != i) {
        // in perception range?
        Boid otherBoid = boids[j];
        PVector myPos = myBoid.getPosition();
        PVector otherPos = otherBoid.getPosition();
        if (dist(myPos.x, myPos.y, otherPos.x, otherPos.y) < boidPerceptionRange) {
          neighbours.add(otherBoid);
        }
      }
    }
    // deal with neighbours
    if (neighbours.size() > 1) {
      PVector sepSum = new PVector(0, 0);
      for (int n = 0; n < neighbours.size(); n++) {
        PVector inverseDistance = calculateInverseDistanceVector(myBoid, neighbours.get(n));
        sepSum = vAddition(sepSum, inverseDistance);
      }
      separation = skalarMultiplication(1.0/neighbours.size(), sepSum);
      // cohesion
      PVector coSum = new PVector(0, 0);
      for (int n = 0; n < neighbours.size(); n++) {
        coSum = vAddition(neighbours.get(n).getPosition(), coSum);
      }
      PVector coAvgPos = skalarMultiplication(1.0/neighbours.size(), coSum);
      cohesion = vSubtraction(coAvgPos, myBoid.getPosition());
      // adhesion
      PVector adSum = new PVector(0, 0);
      for (int n = 0; n < neighbours.size(); n++) {
        adSum = vAddition(neighbours.get(n).getMovement(), adSum);
      }
      alignment = skalarMultiplication(1.0/neighbours.size(), adSum);
      boids[i].weighVectors(separation, cohesion, alignment);
    }
    boids[i].move();
    // spaw out of bounds boids on left frame side
    if (boids[i].outOfBounds(0.0, width, 0.0, height)) {
      boids[i] = new Boid(0, boids[i].position.y, boids[i].movement.x,  0);
    }
    // show boids
    boids[i].show();
  }
}


PVector calculateInverseDistanceVector(Boid myBoid, Boid otherBoid) {
  PVector myPos = myBoid.getPosition();
  PVector otherPos = otherBoid.getPosition();
  float dist = dist(myPos.x, myPos.y, otherPos.x, otherPos.y);
  float partsOfRange = (boidPerceptionRange / dist);
  PVector fromMyToOtherVector = vSubtraction(myPos, otherPos);
  PVector prod = skalarMultiplication(partsOfRange, fromMyToOtherVector);
  prod.x = max(min(prod.x, 50), -5);
  prod.y = max(min(prod.y, 50), -50);
  return prod;
}
