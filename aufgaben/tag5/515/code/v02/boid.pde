class Boid {
  PVector position;
  PVector movement;

  Boid(PVector position, PVector movement) {
    this.position = position;
    this.movement = movement;
  }

  Boid(float positionX, float positionY, float movementX, float movementY) {
    this.position = new PVector(positionX, positionY);
    this.movement = new PVector(movementX, movementY);
  }

  void move() {
    // change Boids position
    this.position = vAddition(this.position, this.movement);
    }
 

  void show() {
    /*noFill();
    circle(this.position.x, this.position.y, 2 * boidPerceptionRange);
    // show boid as circle and orientation as line
    fill(0);*/
    circle(this.position.x, this.position.y, 5);
    line(this.position.x, this.position.y, this.position.x + this.movement.x, this.position.y + this.movement.y);
  }

  void weighVectors(PVector separation, PVector cohesion, PVector alignment) {
    separation = skalarMultiplication(0.5, separation);
    cohesion = skalarMultiplication(0.25, cohesion);
    alignment = skalarMultiplication(0.25, alignment);
    this.movement = vAddition(separation, vAddition(cohesion, alignment));
  }
  
  boolean outOfBounds(float minX, float maxX, float minY, float maxY){
    if (this.position.x < minX || this.position.x > maxX){
      return true;
    } else if (this.position.y < minY || this.position.y > maxY){
      return true;
    } else {
       return false; 
    }
  }
  
  void setPosition(float x, float y){
    this.position = new PVector(x, y);
  }
  
  PVector getPosition(){
    return this.position;
  }
  
  PVector getMovement(){
    return this.movement;
  }
}

PVector vAddition(PVector a, PVector b){
  return new PVector(a.x + b.x, a.y + b.y);
}

PVector vSubtraction(PVector a, PVector b){
  return new PVector(a.x - b.x, a.y - b.y);
}

 //<>//
PVector skalarMultiplication(float skalar, PVector a){
  return new PVector( skalar * a.x, skalar * a.y);
}
