class Powerup{
  boolean pickedUp;
  float inWorldPosX,inWorldPosY;
  final int r = 10;

  
  Type type;
  
  Powerup(float inWorldPosX,float inWorldPosY,Type t){
    this.inWorldPosX = inWorldPosX+blockWidth/2;
    this.inWorldPosY = inWorldPosY+blockHeight/2;
    pickedUp = false;
    this.type = t;
  }
  
  
  void update(Paddle p,float paddleX,float paddleY){
    inWorldPosY += 1;

    if ((inWorldPosX + r > paddleX && inWorldPosX + r < paddleX + p.paddleWidth
        || inWorldPosX - r > paddleX && inWorldPosX - r < paddleX + p.paddleWidth)
        && inWorldPosY + r > paddleY) {
        pickedUp = true;
        // PADDLE_WIDTH,BALL_SPEED,BALL_SIZE,BALL_COUNT
        switch(type){
          case PADDLE_WIDTH:
            p.paddleWidth += 10;
            break;
          case BALL_SPEED:
            for(Ball b : balls){
              b.speed += 1;
            }
            break;
          case BALL_SIZE:
            for(Ball b : balls){
              b.r += 1;
            }
            break;
          case BALL_COUNT:
            balls.add(new Ball(paddleX+p.paddleWidth/2, 500, 10, 5, 0.5f, 1.3f));
            break;
        }
     }
  }
  
  void draw(){
    if(!pickedUp){
      fill(203,91,240);
      ellipse(inWorldPosX,inWorldPosY,r,r);
    }
  }
}
