class Ball {
  //Position des Balls
  float xPos, yPos;
  //Position des Balls im vorherigen Frame
  float xPosOld, yPosOld;
  //Vertikale und horizontale Geschwindigkeit des Balls
  float vx, vy;
  //Durchmesser, Radius des Balls
  float d, r;
  float speed = 3;
  
  Ball(float xPos,float yPos,float d,float r,float vx,float vy) {
    this.xPos = xPos;
    this.yPos = yPos;
    this.d = d;
    this.r = r;
    this.vx = vx;
    this.vy = vy;
    xPosOld = xPos;
    yPosOld= yPos;
  }

  void draw(){
    fill(152, 255, 0);
    circle(xPos, yPos, r);
  }
}
