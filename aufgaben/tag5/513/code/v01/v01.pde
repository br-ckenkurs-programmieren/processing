PVector start, mousePos;
PVector gravity;

Box[] boxes = new Box[10];

Bird bird;

void reset() {
  start = new PVector(200, height - 200);
  bird = new Bird(start);
  gravity = new PVector(0, 0);
}

void resetBoxes() {
  boxes[0] = new Box(1100, 700, 40, 40);
  boxes[1] = new Box(1100, 620, 40, 40);
  boxes[2] = new Box(1100, 540, 40, 40);
  boxes[3] = new Box(1100, 460, 40, 40);
  boxes[4] = new Box(1020, 700, 40, 40);
  boxes[5] = new Box(1020, 620, 40, 40);
  boxes[6] = new Box(1020, 540, 40, 40);
  boxes[7] = new Box(1020, 460, 40, 40);
  boxes[8] = new Box(1060, 380, 40, 40);
  boxes[9] = new Box(1060, 300, 40, 40);
}

void setup() {
  size(1200, 800);
  noStroke();
  reset();
  resetBoxes();
  mousePos = start.copy();
}

void setBackground() {
  background(0, 200, 255);
  fill(0, 255, 0);
  rect(0, height - 30, width, 30);
}

void draw() {
  setBackground();
  mousePos.set(mouseX, mouseY);

  if (mousePressed) {
    reset();
    stroke(255);
    line(mouseX, mouseY, start.x, start.y);
  }
  
  if (bird.pos.x > width || bird.pos.x < 0 || bird.pos.y > height - 30) reset();
  
  bird.update();
  bird.render();
  
  for (Box b : boxes) {
    if (b.show) {
      b.render();
      if(b.checkCollision(bird)) {
        b.show = false;
        reset();
      }
    }
  }
}

void mouseReleased() {
  bird.v.add(new PVector(start.x - mouseX, start.y - mouseY).mult(0.08));
  gravity.set(0, 0.2);
}

class Bird {
  PVector pos, v;
  float r;
  
  Bird(PVector pos) {
    this.pos = pos.copy();
    this.v = new PVector(0, 0);
    this.r = 10;
  }
  
  void update() {
    pos.add(v);
    v.add(gravity);
  }
  
  void render() {
    fill(255, 255, 0);
    noStroke();
    circle(pos.x, pos.y, 2 * r);
  }
}

class Box {
  PVector a, b, c, d;
  float x, y, w, h;
  
  boolean show;
  
  Box(float x, float y, float w, float h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.a = new PVector(x, y);
    this.b = new PVector(x + w, y);
    this.c = new PVector(x + w, y + h);
    this.d = new PVector(x, y + h);
    this.show = true;
  }
  
  Boolean checkCollision(Bird bird) {
    if (a.dist(bird.pos) < bird.r) return true;
    if (b.dist(bird.pos) < bird.r) return true;
    if (c.dist(bird.pos) < bird.r) return true;
    if (d.dist(bird.pos) < bird.r) return true;
    
    float birdX = bird.pos.x;
    float birdY = bird.pos.y;
    
    if (birdX > x && birdX < x + w && birdY > y - bird.r && birdY < y + h + bird.r) return true;
    if (birdX > x - bird.r && birdX < x + w + bird.r && birdY > y && birdY < y + h) return true;
    
    return false;
  }
  
  void render() {
    fill(127, 51, 0);
    rect(x, y, w, h);
  }
}