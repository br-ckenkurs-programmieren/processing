let fieldWidth;
let fieldHeight;
let bombCount;
let b;
let gameOver;
let isWon;

function init(){
  fieldWidth = 50;
  fieldHeight = 50;
  bombCount = 20;
  gameOver = false;
  isWon = false;
}

function setup() {
    createCanvas(800, 800);
  init();
    b = [];
    for (let i = 0; i < 16; i++) {
        b[i] = [];
        for (let j = 0; j < 16; j++) {
            b[i][j] = new Field(i, j);
        }
    }
    generateBombs(bombCount);
  
    getBombCount();
}

function draw() {
    background(200);
    for (let i = 0; i < 16; i++) {
        fill(0);
        line(fieldWidth * i, 0, fieldHeight * i, height);
        line(0, fieldHeight * i, width, fieldHeight * i);
    }
    show();
}

function mouseReleased() {
    if (!b[floor(mouseX / 50.0)][floor(mouseY / 50.0)].isMarked() && ! gameOver && mouseButton == LEFT) {
        b[floor(mouseX / 50.0)][floor(mouseY / 50.0)].reveal();
    }
    if (!b[floor(mouseX / 50.0)][floor(mouseY / 50.0)].isRevealed() && !gameOver && mouseButton == RIGHT) {
        b[floor(mouseX / 50.0)][floor(mouseY / 50.0)].mark();
    } 
}



function generateBombs(num) {
    for (let i = 0; i < num; i++) {
        let ranX = floor(random(16));
        let ranY = floor(random(16));
        if (b[ranX] && b[ranX][ranY] && !b[ranX][ranY].isBomb()) {
            b[ranX][ranY].makeBomb();
        } else {
            i--;
        }
    }
}

function getBombCount() {
    for (let i = 0; i < b.length; i++) {
        for (let j = 0; j < b[i].length; j++) {
            let counter = 0;
            for (let offsetX = -1; offsetX < 2; offsetX++) {
                for (let offsetY = -1; offsetY < 2; offsetY++) {
                    if (offsetX == 0 && offsetY == 0) continue;
                    if (i + offsetX >= 0 && i + offsetX < b.length && j + offsetY >= 0 && j + offsetY < b.length && b[i + offsetX][j + offsetY].isBomb()) { counter++ }
                }
            }
            b[i][j].setSurroundingBombs(counter);
        }
    }
}

function isGameOver() {
    if (gameOver) return;
    for (let i = 0; i < b.length; i++) {
        for (let j = 0; j < b[i].length; j++) {
            if (b[i][j] && !b[i][j].isRevealed() && !b[i][j].isBomb()) {
                gameOver = false;
                return;
            }
        }
    }
    gameOver = true;
    isWon = true;
}

function show() {
    isGameOver();
    for (let i = 0; i < b.length; i++) {
        for (let j = 0; j < b[0].length; j++) {
            b[i][j].show();
        }
    }
    if (gameOver) {
        if (isWon) {
            fill(0, 255, 0);
            textSize(50);
            textAlign(CENTER);
            text("gewonnen", 400, 400);
        } else {
            fill(255, 0, 0);
            textSize(50);
            textAlign(CENTER);
            text("verloren", 400, 400);
        }
    }
}



class Field {

    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.bomb = false;
        this.revealed = false;
        this.marked = false;
        this.surroundingBombs = 0;
    }

    isMarked(){
      return this.marked;
    }
  
    isRevealed() {
        return this.revealed;
    }

    isBomb() {
        return this.bomb;
    }

    makeBomb() {
        this.bomb = true;
    }

    mark() {
        this.marked = true;
    }

    setSurroundingBombs(i) {
        this.surroundingBombs = i;
    }

    reveal() {
        if (this.revealed) return;
        if (this.bomb) {
            gameOver = true;
            isWon = false;
        }
        this.revealed = true;
        if (this.surroundingBombs == 0 && !this.bomb) {
            for (let offsetX = -1; offsetX < 2; offsetX++) {
                for (let offsetY = -1; offsetY < 2; offsetY++) {
                    if (offsetX == 0 && offsetY == 0) continue;
                    if (this.x + offsetX >= 0 && this.x + offsetX < 16 && this.y + offsetY >= 0 && this.y + offsetY < 16) {
                        b[this.x + offsetX][this.y + offsetY].reveal();
                    }
                }
            }
        }
    }

    show() {
        if (this.revealed || b.gameOver) {
            fill(255);
            rect(this.x * 50, this.y * 50, 50, 50);
            if (this.bomb) {
                fill(255, 0, 0);
                circle(this.x * 50 + 25, this.y * 50 + 25, 40);
            } else if (this.surroundingBombs > 0) {
                fill(0);
                textSize(20);
                textAlign(CENTER);
                text(this.surroundingBombs, this.x * 50 + 25, this.y * 50 + 25);
            }
        } else {
            if (this.marked) {
                fill(0, 0, 255);
                rect(this.x * 50 + 10, this.y * 50 + 10, 30, 30);
            }
        }
    }
}





