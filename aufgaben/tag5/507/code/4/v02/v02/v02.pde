// Spielfeldparameter
int fieldWidth  = 16;
int fieldHeight = 16;
int bombCount   = 40;

// Felderparameter
int   fieldSize          = 50;
color revealedFieldColor = color(255);
color hiddenFieldColor   = color(200, 200, 200);
color bombColor          = color(255, 0, 0);

// Das Spielfeld (bestehend aus seinen Feldern)
Field[][] fields = new Field[fieldHeight][fieldWidth];


void setup() {
  size(800, 800);

  // Initialisieren aller Felder
  for (int y = 0; y < fieldHeight; y++) {
    for (int x = 0; x < fieldWidth; x++) {
      fields[y][x] = new Field();
    }
  }

  // Generieren der Bomben
  generateBombs();

  // Berechnen der umliegenden Bomben eines Feldes
  for (int y = 0; y < fieldHeight; y++) {
    for (int x = 0; x < fieldWidth; x++) {
      fields[y][x].surroundingBombs = getSurroundingBombCount(x, y);
    }
  }
}


void draw() {
  // Durchlaufen und zeichnen aller Felder des zweidimensionalen Arrays
  for (int y = 0; y < fieldHeight; y++) {
    for (int x = 0; x < fieldWidth; x++) {
      fields[y][x].drawField(x * fieldSize, y * fieldSize);
    }
  }
}


// Generiert die oben festgelegte Anzahl an Bomben im Spielfeld
void generateBombs() {
  int bombsGenerated = 0;
  int fieldCount     = fieldWidth * fieldHeight;

  // In der Schleife werden solange Bomben generiert bis die angegebene Anzahl erreicht ist
  while (bombsGenerated < bombCount) {
    int   chosenFieldNo = int(random(fieldCount));
    Field chosenField   = fields[chosenFieldNo / fieldWidth][chosenFieldNo % fieldWidth];

    // Wenn auf dem Feld noch keine Bombe ist wird das geändert und bombsGenerated erhöht
    if (!chosenField.isBomb) {
      chosenField.isBomb = true;
      bombsGenerated++;
    }
  }
}


// Berechnet die Anzahl der umliegenden Bomben des Feldes an der angegebenen Position
int getSurroundingBombCount(int x, int y) {
  int surroundingBombCount = 0;

  // Durchlaufen aller angrenzenden Felder
  for (int xOffset = -1; xOffset <= 1; xOffset++) {
    for (int yOffset = -1; yOffset <= 1; yOffset++) {
      int currentX = x + xOffset;
      int currentY = y + yOffset;

      // Überspringen des eigenen Feldes
      if (xOffset == 0 && yOffset == 0) {
        continue;
      }

      // Überprüfung, ob das Feld existiert (Index in bounds) und ob das Feld eine Bombe ist
      if (currentX >= 0 && currentX < fieldWidth &&
          currentY >= 0 && currentY < fieldHeight &&
          fields[currentY][currentX].isBomb) {
        surroundingBombCount++;
      }
    }
  }

  return surroundingBombCount;
}


void mousePressed() {
  int x = mouseX / fieldSize;
  int y = mouseY / fieldSize;
  
  if (mouseButton == LEFT) {
    fields[y][x].reveal();
  }
}


class Field {

  // Gibt an, ob das Feld eine Bombe ist
  boolean isBomb;

  // Gibt an, ob das Feld aufgedeckt ist
  boolean isRevealed;

  // Gibt an, wie viele Bomben sich auf den angrenzenden Feldern befinden
  int surroundingBombs;
  
  // Deckt das Feld auf
  void reveal() {
    this.isRevealed = true;
  }

  // Zeichnet das Feld an der gegebenen Position (x und y in px)
  void drawField(int x, int y) {
    // Abhängig von der Art des Feldes wird hier die Farbe der Darstellung festgelegt
    if (!this.isRevealed) { // Das Feld ist nicht aufgedeckt
      fill(hiddenFieldColor);
    } else if (this.isBomb) { // Das Feld ist aufgedeckt und eine Bombe
      fill(bombColor);
    } else { // Das Feld ist aufgedeckt und keine Bombe
      fill(revealedFieldColor);
    }

    square(x, y, fieldSize);

    // Darstellung der Anzahl angrenzender Bomben wenn das Feld aufgedeckt ist
    if (this.isRevealed && !this.isBomb && this.surroundingBombs > 0) {
      fill(0);
      textSize(20);
      textAlign(CENTER, CENTER);
      text(this.surroundingBombs, x + fieldSize / 2, y + fieldSize / 2);
    }
  }
}
