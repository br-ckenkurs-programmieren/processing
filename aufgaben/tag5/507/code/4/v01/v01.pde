int fieldWidth = 50;
int fieldHeight = 50;
int bombCount = 20;
Board b;

void setup() {
  size(800, 800);
  b = new Board();
  b.generateBombs(bombCount);
  b.getBombCount();
}

void draw() {
  background(200);
  for (int i = 0; i < 16; i++) {
    fill(0);
    line(fieldWidth * i, 0, fieldHeight * i, height);
    line(0, fieldHeight * i, width, fieldHeight * i);
  }
  b.show();
}

void mouseReleased() {
  if (mouseButton == LEFT) {
    b.board[mouseX / 50][mouseY / 50].reveal();
  }
}

class Board {
  Field[][] board = new Field[16][16];

  Board() {
    for (int i = 0; i < board.length; i++) {
      for (int j = 0; j < board[0].length; j++) {
        board[i][j] = new Field(i, j);
      }
    }
  }

  void show() {
    for (int i = 0; i < board.length; i++) {
      for (int j = 0; j < board[0].length; j++) {
        board[i][j].show();
      }
    }
  }

  void generateBombs(int num) {
    for (int i = 0; i < num; i++) {
      int ranX = int(random(16));
      int ranY = int(random(16));
      if (!board[ranX][ranY].isBomb) {
        board[ranX][ranY].isBomb = true;
      } else {
        i--;
      }
    }
  }

  void getBombCount() {
    for (int i = 0; i < board.length; i++) {
      for (int j = 0; j < board[0].length; j++) {
        int counter = 0;
        for (int offsetX = -1; offsetX < 2; offsetX++) {
          for (int offsetY = -1; offsetY < 2; offsetY++) {
            if (offsetX == 0 && offsetY == 0) continue;
            if (i + offsetX >= 0 && i + offsetX < board.length && j + offsetY >= 0 && j + offsetY < board.length && board[i + offsetX][j + offsetY].isBomb) counter++;
          }
        }
        board[i][j].surroundingBombs = counter;
      }
    }
  }
}


class Field {
  int x, y, surroundingBombs;
  boolean isBomb, isRevealed;

  Field(int x, int y) {
    this.x = x;
    this.y = y;
  }

  void reveal() {
    if (this.isRevealed) return;
    isRevealed = true;
  }

  void show() {
    if (isRevealed) {
      fill(255);
      rect(x * 50, y * 50, 50, 50);
      if (this.isBomb) {
        fill(255, 0, 0);
        circle(this.x * 50 + 25, this.y * 50 + 25, 40);
      } else if (this.surroundingBombs > 0) {
        fill(0);
        textSize(20);
        textAlign(CENTER);
        text(surroundingBombs, x * 50 + 25, y * 50 + 25);
      }
    }
  }
}
