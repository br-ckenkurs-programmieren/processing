// Spielfeldparameter
int fieldWidth  = 16;
int fieldHeight = 16;

// Felderparameter
int   fieldSize          = 50;
color revealedFieldColor = color(255);

// Das Spielfeld (bestehend aus seinen Feldern)
Field[][] fields = new Field[fieldHeight][fieldWidth];


void setup() {
  size(800, 800);

  // Initialisieren aller Felder
  for (int y = 0; y < fieldHeight; y++) {
    for (int x = 0; x < fieldWidth; x++) {
      fields[y][x] = new Field();
    }
  }
}


void draw() {
  // Durchlaufen und zeichnen aller Felder des zweidimensionalen Arrays
  for (int y = 0; y < fieldHeight; y++) {
    for (int x = 0; x < fieldWidth; x++) {
      fields[y][x].drawField(x * fieldSize, y * fieldSize);
    }
  }
}


class Field {

  // Gibt an, ob das Feld eine Bombe ist
  boolean isBomb;

  // Zeichnet das Feld an der gegebenen Position (x und y in px)
  void drawField(int x, int y) {
    fill(revealedFieldColor);
    square(x, y, fieldSize);
  }
}
