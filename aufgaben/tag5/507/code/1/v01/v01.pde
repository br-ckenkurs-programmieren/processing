int fieldWidth = 50;
int fieldHeight = 50;
Board b;

void setup() {
  size(800, 800);
  b = new Board();
}

void draw() {
  background(200);
  for (int i = 0; i < 16; i++) {
    fill(0);
    line(fieldWidth * i, 0, fieldHeight * i, height);
    line(0, fieldHeight * i, width, fieldHeight * i);
  }
}

class Board {
  Field[][] board = new Field[16][16];

  Board() {
    for (int i = 0; i < board.length; i++) {
      for (int j = 0; j < board[0].length; j++) {
        board[i][j] = new Field(i, j);
      }
    }
  }
}


class Field {
  int x, y;

  Field(int x, int y) {
    this.x = x;
    this.y = y;
  }
}
