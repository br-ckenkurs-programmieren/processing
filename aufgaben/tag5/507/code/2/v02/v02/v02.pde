// Spielfeldparameter
int fieldWidth  = 16;
int fieldHeight = 16;
int bombCount   = 40;

// Felderparameter
int   fieldSize          = 50;
color revealedFieldColor = color(255);
color bombColor          = color(255, 0, 0);

// Das Spielfeld (bestehend aus seinen Feldern)
Field[][] fields = new Field[fieldHeight][fieldWidth];


void setup() {
  size(800, 800);

  // Initialisieren aller Felder
  for (int y = 0; y < fieldHeight; y++) {
    for (int x = 0; x < fieldWidth; x++) {
      fields[y][x] = new Field();
    }
  }

  // Generieren der Bomben
  generateBombs();
}


void draw() {
  // Durchlaufen und zeichnen aller Felder des zweidimensionalen Arrays
  for (int y = 0; y < fieldHeight; y++) {
    for (int x = 0; x < fieldWidth; x++) {
      fields[y][x].drawField(x * fieldSize, y * fieldSize);
    }
  }
}


// Generiert die oben festgelegte Anzahl an Bomben im Spielfeld
void generateBombs() {
  int bombsGenerated = 0;
  int fieldCount     = fieldWidth * fieldHeight;

  // In der Schleife werden solange Bomben generiert bis die angegebene Anzahl erreicht ist
  while (bombsGenerated < bombCount) {
    int   chosenFieldNo = int(random(fieldCount));
    Field chosenField   = fields[chosenFieldNo / fieldWidth][chosenFieldNo % fieldWidth];

    // Wenn auf dem Feld noch keine Bombe ist wird das geändert und bombsGenerated erhöht
    if (!chosenField.isBomb) {
      chosenField.isBomb = true;
      bombsGenerated++;
    }
  }
}


class Field {

  // Gibt an, ob das Feld eine Bombe ist
  boolean isBomb;

  // Zeichnet das Feld an der gegebenen Position (x und y in px)
  void drawField(int x, int y) {
    // Abhängig von der Art des Feldes wird hier die Farbe der Darstellung festgelegt
    if (this.isBomb) {
      fill(bombColor);
    } else {
      fill(revealedFieldColor);
    }

    square(x, y, fieldSize);
  }
}
