int fieldWidth = 50;
int fieldHeight = 50;
int bombCount = 20;
Board b;

void setup() {
  size(800, 800);
  b = new Board();
  b.generateBombs(bombCount);
}

void draw() {
  background(200);
  for (int i = 0; i < 16; i++) {
    fill(0);
    line(fieldWidth * i, 0, fieldHeight * i, height);
    line(0, fieldHeight * i, width, fieldHeight * i);
  }
  b.show();
}

class Board {
  Field[][] board = new Field[16][16];

  Board() {
    for (int i = 0; i < board.length; i++) {
      for (int j = 0; j < board[0].length; j++) {
        board[i][j] = new Field(i, j);
      }
    }
  }

  void show() {
    for (int i = 0; i < board.length; i++) {
      for (int j = 0; j < board[0].length; j++) {
        board[i][j].show();
      }
    }
  }

  void generateBombs(int num) {
    for (int i = 0; i < num; i++) {
      int ranX = int(random(16));
      int ranY = int(random(16));
      if (!board[ranX][ranY].isBomb) {
        board[ranX][ranY].isBomb = true;
      } else {
        i--;
      }
    }
  }
}


class Field {
  int x, y;
  boolean isBomb;

  Field(int x, int y) {
    this.x = x;
    this.y = y;
  }

  void show() {
    fill(255);
    rect(x * 50, y * 50, 50, 50);
    if (this.isBomb) {
      fill(255, 0, 0);
      circle(this.x * 50 + 25, this.y * 50 + 25, 40);
    }
  }
}
