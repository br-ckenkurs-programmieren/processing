class Explosion {
  constructor(x, y) {
    this.lifetime = explosionLifetime;

    this.particles = new Array(particleCount)
      .fill() // first fill with undefined entries
      .map((_) => new Particle(x, y)); // then replace with instances
  }

  update() {
    this.particles.forEach((p) => p.update(this.lifetime));
    this.lifetime--;
  }
}

class Particle {
  constructor(xstart, ystart) {
    this.pos = createVector(xstart, ystart);
    this.velocity = random(particleMinVel, particleMaxVel);
    this.direction = random(0, 2 * PI);
    this.move = p5.Vector.fromAngle(this.direction)
      .setMag(this.velocity)
      .add([0, -3, 0]);
  }

  update(lifetime) {
    this.move.add(vGravity);
    this.pos.add(this.move);
    fill(color(random(255), random(255), random(255), lifetime));
    circle(this.pos.x, this.pos.y, particleSize);
  }
}

class Rocket {
  constructor(x) {
    this.ex = null;
    this.exploded = false; // Rakete ist noch nicht explodiert
    this.expired = false; // Explosion ist noch nicht abgeschlossen
    this.yv = random(rocketMinVel, rocketMaxVel); // Geschwindigkeit wird zufällig gesetzt
    this.xPos = x; // X Position wird übergeben
    this.yPos = height; // Raketen starten immer am unteren Fensterrand
    this.color = color(random(255), random(255), random(255));
    this.explosionHeight = int(random(height * 0.1, height * 0.4)); //Explosionshoehe wird zufaellig gewaehlt
  }

  drawRocket() {
    fill(this.color);
    circle(this.xPos, this.yPos, 5); // Raketenkopf
    fill(color(255, 255, 0));
    circle(this.xPos, this.yPos + 5, 4); // Raketenschweif
    circle(this.xPos, this.yPos + 4, 3); // Raketenschweif
    circle(this.xPos, this.yPos + 3, 2); // Raketenschweif
    circle(this.xPos, this.yPos + 2, 1); // Raketenschweif
  }

  update() {
    if (this.exploded) {
      if (this.ex.lifetime <= 0) this.expired = true;
      else this.ex.update();
    } else if (this.yPos <= this.explosionHeight) {
      this.ex = new Explosion(this.xPos, this.yPos);
      this.exploded = true;
    } else {
      this.yPos -= this.yv;
      this.drawRocket();
    }
  }
}

const gravity = 0.03;
const particleSize = 3;
const particleMinVel = 0.05;
const particleMaxVel = 1;
const rocketMinVel = 3;
const rocketMaxVel = 10;
const margin = 10;
const rocketCount = 10;
const particleCount = 100;
const explosionLifetime = 255;
let vGravity;
let rockets; // Array für Raketen

function setup() {
  frameRate(60); // Framerate
  createCanvas(1200, 1000); // Fenstergröße
  background(0); // Hintergrund

  vGravity = createVector(0, gravity);

  rockets = new Array(rocketCount)
    .fill() // first fill with undefined entries
    .map((_) => new Rocket(int(random(margin, width - margin)))); // then replace with instances
}

function draw() {
  background(0); // Hintergrund
  rockets.forEach((r) => r.update());
  rockets = rockets.map((r) =>
    r.expired ? new Rocket(int(random(margin, width - margin))) : r
  );
}
