// Diese zweite Version ist erstmal auf standby, da v01 sehr schoen aussieht
int numberOfRockets = 6;
Rocket[] rockets = new Rocket[numberOfRockets];
Explosion[] explosions = new Explosion[8 * numberOfRockets];

void setup(){
  size(1000, 800);
  frameRate(120);
  for (int i = 0; i < rockets.length; i++){
    rockets[i] = new Rocket();
  }
  for (int i = 0; i < explosions.length; i++){
    explosions[i] = new Explosion(0, 0, 0);
  }
}

void draw(){
  background(43, 44, 124); //Farbcode für Marineblau. Jede andere Farbe ist natürlich auch möglich.
  for (int i = 0; i < rockets.length; i++){
    rockets[i].move();
    rockets[i].show();
    rockets[i].explode();
  }
  for (int i = 0; i < explosions.length; i++){
    explosions[i].showExplosion();
  }
}

//Raketendesign
class Rocket{
  color roof;
  color body;
  float xPositionTip;
  float yPositionTip;
  float speed;
  float maxHeight;
  
  Rocket(){
  this.xPositionTip = random(5, 955);
  this.yPositionTip = random(550, 800);
  this.speed = random(2, 10);
  this.maxHeight = random(50, 400);
  this.roof = color(random(255), random(255), random(255));
  this.body = color(random(255), random(255), random(255));
  }
  
  void move(){
    this.yPositionTip = this.yPositionTip - speed;
  }
  
  void show(){
    fill(this.roof);
    triangle(this.xPositionTip, this.yPositionTip, this.xPositionTip - 10, this.yPositionTip + 20, this.xPositionTip + 10, this.yPositionTip + 20);
    fill(this.body);
    rect(this.xPositionTip - 7, this.yPositionTip + 20, 14, 20);
}
  
  void explode(){
    if (this.yPositionTip <= this.maxHeight){ //Der größer-gleich-Vergleich ist hier wichtig; wir können nicht davon ausgehen, dass unsere Racketen ihr maxHeight genau treffen, solange speed und Position zufällig sind
    startExplosion(this.xPositionTip, this.yPositionTip);
    
    //reset Rocket
    this.xPositionTip = random(5, 955);
    this.yPositionTip = random(550, 800);
    this.speed = random(2, 10);
    this.maxHeight = random(50, 400);
    this.roof = color(random(255), random(255), random(255));
    this.body = color(random(255), random(255), random(255));
    }
  }
  
}

void startExplosion(float x, float y){
  boolean saved = false;
  /*Finde den ersten freien Platz im Array 
 * als freier Platz gilt hier: timer == 0, 
 * dh. Zeit zur Explosion ist abgelaufen.
 * Fülle den betreffenden Platz im Array
 * durch eine neue Explosion an der gegebenen
 * Position.
 */
for (int i = 0; i < explosions.length && !saved; i++){
  if (explosions[i].timer <= 0){
  explosions[i] = new Explosion(x, y);
  saved = true;
  }
  }
}

class Explosion{
  float xPosition;
  float yPosition;
  Particle[] particles; 
  float timer = 880; 
  
  Explosion(float x, float y, float c){
   this.xPosition = x;
    this.yPosition = y;
    this.timer = c;
  }
  
  Explosion(float x, float y){
    this.xPosition = x;
    this.yPosition = y;
    this.particles = new Particle[int(random(5, 10))];
    for(int i = 0; i < this.particles.length; i++){
      this.particles[i] = new Particle(x, y);
    }
  }
  
 
  
  void showExplosion(){
    /*
    * // Leichtere Option: Darstellung der Explosionen als erst wachsende,
    * // dann schrumpfende Kreise. Erspart das Programmieren der Partikel.
    * // Erfordert das Entfernen des particles Array und Stellen des Timers
    * // auf 180
    * if (this.timer > 0){
    *   if (this.timer < 90){
    *      // Wachsender Kreis für die erste Hälfte der Explosionsdauer
    *      circle(this.xPosition, this.yPosition, this.timer);
    *   } else {
    *      // Schrumpfender Kreis für die zweite Hälfte der Explosionsdauer
    *     circle(this.xPosition, this.yPosition, 180 - this.timer);
    *   }
    *  }
    * }
    * this.timer--;
    */
    
    if (this.timer > 0){
      for(int i = 0; i < this.particles.length; i++){
        Particle par = this.particles[i]; //Kürzere Variable für Übersichtlichkeit
        fill(par.parColor);
        circle(par.xPosition + par.sign * par.xSpeed * par.parableX, par.yPosition + (0.5 *par.parableX * par.parableX) + (-20*par.parableX), 10);
        par.parableX = par.parableX + par.ySpeed;
      }
      this.timer--;
    }
  }
}

class Particle{
  float xPosition;
  float yPosition;
  int xSpeed;
  int ySpeed;
  int parableX;
  int sign;
  color parColor;
  
  Particle(float x, float y){
   this.xPosition = x;
   this.yPosition = y;
   this.parableX = 0;
   this.ySpeed = int(random(1, 8));
   this.xSpeed = int(random(1, 5));
   this.parColor = color(random(255), random(255), random(255)); //Zufällige Farbe des Partikels
   
   if (random(0, 2) >= 1){
     this.sign = -1;
   } else {
    this.sign = 1; 
   }
  }
}
