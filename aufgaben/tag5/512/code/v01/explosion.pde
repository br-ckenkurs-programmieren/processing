class Explosion {  
  int particle_count = 800;                         //Anzahl der Partikel ausgehend von der Explosion
  int tick;                                         //Variable um Frames seit Explosion zu zaehlen(benutzt für Partikelposition) 
  int deadCounter;                                  // Counter der die Partikel zaehlt, welche aus dem Fenster gefallen sind
  
  Particle[] particles;                             //Array das die Partikel enthaelt

  Explosion(int x, int y) {                         // Konstruktor
    tick = 0;                                       // Tick wird zurueck gesetzt
    particles =  new Particle[particle_count];      //Initialisierung des Partikelarrays 
    for (int i = 0; i < particles.length; i++) {    //Füllt jede Stelle des Arrays   
      particles[i] = new Particle(x, y);            // Jedes Partikel startet am Explosionspunkt
    }
  }

  boolean update() {                                // Updated alle Partikel im Array /liefert boolean zum check ob explosion abgeschlossen ist
    deadCounter = particle_count;                   // setzt Deadcounter auf die Anzahl der Partikel
    for (Particle p : particles) {                  // Alle Partikel werden durchlaufen
      if (p.yPos < height ) {                       // Ist das Partikel unterhalb des Fensterrahmen?
        p.update(tick);                             // ruft update() des Partikels auf und übergibt die tick variable
        deadCounter --;                             //fuer jedes Partikel, das noch im Fenster ist, wird deadcounter verringert
      }
    }
    tick++;                                        //tick erhöht sich nachdem jedes Parikel updated wurde
    if (deadCounter >= particle_count * 0.9 || tick >= 53) {     //ist ein Grossteil der Partikel aus dem Bild gefallen? ODER
                                                                 //ist die Explosion aelter, als 52 Frames? 
      return true;
    } else {
      return false;
    }
  }
}
