int margin = 10;                      // Abstand für Raketen zum Bildrand //<>//
int rocketCount = 10;                 // Raketenanzahl
float minVelocity = 5;                // Minimale Geschwindigkeit der Partikel
float maxVelocity = 150;			  // Maximale 
float gravity = - 220;               // Schwerkraft

Rocket[] rockets;                     // Array für Raketen

void setup() {
  frameRate(60);                                 // Framerate
  size(1200,1000);                               // Fenstergröße
  background(0);                                 // Hintergrund
  rockets = new Rocket[rocketCount];             // Initialisierung des Raketen-Arrays
  for (int i = 0; i< rockets.length; i++) {      // für jede Stelle im Raketen-Array wird eine neue Rakete erstellt
    rockets[i] = new Rocket(int(random(margin, width-margin)));  //neue Rakete erhält eine zufaellige x-Position
  }
}

void draw() { 
  background(0);                        // Hintergrund
  for (Rocket rock : rockets) {         // update() jede Rakete im Array wird aufgerufen
    rock.update();
  }
}
