class Rocket {
  Explosion ex;                          // jede Rakete hat eine Explosion
  float   yv;                            // Geschwindigkeit
  int     xPos;                          // X Position
  int     yPos;                          // Y Position
  int     explosionHeight;               // Hoehe an der die Rakete explodieren soll
  boolean exploded;                      // zeigt an ob Rakete explodiert ist
  boolean expired;                       // zeigt an ob Explosion abgeschlossen ist

  Rocket(int x) {
    this.exploded = false;                         // Rakete ist noch nicht explodiert
    this.expired = false;                          // Explosion ist noch nicht abgeschlossen
    this.yv = random(minVelocity, maxVelocity);    // Geschwindigkeit wird zufällig gesetzt
    this.xPos = x;                                 // X Position wird übergeben
    this.yPos = height;                            // Raketen starten immer am unteren Fensterrand
    this.explosionHeight = int(random(height*0.1, height*0.4));  //Explosionshoehe wird zufaellig gewaehlt
  }
  void update() {
    if (!expired) {                                  //ist die Explosion abgeschlossen?
      if (exploded) {                                      //ist die Rakete explodiert?
        expired = ex.update();                                  //updated Explosion und speichert, ob diese abgeschlossen ist
      } else {                                             
        if (this.yPos <= explosionHeight) {                      //hat die Rakete explosionshoehe erreicht?
          ex = new Explosion(xPos, yPos);                            // an Raketenposition wird neue Explosion erschaffen
          exploded= true;                                            // boolean fuer explodiert wird auf true gesetzt
        } else {                                                 
          this.yPos -= yv;                                            //Rakete bewegt sich nach oben mit Geschwindigkeit
          drawRocket();                                               //zeichnet Rakete
        }
      }
    } else {                                         
      resetRocket();                                       //setzt Rakete zurueck
    }
  }
  void resetRocket() {                                         //setzt Rakete zurueck (Sieht fuer Beobachter aus, als ob eine neue Rackete entstanden ist)
    this.exploded = false;                                      //Rakete ist noch nicht explodiert
    this.expired = false;                                      //Explosion ist noch nicht abgeschlossen
    this.yv = random(minVelocity, maxVelocity);                //Geschwindigkeit wird zufällig gesetzt
    this.xPos = int(random(10, width-10));                     // X Position wird zufaellig neu gesetzt
    this.yPos = height;                                        // Raketen starten wieder am unteren Fensterrand
    this.explosionHeight = int(random(height*0.1, height*0.4));//Explosionshoehe wird zufaellig gewaehlt
  }

  void drawRocket() {                                        // zeichnet Rakete
    fill(color(random(255), random(255), random(255)));      // zufaellige Farbe für Raketenkopf
    circle(xPos, yPos, 5);                                   // Raketenkopf
    fill(color(255, 255, 0));                                // Raketenschweif ist Gelb
    circle(xPos, yPos+5, 4);                                 // Raketenschweif
    circle(xPos, yPos+4, 3);                                 // Raketenschweif
    circle(xPos, yPos+3, 2);                                 // Raketenschweif
    circle(xPos, yPos+2, 1);                                 // Raketenschweif
  }
}
