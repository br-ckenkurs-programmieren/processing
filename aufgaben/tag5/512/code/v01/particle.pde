class Particle {
  float ystart;                           // Y Startpunkt des Partikel 
  float xstart;                           // X Startpunkt des Partikel 
  float velocity;                         // Geschwindigkeit 
  int   particleSize = 3;                 // Partikelgroesse
  float xPos;                             // X Position 
  float yPos;                             // Y Position 
  float direction;                        // Richtung der Flugbahn
  
  Particle(int xstart, int ystart) {      // Konstruktor
    this.velocity = random(minVelocity, maxVelocity);    //Geschwindigkeit wird zufällig gesetzt
    this.xPos = xstart;                   // Partikel startet am X-Wert des Startpunkts, spaeter aendert sich dieser Wert
    this.yPos = ystart;                   // Partikel startet am Y-Wert des Startpunkts, spaeter aendert sich dieser Wert
    this.xstart = xstart;                 // X Startwert des Partikel wird gespeichert (fuer Positionsberechnung) dieser Wert wird nicht veraendert
    this.ystart = ystart;                 // Y Startwert des Partikel wird gespeichert (fuer Positionsberechnung) dieser Wert wird nicht veraendert
    this.direction = random(0, 2*PI);     // Richtung der Flugbahn wird zufällig gewählt (0 bis (2π) = 360°)
  }
  void update(int tick) {                            
    float seconds = (tick/frameRate);                           // Ergibt die Skunden seit der Explosion (ausgehend von der Annahme: es wird einmal pro Frame update aufgerufen)
    this.xPos = xstart + velocity * cos(direction) * seconds;                                 //das Zeit-Orts-Gesetz der gleichförmigen Bewegung in x-Richtung
    this.yPos = ystart + (velocity * sin(direction)) * seconds - 0.5 * gravity * sq(seconds); //das Zeit-Orts-Gesetz der gleichförmigen Bewegung in y-Richtung
    fill(color(random(255), random(255), random(255), (255-(tick*5))));                       // Partikel bekommen zufaellige Farbe, die mit jedem Tick dunkler wird
    circle(this.xPos, this.yPos, particleSize);                                               //zeichnet Partikel
  }
}
