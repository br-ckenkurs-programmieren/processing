var blocks;
var player;
var pHeight = 80;
var pWidth = 40;
var groundVel = 0;
var gravity = 1;
var pressedKeys;
var onGr = false;

function setup() {
  player = new Player(500, 200);
  blocks = [];
  pressedKeys = [];
  createCanvas(1000, 600);
  blocks.push(new Block(570, 120, 260, 50));
  blocks.push(new Block(170, 220, 260, 50));
  blocks.push(new Block(550, 320, 300, 50));
  blocks.push(new Block(150, 420, 300, 50));
  blocks.push(new Block(10, 520, width - 20, 50));
  textAlign(CENTER);
}

function draw() {
  background(50);
  blocks.forEach(function (block) {
    block.display();
  });

  // Aktualisierung Spielfigur
  player.display();
  // Darstellung Anleitung
  fill(200);
  text("A und D zum laufen", width / 2, 20);
  text("SPACE zum springen", width / 2, 40);
  text("R um Spieler zurückzusetzen", width / 2, 60);
}

// Klasse zur Verarbeitung von Key-Events
function contains(a, obj) {
  var i = a.length;
  while (i--) {
    if (a[i] === obj) {
      return true;
    }
  }
  return false;
}

function keyPressed() {
  if (!contains(pressedKeys, keyCode)) {
    pressedKeys.push(keyCode);
  }
  if (keyIsDown(82)) {
    player.min = new p5.Vector(500, 120);
    player.lastPos = player.min.copy();
  }
}

function keyReleased() {
  if (contains(pressedKeys, keyCode)) {
    pressedKeys.pop(keyCode);
  }
}

// Klasse für Blöcke
class Block {
  // Konstruktor
  constructor(a, b, c, d) {
    this.min = new p5.Vector(a, b);
    this.max = new p5.Vector(a + c, b + d);
    this.w = c;
    this.h = d;
  }

  // Darstellung Block
  display() {
    fill(100);
    rect(this.min.x, this.min.y, this.w, this.h);
  }

  // Kollisionskontrolle
  playerCollision() {
    if (
      abs(player.min.y - (this.min.y + this.h / 2)) >
        pHeight / 2 + this.h / 2 ||
      abs(player.min.x - (this.min.x + this.w / 2)) > pWidth / 2 + this.w / 2
    ) {
      return false;
    }
    return true;
  }
}

// Klasse für Spielfigur
class Player {
  // Konstruktor
  constructor(x, y) {
    this.min = new p5.Vector(x, y);
    this.max = new p5.Vector(x + pWidth, y + pHeight);
    this.lastPos = new p5.Vector(x, y);
    this.vel = new p5.Vector(0, 0);
    this.acc = new p5.Vector(0, 0);
  }

  // Aktualisierung Attribute und Darstellung Spielfigur
  // Trägheit: Objekte in Bewegung bleiben in Bewegung
  display() {
    this.vel.x = this.min.x - this.lastPos.x - groundVel;
    this.vel.y = this.min.y - this.lastPos.y;
    this.vel.x *= 0.7;
    this.vel.y *= 0.98;
    this.accelerate();
    var nextX = this.min.x + this.vel.x + this.acc.x + groundVel;
    var nextY = this.min.y + this.vel.y + this.acc.y;
    this.lastPos.x = this.min.x;
    this.lastPos.y = this.min.y;
    this.min.x = nextX;
    this.min.y = nextY;
    this.acc.x = 0;
    this.acc.y = gravity;
    this.solve();
    // Darstellung Spielfigur
    fill(220);
    rect(
      this.min.x - pWidth / 2,
      this.min.y - pHeight / 2,
      pWidth,
      pHeight,
      10,
      10,
      10,
      10
    );
  }

  // Beschleunigung Spielfigur
  accelerate() {
    // Run
    if (keyIsDown(65)) this.acc.x = this.acc.x - 2;
    if (keyIsDown(68)) this.acc.x = this.acc.x + 2;
    // Jump
    if (keyIsDown(32) && this.vel.y == 0) { //&& onGr) {
      this.acc.y = this.acc.y - 20;
      print(this.acc.y);
    }
  }

  // Pruefung, ob Spielfigur auf einem der Bloecke/Boden sitzt
  onGround() {
    onGr = false;
    blocks.forEach(function (block) {
      if (onBlock(block)) {
        onGr = true;
      }
    });
  }

  // Pruefung, ob Spielfigur auf Block sitzt
  onBlock(block) {
    var yBelowPlayer = this.min.y + pHeight / 2 + 1;
    if (
      this.min.x + pWidth / 2 - 5 < block.min.x ||
      this.min.x - pWidth / 2 + 5 > block.max.x
    ) {
      return false;
    } else if (yBelowPlayer > block.min.y && yBelowPlayer < block.max.y) {
      return true;
    } else {
      return false;
    }
  }

  // Loesung Kollision
  // bei Kollision -> Versetzen Spielfigur auf naechste freie Position
  solve() {
    blocks.forEach(function (block) {
      if (block.playerCollision()) {
        // Abstand Center Block --> Player.Min
        var cToC = ctc(block);
        // Zielkoordinate
        var fixedPos = player.min.copy();
        //
        if (
          abs(cToC.x) - (pWidth / 2 + block.w / 2) >
          abs(cToC.y) - (pHeight / 2 + block.h / 2)
        ) {
          if (cToC.x > 0) {
            fixedPos.x = block.min.x - pWidth / 2;
          } else {
            fixedPos.x = block.max.x + pWidth / 2;
          }
        } else {
          if (cToC.y > 0) {
            fixedPos.y = block.min.y - pHeight / 2;
          } else {
            fixedPos.y = block.max.y + pHeight / 2;
          }
        }
        player.min = fixedPos;
      } else {
      }
    });
  }
}

function ctc(b) {
  var xVal = b.min.x + b.w / 2 - player.min.x;
  var yVal = b.min.y + b.h / 2 - player.min.y;
  return new p5.Vector(xVal, yVal);
}
