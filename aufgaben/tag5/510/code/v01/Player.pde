// Klasse zum Erzeugen der Spielfigur

class Player {
  //Attribute
  PVector min;
  PVector max;
  PVector lastPos;
  PVector vel;
  PVector acc;
  float h = 80;
  float w = 40;
  float groundVel = 0;
  float gravity = 1;

  
  // Konstruktor
  Player(float x, float y) {
    min = new PVector(x, y);
    max = new PVector(x+width, y+height);
    lastPos = new PVector(x, y);
    vel = new PVector(0, 0);
    acc = new PVector(0, 0);
  }
  
  // Aktualisierung Attribute und Darstellung Spielfigur
  // Trägheit: Objekte in Bewegung bleiben in Bewegung
  void update() {
    vel.x = min.x - lastPos.x - groundVel;
    vel.y = min.y - lastPos.y;
    vel.x *= 0.7; 
    vel.y *= 0.98;

    accelerate();

    float nextX = min.x + vel.x + acc.x + groundVel;
    float nextY = min.y + vel.y + acc.y;

    lastPos.x = min.x;
    lastPos.y = min.y;

    min.x = nextX;
    min.y = nextY;

    acc.x = 0;
    acc.y = gravity;

    solve();

    //Darstellung Spielfigur
    fill(220);
    rect(min.x-w/2, min.y-h/2, w, h, 10, 10, 10, 10);
  }

  // Beschleunigung Spielfigur
  void accelerate() {
    //Run
    if (keyIsDown(LEFT)) acc.x -= 2;
    if (keyIsDown(RIGHT)) acc.x += 2;

    //Jump
    if (keyIsDown(UP) && vel.y==0 && onGround()) {
      acc.y -= 20;
    }
  }

  //Pruefung, ob Spielfigur auf einem der Bloecke/Boden sitzt
  boolean onGround() {
    for (Block b : blocks) {
      if (onBlock(b)) {
        return true;
      }
    }
    return false;
  }
  
  // Pruefung, ob Spielfigur auf Block sitzt
  boolean onBlock(Block b) {
    if (min.x+w/2-5 < b.min.x || min.x-w/2+5 > b.max.x) {
      return false;
    }
    float yBelowPlayer = min.y+h/2+1;
    if (yBelowPlayer > b.min.y && yBelowPlayer < b.max.y) {
      return true;
    }
    return false;
  }

  //Loesung Kollision
  //bei Kollision -> Versetzen Spielfigur auf naechste freie Position
  void solve() {
    for (Block b : blocks) {
      if (b.playerCollision()) {
        PVector cToC = centerToCenter(ply, b);
        PVector fixedPos = min.copy();
        if (abs(cToC.x)-(w/2 + b.w/2) > abs(cToC.y)-(h/2 + b.h/2)) {
          if (cToC.x>0) {
            fixedPos.x = b.min.x - (ply.w/2);
          } else {
            fixedPos.x = b.max.x + (ply.w/2);
          }
        } else {
          if (cToC.y>0) {
            fixedPos.y = b.min.y - (ply.h/2);
          } else {
            fixedPos.y = b.max.y + (ply.h/2);
          }
        }
        min = fixedPos;
      }
      if (max.y > b.min.y) {
      }
    }
  }
  
  PVector centerToCenter(Player ply, Block b) {
    float xVal = (b.min.x+b.w/2)- ply.min.x;
    float yVal = (b.min.y+b.h/2)- ply.min.y;
    return new PVector(xVal, yVal);
  }
}
