Player ply = new Player(500, 200);              // Erstellen Speilfigur mit Startposition
ArrayList<Block> blocks = new ArrayList<Block>(); 

void setup() {
  size(1000, 600);
  blocks.add(new Block(570, 100, 260, 50));
  blocks.add(new Block(170, 200, 260, 50));
  blocks.add(new Block(550, 300, 300, 50));
  blocks.add(new Block(150, 400, 300, 50));
  blocks.add(new Block(10, 500, width-20, 50));   //Boden
  textAlign(CENTER);
}

void draw() {
  background(50);
  // Zeichnen der Bloecke 
  for (Block b : blocks) {
    b.show();
}

  //Aktualisierung Spielfigur
  ply.update();

  //Darstellung Anleitung
  text("Pfeiltasten zum laufen und springen", width/2, 20);
  text("R um Spieler zurückzusetzen", width/2, 40);
}
