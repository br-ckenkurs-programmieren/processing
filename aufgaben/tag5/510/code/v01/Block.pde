// Klasse zum Erzeugen von Bloecken

class Block {
  // Attribute 
  PVector min;  //Minimum
  PVector max;  //Maximum
  float w;  //Breite
  float h;  //Hoehe
  
  // Konstruktor
  Block(float x, float y, float w, float h) {
    min = new PVector(x, y);
    max = new PVector(x+w, y+h);
    this.w = w;
    this.h = h;
  }
  
  // Kollisionskontrolle
  boolean playerCollision() {
    if (abs(ply.min.y - (min.y+h/2)) > ply.h/2+h/2 || abs(ply.min.x - (min.x+w/2)) > ply.w/2+w/2) { 
      return false;
    }
    return true;
  }
  
  // Darstellung Block
  void show() {
    fill(100);
    rect(min.x, min.y, w, h);
  }
}
