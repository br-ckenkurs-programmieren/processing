import java.util.List;

// Hindernisse sowie auch Personen
List<Obstacle> obstacles = new ArrayList<Obstacle>();

void setup() {
  size(800, 600);

  // linke Wand erzeugen
  for (int i = 0; i<360/20; i++) {
    obstacles.add(new Obstacle(10 + i*20, 210, 10, true));
  }
  // rechte Wand erzeugen
  for (int i = 0; i<360/20; i++) {
    obstacles.add(new Obstacle(450 + i*20, 210, 10, true));
  }
  // großes Hindernis in der Mitte erzeugen
  obstacles.add(new Obstacle(400, 400, 50, true));
}

void draw() {
  background(100);

  // Hindernisse sowie Personen bewegen
  int n = 0;
  while (n<obstacles.size()) {
    int prevSize = obstacles.size();
    obstacles.get(n).update();
    if (prevSize==obstacles.size()) n++;
  }

  // Kollisionscheck und -handling
  for (int iterations = 0; iterations<5; iterations++) { 
    for (int i = 0; i<obstacles.size(); i++) {
      for (int j = i+1; j<obstacles.size(); j++) {
        obstacles.get(i).collide(obstacles.get(j));
      }
    }
  }

  // Hindernisse sowie Personen zeichnen
  for (Obstacle o : obstacles) {
    o.drawThis();
  }

  // Wände zeichnen
  fill(255);
  rect(0, 200, 360, 20);
  rect(440, 200, 360, 20);
}

class Obstacle {
  float x;
  float y;
  float r;
  boolean isFixed;
  Obstacle(float x, float y, float r, boolean isFixed) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.isFixed = isFixed;
  }
  
  void update() {
  }
  
  void drawThis() {
    if (isFixed) {
      fill(255);
    } else {
      fill(30);
    }
    circle(x, y, 2*r);
  }
  
  void collide(Obstacle o) {
    if (!isFixed || !o.isFixed) {
      if (dist(o.x, o.y, x, y) < o.r+r) {
        if (!o.isFixed && !isFixed) {
          // Kollision zwei loser Objekte
          PVector v = new PVector(o.x-x, o.y-y);
          PVector center = new PVector((o.x+x)/2, (o.y+y)/2);
          v.normalize();
          v.mult((o.r + r)/2);
          x = center.x - v.x;
          y = center.y - v.y;
          o.x = center.x + v.x;
          o.y = center.y + v.y;
        } else {
          // Kollision eines losen mit einem befestigten Objekt
          Obstacle fixed = o.isFixed ? o : this;
          Obstacle loose = o.isFixed ? this : o;
          PVector v = new PVector(loose.x-fixed.x, loose.y-fixed.y);
          v.normalize();
          v.mult(fixed.r + loose.r);
          loose.x = fixed.x + v.x;
          loose.y = fixed.y + v.y;
        }
      }
    }
  }
  
}

class Person extends Obstacle {
  float speed = random(3, 5);
  float xSpeed = 0;
  float ySpeed = 0;
  Person(float x, float y) {
    super(x, y, 20, false);
  }
  void update() {
    if (mousePressed && mouseButton!=LEFT) {

      // bei Berührung mit Maus, lösche Person
      if (dist(mouseX, mouseY, x, y)<r*1.2) {
        obstacles.remove(this);
      }

      PVector dir = new PVector(mouseX-x, mouseY-y).normalize();
      if (mouseButton==RIGHT) {
        // auf Mauszeiger zubewegen
        x += dir.x * speed;
        y += dir.y * speed;
      }
      if (mouseButton==CENTER) {
        // von Mauszeiger wegbewegen
        x -= dir.x * speed;
        y -= dir.y * speed;
      }
    } else {
      // zufällige Bewegungsrichtung
      xSpeed = constrain(xSpeed + random(-0.1, 0.1), -1, 1);
      ySpeed = constrain(ySpeed + random(-0.1, 0.1), -1, 1);
      x += xSpeed;
      y += ySpeed;
    }
  }
}

void mousePressed() {
  if (mouseButton==LEFT) {
    // Erzeuge Person
    obstacles.add(new Person(mouseX, mouseY));
  }
}
