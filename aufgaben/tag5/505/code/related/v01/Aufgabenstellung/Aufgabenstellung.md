# Gedrängel
In dieser Aufgabe soll eine Menschenmenge simuliert werden, welche sich auf engem Raum bewegt.

![Gedrängel](gedraengel.gif)

## Theorie

Kollisionscheck und -handling können sehr kompliziert werden. Der Einfachheit halber werden
wir in dieser Aufgabe ausschließlich Kreise kollidieren lassen. Das bedeuted ob zwei Objekte
sich berühren lässt sich mit Radius und Position beider Objekte berechnen.

Erstelle zunächst eine Klasse Person.

Erstelle ein Klasse Obstacle, welche von Person geerbt wird.

Objekte der Klasse Person sollen sich direkt auf die Mausposition zubewegen. Dabei soll eine
Methode collide() verhinden, dass verschiedene Objekte sich überlappen. Diese Methode korrigiert
die Position zweier sich überlappender Objekte, sodass sie sich nur noch berühren. Sie sollte
in der Klasse Obstacle definiert werden. Bei der Implementierung muss zwischen beweglichen und
unbeweglichen Hindernissen unterschieden werden. Wieso wird im folgenden erläutert.

## Kollisionscheck und Positionskorrektur

Um festzustellen, ob zwei Kreise sich überhaupt berühren, genügt es den Abstand beider
Mittelpunkte mit der Summe der Radien zu vergleichen. Ist Ersteres kleiner als Letzteres,
so überlappen sich die Kreise. Eine Korrektur muss gemacht werden. 

![Abstand](tutorial_distance.png)

Dazu wird zunächst der Mittelpunkt zwischen den Mittelpunkten der beiden Kreise berechnet.
Dieser ist ((x1+x2)/2 | (y1+y2)/2)).
```
PVector centerPoint = new PVector((x1+x2)/2,(y1+y2)/2);
```

![Mittelpunkt](tutorial_center.png)

Nun wird die korrekte Position ausgehend von diesem Punkt berechnet. Der Zielpunkt
ist genau eine Radiuslänge diesem Punkt entfernt. Jetzt müssen wir noch die richtige
Richtung in Erfahrung bringen. Dazu berechnen wir den Einheitsvektor, also den Vektor
mit der Länge 1, welcher von Kreis 1 auf Kreis 2 zeigt. Folgender Code tut dies:
```
PVector v = new PVector(x2-x1, y2-y1);
v.normalize();
```
x und y sind hierbei die Koordinaten von Kreis 1 und zwei. Der resultierende Vektor
v sieht wie folgt aus:

![Vektor](tutorial_vektor.png)

Durch Multiplizieren von v mit dem Radius und Addieren zu dem zuvor gefundenen Mittelpunkt
lässt sich nun die korrekte Position bestimmen. Folgender Code tut dies:
```
PVector correction = v.mult(r);
PVector correctedPosKreis = centerPoint.add(correction);
```

![Korrektur](tutorial_corrected.png)

Gegebenenfalls muss der Vektor `correction` mit -1 multipliziert werden um die gewünschte
Richtung zu erhalten.

Bei Kollision von Kreisen verschiedener Radien, kann dass Verhältnis zwischen diesen berechnet werden.
Dies kann helfen um unerwünschte Effekte bei Kollision von Kreisen mit hohem Größenunterschied zu vermeiden.

Bei der Kollision eines befestigten Kreises mit einem bewegbaren wird nur ein Kreis bewegt.
Die Berechnung der neuen Position verläuft fast genause wie gezeigt wurde. Der einzige Unterschied ist, 
dass nun `v` mit der Summe beider Kreisradien multipliziert werden muss.

## Gesamtprogramm

Um das Programm fertigzustellen, muss dafür gesorgt werden, dass alle Objekte erstellt und 
angezeichnet werden. Dies geht z.b. mit einem Array oder einer ArrayList.

Damit die Personen sich bewegen muss die Position im draw()-Event aktualisiert werden. (z.B. kann
diese um 3 Pixel in Richtung Maus bewegt werden) In draw() muss auch collide() aufgerufen werden.

## Hinweise

Im Beispielgif ist ein Spalt zwischen zwei Wänden zu sehen. Diese scheinen Rechtecke zu sein,
doch in der Programmlogik bestehen beide Wände aus vielen kleinen Kreisen welche aneinandergereiht
wurden.
