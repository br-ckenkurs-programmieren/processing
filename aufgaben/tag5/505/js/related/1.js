// Hindernisse sowie auch Personen
let obstacles = [];

function setup() {
  createCanvas(800, 600);

  // linke Wand erzeugen
  for (let i = 0; i<360/20; i++) {
    obstacles.push(new Obstacle(10 + i*20, 210, 10, true));
  }
  // rechte Wand erzeugen
  for (let i = 0; i<360/20; i++) {
    obstacles.push(new Obstacle(450 + i*20, 210, 10, true));
  }
  // großes Hindernis in der Mitte erzeugen
  obstacles.push(new Obstacle(400, 400, 50, true));
}

function draw() {
  background(100);

  // Hindernisse sowie Personen bewegen
  let n = 0;
  while (n<obstacles.length) {
    let prevSize = obstacles.length;
    obstacles[n].update();
    if (prevSize==obstacles.length) {
      n++
    }
  }

  // Kollisionscheck und -handling
  for (let iterations = 0; iterations<5; iterations++) { 
    for (let i = 0; i<obstacles.length; i++) {
      for (let j = i+1; j<obstacles.length; j++) {
        obstacles[i].collide(obstacles[j]);
      }
    }
  }

  // Hindernisse sowie Personen zeichnen
  for (let i = 0; i < obstacles.length; i++) {
    obstacles[i].drawThis();
  }

  // Wände zeichnen
  fill(255);
  rect(0, 200, 360, 20);
  rect(440, 200, 360, 20);
}

class Obstacle {

  constructor( x, y, r, isFixed) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.isFixed = isFixed;
  }
  
   drawThis() {
    if (this.isFixed) {
      fill(255);
    } else {
      fill(30);
    }
    circle(this.x, this.y, 2*this.r);
  }
  
  update(){
  }
  
  collide(obstacle) {
    if (!this.isFixed || !obstacle.isFixed) {
      if (dist(obstacle.x, obstacle.y, this.x, this.y) < obstacle.r+ this.r) {
        if (!obstacle.isFixed && !this.isFixed) {
          // Kollision zwei loser Objekte
          let v = createVector(obstacle.x- this.x, obstacle.y- this.y);
          let center = createVector((obstacle.x+ this.x)/2, (obstacle.y+ this.y)/2);
          v.normalize();
          v.mult((obstacle.r + this.r)/2);
          this.x = center.x - v.x;
          this.y = center.y - v.y;
          obstacle.x = center.x + v.x;
          obstacle.y = center.y + v.y;
        } else {
          // Kollision eines losen mit einem befestigten Objekt
          let fixed = obstacle.isFixed ? obstacle : this;
          let loose = obstacle.isFixed ? this : obstacle;
          let v = createVector(loose.x-fixed.x, loose.y-fixed.y);
          v.normalize();
          v.mult(fixed.r + loose.r);
          loose.x = fixed.x + v.x;
          loose.y = fixed.y + v.y;
        }
      }
    }
  }
  
}

class Person extends Obstacle {
  constructor(x, y) {
    super(x, y, 20, false);
    this.speed = random(3, 5);
    this.xSpeed = 0;
    this.ySpeed = 0;
  }
  
  update() {
    if (mousePressed && mouseButton!=LEFT) {

      // bei Berührung mit Maus, lösche Person
      if (dist(mouseX, mouseY, this.x, this.y)< this.r*1.2) {
        obstacles.splice(obstacle.indexOf(this), 1);
      }

      let dir = createVector(mouseX-this.x, mouseY-this.y).normalize();
      if (mouseButton==RIGHT) {
        // auf Mauszeiger zubewegen
        this.x += this.dir.x * this.speed;
        this.y += this.dir.y * this.speed;
      }
      if (mouseButton==CENTER) {
        // von Mauszeiger wegbewegen
        this.x -= this.dir.x * this.speed;
        this.y -= this.dir.y * this.speed;
      }
    } else {
      // zufällige Bewegungsrichtung
      this.xSpeed = constrain(this.xSpeed + random(-0.1, 0.1), -1, 1);
      this.ySpeed = constrain(this.ySpeed + random(-0.1, 0.1), -1, 1);
      this.x += this.xSpeed;
      this.y += this.ySpeed;
    }
  }
}

function mousePressed() {
  if (mouseButton==LEFT) {
    // Erzeuge Person
    obstacles.push(new Person(mouseX, mouseY));
  }
}

