class Insekt {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.xSpeed = 1;
    this.ySpeed = 1;
  }

  // bei geringem Abstand beschleunige in entgegengesetzte Richtung
  collide(m) {
    if (dist(this.x, this.y, m.x, m.y) < 10) {
      this.xSpeed -=
        ((m.x - this.x) / 2 / mag(m.x - this.x, m.y - this.y)) * 10;
      this.ySpeed -=
        ((m.y - this.y) / 2 / mag(m.x - this.x, m.y - this.y)) * 10;
      m.xSpeed -= ((this.x - m.x) / 2 / mag(m.x - this.x, m.y - this.y)) * 10;
      m.ySpeed -= ((this.y - m.y) / 2 / mag(m.x - this.x, m.y - this.y)) * 10;
    }
  }

  update() {
    // bewege Muecke
    this.x += this.xSpeed;
    this.y += this.ySpeed;

    // beschleunige in Richtung Mauszeiger
    this.xSpeed += (mouseX - this.x) / 100;
    this.ySpeed += (mouseY - this.y) / 100;

    // begrenze Geschwindigkeit auf maxSpeed
    this.xSpeed = constrain(this.xSpeed, -insektMaxSpeed, insektMaxSpeed);
    this.ySpeed = constrain(this.ySpeed, -insektMaxSpeed, insektMaxSpeed);

    // zeichne Muecke
    this.drawInsekt();
  }

  drawInsekt() {
    fill(0);
    circle(this.x, this.y, 5);
  }
}

const insektenAnzahl = 20;
const insektMaxSpeed = 10;

let insekten;

function setup() {
  createCanvas(400, 400);

  insekten = new Array(insektenAnzahl)
    .fill()
    .map((_) => new Insekt(random(width), random(height)));
}

function draw() {
  // Hintergrund
  background(160, 160, 250);

  insekten.forEach((i) => {
    insekten.filter((j) => i != j).forEach((j) => i.collide(j));
    i.update();
  });
}
