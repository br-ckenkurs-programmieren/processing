class Obstacle{


    constructor(speed){
        this.x=width;
        this.y=420;
        this.w=50;
        this.h=80;
        this.obstacleSpeed = speed;
        this.image = loadImage("cac.png");
    }

    draw(){
        fill(0);
        image(this.image,this.x,this.y,this.w,this.h);

    }

    move(){
        //Wird nach links bewegt
        this.x -=this.obstacleSpeed;
        //Testen, ob das Dino Rechteck mit dem von derm Hinderniss überlapt und ruft dann "gameover()" auf
        if (this.x < dino_x+dino_x_offset + dino_w+dino_w_offset &&
            this.x + this.w > dino_x+dino_x_offset &&
            this.y < dino_y+dino_y_offset + dino_h+dino_h_offset &&
            this.y + this.h > dino_y+dino_y_offset) {
            gameOver();
        }

    }
}


//Dino Koordinaten
let dino_y = 400;
let dino_x = 150;
let dino_h = 100;
let dino_w = 90;

//Verkleinerung der Dino Kollisionsboxen im Vergleich zur Texturgröße, damit die äußeren "leeren" Ecken der Textur nicht mit den Kakteen kollidieren
const dino_x_offset = 30;
const dino_y_offset = 10;
const dino_h_offset = -40;
const dino_w_offset = -60;

//Geschwindigkeit & Beschleunigung des Dino auf der y Achse
let y_Velocity = 0;
let y_Acceleration = 0;

//Die Verschiedenen Bilder der Animation des Dinos
let spritesheet;
let dinos = [];
//Welches Bild grade gezeigt werden soll
let dinoAnimIndex = 0;

//"Erdbeschleunigung"
const groundAcc = 0.45;

//Wie stark der Dino Springen soll
const jumpPower = -10;


let score = 0;
let highscore = 0;

// Eine List mit Hindernissen, welche auftauchen
let obstacles = [];

function preload() {
    dinos.push(loadImage("dino.png"));
    dinos.push(loadImage("dino1.png"));
    dinos.push(loadImage("dino2.png"));
    dinos.push(loadImage("dino3.png"));
    dinos.push(loadImage("dino4.png"));
    console.log(dinos);
}

function setup(){
    createCanvas(1280, 720);
    onStart();
    
    
}

//Hier werden die ganzen Werte auf Standarteinstastellungen zurück gesetzt
function onStart(){
   score = 0;
   y_Velocity = 0;
   y_Acceleration = 0;
   dino_y = 400;
   dino_x = 150;
   score = 0;
   obstacles = [];
}



function draw(){
    //Alle 90 Frames wird ein neues Hinderniss hinzugefügt
    if(frameCount % 90 == 0){
      obstacles.push(new Obstacle(8 + (score/500)));
    }
    //Score um 1 erhöht
    score++;
    //Berechnung der Y- Position des Dions
    //Beschleunigung wird um erdbeschleunigung erhöht, geschwindigkeit um beschleunigung, y um geschwindigkeit
    y_Acceleration +=groundAcc;
    y_Velocity+= y_Acceleration;
    dino_y+=y_Velocity;
    
    //Wenn der Dino landet
    if(dino_y>=400){
      y_Acceleration = 0;
      dino_y = 400;
    }
    //Geschwindigkeit wird jeden frame zurückgesetzt
    y_Velocity=0;
    
    
    
    //Zeichnen
    background(240);
    
    //Hier werden die Hindernisse bewegt und gezeichnet
    for(let i=0;i<obstacles.length;i++){
      let ob = obstacles[i];
      ob.move();
      ob.draw();
    }
    
    fill(0);
    //Boden
    line(100,500,1180,500);
    //"Dino"
    image(dinos[dinoAnimIndex],dino_x,dino_y,dino_w,dino_h);
    console.log(dinos[dinoAnimIndex]);
    if(frameCount % 10 == 0){
      dinoAnimIndex ++;
      if(dinoAnimIndex >= dinos.length){
        dinoAnimIndex = 0;
      }
    }
    
    //Kann genutzt werden um die Kollisionsbox des Dinos anzuzeigen
    //color(255,0,0);
    //rect(dino_x+dino_x_offset,dino_y+dino_y_offset,dino_w+dino_w_offset,dino_h+dino_h_offset);
    stroke(1);
    
    //Scores
    textSize(32);
    text("No internet",100,600);
    text("Score: "+score+" Highscore: "+highscore,500,100);
}
//wird aufgerufen, wenn der dino "kollidiert"
function gameOver(){
  if(score>highscore){
    highscore = score;
  }
  onStart();
}
//Springen bei "W" oder "Leertaste"
function keyPressed(){
  if((key == 'w' || key==' ') && dino_y>=400){
        y_Acceleration= -10;
    }
}
