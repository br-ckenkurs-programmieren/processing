class Obstacle{
  
  int obstacleSpeed = 8;
  //Parameter der Hindernisses
  int x,y,w,h;
  PImage image;
  public Obstacle(int speed){
    x=width;
    y=420;
    w=50;
    h=80;
    obstacleSpeed = speed;
    image = loadImage("cac.png");
  }
  
  void draw(){
    fill(0);
    image(image,x,y,w,h);
    
  }
  
  void move(){
    //Wird nach links bewegt
    this.x -=obstacleSpeed;
    //Testen, ob das Dino Rechteck mit dem von derm Hinderniss überlapt und ruft dann "gameover()" auf
    if (x < dino_x+dino_x_offset + dino_w+dino_w_offset &&
       x + w > dino_x+dino_x_offset &&
       y < dino_y+dino_y_offset + dino_h+dino_h_offset &&
       y + h > dino_y+dino_y_offset) {
       gameOver();
    }
    
  }
}
