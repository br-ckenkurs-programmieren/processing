//Dino Koordinaten
int dino_y = 400;
int dino_x = 150;
int dino_h = 100;
int dino_w = 90;

//Verkleinerung der Dino Kollisionsboxen im Vergleich zur Texturgröße, damit die äußeren "leeren" Ecken der Textur nicht mit den Kakteen kollidieren
int dino_x_offset = 30;
int dino_y_offset = 10;
int dino_h_offset = -40;
int dino_w_offset = -60;

//Geschwindigkeit & Beschleunigung des Dino auf der y Achse
double y_Velocity = 0;
double y_Acceleration = 0;

//Die Verschiedenen Bilder der Animation des Dinos
PImage spritesheet;
PImage[] dinos = new PImage[5];
int dinoAnimIndex = 0;

//"Erdbeschleunigung"
final double groundAcc = 0.45d;

//Wie stark der Dino Springen soll
final double jumpPower = -10d;


int score = 0;
int highscore = 0;

// Eine List mit Hindernissen, welche auftauchen
ArrayList<Obstacle> obstacles = new ArrayList<Obstacle>();


void setup(){
    size(1280,720);
    surface.setTitle("Dino Spiel");
    onStart();
    
    spritesheet= loadImage("dino.png");
    dinos[0] = spritesheet.get(1,0,44,47);
    dinos[1] = spritesheet.get(45,0,44,47);
    dinos[2] = spritesheet.get(89,0,44,47);
    dinos[3] = spritesheet.get(133,0,44,47);
    dinos[4] = spritesheet.get(177,0,44,47);
}

//Hier werden die ganzen Werte auf Standarteinstastellungen zurück gesetzt
void onStart(){
   score = 0;
   y_Velocity = 0;
   y_Acceleration = 0;
   dino_y = 400;
   dino_x = 150;
   score = 0;
   obstacles.clear();
}

void draw(){
    //Alle 90 Frames wird ein neues Hinderniss hinzugefügt
    if(frameCount % 90 == 0){
      obstacles.add(new Obstacle(8 + (score/500)));
    }
    //Score um 1 erhöht
    score++;
    //Berechnung der Y- Position des Dions
    //Beschleunigung wird um erdbeschleunigung erhöht, geschwindigkeit um beschleunigung, y um geschwindigkeit
    y_Acceleration +=groundAcc;
    y_Velocity+= y_Acceleration;
    dino_y+=y_Velocity;
    
    //Wenn der Dino landet
    if(dino_y>=400){
      y_Acceleration = 0;
      dino_y = 400;
    }
    //Geschwindigkeit wird jeden frame zurückgesetzt
    y_Velocity=0;
    
    
    
    //Zeichnen
    background(240);
    
    //Hier werden die Hindernisse bewegt und gezeichnet
    for(int i=0;i<obstacles.size();i++){
      Obstacle ob = obstacles.get(i);
      ob.move();
      ob.draw();
    }
    
    fill(0);
    //Boden
    line(100,500,1180,500);
    //"Dino"
    
    image(dinos[dinoAnimIndex],dino_x,dino_y,dino_w,dino_h);
    if(frameCount % 10 == 0){
      dinoAnimIndex ++;
      if(dinoAnimIndex >= dinos.length){
        dinoAnimIndex = 0;
      }
    }
    
    //Kann genutzt werden um die Kollisionsbox des Dinos anzuzeigen
    //color(255,0,0);
    //rect(dino_x+dino_x_offset,dino_y+dino_y_offset,dino_w+dino_w_offset,dino_h+dino_h_offset);
    stroke(1);
    
    //Scores
    textSize(32);
    text("No internet",100,600);
    text("Score: "+score+" Highscore: "+highscore,500,100);
}
//wird aufgerufen, wenn der dino "kollidiert"
void gameOver(){
  if(score>highscore)
    highscore = score;
  onStart();
}
//Springen bei "W" oder "Leertaste"
void keyPressed(){
  if((key == 'w' || key==' ') && dino_y>=400){
        y_Acceleration= -10d;
    }
}
