PImage muschel;             // Bild-Variable //<>//
boolean mouse = false;      // ist die Maus geklickt worden?
int ansNum = int(random(10));                 // zufälliger Wert für Antwort
int onTime = 0;             // Timer für Antwort


// Antwortmöglichkeiten werden in den Constructor geschickt und in ein Array gepackt
Antwort[] answ = {new Antwort("Heute nicht."), 
                  new Antwort("Ich glaube nicht."), 
                  new Antwort("Mhmm nein."), 
                  new Antwort("Frag mich nochmal."), 
                  new Antwort("Kannst du das wiederholen?"),  
                  new Antwort("Nein."), 
                  new Antwort("Können Schweine fliegen?"), 
                  new Antwort("Ja."), new Antwort("Auf jeden Fall."), 
                  new Antwort("Du darfst heute alles.")};

void setup() {
  size(1000, 600);
  muschel = loadImage("muschel.jpg");       // Bild der Miesmuschel wird in Variable deklariert
  //Damit dieser Zugriff möglich ist, gehen Sie in processing auf
  // Sketch -> Datei hinzufügen und öffnen sie die gewünschte 
  // Datei.
  tint(240);                                // setzt Basis-Bildeffekt
  image(muschel, 0, 0, width, height);      // Bild wird dargestellt
  frameRate(4);                             // Framerate wird von 15xperSek heruntergesetzt damit die versch. Antworten nacheinander wahrscheinlicher sind
}

void draw() {

  // Timer für Antwort
  if (onTime >= 16) {
    tint(240);                              // setzt Bildeffekt zurück
    image(muschel, 0, 0, width, height);
  }


  // Aktualisierung neue Frage
  if (mouse == true) {
    onTime = 0;
    strokeWeight(5);
    noTint();                              // setzt Bildeffekt
    image(muschel, 0, 0, width, height);    // löscht alte Antwort

    // Sprach-Visualisierung
    for (int i = 0; i <= 3; i++) {
      pushMatrix();
      translate(width/2, height/2);
      rotate(radians(i*10));
      line(30, -30, 70, -70);
      popMatrix();
    }
  }

  // Text-Ausgabe
  float d = dist(mouseX, mouseY, 658, 154 ); 
  fill(0);                                                                       // Textfarbe
  if (mouse == true && d <=15.0) {                              // "geheime" Postition für positive Antwort
    if (ansNum <= 2) {
      answ[7].answ();                                                              // Methodenaufrufe
    } else if (ansNum <= 5) {
      answ[8].answ();
    } else {
      answ[9].answ();
    }
  } else if (mouse == true) {               //Beliebige Antwort     
    answ[ansNum].answ();
  }
  mouse = false;                          // Mausklick wird zurückgesetzt (sonst wuerde der naechste Klick nicht registiert)
  onTime++;                               // Timer für Antwortwiedergabe zählt hoch

  // Finde die Koordinaten der "geheim"-Postion auf dem Bild
  // println(mouseX, mouseY);
  //  658 154 bei size(1000, 600) --> x=width/(width/658) y=height/(height/154) Muschel "Ja"-Trigger-Punkt
}

// Maus-Trigger für Antwort
void mousePressed() {
  if (mouse == false) {
    ansNum = int(random(10));  //Neue zufällige Zahl für neue zufällige Antwort
    mouse = true;
  }
}

// Klasse Antwort
class Antwort {            // Definierung der Klasse "Antwort" | Vergleich: Bsp: rect(); ist die Funktion einer vordefinierten Klasse(Objekt) "Rechteck"
  String answ;

  // Constructor
  Antwort(String a) {    // Constructor übergibt die Variablen von einzelnen Objekten z.B. anws1 zur Erstellung an die Klasse 
    this.answ = a;
  }

  // Methode zum Darstellen des Antwort-Textes
  void answ() {
    textSize(width/20);                    // legt Schriftgröße fest
    textAlign(CENTER);                     // richtet Text an der Mitte der "gewählten Text-Koordinaten" aus
    text(answ, width/2, height/10);        // gibt Text aus
  }
}
