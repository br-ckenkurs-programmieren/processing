let img;
let mouse = false;      // ist die Maus geklickt worden?
let ansNum;                // zufälliger Wert für Antwort
let onTime = 0;             // Timer für Antwort
let answ = [];

// Klasse Antwort
class Antwort {            // Definition der Klasse "Antwort" | Vergleich: Bsp: rect(); ist die Funktion einer vordefinierten Klasse(Objekt) "Rechteck"

    // Constructor
    constructor(a) {    // Constructor übergibt die Variablen von einzelnen Objekten z.B. anws1 zur Erstellung an die Klasse 
        this.answ = a;
    }

    // Methode zum Darstellen des Antwort-Textes
    a() {
        textSize(width / 20);                    // legt Schriftgröße fest
        textAlign(CENTER);                     // richtet Text an der Mitte der "gewählten Text-Koordinaten" aus
        text(this.answ, width / 2, height / 10);        // gibt Text aus
    }
}

function setup() {
    var canvas = createCanvas(1000, 600);
    canvas.parent("p5js");
    img = new Image();
    img.src = "muschel.jpg";

    frameRate(4);                             // Framerate wird von 15xperSek heruntergesetzt damit die versch. Antworten nacheinander wahrscheinlicher sind

    answ = [new Antwort("Ich glaube nicht."),
    new Antwort("Mhmm nein."),
    new Antwort("Frag mich nochmal."),
    new Antwort("Kannst du das wiederholen?"),
    new Antwort("Nein."),
    new Antwort("Können Schweine fliegen?"),
    new Antwort("Ja."), new Antwort("Auf jeden Fall."),
    new Antwort("Du darfst heute alles.")];

    ansNum = floor(random(answ.length));
}

function draw() {
    drawingContext.drawImage(img, 0, 0);
    // Timer für Antwort
    if (onTime >= 16) {
        tint(240);
        drawingContext.drawImage(img, 0, 0);
    }


    // Aktualisierung neue Frage
    if (mouse == true) {
        onTime = 0;
        strokeWeight(5);
        noTint();                              // setzt Bildeffekt
        drawingContext.drawImage(img, 0, 0);
        // Sprach-Visualisierung
    }

    // Text-Ausgabe
    let d = dist(mouseX, mouseY, 658, 154);
    fill(0);                     // Textfarbe
    if (mouse == true && d <= 15.0) {          // "geheime" Postition für positive Antwort
        if (ansNum <= 2) {
            answ[7].a();                 // Methodenaufrufe
        } else if (ansNum <= 5) {
            answ[8].a();
        } else {
            answ[9].a();
        }
    } else if (mouse == true) {               //Beliebige Antwort     
        answ[ansNum].a();
    }
    mouse = false;                          // Mausklick wird zurückgesetzt (sonst wuerde der naechste Klick nicht registiert)
    onTime++;                               // Timer für Antwortwiedergabe zählt hoch
}

// Maus-Trigger für Antwort
function mousePressed() {
    if (mouse == false) {
        ansNum = floor(random(answ.length));  //Neue zufällige Zahl für neue zufällige Antwort
        mouse = true;
    }
}