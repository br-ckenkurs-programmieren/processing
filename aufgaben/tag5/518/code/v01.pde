//         ---Mandlebrot Set---
//Quelle: https://jonisalonen.com/2013/lets-draw-the-mandelbrot-set/

//      ---Variablen---
//Iterationen (pausieren)
int iteration = 1;
int maxInterations =40;
boolean plusMinus = true;
boolean pause = false;

//Zoom
float zoomMax = 1;
float zoomMin = 0.2;
float zoom = 1;
float zoomStep = 0.009;

//Verschieben des Mandelbrot Set
int rowOffset = 0;
int colOffset = -100;
int offset = 5;

double infinity = 5;

void setup() {
  size(1000, 1000);
  frameRate(12);
}

void draw() {
  background(0);
  //    ---Iteration---
  if (iteration  <= 0)plusMinus = true;
  if (iteration > maxInterations) plusMinus = false;
  if (!pause) {
    if (plusMinus) iteration++;
    else iteration--;
  }

  loadPixels();

  //    ---Berechnen Mandelbrot Set---
  for (int row = 0 + rowOffset; row < height + rowOffset; row++) {
    for (int col = 0 + colOffset; col < width + colOffset; col++) {

      //Koordinaten von Displaygröße auf den Bereich vom Set bringen * den Zoom Faktor
      double c_re = (col - width/2.0)*4.0/width * zoom;
      double c_im = (row - height/2.0)*4.0/width * zoom;

      //Imaginäre Komponenten
      double x = 0, y = 0;

      //Absoluter Wert um die Distanz zum Mittelpunkt zu bestimmen
      double abs = 0;


      int i = 0;

      //Solange Iterieren bis max erreicht ist oder Folge undendlich wird
      while (x*x + y*y <= infinity && i < iteration) {
        double x_new = x*x - y*y + c_re;
        y = 2*x*y + c_im;
        x = x_new;
        i++;

        abs = sqrt((float)(x*x + y*y));
        if (abs > infinity) break;
      }

      //Offset zum verschieben
      int xDisp = row - rowOffset;
      int yDisp = col - colOffset;


      if (i < iteration) {
        //    ---Zeichnen---
        float r = map(i, 0, iteration, 100, 255);                    //Anzahl der Iterationen
        float g = map((float)abs, 0, (float)infinity, 0, 255);       //Wie weit vom Mittelpunkt
        float b = map(row, 0, height, 0, 255);                       //Wie groß row ist
        putPixel(yDisp, xDisp, color(r, g, b));
      }
    }
  }

  updatePixels();
}

void keyPressed() {
  switch(key) {
  case '-':
    if (zoom < zoomMax) zoom += zoomStep;
    break;
  case '+':
    if (zoom > zoomMin) zoom -= zoomStep;
    break;
  case ' ':
    pause = !pause;
    break;
  case 'a':
    colOffset += offset;
    break;
  case 'd':
    colOffset -= offset;
    break;
  case 'w':
    rowOffset += offset;
    break;
  case 's':
    rowOffset -= offset;
    break;
  }
}

float abs(float x, float y) {
  return sqrt(x * x + y * y);
}

void putPixel(int x, int y, color c) {
  if (x < 0 || x >= height || y < 0 || y >= width) return;
  pixels[x + y * width] = c;
}
