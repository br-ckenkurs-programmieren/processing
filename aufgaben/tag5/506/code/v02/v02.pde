float bally = 500;
float yvel = 0;

//Es werden nie mehr als 20 pipes da sein.
Pipe[] pipes = new Pipe[20];

boolean pause = false;
int frame = 0;

PFont f;

class Pipe
{
  boolean isTop;
  float h;
  float xpos;
  
  Pipe()
  {
   isTop = random(1.0) > 0.5;
   h = random(100, 400);
   xpos = 500;
  }
  
  boolean collides()
  {
    if (xpos < 100+25)
    {
      if (isTop) return bally-25 < 0 + h;
      else return bally+25 > 1000-h;
    }
    return false;
  }
}

void setup()
{
  size(500, 1000);
  background(0);
  fill(255);
  f = createFont("Arial", 30);
  textFont(f); 
}

Pipe neuePipe()
{
  for (int i = 0; i < pipes.length; i++)
  {
    if (pipes[i] == null)
    {
      pipes[i] = new Pipe();
      return pipes[i];
    }
  }
  return null;
}

void zeichnePipes()
{
  for (int i = 0; i < pipes.length; i++)
  {
    Pipe pipe = pipes[i];
    if (pipe != null)
    {
        fill(255);
        if (pipe.isTop)
          rect(pipe.xpos, 0, 50, pipe.h); 
        else
          rect(pipe.xpos, 1000-pipe.h, 50, pipe.h);   
    }
  }
}

void bewegePipes()
{
  for (int i = 0; i < pipes.length; i++)
  {
    Pipe pipe = pipes[i];
    if (pipe != null)
    {
      pipe.xpos -= 5;
      if (pipe.xpos < 0)
        pipes[i] = null;
      if (pipe.collides())
        pause = true;
    }
  }
}

void draw()
{
  if (!pause)
  {
    background(0);
    yvel += 1;
    bally += yvel;
    circle(100, bally, 50);
    
    if (random(1.0) > 0.99)
      neuePipe();
      
    bewegePipes();
    zeichnePipes();
    
    if (bally < 0+25 || bally > 1000-25)
      pause = true;
      
    text(frame, 25, 25);
    frame++;
  }  
}

void mousePressed()
{
  yvel = -20;  
}
