\section{Flappy Bird}
\medium{116 Z.}{\geometrie \mouse \methoden \klassen \vektoren}

Anfang 2014 wurde das Spiel \emph{Flappy Bird} explosionsartig berühmt - und nur kurze Zeit später verschwand es wieder.
Jetzt haben Sie die Gelegenheit, es erneut zum Leben zu erwecken, indem Sie es in Processing realisieren.

Bei \emph{Flappy Bird} gibt es einen Vogel, welcher vom Spieler durch Röhren manövriert werden muss.
Der Vogel bewegt sich nur auf der y-Achse, während die Röhren sich nach rechts bewegen.
Per Knopfdruck springt der Vogel nach oben, sonst fällt er nach unten.
Für jede durchtroffene Röhre wird dem/der Spieler:in ein Punkt verliehen.

\subsection{Der Vogel}

Das wichtigste zuerst: der Vogel von Flappy Bird (Übrigens heißt er im Original \emph{Faby}) hat nur zwei Zustände - entweder springt er, oder er fällt.
Erstellen Sie die Klasse \code{class Bird}, welche die Position des Vogels \code{x, y}, und dessen vertikale Geschwindigkeit \code{vy} speichert.

Implementiere zunächst die Methoden \code{jump()}, \code{show()} und \code{update()}.
Die Methode \code{jump()} soll mithilfe von \code{keyReleased()} aufgerufen werden.
Die Methoden \code{show()} (zeichnet den Vogel) und \code{update()} (aktualisiert dessen Werte) sollen in der \code{draw()}-Methode aufgerufen werden.

\begin{itemize}
	\item Mit der (globalen) Variable \code{gravity} können Sie den Wert festlegen, welcher bei jedem \code{update()} des Vogels zu \code{vy} addiert wird - dadurch entsteht ein natürlicher Fall, wenn darauf \code{vy} in jedem Zug zu \code{y} addiert wird.

	\item Die Methode \code{show()} sollte gut gewählte Farben und Formen haben, um einen mehr oder weniger erkennbaren Vogel an den Positionen \code{x, y} zu zeichnen.
\end{itemize}

Bei Ausführung sollten Sie einen Vogel haben, welcher beginnt, zu fallen.
Drückt man eine beliebige Taste, \enquote{springt} der Vogel ein bisschen, und fängt dann wieder an, zu fallen.

\begin{center}
	\imageplay{\img/flappyb1.png}{.4\linewidth}{1}
\end{center}

\subsection{Die Darstellung der Röhren}

Nun kommt ein anspruchsvoller Teil:
Wie kann man zufällig Röhren generieren?
Zunächst kümmern wir uns nur um die Darstellung, und noch nicht die Kollision.

Erstellen Sie eine Klasse \code{class Pipe}, welche die Variablen \code{x, y} als Position der Röhre speichert (Achtung: jede Röhre besteht aus zwei Röhren - oben und unten!).
Zudem benötigen Sie eine (globale) Variable \code{pipeSpace}, welche definiert, wie viel Abstand zwischen der oberen und unteren Röhre sein soll, und \code{pipeWidth}, um die Breite von Röhren festzulegen.

Zusätzlich zum Konstruktor (welcher sich für \code{y} eine Zufallsvariable aussuchen soll), benötigen Sie die Methoden \code{show()}, welche wie bei \code{class Bird} auch die beiden Röhren graphisch darstellt (dafür wird \code{pipeSpace} benötigt).
Die Methode \code{move()} soll die \code{x}-Position um eine (globale) Variable der Geschwindigkeit verringern (sodass bei jedem Aufruf von \code{move()} sich die jeweilige Röhre nach links verschiebt).

Um Ihre Klasse auszuprobieren, implementieren Sie eine \code{myPipe = new Pipe();} in \code{setup()}, welche dann in \code{draw()} bei jedem Durchlauf mit \code{myPipe.move()} und \code{myPipe.show()} dargestellt werden soll.

Ihr Ergebnis sollte eine Röhre sein, welche von der rechten Seite erscheint, sich langsam dem Vogel annähert, und irgendwann auf der linken Seite wieder verschwindet.

\begin{center}
	\imageplay{\img/flappyb2.png}{.4\linewidth}{1}
\end{center}

\subsection{Röhren in der Endlosschleife}

Vielleicht ist Ihnen schon Aufgefallen, dass wir nur eine einzelne Röhre implementiert haben, obwohl wir ja eigentlich unendlich viele haben möchten.
Darum kümmern wir uns jetzt!

Mit der Anweisung \code{pipes = new ArrayList<>()} in \code{setup()} und \code{List<Pipe> pipes} als globale Variable können Sie eine sog. \emph{ArrayList} erstellen.
Dabei handelt es sich um ein flexibles Array, welches die Arbeit mit \enquote{unendlich} vielen Röhren erleichtern soll. \code{myPipe} können Sie nun entfernen.

Schreiben Sie eine Methode \code{generatePipe()}, welche \code{pipes} eine neue \code{Pipe} hinzufügen soll.
Dafür gibt es bei ArrayLists den Befehl \code{pipes.add(o)}.
Schreiben Sie außerdem eine Methode \code{loadPipes()}, welche für alle \code{pipes} die Methoden \code{move()} und \code{show()} ausführt.
Für Ihre \code{for}-Schleife können Sie \code{pipes.size()} verwenden.
Um eine bestimmte Röhre zu bekommen, wird \code{Pipe p = pipes.get(i)} verwendet.

Prüfen Sie in \code{loadPipes()} außerdem, ob sich die Röhre nicht mehr im Bildschirm befindet, und entferne sie dann mit \code{pipes.remove(i--)}.

Zusätzlich sollen Sie über ein festgelegtes Zeitintervall neue Röhren generieren.
Erstellen Sie eine Variable \code{tick}, welche bei jedem \code{draw()}-Zyklus erhöht wird, und bei einem bestimmten Wert \code{generatePipe()} aufruft und wieder auf 0 gesetzt wird.

Nun sollten nach und nach Röhren erscheinen, auf zufälligen Höhen und in angemessenem Abstand.

\begin{center}
	\imageplay{\img/flappyb3.png}{.4\linewidth}{1}
\end{center}

\subsection{Kollisionserkennung}

Nun kümmern wir uns um die Bestimmung, ob sich der Vogel derzeit in einer Röhre befindet (bzw. mit dieser kollidiert).
Dafür erweitern wir \code{class Bird} um die Methode \code{deathCheck()}, welche ein \code{boolean}-Wert zurückgibt.

Die Methode soll über alle vorhandenen Röhren in \code{pipes} iterieren, und prüfen, ob sich die Instanz von \code{Bird} in einer der Röhren befindet (dann gibt die Methode \code{true} aus).
Die Bedingungen für \code{true} sind wie folgt (wobei \code{p} die derzeit iterierte \code{Pipe} ist):

\begin{itemize}
	\item \code{x} muss größer als \code{p.x} minus der Hälfte von \code{pipeWidth} sein, UND...
	\item \code{x} muss kleiner als \code{p.x} plus der Hälfte von \code{pipeWidth} sein.
	\item \code{y} muss kleiner als \code{p.y} minus der Hälfte von \code{pipeSpace} sein, ODER...
	\item \code{y} muss größer als \code{p.y} plus der Hälfte von \code{pipeSpace} sein.
\end{itemize}

Zudem gibt es eine weitere Möglichkeit, zu sterben:

\begin{itemize}
	\item \code{y} ist so groß, dass der Vogel außerhalb des Bildes (auf den \enquote{boden} gefallen) ist.
\end{itemize}

Für den Fall, dass \code{deathCheck()} wahr ist, können Sie sich einen \enquote{Game Over} Bildschirm überlegen, oder alternativ mit einem Aufruf von \code{setup()} das Spiel in den Ausgangszustand versetzen (sodass erneut gespielt werden kann).
Dafür ist es wichtig, alle initialen Zuweisungen in der \code{setup()}-Methode zu machen, und nicht außerhalb!

\subsection{Erweiterungsideen}

Sie haben es geschafft! Nun können Sie das Spiel nach Bedarf noch erweitern. Hier ein paar Anregungen:

\begin{itemize}

	\item \code{showFloor()} -- wie wär's mit einem Boden?
	\item \code{gameOverScreen()} -- erzeugt ein Bild, wodurch das Spiel neu gestartet werden kann.
	Tipp: hierfür ist es sinnvoll, eine Variable \code{gameOver} einzuführen.
	\item \code{showStartMenu()} -- ein Startmenü, welches dem Spielenden die Möglichkeit gibt, das Spiel erst zu starten, wenn man bereit ist.
	Es könnte auch eine Überschrift zeigen.
	\item \code{bird.hover()} -- für Fortgeschrittene unter Fortgeschrittenen: können Sie eine Methode schreiben, welche dem Vogel den Anschein verleiht, sich leicht nach oben und unten zu bewegen, bevor man das Spiel startet?
	\item \code{showScore()} -- zeigt die derzeitige Punktzahl an.
	Sie könnten einen Punkt verleihen, wenn eine Röhre in \code{loadPipes()} gelöscht wird (da sie dann eindeutig \enquote{überlebt} wurde).
	Hierfür brauchen Sie selbstverständlich auch eine \code{score} Variable.
	\item \code{highScore} -- aufbauend auf \code{score} könnten Sie eine Höchstpunktzahl einführen, welche nach Spielende anhand von \code{score} aktualisiert wird.
	\item Können Sie anhand von \code{rotate(a)} den Vogel abhängig von dessen derzeitiger \code{vy} so rotieren lassen, dass er tatsächlich so aussieht, als würde er fliegen?
	\item \code{speed} -- lassen Sie das Spiel langsam schwerer werden, indem Sie Röhren um \code{speed} verschieben, und diese Variable immer höher werden lassen.

\end{itemize}

So könnte das Spiel aussehen, wenn Sie es weiter ausbauen.
Natürlich können Sie Ihre eigenen Ideen einfließen lassen, und so das Spiel erweitern.

\begin{center}
	\imageplay{\img/fbmenu.png}{.3\linewidth}{1}
	\imageplay{\img/flappybird.png}{.3\linewidth}{1}
	\imageplay{\img/fbgameover.png}{.3\linewidth}{1}
\end{center}
