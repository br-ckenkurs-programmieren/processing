/* Settings */
// starting speed of pipes
var baseSpeed;

// value by which speed increases each tick
var speedMultiplier;

// thickness of pipes
var pipeWidth;

// vertical thickness of connector at the end of pipes
var connectorThickness;

// space between upper and lower pipe. less = more difficult
var pipeSpace;

// time interval between new pipes. lower = more difficult
var pipeGenerationRate;

// x position of the bird
var birdX;

var gravity;

/* Colors */
var backgroundColor;

var floorColor;

var floorOutline;

var birdBody;

var birdBodyOutline;

var birdEye;

var birdEyeOutline;

var birdMouth;

var birdMouthOutline;

var pipe;

var pipeOutline;

var titleText;

var gameOverColor;

var gameOverText;

var scoreColor;

var highScoreIndicator;

var scoreIsHighScore;

/* Game Variables */
var pipes;

var speed;

var counter;

var tick;

var score;

var highScore;

var gameOver;

var gameStarted;

var bird;

/* Methods */
function setup() {
    initializeFields();
    createCanvas(600, 800);
    pixelDensity(2);
    strokeWeight(5);
    textAlign(CENTER);
    speed = baseSpeed;
    // immediately generates first pipe
    tick = 1000;
    bird = new Bird();
    gameOver = false;
    gameStarted = false;
    pipes = [];
    highScore = (score > highScore) ? score : highScore;
    score = 0;
}

function draw() {
    background(backgroundColor);
    bird.show();
    if (gameOver) {
        // player died
        showGameOverScreen();
    } else if (gameStarted) {
        // player alive, game started
        if (tick++ >= pipeGenerationRate) {
            // generate pipe if tick is high enough
            generatePipe();
            tick = 0;
        }
        loadPipes();
        speed *= speedMultiplier;
        bird.update();
        gameOver = bird.deathCheck();
        showScore();
        showFloor();
    } else {
        // game not started yet
        showStartMenu();
        bird.hover();
        showFloor();
    }
    // saveFrame("mySketch-####.png");
}

function keyPressed() {
    if (gameOver) {
        // resets the game if gameOver is true
        setup();
    } else if (gameStarted) {
        bird.jump();
    } else {
        gameStarted = true;
        bird.jump();
    }
}

function showScore() {
    if (score > highScore) {
        fill(highScoreIndicator);
        textSize(15);
        text("HIGHSCORE!", 100, 120);
        fill(scoreIsHighScore);
    } else {
        fill(scoreColor);
    }
    textSize(75);
    text(score, 100, 100);
}

function showStartMenu() {
    fill(titleText);
    textSize(60);
    text("Floppy Bird", width / 2, height / 4);
    textSize(20);
    text("Press any key to play", width / 2, height / 4 + 50);
    text("Highscore: " + highScore, width / 2, height / 4 + 75);
}

function showGameOverScreen() {
    background(gameOverColor);
    fill(gameOverText);
    textSize(50);
    text("Game Over", width / 2, height / 2);
    textSize(25);
    text("Press any key to go back to the menu", width / 2, height / 2 + 50);
}

function showFloor() {
    stroke(floorOutline);
    fill(floorColor);
    rect(0, height - 50, width, height);
}

function generatePipe() {
    pipes.push(new Pipe());
}

function loadPipes() {
    for (var i = 0; i < pipes.length; i++) {
        var p = pipes[i];
        p.move();
        p.show();
        if (p.x < -pipeWidth) {
            // remove pipes that left the screen
            // i must be decremented, otherwise you would skip the following pipe
            pipes.splice(i, 1);
            i--;
            score++;
        }
    }
}

/* Classes */
class Pipe {
    constructor() {
        // places pipe just outside frame
        this.x = width + pipeWidth;
        // vertical location
        this.y = height / 6 + random(height - (height / 6) * 2);
    }

    show() {
        var top = this.y - pipeSpace / 2;
        var bottom = this.y + pipeSpace / 2;
        fill(pipe);
        stroke(pipeOutline);
        // top pipe
        rect(this.x, 0, pipeWidth * 0.8, top);
        // connector
        rect(this.x - pipeWidth * 0.1, top - connectorThickness, pipeWidth, connectorThickness);
        // bottom pipe
        rect(this.x, bottom, pipeWidth * 0.8, height);
        // connector
        rect(this.x - pipeWidth * 0.1, bottom, pipeWidth, connectorThickness);
      //noFill();
      //rect(this.x, 0, pipeWidth, height);
      //rect(this.x, this.y - pipeSpace / 2, pipeWidth, pipeSpace  )
    }

    move() {
        this.x -= speed;
    }

}


class Bird {

    constructor() {
        this.x = birdX;
        this.y = height / 2;
        this.vy = 0;
        this.jumpingCounter = 0;
        this.hoverValue = 0;
    }

    update() {
        if (this.jumpingCounter > 0)
            this.jumpingCounter--;
        this.vy += gravity;
        this.y += this.vy;
    }

    show() {
        push();
        translate(this.x, this.y);
        // rotate based on y velocity
        rotate(radians(this.vy * 10));
        fill(birdBody);
        stroke(birdBodyOutline);
        // body
        circle(0, 0, 50);
        fill(birdEye);
        stroke(birdEyeOutline);
        // eye
        circle(5, -10, 10);
        fill(birdMouth);
        stroke(birdMouthOutline);
        triangle(20, -5, 20, 5, 30, 0);
        // beak
        pop();
    }

    jump() {
        if (this.jumpingCounter == 0) {
            this.vy = -4;
            this.jumpingCounter = 10;
        }
    }

    deathCheck() {
        for (var i = 0; i < pipes.length; i++) {
            var p = pipes[i];
            if (this.x > p.x && this.x < p.x + pipeWidth) { //"is bird within a pipe?"
                if ( this.y < p.y - pipeSpace / 2 || this.y > p.y + pipeSpace / 2){
                    return true;
                }
            }
        }
        if (// "is bird below ground?"
            this.y > height - 50){
            return true;
        }
        return false;
    }
  
    hover() {
        this.y += sin(this.hoverValue++ / 10.0) * 1.5;
    }
}

function initializeFields() {
    baseSpeed = 1;
    speedMultiplier = 1.0001;
    pipeWidth = 75;
    connectorThickness = 15;
    pipeSpace = 225;
    pipeGenerationRate = 225;
    birdX = 100;
    gravity = 0.1;
    backgroundColor = color(0x64, 0x64, 0xFF);
    floorColor = color(0xCC, 0xB7, 0x35);
    floorOutline = color(0x8C, 0x5b, 0x08);
    birdBody = color(0xF4, 0xF0, 0x02);
    birdBodyOutline = color(0x00, 0x00, 0x00);
    birdEye = color(0x00, 0x00, 0x00);
    birdEyeOutline = color(0xFF, 0xFF, 0xFF);
    birdMouth = color(0xAD, 0x22, 0x27);
    birdMouthOutline = color(0x8C, 0x1C, 0x1F);
    pipe = color(0x07, 0xBF, 0x20);
    pipeOutline = color(0x05, 0x91, 0x18);
    titleText = color(0xED, 0xED, 0x53);
    gameOverColor = color(0x00, 0x00, 0x00);
    gameOverText = color(0xC1, 0x05, 0x08);
    scoreColor = color(0xFC, 0xFC, 0x00);
    highScoreIndicator = color(0x8E, 0x1D, 0x0E);
    scoreIsHighScore = color(0xFC, 0xCE, 0x00);
    pipes = null;
    speed = 0;
    counter = 0;
    tick = 0;
    score = 0; 
    highScore = 0;
    gameOver = false;
    gameStarted = false;
    bird = null;
}