class Platform {
  int x;                                            // X Koordinate
  int y;                                            // Y Koordinate
  int vy = 2;                                       // Plattform Geschwindigkeit

  int rectW = 100;                                  // Breite der Plattform
  int rectH = 10;                                   // Hoehe der Plattform
  int margin = 20;                                  //Abstand zum Bildschirmrand

  Platform(int yval) {
    x = int(random(margin, width-margin-rectW));    // Generiert zufaellige X-Position für die neue Platform im erlaubten Bereich
    y = yval;                                       // setzt Hoehe der Plattform auf uebergebenen Wert
  }

  void move() {                                     //Bewegt Plattform
    y += vy;                                        //Auf Hoehe wird Geschwindigkeit addiert
    if (y>height) {                                 //Erreicht Plattform den unteren Fensterrand? 
      resetPosition();                              //Plattform wird auf Anfang zurückgesetzt
    }
  }

  void resetPosition() {                            //Plattform wird auf Anfang zurückgesetzt
    x = int(random(margin, width-margin-rectW));    // Generiert zufaellige X-Position für die Plattform im erlaubten Bereich
    y = 0;                                          // Plattform wird auf oberen Fensterrand gesetzt
  }

  void drawPlatform() {                             //Zeichnet Plattform
    fill(#00ff00);                                  //Plattform Farbe
    rect(x, y, rectW, rectH);                       // Ein Rechteck repraesentiert die Plattform
  }
}
