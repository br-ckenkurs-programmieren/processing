class Player {

  float x;                                 // X Koordinate
  float y;                                 // Y Koordinate
  float vy = 0;                            // Spieler Fallgeschwindigkeit
  float vx = 4;                            // Spieler horizontale Geschwindigkeit   
  float g = 0.2;                           //"Gravitation"
  float v = -10;                           // Initial Geschwindigkeit nach Abprallen (negativ heißt nach oben(y))
  int radius = 10;                         // Spieler Größe

  Player(int x, int y) {                   // Konstruktor für neuen Spieler
    this.x = x;                            // setzt X Position
    this.y = y;                            // setzt Y Position
  }

  void move() {                            // Bestimmt aktuelle position des Spielers
    if (keyPressed) {                      // Ist eine Taste gedrückt
      if (keyCode==LEFT) {                 // Ist gedrückte Taste linke Pfeiltaste 
        x-= vx;                            // X position wird nach links verschoben
      } else if (keyCode==RIGHT) {         // Ist gedrückte Taste rechte Pfeiltaste
        x+= vx;                            // X position wird nach rechts verschoben
      }
    }
    x = x % width;                         // Rechter Ueberlauf des Spielfeld 
    if (x<=0) {                            // Erreicht Spieler linken Spielrand (oder ist darunter) ?
      x=width;                             // Spieler wird auf Rechten Rand gesetzt
    }
  }

  void jump() {                           // Berechnet Spungposition des Spielers 
    vy += g;                              // "Gravitation" beschleunigt die nach unten
    y += vy;                              // Neue Y position ergibt sich durch addieren der Geschwindigkeit
    if (y>height-radius) {                // UeberpruefSpieler unter dem unteren Spielrand kommt
      bounce();                           // Neuer Sprung wird initialisiert
    }
  }

  void bounce() {                        // Neuer Sprung wird initialisiert
    vy = v;                              // Spieler Geschwindigkeit wird auf initialisierungswer gesetzt
  }

  void drawPlayer() {                     // Stellt Spieler dar
    fill(#00ff00);                        // Spieler Farbe
    ellipse(x, y, radius*2, radius*2);    // Spielermodell
  }
}
