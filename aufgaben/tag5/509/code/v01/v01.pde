int score = 0;                                                     // Start Score
int highScore = 0;                                                 // Highscore zu beginn
int framerate = 120;                                               // Framerate: regelt die Peschwindigkeit des Spiels
int nPlatforms = 4;                                                // Anzahl an Platformen

Platform[] platforms = new Platform[nPlatforms];                   // neues Array an Platformen
Player player;                                                     // Spieler 

void setup() {
  size(600, 1000);                                                // Fenstergroeße (Breite x Hoehe)
  initPlatforms();                                                //Erstellt neue Platformen mit entsprechenden Abstaenden
  player = new Player(width/2, height);                           //Erstellt neuen Spieler am unteren Fensterrand und mittig
  frameRate(framerate);                                           // setzt Framerate
}
void draw() {
  background(0);                                                  // Setzt Hintergrund auf Schwarz
  playerCollides();                                               // Behandelt die Kollision mit einer Platform
  updatePlayer();                                                 // Aktuallisiert Spieler
  updatePlatforms();                                              //Aktuallisiert alle Platformen
  drawScore();                                                    // Zeigt Score an
  gameOver();                                                     // Behandelt wenn ein Spieler verliert
}   

void updatePlayer() {                                              // Aktuallisiert Spieler
  player.jump();                                                   // Bewegt Spieler horizontal
  player.move();                                                   // Bewegt Spieler vertikal
  player.drawPlayer();                                             // Stellt Spieler dar
}

void updatePlatforms() {                                          //Aktuallisiert alle Platformen
  for (Platform p : platforms) {                                  // Behandelt alle Platformen
    p.move();                                                     // Bewegt Platform 
    p.drawPlatform();                                             // Stellt Platform dar
  }
}
void playerCollides() {                                            // Behandelt die Kollision mit einer Platform
  if (player.vy>0) {                                               // Ueberpruefung ob der Spuell am Fallen ist 
    for (Platform p : platforms) {                                 // Behandelt alle Platformen
      if (inBetween(player.y+player.radius, p.y, p.rectH)) {       // Hat Spieler die Höhe für eine Kollision mit betrachteter Platform ? 
        if (inBetween(player.x+player.radius, p.x, p.rectW) ||     // Hat Spieler die X-Position für eine Kollision mit betrachteter Platform (rechts) ? 
          inBetween(player.x-player.radius, p.x, p.rectW)) {       // Hat Spieler die X-Position für eine Kollision mit betrachteter Platform (links) ?     
          player.bounce();                                         // Spieler springt auf Platform
          score++;                                                 // Score wird incrementiert
          frameRate += 1;                                          // Framerate steigert sich bei jedem Sprung (hoehere Schwierigkeit bei groesserem Score)
        }
      }
    }
  }
}

void initPlatforms() {
  int yOffsetPlatforms = height/(nPlatforms);                      // Berechnet den Abstand der Platformen
  for (int i = 0; i<nPlatforms; i++) {                             //Es werden alle Platformen mit Abstand generiert
    int yOffset = yOffsetPlatforms*(i+1);                          //Berechnet die Y-Position anhand des Abstands und der Platformen
    platforms[i] = new Platform(yOffset);                          //Erstellt neue Platform mit entsprechendem Abstand
  }
}

void gameOver() {                                                // Behandelt wenn ein Spieler verliert
  if (player.y>=height) {                                          // Ueberprueft ob der Spieler den unteren Spielrand beruehrt
    highScore = max(score, highScore);                             // Falls Score > Highscore wird neuer Highscore gesetzt
    score = 0;                                                     // Score wird zurueck gesetzt
    frameRate = 120;                                               // Framerate wird zurueck gesetzt
  }
}

void drawScore() {                                                 // Zeigt Score an  
  fill(#00ff00);
  textSize(20);                                                    // Textgroesse
  text("Score: "+score, 10, 20);                                   // Setzt Scoretext
  text("High-Score:"+max(score, highScore), 10, 40);               // Setzt Highscoretext
}

boolean inBetween(float val, float min, float limit) {             // Prueft ob val zwischen min und limit liegt 
  return min <= val && val<=(min+limit);
}
