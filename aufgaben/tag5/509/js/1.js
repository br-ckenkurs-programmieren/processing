let rectW = 100; // Breite der Plattform
let rectH = 10; // Hoehe der Plattform
let margin = 20; //Abstand zum Bildschirmrand
let score = 0; // Start Score
let highScore = 0; // Highscore zu beginn
let framerate = 120; // Framerate: regelt die Peschwindigkeit des Spiels
let nPlatforms = 4; // Anzahl an Platformen

let platforms = []; // neues Array an Platformen
let player; // Spieler

class Platform {
  constructor(yval) {
    this.x = int(random(margin, width - margin - rectW)); // Generiert zufaellige X-Position für die neue Platform im erlaubten Bereich
    this.y = yval; // setzt Hoehe der Plattform auf uebergebenen Wert
    this.vy = 2;
  }

  move() {
    //Bewegt Plattform
    this.y += this.vy; //Auf Hoehe wird Geschwindigkeit addiert
    if (this.y > height) {
      //Erreicht Plattform den unteren Fensterrand?
      this.resetPosition(); //Plattform wird auf Anfang zurückgesetzt
    }
  }

  resetPosition() {
    //Plattform wird auf Anfang zurückgesetzt
    this.x = int(random(margin, width - margin - rectW)); // Generiert zufaellige X-Position für die Plattform im erlaubten Bereich
    this.y = 0; // Plattform wird auf oberen Fensterrand gesetzt
  }

  drawPlatform() {
    //Zeichnet Plattform
    fill("#00ff00"); //Plattform Farbe
    rect(this.x, this.y, rectW, rectH); // Ein Rechteck repraesentiert die Plattform
  }

  getY() {
    return this.y;
  }

  getX() {
    return this.x;
  }
}

class Player {
  constructor(x, y) {
    // Konstruktor für neuen Spieler
    this.x = x; // setzt X Position
    this.y = y; // setzt Y Position
    this.vy = 0;
    this.vx = 30;
    this.g = 0.2;
    this.v = -10;
    this.radius = 10;
  }

  move() {
    // Bestimmt aktuelle position des Spielers
    this.x = this.x % width; // Rechter Ueberlauf des Spielfeld
    if (this.x <= 0) {
      // Erreicht Spieler linken Spielrand (oder ist darunter) ?
      this.x = width; // Spieler wird auf Rechten Rand gesetzt
    }
  }

  jump() {
    // Berechnet Spungposition des Spielers
    this.vy += this.g; // "Gravitation" beschleunigt die nach unten
    this.y += this.vy; // Neue Y position ergibt sich durch addieren der Geschwindigkeit
    //if (this.y > height - this.radius) {
      // UeberpruefSpieler unter dem unteren Spielrand kommt
    //  this.bounce(); // Neuer Sprung wird initialisiert
   // }
  }

  bounce() {
    // Neuer Sprung wird initialisiert
    this.vy = this.v; // Spieler Geschwindigkeit wird auf initialisierungswer gesetzt
  }

  drawPlayer() {
    // Stellt Spieler dar
    fill("#00ff00"); // Spieler Farbe
    ellipse(this.x, this.y, this.radius * 2, this.radius * 2); // Spielermodell
  }

  getVY() {
    return this.vy;
  }

  getRadius() {
    return this.radius;
  }

  getY() {
    return this.y;
  }

  getX() {
    return this.x;
  }

  getVX() {
    return this.vx;
  }

  setX(newX) {
    this.x = newX;
  }
}

function setup() {
  createCanvas(600, 1000); // Fenstergroeße (Breite x Hoehe)
  initPlatforms(); //Erstellt neue Platformen mit entsprechenden Abstaenden
  player = new Player(width / 2, height - 700); //Erstellt neuen Spieler am unteren Fensterrand und mittig
  frameRate(framerate); // setzt Framerate
}
function draw() {
  background(0); // Setzt Hintergrund auf Schwarz
  playerCollides(); // Behandelt die Kollision mit einer Platform
  updatePlayer(); // Aktuallisiert Spieler
  updatePlatforms(); //Aktuallisiert alle Platformen
  drawScore(); // Zeigt Score an
  gameOver(); // Behandelt wenn ein Spieler verliert
  frameRate(framerate);
}

function updatePlayer() {
  // Aktuallisiert Spieler
  player.jump(); // Bewegt Spieler horizontal
  player.move(); // Bewegt Spieler vertikal
  player.drawPlayer(); // Stellt Spieler dar
}

function updatePlatforms() {
  //Aktuallisiert alle Platformen
  for (let i = 0; i < platforms.length; i++) {
    // Behandelt alle Platformen
    platforms[i].move(); // Bewegt Platform
    platforms[i].drawPlatform(); // Stellt Platform dar
  }
}
function playerCollides() {
  // Behandelt die Kollision mit einer Platform
  if (player.getVY() > 0) {
    // Ueberpruefung ob der Spuell am Fallen ist
    for (let i = 0; i < platforms.length; i++) {
      // Behandelt alle Platformen
      if (
        inBetween(
          player.getY() + player.getRadius(),
          platforms[i].getY(),
          rectH
        )
      ) {
        // Hat Spieler die Höhe für eine Kollision mit betrachteter Platform ?
        if (
          inBetween(
            player.getX() + player.getRadius(),
            platforms[i].getX(),
            rectW
          ) || // Hat Spieler die X-Position für eine Kollision mit betrachteter Platform (rechts) ?
          inBetween(
            player.getX() - player.getRadius(),
            platforms[i].getX(),
            rectW
          )
        ) {
          // Hat Spieler die X-Position für eine Kollision mit betrachteter Platform (links) ?
          player.bounce(); // Spieler springt auf Platform
          score++; // Score wird incrementiert
          framerate += 1; // Framerate steigert sich bei jedem Sprung (hoehere Schwierigkeit bei groesserem Score)
        }
      }
    }
  }
}

function initPlatforms() {
  let yOffsetPlatforms = height / nPlatforms; // Berechnet den Abstand der Platformen
  for (let i = 0; i < nPlatforms; i++) {
    //Es werden alle Platformen mit Abstand generiert
    let yOffset = yOffsetPlatforms * (i + 1); //Berechnet die Y-Position anhand des Abstands und der Platformen
    platforms[i] = new Platform(yOffset); //Erstellt neue Platform mit entsprechendem Abstand
  }
}

function gameOver() {
  // Behandelt wenn ein Spieler verliert
  if (player.getY() >= height) {
    // Ueberprueft ob der Spieler den unteren Spielrand beruehrt
    highScore = max(score, highScore); // Falls Score > Highscore wird neuer Highscore gesetzt
    score = 0; // Score wird zurueck gesetzt
    framerate = 120; // Framerate wird zurueck gesetzt
    player.y = height - 700;
  }
}

function drawScore() {
  // Zeigt Score an
  fill("#00ff00");
  textSize(20); // Textgroesse
  text("Score: " + score, 10, 20); // Setzt Scoretext
  text("High-Score:" + max(score, highScore), 10, 40); // Setzt Highscoretext
}

function inBetween(val, min, limit) {
  // Prueft ob val zwischen min und limit liegt
  return min <= val && val <= min + limit;
}

function keyPressed() {
  // Ist eine Taste gedrückt
  if (keyCode == LEFT_ARROW) {
    // Ist gedrückte Taste linke Pfeiltaste
    player.setX(player.getX() - player.getVX()); // X position wird nach links verschoben
  } else if (keyCode == RIGHT_ARROW) {
    // Ist gedrückte Taste rechte Pfeiltaste
    player.setX(player.getX() + player.getVX()); // X position wird nach rechts verschoben
  }
}
