class Player {
  // Attribute Spieler:in
  private String name;
  private Table table;

  // Konstruktor
  public Player(String name) {
    this.name = name;
    table = new Table();
  }

  // Zeichnen des Spielfeldes mit beiden Grids
  public void drawPlayer() {
    table.drawGrids(name);
  }

  // Spieler:in setzt ein Schiff
  public void setShip(int x, int y, boolean own) {
    table.setShip(x, y, own);
  }

  // Spieler:in setzt einen Schuss
  public void shoot(int x, int y, boolean move) {
    table.shoot(x, y, move);
  }

  // Zählen der Treffer um Spielende prüfen zu können
  public int countHits() {
    int count = 0;
    for (int i = 0; i < table.getFleet(false).length; i++) {
      if (table.getFleet(false)[i] == 'X') {
        count++;
      }
    }
    return count;
  }

  // Prüfen auf Spielende (maximal 30 Treffer zu setzen)
  public boolean playerWon() {
    return (countHits() == table.getEnemyShipCount());
  }
}
