// Erstellen der beiden Spieler:innen
Player a = new Player("A");
Player b = new Player("B");

// Zählervariable für die Spielphase
// Phase       0  1  2  3  4  5  6  7  8  9  10 ...
// Spieler:in  A  -  B  -  A  -  B  -  A  -  B  ...
// Phasen 0, 2: Setzen der Schiffe
// Phasen ab 4: Schüsse abgeben
// A, B : Anzeigen Spielfeld Spieler:in A/B
// '-'  : Zeigt weißen Bildschirm beim Wechseln der Spieler:innen
int stage = 0;

void setup() {
  size(1000, 750);
}

void draw() {
  background(playerColor());
  showPlayerTable();
  drawCommands();
  readyButton();
  curtain();
  gameOver();
}

// Anzeigen des gesamten Spielfelder eines Spielers/ einer Spielerin
void showPlayerTable() {
  // Spieler:in A
  if (stage%4 == 0) {
    a.drawPlayer();
  }
  // Spieler:in B
  else if (stage%4 == 2) {
    b.drawPlayer();
  }
  // "Vorhang" beim Wechsel der Spieler:innen
  else {
    background(255);
  }
}

void mousePressed() {
  int x, y;

  // Auswählen der Zellen zum Setzten von Schiffen
  // das Grid befindet spannt sich von 80 bis 480 (x-Achse) und 120 bis 520 (y-Achse)
  if (mouseX > 80 && mouseX < 480 && mouseY > 120 && mouseY < 520) {
    // Berechnen des Index für Feld
    x = (mouseX-80) / 40;
    y = (mouseY-120) / 40;

    // Spieler:in A setzt Schiffe
    if (stage == 0) {
      a.setShip(x, y, true);
      b.setShip(x, y, false);
    }

    // Spieler:in B setzt Schiffe
    else if (stage == 2) {
      b.setShip(x, y, true);
      a.setShip(x, y, false);
    }
  }

  // Auswählen von Feldern im gegnerischen Grid zum Beschießen
  // das Grid befindet spannt sich von 520 bis 960 (x-Achse) und 120 bis 520 (y-Achse)
  if (mouseX > 520 && mouseX < 960 && mouseY > 120 && mouseY < 520) {
    x = (mouseX-560) / 40;
    y = (mouseY-120) / 40;

    // Spieler:in A gibt Schuss ab
    if (stage >= 4 && stage%4 == 0) {
      a.shoot(x, y, true);
      b.shoot(x, y, false);
    }
    // Spieler:in B setzt Schuss ab
    else if (stage >= 4 && stage%4 == 2) {
      b.shoot(x, y, true);
      a.shoot(x, y, false);
    }
  }

  // Betätigen des READY-Buttons
  if (mouseX > 40 && mouseX < 140 && mouseY > 600 && mouseY < 650 && !gameOver()) {
    stage++;
  }
}

// Zeichnen des READY-Buttons
void readyButton() {
  fill(200);
  rect(40, 600, 100, 50);
  fill(0);
  textAlign(CENTER);
  textSize(20);
  text("READY", 90, 635);
}


// Anzeigen der Spielanweisungen
void drawCommands() {
  fill(0);
  textSize(30);
  textAlign(LEFT);
  if (stage == 0 || stage == 2) {
    text("Setzen Sie Ihre Schiffe!", 40, 575);
    text("Ihre Flotte:", 500, 575);
    textSize(20);
    text("1 x Schlachtschiff (5 Kästchen)", 500, 605);
    text("2 x Kreuzer (je 4 Kästchen)", 500, 630);
    text("3 x Zerstörer (je 3 Kästchen)", 500, 655);
    text("4 x U-Boot (je 2 Kästchen)", 500, 680);
  }
  if (stage >= 4 && (stage%4 == 0 || stage%4 == 2)) {
    text("Wählen Sie eine Zelle zum Beschuss aus!", 40, 575);
  }
}

// Prüft auf Ende des Spiels
boolean gameOver() {
  if (stage >= 4 && (a.playerWon() || b.playerWon())) {
    textSize(100);
    textAlign(CENTER);
    fill(0);
    if (stage%4 == 0) {
      text("Player " + a.name + " won!!!", width/2, height/2);
    } else {
      text("Player " + b.name + " won!!!", width/2, height/2);
    }
    noLoop();
    return true;
  } else {
    return false;
  }
}

// Wechseln der Farbe zur besseren Unterscheidung der Spieler:innen
int playerColor() {
  if (stage%4==0) {
    return 255;
  } else if (stage%4 == 2) {
    return 100;
  } else {
    return 0;
  }
}

// Zeichnen des "Vorhangs" beim Wechseln der Spieler:innen
void curtain() {
  fill(0);
  textSize(75);
  textAlign(CENTER);
  if (stage%4 == 3) {
    text("Wait for player " + a.name, width/2, height/2);
  } else if (stage%4 == 1) {
    text("Wait for player " + b.name, width/2, height/2);
  }
}
