class Table {
  // Variablen zum Zeichnen des Spielfeldes
  private int numbOfRows = 10;
  private int cellSize = 40;
  private int gridShift = 12 * cellSize;
  private int leftBorder = cellSize;
  private int topBorder = 2*cellSize;

  // Flotten der Spieler
  private char[] ownFleet;
  private char[] enemyFleet;

  // Anzahl der für Schiffe gesetzten Felder des Gegners (zur Kontrolle für GameOver)
  int enemyShipCount = 0;

  // Konstruktor
  public Table() {
    ownFleet = new char[numbOfRows*numbOfRows];
    enemyFleet = new char[numbOfRows*numbOfRows];

    // Initialisierung beider Flotten-Arrays mit '-' (= leeres Feld, auf das nicht geschossen wurde)
    for (int i = 0; i < ownFleet.length; i++) {
      ownFleet[i] = '-';
      enemyFleet[i] = '-';
    }
  }

  // Zeichnen des gesamten Spielfeldes mit beiden Grids
  public void drawGrids(String name) {
    fill(0);
    textAlign(LEFT);
    textSize(30);
    // Anzeigen des Spielers
    text("Player: " + name, leftBorder, 30);

    drawOwnGrid();
    drawEnemyGrid();
  }

  // Zeichnen des eigenen Grids
  private void drawOwnGrid() {
    textSize(20);
    text("My fleet", leftBorder, 60);

    for (int i = 0; i < numbOfRows; i++) {
      for (int j = 0; j < numbOfRows; j++) {
        // Feld enthält Schiff, auf das noch nicht geschossen wurde (-> grau)
        if (ownFleet[i*10+j] == 'S') {
          fill(200);
        }
        // Feld enthält Schiff, das getroffen wurde (-> rot)
        else if (ownFleet[i*10+j] == 'X') {
          fill(255, 0, 0);
        }
        // Feld ist leer und wurde nicht beschossen (-> weiß)
        else {
          fill(255);
        }
        square(leftBorder + (j+1)*cellSize, topBorder + (i+1)*cellSize, cellSize);
      }
    }

    // Einzeichnen der Spalten und Zeilen für die Koordinaten
    for (int i = 0; i <= numbOfRows; i++) {
      fill(200);
      square(leftBorder+ i*cellSize, topBorder, cellSize);
      square(leftBorder, topBorder + i*cellSize, cellSize);
    }

    // Beschriften der Koordinatenfelder
    fill(0);
    textSize(30);
    textAlign(CENTER);
    for (int i = 0; i < numbOfRows; i++) {
      text(i+1, leftBorder + 0.5*cellSize, topBorder + (i+1.75)*cellSize);
      text(char(65+i), leftBorder+(i+1.5)*cellSize, topBorder + 0.75*cellSize);
    }
  }

  // Zeichnen des gegnerischen Grids
  private void drawEnemyGrid() {
    fill(0);
    textAlign(LEFT);
    textSize(20);
    text("Enemy fleet", gridShift + leftBorder, 60);

    for (int i = 0; i < numbOfRows; i++) {
      for (int j = 0; j < numbOfRows; j++) {
        // Feld wurde beschossen, enthält aber kein Schiff (-> blau)
        if (enemyFleet[i*10+j] == 'O') {
          fill(0, 0, 255);
        }
        // Feld enthält Schiff und wurde beschossen (-> rot)
        else if (enemyFleet[i*10+j] == 'X') {
          fill(255, 0, 0);
        }
        // Feld ist leer und wurde nicht beschossen (-> weiß)
        else {
          fill(255);
        }
        square(leftBorder + gridShift + (j+1)*cellSize, topBorder + (i+1)*cellSize, cellSize);
      }
    }

    // Einzeichnen der Spalten und Zeilen für die Koordinaten
    fill(200);
    for (int i = 0; i <= numbOfRows; i++) {
      square(leftBorder + gridShift + i*cellSize, topBorder, cellSize);
      square(leftBorder + gridShift, topBorder + i*cellSize, cellSize);
    }

    // Beschriften der Koordinatenfelder
    fill(0);
    textSize(30);
    textAlign(CENTER);
    for (int i = 0; i < numbOfRows; i++) {
      text(i+1, gridShift + leftBorder + 0.5*cellSize, topBorder + (i+1.75)*cellSize);
      text(char(65+i), gridShift + leftBorder+(i+1.5)*cellSize, topBorder + 0.75*cellSize);
    }
  }

  // Schiffe setzen
  public void setShip(int x, int y, boolean move) {
    // wenn der Spieler am Zug ist (move == True), wird das Schiff im eigenene Flotten-Array (ownFleet) gesetzt
    if (move) {
      // wenn das Feld leer ist, wird ein Schiff gesetzt (- -> S)
      if (ownFleet[y*10+x] == '-') {
        ownFleet[y*10+x] = 'S';
      }
      // wenn bereits ein Schiff gesetzt ist, wird es entfernt (S -> -)
      else if (ownFleet[y*10+x] == 'S') {
        ownFleet[y*10+x] = '-';
      }
    }
    // wenn der Gegner am Zug ist (move == False), wird das Schiff im gegnerischen Flotten-Array (enemyFleet) gesetzt
    else {
      // wenn das Feld leer ist, wird ein Schiff gesetzt (- -> S)
      if (enemyFleet[y*10+x] == '-') {
        enemyFleet[y*10+x] = 'S';
        enemyShipCount++;
      }
      // wenn bereits ein Schiff gesetzt ist, wird es entfernt (S -> -)
      else if (enemyFleet[y*10+x] == 'S') {
        enemyFleet[y*10+x] = '-';
        enemyShipCount--;
      }
    }
  }

  // Schuss abgeben
  public void shoot(int x, int y, boolean move) {
    // wenn der Spieler am Zug ist (move == True), wird der Schuss im gegnerischen Flotten-Array (enemyFleet) gesetzt
    if (move) {
      // wenn dort ein Schiff ist, setze einen Treffer (S -> X)
      if (enemyFleet[y*10+x] == 'S') {
        enemyFleet[y*10+x] = 'X';
      }
      // wenn dort kein Schiff ist, setze ein Miss (- -> O)
      else if (enemyFleet[y*10+x] == '-') {
        enemyFleet[y*10+x] = 'O';
        stage++;
      }
    }
    // ist der Gegner am Zug, müssen dessen Schüsse gleichzeitig auch im eigenen Flotten-Array (ownFleet) vermerkt werden, um diese richtig darstellen zu können
    else {
      // wenn dort ein Schiff ist, setze einen Treffer (S -> X)
      if (ownFleet[y*10+x] == 'S') {
        ownFleet[y*10+x] = 'X';
      }
      // wenn dort kein Schiff ist, setze einen Miss (- -> O)
      else {
        ownFleet[y*10+x] = 'O';
      }
    }
  }

  // Rückgabe des geforderten Grids
  public char[] getFleet(boolean own) {
    if (own) {
      return ownFleet;
    } else {
      return enemyFleet;
    }
  }

  // gibt die Anzahl der vom Gegner gesetzten Schiffe zurück (damit ermittelt werden kann, ob das Spiel vorbei ist)
  public int getEnemyShipCount() {
    return enemyShipCount;
  }
}
