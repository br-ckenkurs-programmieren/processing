let a;
let b;

let stage = 0;

let numbOfRows = 10;
let cellSize = 40;
let gridShift = 12 * cellSize;
let leftBorder = cellSize;
let topBorder = 2*cellSize;

class Table{
  constructor(){
    this.ownFleet = [];
    this.enemyFleet = [];
    this.enemyShipCount = 0;
    for (let i = 0; i < (numbOfRows*numbOfRows); i++) {
      this.ownFleet[i] = '-';
      this.enemyFleet[i] = '-';
    }
  }

  drawOwnGrid() {
    textSize(20);
    text("My fleet", leftBorder, 60);

    for (let i = 0; i < numbOfRows; i++) {
      for (let j = 0; j < numbOfRows; j++) {
        // Feld enthält Schiff, auf das noch nicht geschossen wurde (-> grau)
        if (this.ownFleet[i*10+j] == 'S') {
          fill(200);
        } 
        // Feld enthält Schiff, das getroffen wurde (-> rot)
        else if (this.ownFleet[i*10+j] == 'X') {
          fill(255, 0, 0);
        } 
        // Feld ist leer und wurde nicht beschossen (-> weiß)
        else {
          fill(255);
        }
        square(leftBorder + (j+1)*cellSize, topBorder + (i+1)*cellSize, cellSize);
      }
    }

    // Einzeichnen der Spalten und Zeilen für die Koordinaten
    for (let i = 0; i <= numbOfRows; i++) {
      fill(200);
      square(leftBorder+ i*cellSize, topBorder, cellSize);
      square(leftBorder, topBorder + i*cellSize, cellSize);
    }

    // Beschriften der Koordinatenfelder
    fill(0);
    textSize(30);
    textAlign(CENTER);
    for (let i = 0; i < numbOfRows; i++) {
      text(i+1, leftBorder + 0.5*cellSize, topBorder + (i+1.75)*cellSize);
      text(char(65+i), leftBorder+(i+1.5)*cellSize, topBorder + 0.75*cellSize);
    }
  }

  // Zeichnen des gegnerischen Grids 
  drawEnemyGrid() {
    fill(0);
    textAlign(LEFT);
    textSize(20);
    text("Enemy fleet", gridShift + leftBorder, 60);

    for (let i = 0; i < numbOfRows; i++) {
      for (let j = 0; j < numbOfRows; j++) {
        // Feld wurde beschossen, enthält aber kein Schiff (-> blau) 
        if (this.enemyFleet[i*10+j] == 'O') {
          fill(0, 0, 255);
        } 
        // Feld enthält Schiff und wurde beschossen (-> rot)
        else if (this.enemyFleet[i*10+j] == 'X') {
          fill(255, 0, 0);
        } 
        // Feld ist leer und wurde nicht beschossen (-> weiß)
        else {
          fill(255);
        }
        square(leftBorder + gridShift + (j+1)*cellSize, topBorder + (i+1)*cellSize, cellSize);
      }
    }

    // Einzeichnen der Spalten und Zeilen für die Koordinaten
    fill(200);
    for (let i = 0; i <= numbOfRows; i++) {
      square(leftBorder + gridShift + i*cellSize, topBorder, cellSize);
      square(leftBorder + gridShift, topBorder + i*cellSize, cellSize);
    }
    
    // Beschriften der Koordinatenfelder
    fill(0);
    textSize(30);
    textAlign(CENTER);
    for (let i = 0; i < numbOfRows; i++) {
      text(i+1, gridShift + leftBorder + 0.5*cellSize, topBorder + (i+1.75)*cellSize);
      text(char(65+i), gridShift + leftBorder+(i+1.5)*cellSize, topBorder + 0.75*cellSize);
    }
  }
  
  // Zeichnen des gesamten Spielfeldes mit beiden Grids
 drawGrids(ply) {
    fill(0);
    textAlign(LEFT);
    textSize(30);
    // Anzeigen des Spielers
    text("Player: " + ply, leftBorder, 30);

    this.drawOwnGrid();
    this.drawEnemyGrid();
  }

  // Schiffe setzen
  setShip(x, y, move) {
    // wenn der Spieler am Zug ist (move == True), wird das Schiff im eigenene Flotten-Array (this.ownFleet) gesetzt
    if (move) {
      // wenn das Feld leer ist, wird ein Schiff gesetzt (- -> S)
      if (this.ownFleet[y*10+x] == '-') {
        this.ownFleet[y*10+x] = 'S';
      } 
      // wenn bereits ein Schiff gesetzt ist, wird es entfernt (S -> -)
      else if (this.ownFleet[y*10+x] == 'S') {
        this.ownFleet[y*10+x] = '-';
      }
    }
    // wenn der Gegner am Zug ist (move == False), wird das Schiff im gegnerischen Flotten-Array (this.enemyFleet) gesetzt
    else {
      // wenn das Feld leer ist, wird ein Schiff gesetzt (- -> S)
      if (this.enemyFleet[y*10+x] == '-') {
        this.enemyFleet[y*10+x] = 'S';
        this.enemyShipCount++;
      } 
      // wenn bereits ein Schiff gesetzt ist, wird es entfernt (S -> -)
      else if (this.enemyFleet[y*10+x] == 'S') {
        this.enemyFleet[y*10+x] = '-';
        this.enemyShipCount--;
      }
    }
  }

  // Schuss abgeben
  shoot(x, y, move) {
    // wenn der Spieler am Zug ist (move == True), wird der Schuss im gegnerischen Flotten-Array (this.enemyFleet) gesetzt
    if (move) {
      // wenn dort ein Schiff ist, setze einen Treffer (S -> X)
      if (this.enemyFleet[y*10+x] == 'S') {
        this.enemyFleet[y*10+x] = 'X';
      }
      // wenn dort kein Schiff ist, setze ein Miss (- -> O)
      else if (this.enemyFleet[y*10+x] == '-') {
        this.enemyFleet[y*10+x] = 'O';
        stage++;
      }
    }
    // ist der Gegner am Zug, müssen dessen Schüsse gleichzeitig auch im eigenen Flotten-Array (this.ownFleet) vermerkt werden, um diese richtig darstellen zu können
    else {
      // wenn dort ein Schiff ist, setze einen Treffer (S -> X)
      if (this.ownFleet[y*10+x] == 'S') {
        this.ownFleet[y*10+x] = 'X';
      }
      // wenn dort kein Schiff ist, setze einen Miss (- -> O)
      else {
        this.ownFleet[y*10+x] = 'O';
      }
    }
  }

  // Rückgabe des geforderten Grids
  getFleet(own) {
    if (own) {
      return this.ownFleet;
    } else {
      return this.enemyFleet;
    }
  }

  // gibt die Anzahl der vom Gegner gesetzten Schiffe zurück (damit ermittelt werden kann, ob das Spiel vorbei ist) 
  getEnemyShipCount() {
    return this.enemyShipCount;
  }
}

class Player {
// Konstruktor
  constructor(name) {
    this.name = name;
    this.table = new Table();
  }

  // Zeichnen des Spielfeldes mit beiden Grids
  drawPlayer() {
    this.table.drawGrids(this.name);
  }

  // Spieler:in setzt ein Schiff
  setShip(x, y, own) {
    this.table.setShip(x, y, own);
  }

  // Spieler:in setzt einen Schuss
  shoot(x, y, move) {
    this.table.shoot(x, y, move);
  }

  // Zählen der Treffer um Spielende prüfen zu können
  countHits() {
    let count = 0;
    for (let i = 0; i < this.table.getFleet(false).length; i++) {
      if (this.table.getFleet(false)[i] == 'X') {
        count++;
      }
    }
    return count;
  }

  // Prüfen auf Spielende (maximal 30 Treffer zu setzen)
  playerWon() {
    return (this.countHits() == this.table.getEnemyShipCount());
  }
}



function setup() {
  createCanvas(1000, 750);
  // Erstellen der beiden Spieler:innen
  a = new Player("A");
  b = new Player("B");
}

function draw() {
  background(playerColor());
  showPlayerTable();
  drawCommands();
  readyButton();
  curtain();
  gameOver();
}

// Anzeigen des gesamten Spielfelder eines Spielers/ einer Spielerin
function showPlayerTable() {
  // Spieler:in A
  if (stage%4 == 0) {
    a.drawPlayer();
  }
  // Spieler:in B
  else if (stage%4 == 2) {
    b.drawPlayer();
  }
  // "Vorhang" beim Wechsel der Spieler:innen
  else {
    background(255);
  }
}

function mousePressed() {
  // Auswählen der Zellen zum Setzten von Schiffen
  // das Grid befindet spannt sich von 80 bis 480 (x-Achse) und 120 bis 520 (y-Achse)
  if (mouseX > 80 && mouseX < 480 && mouseY > 120 && mouseY < 520) {
    // Berechnen des Index für Feld
    let x = int((mouseX-80) / 40);
    let y = int((mouseY-120) / 40);

    // Spieler:in A setzt Schiffe
    if (stage == 0) {
      a.setShip(x, y, true);
      b.setShip(x, y, false);
    }

    // Spieler:in B setzt Schiffe
    else if (stage == 2) {
      b.setShip(x, y, true);
      a.setShip(x, y, false);
    }
  }

  // Auswählen von Feldern im gegnerischen Grid zum Beschießen
  // das Grid befindet spannt sich von 520 bis 960 (x-Achse) und 120 bis 520 (y-Achse)
  if (mouseX > 520 && mouseX < 960 && mouseY > 120 && mouseY < 520) {
    x = int((mouseX-560) / 40);
    y = int((mouseY-120) / 40);

    // Spieler:in A gibt Schuss ab
    if (stage >= 4 && stage%4 == 0) {
      a.shoot(x, y, true);
      b.shoot(x, y, false);
    }
    // Spieler:in B setzt Schuss ab
    else if (stage >= 4 && stage%4 == 2) {
      b.shoot(x, y, true);
      a.shoot(x, y, false);
    }
  }

  // Betätigen des READY-Buttons
  if (mouseX > 40 && mouseX < 140 && mouseY > 600 && mouseY < 650 && !gameOver()) {
    stage++;
  }
}

// Zeichnen des READY-Buttons
function readyButton() {
  fill(200);
  rect(40, 600, 100, 50);
  fill(0);
  textAlign(CENTER);
  textSize(20);
  text("READY", 90, 635);
}


// Anzeigen der Spielanweisungen
function drawCommands() {
  fill(0);
  textSize(30);
  textAlign(LEFT);
  if (stage == 0 || stage == 2) {
    text("Setzen Sie Ihre Schiffe!", 40, 575);
    text("Ihre Flotte:", 500, 575);
    textSize(20);
    text("1 x Schlachtschiff (5 Kästchen)", 500, 605);
    text("2 x Kreuzer (je 4 Kästchen)", 500, 630);
    text("3 x Zerstörer (je 3 Kästchen)", 500, 655);
    text("4 x U-Boot (je 2 Kästchen)", 500, 680);
  }
  if (stage >= 4 && (stage%4 == 0 || stage%4 == 2)) {
    text("Wählen Sie eine Zelle zum Beschuss aus!", 40, 575);
  }
}

// Prüft auf Ende des Spiels
function gameOver() {
  if (stage >= 4 && (a.playerWon() || b.playerWon())) {
    textSize(100);
    textAlign(CENTER);
    fill(0);
    if (stage%4 == 0) {
      text("Player " + a.name + " won!!!", width/2, height/2);
    } else {
      text("Player " + b.name + " won!!!", width/2, height/2);
    }
    noLoop();
    return true;
  } else {
    return false;
  }
}

// Wechseln der Farbe zur besseren Unterscheidung der Spieler:innen
function playerColor() {
  if (stage%4==0) {
    return 255;
  } else if (stage%4 == 2) {
    return 100;
  } else {
    return 0;
  }
}

// Zeichnen des "Vorhangs" beim Wechseln der Spieler:innen
function curtain() {
  fill(0);
  textSize(75);
  textAlign(CENTER);
  if (stage%4 == 3) {
    text("Wait for player " + a.name, width/2, height/2);
  } else if (stage%4 == 1) {
    text("Wait for player " + b.name, width/2, height/2);
  }
}