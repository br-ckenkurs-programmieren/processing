var center;

var r;

var n;

function setup() {
    initializeFields();
    createCanvas(800, 600);
    center = createVector(width / 2, height / 2);
}

// Schiebt einen Punkt auf einer Kreisbahn um den Winkel angle weiter.
function rotZ(pos, angle) {
    // Verschiebung auf x
    var x = pos.x * cos(radians(angle)) + pos.y * sin(radians(angle));
    // Verschiebung auf y
    var y = pos.x * -sin(radians(angle)) + pos.y * cos(radians(angle));
    return createVector(x, y);
}

function draw() {
    // Je weiter rechts der Mauszeiger ist, in desto mehr Teile wird der Kreis aufgeteilt.
    n = mouseX / (width / 30) + 2;
    background(0);
    noFill();
    stroke(255);
    strokeWeight(4);
    circle(center.x, center.y, 2 * r);
    // Die Verschiebung, die einen Punkt vom Mittelpunkt auf den Zirkel unseres Kreises setzt.
    var p = createVector(0, -r);
    for (var i = 0; i < n; i++) {
        stroke(255);
        // zeichnet die durch den Kreis führende Linie, um ihn in Dreiecke einzuteilen
        line(center.x, center.y, p.x + center.x, p.y + center.y);
        // Wir speichern den aktuellen Punkt auf dem Zirkel als alten Punkt
        var pPrev = p;
        // Wir wandern einen Schritt weiter entlang des Zirkels.
        // Unser neuer Punkt ist dazu auf dem Zirkel um den Grad 2pi/n == 360/n zu verschieben.
        p = rotZ(p, 360 / n);
        // rote Linien, die dem Umfang des Kreises nachzeichnen
        stroke(255, 0, 0);
        // Die Linien zeichnen wir vom letzten Punkt auf dem Zirkel zum aktuellen Punkt auf dem Zirkel.
        line(pPrev.x + center.x, pPrev.y + center.y, p.x + center.x, p.y + center.y);
    }
    // Unser aktueller Winkel der Dreiecke nach der Formel 2pi/n == 360/n
    var alpha = 360 / (2 * n);
    // Errechnet die Länge der (bei uns in rot dargestellte) Gegenkathete zu a der rechtwinkligen Dreiecke
    var opposite = sin(radians(alpha)) * r;
    // aktuell errechneter Umfang aus den Gegenkatheten zu a der rechtwinkligen Dreiecke (von den es n*2 gibt)
    var circumference = opposite * 2 * n;
    // Errechnung von pi
    var pi = circumference / (2 * r);
    // Ausgabe des für pi errrechneten Wertes im Fenster
    textSize(100);
    text(pi, 0, 100);
    // Ausdrucken des Wertes in der Konsole
    print(pi + "\n");
}

function initializeFields() {
    center = null;
    r = 200;
    n = 2;
}
