PVector center;

float r = 200;
int n = 2;

void setup() {
  size(800, 600);
  center = new PVector(width/2, height/2);
}

// Schiebt einen Punkt auf einer Kreisbahn um den Winkel angle weiter.
PVector rotZ(PVector pos, float angle) {
  //Verschiebung auf x
  float x = pos.x * cos(radians(angle)) + pos.y * sin(radians(angle));
  //Verschiebung auf y
  float y = pos.x * -sin(radians(angle)) + pos.y * cos(radians(angle));
  return new PVector(x, y);
}

void draw() {
  //Je weiter rechts der Mauszeiger ist, in desto mehr Teile wird der Kreis aufgeteilt.
  n = mouseX / (width/30) + 2;

  
  background(0);
  noFill();
  stroke(255);
  strokeWeight(4);

  circle(center.x, center.y, 2*r);
  //  Die Verschiebung, die einen Punkt vom Mittelpunkt auf den Zirkel unseres Kreises setzt.
  PVector p = new PVector(0, -r);
  for (int i = 0; i<n; i++) {
    stroke(255);
    // zeichnet die durch den Kreis führende Linie, um ihn in Dreiecke einzuteilen
    line(center.x, center.y, p.x+center.x, p.y+center.y);
    
    // Wir speichern den aktuellen Punkt auf dem Zirkel als alten Punkt
    PVector pPrev = p;
    // Wir wandern einen Schritt weiter entlang des Zirkels. 
    // Unser neuer Punkt ist dazu auf dem Zirkel um den Grad 2pi/n == 360/n zu verschieben.
    p = rotZ(p, 360/n);
    
    // rote Linien, die dem Umfang des Kreises nachzeichnen
    stroke(255,0,0);
    // Die Linien zeichnen wir vom letzten Punkt auf dem Zirkel zum aktuellen Punkt auf dem Zirkel.
    line(pPrev.x+center.x, pPrev.y+center.y, p.x+center.x, p.y+center.y);
  }

  // Unser aktueller Winkel der Dreiecke nach der Formel 2pi/n == 360/n
  float alpha = 360/(2*n); 
  // Errechnet die Länge der (bei uns in rot dargestellte) Gegenkathete zu a der rechtwinkligen Dreiecke 
  float opposite = sin(radians(alpha))*r;
  // aktuell errechneter Umfang aus den Gegenkatheten zu a der rechtwinkligen Dreiecke (von den es n*2 gibt)
  float circumference = opposite * 2*n;
  // Errechnung von pi
  float pi = circumference / (2*r);
  // Ausgabe des für pi errrechneten Wertes im Fenster
  textSize(100);
  text(pi, 0, 100);
  //Ausdrucken des Wertes in der Konsole
  print(pi + "\n");
}
