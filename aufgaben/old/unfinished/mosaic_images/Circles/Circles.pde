//Inspired by Daniel Shiffman 
//https://www.youtube.com/watch?v=QHEQuoIKgNE

ArrayList<Circle> circles;
PImage image;
Circle c;
float growSpeed;
int resolution, maxAttempts;

void setup() {
  size(1100, 1100);
  circles = new ArrayList<Circle>();
  image = loadImage("Voxel_amanita.png");
  image.resize(width, height);
  image.loadPixels();
  growSpeed = 0.125;
  resolution = 200;
  maxAttempts = 2000;
}

void draw() {
  background(20);

  int total = resolution;
  int count = 0;
  int attempts = 0;

  while (count < total) {
    c = newCircle();
    if (c != null) {
      circles.add(c);
      count++;
    }
    attempts++;
    if (attempts > maxAttempts) {
      noLoop();
      println("Finished");  
      break;
    }
  }

  for (Circle c : circles) {
    if (c.grow) {
      if (c.edges()) {
        c.grow = false;
      } else {
        for (Circle other : circles) {
          if (c != other) {
            float d = dist(c.x, c.y, other.x, other.y);
            if (d < c.r + other.r) {
              c.grow = false;
              break;
            }
          }
        }
      }
    }
    c.show();
    c.grow();
  }
}

void mousePressed() {
  saveFrame("screenshot.jpg");
}

Circle newCircle() {

  int x = int(random(image.width));
  int y = int(random(image.height));
  boolean valid = true;

  for (Circle c : circles) {
    float d = dist(x, y, c.x, c.y);
    if (d < c.r + 3) {
      valid = false;
      break;
    }
  }
  
  int index = int(x + y * image.width);
  color c = image.pixels[index];

  if (valid) {
    return new Circle(x, y, c);
  } else {
    return null;
  }
}
