# Contributing

## Generelle Richtlinien

Dieses Repo macht von Tags und anderen GitLab-Funktionen gebrauch, um die Funktionalität und Anwendungserfahrung zu erweitern.
Besonders beim Bearbeiten von Tickets sind folgende Regeln zu beachten:

1. Ein Ticket steht für eine bestimmte Aufgabe, und folgt dem Format `<tag>/<ID>: <Name>`.
2. Es gibt drei Tags, welche den Zustand einer Aufgabe deutlich darstellen sollen:
	1. **Aufgabenstellung fehlt / unbefriedigend**: Diese Aufgabe ist entweder nicht angefangen oder in einem unfertigen oder unbefriedigendem Zustand. 
		Sie muss komplett überarbeitet werden.
	2. **Code fehlt / unbefriedigend**: Diese Aufgabe hat eine solide Aufgabenstellung, aber der Code ist verbesserungsbedürftig oder fehlt.
	3. **Musterlösung fehlt**: eine Formalität - vorhandener Code muss nur noch als Musterlösung dokumentiert werden.
3. Eine Aufgabe mit unfertiger Aufgabenstellung benötigt offensichtlich auch noch (zumindest aktualisierten) Code und eine Musterlösung.
	Dieser sollte beim Verfassen der Aufgabenstellung programmiert werden.
	Aus Erfahrung hat sich gezeigt, dass Aufgabenstellungen, welche ohne begleitende Musterlösung erstellt wurden, selten solide waren.
4. Eine Aufgabe, die vom Bearbeitenden als fertig erachtet wurde kann mit dem Tag `Ready` markiert werden.
	1. In der Zeit des Brückenkurses wird das Merge-Request nach einem Review entweder übernommen und das Tickt geschlossen, oder weitere Änderungen angefragt.
	2. Außerhalb dieser Zeit genügt es, ein Merge-Request einzureichen, auf welches dann Feedback gegeben wird, falls notwendig.
5. Außer absolut unmöglich, sollten Aufgaben in `.tex`-Dokumenten verfasst und bearbeitet werden.
	Im Notfall ist eine Markdown-Datei akzeptabel, machen Sie sich jedoch bewusst, dass damit die Arbeit auf jemand anderen fällt, Ihr Ticket zu vollenden.
6. Falls sinnvoll ist es empfohlen, auch eine p5js-Version des Codes einer Aufgabe zu erstellen, und ein Bild mit Play-Button zu implementieren.
	Dabei können online-Converter helfen, oft wird allerdings Nachbearbeitung benötigt (besonders bei Klassen).
	
> Zum Bearbeiten von Aufgaben liegt in /editing/ die Datei [template.tex](/aufgaben/editing/template.tex).
	Dort kann gezielt eine einzelne Aufgabe geladen und kompiliert werden.
	Weitere Informationen sind in der Datei.
	
	
## Codestil

Beim Arbeiten mit den Codedateien für den Brückenkurs Programmieren gibt es einige Richtlinien zu verfolgen, um Processing-Code und TeX-Quellcode einheitlich und verständlich zu gestalten.

### Processing

Die Processing IDE gibt in limitierter Kapazität bereits einen Codestil vor, welcher mit `STRG` + `T` abgerufen werden kann.
Dieses soll zur Verbesserung von Codeverständnis mit einem IntelliJ Autoformat - ähnlichen Stil erweitert werden.
Die wichtigsten Punkte:

1. Zwischen Literalen / Keywords und Operatoren sollen Leerzeichen:
    * `int a=1` -> `int a = 1`
    * `for(int i=1;i<10;i++)` -> `for (int i = 1; i < 10; i++)`
2. Zwischen Anweisungen und Klammern sollen Leerzeichen (hier sind eigentlich auch Zeilenumbrüche):
    * `if( ... ){ ... }` -> `if ( ... ) { ... }`
    * `while(true){ ... }` -> `while (true) { ... }`
3. Der Zeilenumbruch, falls vorhanden, soll hinter der öffnenden geschweiften Klammer geschehen:
	
    ```java
    if ( ... )
    {
      ...
    }
    ```

    ... wird zu ...

    ```java
    if ( ... ) {
        ...
    }
    ```

4. Kommt nach einer Anweisung zur Verzweigung nur eine Zeile Code, können die geschweiften Klammern wegfallen. 
    Ist der Code nicht sinnvoll in nur eine Zeile zu bringen, sollte dies bitte nicht erzwungen sein.
    * `for ( ... ) { a++; }` ->  `for ( ... ) a++;`
    * `if ( a ) { return true; }` -> `if ( a ) return true;` oder falls möglich  `return a;`
5. Code sollte so formuliert sein, dass er für nicht-Erstis auf dem ersten Blick verständlich ist. Das bedeutet:
    * Keine tiefen Verschachtelungen
    * Sinnvolle Variablen- und Methodennamen
    * Kommentare, wo sie sinnvoll sind
6. Code muss Englisch sein, Kommentare *können* in Deutsch verfasst werden.
7. Kommentare sollten nicht das kommentieren, was der Code bereits offensichtlich tut: `a++; // erhöht a um eins`.
    Nicht jede Zeile sollte einen Kommentar bekommen, nur um einen Kommentar zu haben.

### LaTeX

Für das Schreiben von LaTeX-Quellcode gibt es eine ausführliche Einführung mit dem Dokument [How To LaTeX](/dokumente/How%20To%20LaTeX/How%20To%20LaTeX.pdf).
Sind Sie schon mit LaTeX vertraut, hier die wichtigsten Punkte:

1. Ein neuer Satz muss auf einer neuen Zeile beginnen. 
    LaTeX rendert einfache Zeilenumbrüche nicht, Git diff wird aber davon profitieren.
    Innerhalb von Macros (also `\bla{text}`) kann der Text auf der neuen Zeile eingerückt werden, um Lesbarkeit zu verbessern.
2. Verwenden Sie bitte vorhandene Macros.
    Dazu hilft nichts anderes als How To LaTeX anschauen und ggf. vorliegende Aufgaben betrachten, um Konsistenz aufrechtzuhalten.
3. Wir gendern mit Doppelpunkt oder Substantivierung: Studenten -> Studierende; Gießener -> Gießener:innen.

### JavaScript

JavaScript-Code wird in diesem Repo (derzeit) nur für p5js-Versionen von Processing-Code verwendet.
Deshalb ist es nicht von großer Wichtigkeit, dass der Code optimal aussieht, sondern viel mehr, dass er verlässlich funktioniert.
Das Testen mit dem p5js-Web-Editor ist ausdrücklich empfohlen.

## Dateiverwaltung

Generell ist das `.gitignore` so ausgelegt, dass die "uninteressanten" Dateien nicht übermittelt werden.
Diese Filter sollten nicht umgangen werden - wenn nötig, hierzu bitte nachfragen.

