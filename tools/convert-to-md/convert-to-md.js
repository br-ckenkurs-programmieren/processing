const fs = require('fs');
const path = require('path');

if (process.argv.length < 2 || process.argv[2] === '--help' || process.argv[2] === '-h' || process.argv[2] === '-?') {
    console.log('');
    console.log('TeX to Markdown converter for Processing -> Atlas, by @hwgn95');
    console.log('');
    console.log('Usage: node convert-to-md.js [input.tex] [output.md] [exercise id]');
    console.log('');
    console.log('Example: node convert-to-md.js input.tex output.md 1');
    console.log('');
    console.log('Notes:');
    console.log(' - If no output file is specified, the output will be written to output.md');
    console.log(' - If no exercise id is specified, the exercise id will be set to ID_HERE');
    console.log(' - Codeblock indentation is not fully supported, so you may need to fix the indentation manually.');
    console.log(' - Labels only work for headings, and must be placed right under sections to work. Labels cannot be used across multiple files.');
    console.log(' - Since this is not an actual interpreter, nested macros will not work properly.');
    console.log('');

    process.exit(0);
}

let input_file, output_file, id, input;

if (process.argv[2] !== '--recursive') {
    input_file = process.argv[2];
    output_file = process.argv[3] || 'output.md';
    id = process.argv[4] || 'ID_HERE';

    input = fs.readFileSync(input_file, 'utf8');
}

class ConversionHandler {
    constructor(input) {
        this.str = input || '';
        this.removals = [];
        this.replacements = [];
    }

    result() {
        return this.str;
    }

    removeMacro(macro) {
        this.removals.push(new RegExp(`\\\\` + macro, 'g'))
        return this;
    }

    removeMacros(macros) {
        macros.forEach(macro => this.removeMacro(macro));
        return this;
    }

    removeWithArgs(macro) {
        this.removals.push(new RegExp(`\\\\` + macro + `(?:\\{([^{}]*)})+`, 'g'))
        return this;
    }

    removeMacrosWithArgs(macros) {
        macros.forEach(macro => this.removeWithArgs(macro));
        return this;
    }

    replaceWithArgs(macro, replacement) {
        this.replacements.push([new RegExp(`\\\\` + macro + `(?:\\{([^{}]*)})(?:\\{([^{}]*)})?(?:\\{([^{}]*)})?(?:\\{([^{}]*)})?`, 'g'), replacement])
        return this;
    }

    replaceMacro(macro, replacement) {
        this.replacements.push([new RegExp(`\\\\` + macro, 'g'), replacement])
        return this;
    }

    replace(macro, replacement) {
        this.replacements.push([new RegExp(macro, 'g'), replacement])
        return this;
    }

    removeEnvironment(environment) {
        this.removals.push(new RegExp(`\\\\begin\\{` + environment + `\\}([^]*?)\\\\end\\{` + environment + `\\}`, 'g'))
        return this;
    }

    unwrapEnvironment(environment) {
        this.replacements.push([new RegExp(`\\\\begin\\{` + environment + `\\}([^]*?)\\\\end\\{` + environment + `\\}`, 'g'), '$1'])
        return this;
    }

    unwrapEnvironmentWithArgs(environment) {
        this.replacements.push([new RegExp(`\\\\begin\\{` + environment + `\\}(?:\\{(?:[^{}]*)})+([^]*?)\\\\end\\{` + environment + `\\}`, 'g'), '$1'])
        return this;
    }

    replaceEnvironment(environment, replacement) {
        this.replacements.push([new RegExp(`\\\\begin\\{` + environment + `\\}([^]*?)\\\\end\\{` + environment + `\\}`, 'g'), replacement])
        return this;
    }

    handleComments() {
        let isInCodeBlock = false;
        let lines = this.str.split('\n');
        for (let i = 0; i < lines.length; i++) {
            if (lines[i].startsWith('```')) {
                isInCodeBlock = !isInCodeBlock;
            }
            if (!isInCodeBlock) {
                if (lines[i].length === 0) continue;
                lines[i] = lines[i].replace(/(?<=^|\s)%.*/gm, '');
                if (lines[i].length === 0) {
                    // remove empty lines
                    lines.splice(i, 1);
                    i--;
                }
            }
        }
        this.str = lines.join('\n');
    }

    handleLabels() {
        const section_followed_by_label_regex = /\\(sub)*section\{([^}]*)\}(\s*)\\label\{([^}]*)\}/g;
        const label_store = {};
        let match;
        while ((match = section_followed_by_label_regex.exec(this.str)) !== null) {
            label_store[match[4]] = match[2];
        }

        // Replace all refs with links
        const reference_regex = /\\ref\{([^}]*)\}/g;
        let reference_match;
        while ((reference_match = reference_regex.exec(this.str)) !== null) {
            const label = reference_match[1];
            if (label in label_store) {
                const section = label_store[label];
                this.str = this.str.replace(reference_match[0], ` [${section}](#${section.toLowerCase().replace(/ /g, '-')})`);
            } else {
                console.warn(`Warning: label ${label} not found`);
                this.str = this.str.replace(reference_match[0], ` [Unknown Reference to ${label}!](#${label})`);
            }
        }
    }

    apply(str) {
        if (str) this.str = str;
        if (!this.str) error('No input string specified');

        this.handleLabels();

        let prev;

        do {
            prev = (' ' + this.str).slice(1); // copy string

            this.removals.forEach(re => {
                this.str = this.str.replace(re, '')
            })
            this.replacements.forEach(re => {
                this.str = this.str.replace(re[0], re[1])
            })
        } while (this.str.length !== prev.length);

        this.handleComments();

        this.str = this.str
            .replace(/Ξ/g, '    ') // unwrap single line breaks
            .replace(/Ψ/g, '  ') // indent codeblocks
            .replace(/Ω/g, id) // replace exercise id
        return this;
    }
}

const handler = new ConversionHandler()
    .replace(/\\\\/g, 'Ξ') // Convert \\ to newline
    .replace(/\\begin\{tabular\}(?:\[[^\}]*\])?\{([^}]*)\}([^]*?)\\end\{tabular\}/g, (match, p1, p2) => {
        const columns = [...p1].filter(c => c !== '|').length
        const header = p2.split('Ξ')[0];
        const rows = p2.split('Ξ').slice(1).map(s => s.replace(/\s?\n\r?/g, '')).filter(s => s !== ' ' && s !== '');
        const header_cells = header.split('&');
        const row_cells = rows.map(row => row.split('&'));
        const header_row = '| ' + header_cells.map(cell => cell.trim()).join(' | ') + ' |';
        const separator_row = '|' + Array(columns).fill(' --- ').join('|') + '|';
        const body_rows = row_cells.map(cells => cells ? '| ' + cells.map(cell => cell.trim()).join(' | ') + ' |' : '');
        return [header_row, separator_row, ...body_rows].join('\n');
    })

    .removeMacro('hardsymbol')
    .removeMacro('hfil')
    .removeWithArgs('label')
    .removeMacro('centering')
    .removeMacro('pagebreak')
    .removeMacro('hline')
    .removeMacros(['linewidth', 'textwidth', 'textheight', 'topmargin', 'bottommargin', 'oddsidemargin', 'evensidemargin'])
    .removeMacro('img')

    .removeWithArgs('vspace')
    .removeWithArgs('boxed')
    .removeMacro('scriptsize')

    .removeMacrosWithArgs(['byte', 'recommended', 'easy', 'medium', 'hard', 'expert'])
    .removeMacros(['lbyte', 'lrec', 'leasy', 'lmed', 'lhard', 'lexp'])
    .removeWithArgs('resizebutton')
    .removeWithArgs('renewcommand')
    .removeWithArgs('req')
    .removeWithArgs('rightline') // We also remove the content of rightline, as it's only used for "go to the next page"


    // replacements
    .replaceWithArgs('section', '\n# $1')
    .replaceWithArgs('subsection', '\n## $1')
    .replaceWithArgs('subsubsection', '\n### $1')

    .replaceWithArgs('url', '[$1]($1)') // Convert \url{...} to [url](url)
    .replaceWithArgs('href', `[$2]($1)`) // Convert \href{url}{text} to [text](url)
    .replaceWithArgs('euro', '€')

    .replaceWithArgs('code', '`$1`')
    .replaceWithArgs('textbf', '**$1**')
    .replaceWithArgs('stress', '**$1**')
    .replaceWithArgs('emph', '*$1*')
    .replaceWithArgs('textit', '*$1*')
    .replaceWithArgs('clue', 'Hinweis: *$1*Ξ')
    .replaceWithArgs('underline', '__$1__')
    .replaceMacro('item', '*')
    .replaceWithArgs('enquote', '„$1“')

    .replace(/\\imagebox\{\/([^}]*)\}(?:\{([^}]*)\})*/g, '![](/api/assets/bkp/Ω/$1)') // Convert \imagebox{\img...}{...} to ![](/api/assets/bkp/ID/...)
    .replace(/\\imageplay\{\/([^}]*)\}(?:\{([^}]*)\})*/g, '![](/api/assets/bkp/Ω/$1)') // Convert \imageplay{\img...}{...}{...} to ![](/api/assets/bkp/ID/...)

    .unwrapEnvironment('itemize')
    .unwrapEnvironment('enumerate')
    .unwrapEnvironment('center')
    .unwrapEnvironment('figure')
    .unwrapEnvironment('description')
    .unwrapEnvironment('table')
    .unwrapEnvironment('einstieg')
    .unwrapEnvironment('einstiegl')
    .unwrapEnvironmentWithArgs('wrapfigure')
    .unwrapEnvironmentWithArgs('minipage')
    .replaceEnvironment('eqnarray', (match, p1) => {
        return '$$\n' + p1.replace(/&/g, '').replace(/\n/g, '\\\\') + '\n$$'
    })
    .replaceEnvironment('eqnarray\\*', (match, p1) => {
        return '$$\n' + p1.replace(/&/g, '').replace(/\n/g, '\\\\') + '\n$$'
    })
    .replaceEnvironment('equation', (match, p1) => {
        return '$$\n' + p1.replace(/&/g, '').replace(/\n/g, '\\\\') + '\n$$'
    })
    .replaceEnvironment('equation\\*', (match, p1) => {
        return '$$\n' + p1.replace(/&/g, '').replace(/\n/g, '\\\\') + '\n$$'
    })

    .replace(/\\begin\{lstlisting\}(\[([^]*?)\])?([^]*?)\\end\{lstlisting\}/g, (match, _, p1, p2) => {
        let language = 'processing' // default language
        let caption = ''
        let title = ''
        let code = p2
        // split args by comma, but not if comma is inside curly braces
        const args = p1?.split(/,(?![^{}]*})/g).map(arg => {
            arg = arg.split('=')
            if (arg[1].startsWith('{') && arg[1].endsWith('}')) {
                arg[1] = arg[1].slice(1, -1)
            }
            return arg
        }) ?? []

        for (let i = 0; i < args.length; i++) {
            const arg = args[i]
            if (arg[0] === 'language') {
                language = arg[1]
            } else if (arg[0] === 'caption') {
                caption = '\n' + arg[1];
            } else if (arg[0] === 'title') {
                title = arg[1] + '\n';
            }
        }

        let lines = code.split('\n');
        let indent = 0;
        lines = lines.map(line => {
            const line_without_comments = line.replace(/\/\/.*$/, '');
            if (line_without_comments.match(/\}\s*/g)) indent--;
            const result = indent > 0 ? 'Ψ'.repeat(indent) + line : line;
            if (line_without_comments.match(/\{\s*/g)) indent++;
            return result;
        })

        return title + '```' + language + '' + lines.join('\n') + '```' + caption
    })

    // trim leading whitespace except within codeblocks
    .replace(/[^\S\r\n]+/g, ' ')
    .replace(/(\r?\n ?){3,}/g, '\n\n') // linux and windows newlines
    .replace(/^\s+|\s+$/g, '')
    // remove whitespace after newlines
    .replace(/(\r?\n) +/g, '$1')


if (process.argv[2] !== '--recursive') {
    console.log(handler.apply(input).result())
    fs.writeFileSync(output_file || 'output.md', handler.result());
} else {
    if (process.argv.length <= 2 || process.argv[3] === '--help' || process.argv[3] === '-h' || process.argv[3] === '-?') {
        console.log('Usage: node convert-to-md-dir.js --recursive [folder]');
        process.exit(0);
    }

    let dirs = [process.argv[3]];
    let i = 0;

    do {
        parseDirectory(dirs[i]);
    } while (++i < dirs.length);

    function parseDirectory(dir) {
        fs.readdirSync(dir).forEach(f => {
            const rel_path = `${dir}${path.sep}${f}`
            const stats = fs.lstatSync(rel_path);
            if (stats.isDirectory()) {
                dirs.push(rel_path);
            } else if (stats.isFile()) {
                parseFile(rel_path);
            }
        })
    }

    function parseFile(file) {
        if (path.extname(file) === '.tex') {
            id = path.basename(file, '.tex').replace('l', '');
            if (isNaN(id)) return;
            console.log(`Parsing ${file}`);
            const res = handler.apply(fs.readFileSync(file, 'utf8'), file).result();
            fs.writeFileSync(file.replace('.tex', '.md'), res);
        }
    }
}




