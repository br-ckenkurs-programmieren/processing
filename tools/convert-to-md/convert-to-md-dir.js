const fs = require('fs');
const path = require('path');

if (process.argv.length == 2 || process.argv[2] === '--help' || process.argv[2] === '-h' || process.argv[2] === '-?') {
    console.log('Usage: node convert-to-md-dir.js [folder]');
    process.exit(0);
}

let dirs = [ process.argv[2] ];
let i = 0;

do {
    parseDirectory(dirs[i]);
} while(++i < dirs.length);

function parseDirectory(dir) {
    fs.readdirSync(dir).forEach(f => {
        const rel_path = `${dir}${path.sep}${f}`
        const stats = fs.lstatSync(rel_path);
        if (stats.isDirectory()) {
            dirs.push(rel_path);
        } else if (stats.isFile()) {
            parseFile(rel_path);
        }
    })
}

function parseFile(file) {
    if (path.extname(file) === '.tex') {
        console.log(`Parsing ${file}`);
    }
}
