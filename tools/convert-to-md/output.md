# Einstiegsaufgabe: Konfettikanone

Wir wollen fürs jährliche Karneval eine Konfettikanone entwickeln.     
Dabei soll per Knopfdruck am derzeitigen Ort der Maus ein Haufen Konfetti sprühen.

## Konfettipartikel

Zunächst wollen wir ein Konfettipartikel programmieren.
Da wir schon jetzt wissen, dass später hunderte generiert werden sollen, ist hier eine Klasse sinnvoll.

Schreiben Sie die Klasse `Particle`, in welcher die Instanzvariablen `float x, y` und `color c`, sowie die Geschwindigkeiten `float vx, vy` gespeichert werden.

Mit dem Konstruktor `Particle( ... )` soll man die `x` und `y`-Variablen, sowie die Farbe `c` festlegen können.
Lassen Sie die Geschwindigkeitsvariablen zunächst unberührt.

Schreiben Sie zudem eine Methode `void show()`, welche dieses Partikel an der derzeitigen Position zeichnet.
Platzieren Sie dann einen roten Partikel in die Bildmitte.

![](/api/assets/bkp/ID_HERE/1.png)

## Der erste Flug

Was wäre ein Konfettipartikel ohne Bewegung?

Legen Sie im Konstruktor eine zufällig gewählte horizontale Geschwindigkeit fest, und setzen Sie `vy` zu einem kleinen, positiven Wert (damit die Partikel erst leicht hochspringen, bevor sie nach unten fallen).

Schreiben Sie eine Methode `void move()`, welche auf `x` und `y` die entsprechenden Geschwindigkeiten anwendet, und dann auf `vy` eine Schwerkraft wirken lässt.

## Gib mir mehr!

Ein Partikel ist ja schön und gut...
Wir wollen aber mehr!

Schreiben Sie zunächst für die Klasse `Particle` eine weitere Methode `boolean isOutside()`, welche `true` ausgibt, wenn das Partikel außerhalb des Bildschirms ist.

Legen Sie dann eine Variable `int count` fest (die Anzahl der Partikel), und mithilfe dessen eine Variable `Particle[] particles` fest.
Schreiben Sie in `draw()` eine for-each Schleife, die alle Partikel zeichnen und bewegen soll.
Überprüfen Sie davor, ob `p == null` gilt - und machen Sie in diesem Fall nichts (da ein leeres Partikel nicht gezeichnet werden kann).

Schreiben Sie dann eine Funktion `void loadNewParticles(int n)`, welches `n` neue Partikel generieren soll.
Verwenden Sie dazu eine Schleife, welche Elemente des `particles`-Array durchläuft und überprüft, ob das vorliegende Partikel entweder außerhalb der Bildfläche ist, oder noch nicht existiert.
Brechen Sie die Schleife vorzeitig ab, wenn Sie `n` aktualisierte Partikel erreicht haben.

Überprüfen Sie in `draw()` nun, ob derzeit `mousePressed` `true` ist.
In diesem Fall soll `loadNewParticles()` mit einem von Ihnen gewählten Wert aufgerufen werden.

![](/api/assets/bkp/ID_HERE/2.png) 
![](/api/assets/bkp/ID_HERE/3.png)

## Wellen

Bauen Sie Ihr Programm so um, dass die Bälle nun keine Schwerkraft mehr haben, sondern stattdessen konstante Faktoren für die Bewegung in die `x`- und `y`-Richtungen.

Um dies zu erleichtern, wollen wir nun mit der Klasse `PVector` arbeiten.
Dabei soll jeder Ball zwei solcher Vektoren haben:
Einen, um die Position zu speichern, und ein weiterer für die Richtung.

Ihr Konstruktor soll nun einen `PVector pos` entgegennehmen und speichern.
Wenn Sie eine neue Instanz erzeugen, geben Sie anstatt von `mouseX` und `mouseY` einen Vektor aus diesen: `new PVector(mouseX, mouseY)`.
Den Geschwindigkeitsvektor `PVector vel` können Sie mithilfe der Methode `PVector.fromAngle(rad)` generieren.

Hinweis: *Um einen zufälligen Winkel zwischen $0$ und $360^\circ$ zu erhalten, können Sie `radians(random(360))`, oder noch kürzer, `random(TWO_PI)` verwenden.*         
Hinweis: *Mit `myVector.x` und `myVector.y` können Sie auf die Werte eines Vektors zugreifen.
Mit `myVector.add(otherVector)` addieren sie einen anderen Vektor auf einen Vektor.*    