# Brückenkurs Programmieren: Processing

Dieses Repo beinhaltet Projektdateien zu den Folien und den Aufgabendokumenten, welche im Brückenkurs Programmieren an der THM verwendet werden.

Falls Sie am Projekt teilhaben möchten, lesen Sie sich bitte in [CONTRIBUTING](CONTRIBUTING.md) ein, um weitere Informationen zu erhalten.



## Namensnennungen

* [Processing Code Highlighting für LaTeX](https://forum.processing.org/two/discussion/27887/code-coloring-for-processing-in-latex-listings-package) von [ebigunso](https://forum.processing.org/two/profile/41300/ebigunso) (CC-BY-SA 4.0)
